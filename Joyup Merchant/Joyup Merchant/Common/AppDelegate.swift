//
//  AppDelegate.swift
//  Joyup
//
//  Created by Meniny on 2017-07-12.
//  Copyright © 2017年 Meniny. All rights reserved.
//

import UIKit
import Fire
import Logify
import OhCrap
import CocoaHelper
import Imagery
import EasyGCD
import Today
import UserNotifications
import Jsonify

public let kDefaultURL = "https://joyuponline.com/"

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, CAAnimationDelegate, UNUserNotificationCenterDelegate {
    
    var window: UIWindow?
    
    // MARK: 启动完成
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        
        // MARK: OhCrap
        OhCrap.isEnabled = true
        OhCrap.delegate = self
        
        // 初始化 API baseURL
        Fire.API.initialize()
        
        // MARK: 注册通知
        if #available(iOS 10.0, *) {
            ApplePushNotification.register(delegate: self)
        } else {
            ApplePushNotification.register(categories: nil)
        }
        
        ApplePushNotification.userInfo = (launchOptions?[UIApplicationLaunchOptionsKey.remoteNotification]) as? [AnyHashable : Any]
        UIApplication.shared.applicationIconBadgeNumber = 0
        
        return true
    }
    
    // MARK: 注册推送通知成功, 设置 token 并登录
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        ApplePushNotification.registerFailed = false
        ApplePushNotification.setToken(data: deviceToken)
    }
    
    func application(_ application: UIApplication, didRegister notificationSettings: UIUserNotificationSettings) {
        
    }
    
    // MARK: 注册推送通知失败
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        ApplePushNotification.registerFailed = true
    }
    
    func applicationDidReceiveMemoryWarning(_ application: UIApplication) {
        ImageryCache.clearMemoryCache()
    }
    
    func application(_ app: UIApplication, open url: URL, options: [UIApplicationOpenURLOptionsKey : Any] = [:]) -> Bool {
        return true
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any]) {
        handleUserInfo(userInfo)
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any], fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        handleUserInfo(userInfo)
        completionHandler(.noData)
    }
    
    @available(iOS 10.0, *)
    public func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Swift.Void) {
        handleUserInfo(response.notification.request.content.userInfo)
    }
}

func handleUserInfo(_ userInfo: [AnyHashable: Any]) {
    UIApplication.shared.applicationIconBadgeNumber = 0
    Log.d(userInfo)
    if let dict = userInfo as? [String: Any] {
        let json = Jsonify(dictionary: dict)
        if let message = json["aps"]["alert"].string {
            let title = json["aps"]["title"].string
            JoyupNotice.showMessage(message, title: title)
            return
        }
    }
    if let aps = userInfo["aps"] as? [String: Any], let message = aps["alert"] as? String {
        let title = aps["title"] as? String
        JoyupNotice.showMessage(message, title: title)
    }
}

extension AppDelegate: OhCrapDelegate {
    func ohCrapDidCatch(_ exception: NSException, forType type: OhCrap.CrashType) {
        let strings: [String] = [
            "Name" + exception.name.rawValue,
            "Reason" + (exception.reason ?? "N/A"),
            "CallStackSymbols" + exception.callStackSymbols.joined(separator: "\n"),
            "CallStackReturnAddresses" + exception.callStackReturnAddresses.stringsValue.joined(separator: "\n"),
            "UserInfo" + (exception.userInfo?.stringsValue.joined(separator: "\n") ?? "N/A")
        ]
        let result = strings.joined(separator: "\n###########################\n")
        let documentUrl = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
        let now = Date()
        let path = documentUrl.appendingPathComponent("OhCrap_\(now.timestamp)_\(now.formattedString("yyyyMMdd_HHmmssSSS")).log")

        if (try? result.write(to: path, atomically: true, encoding: .utf8)) != nil {
            exit(0)
        } else {
            exit(0)
        }
    }
}
