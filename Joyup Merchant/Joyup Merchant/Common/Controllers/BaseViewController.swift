//
//  BaseViewController.swift
//  Joyup
//
//  Created by Meniny on 2017-05-09.
//  Copyright © 2017年 Meniny. All rights reserved.
//

import UIKit
import CocoaHelper
import SnapKit
import Fire
import Jsonify
//import SDWebImage
import EasyGCD
import ColaExpression
import Imager
import Oops
//import SCLAlertView
import Logify
import Localization

open class BaseViewController: UIViewController, UINavigationControllerDelegate, UIImagePickerControllerDelegate, UIGestureRecognizerDelegate {
    
    public func gestureRecognizerShouldBegin(_ gestureRecognizer: UIGestureRecognizer) -> Bool {
        if let vcs = navigationController?.viewControllers {
            return vcs.count > 1
        }
        return false
    }
    
    public var tag: Int = 0
    
    open let firstPage: Int = 0
    open var page: Int = 0
    open let pageSize: Int = 10

    override open var tabBarController: TabBarController? {
        return super.tabBarController as? TabBarController
    }
    
    override open var navigationController: NavigationController? {
        return super.navigationController as? NavigationController
    }
    
    init() {
        super.init(nibName: nil, bundle: nil)
        self.setup()
    }
    
    public override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        self.setup()
    }
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.setup()
    }
    
    private func setup() {
        extendedLayoutIncludesOpaqueBars = false
        edgesForExtendedLayout = []
        hidesBottomBarWhenPushed = Joyup.shouldHidesBottomBarWhenPushed(for: self)
        
        #if DEBUG
            self.title = self.nameOfClass
        #endif
    }
    
    open override func awakeFromNib() {
        super.awakeFromNib()
        self.setup()
    }

    override open func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor.white
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(viewTapped))
        tap.numberOfTapsRequired = 1
        tap.cancelsTouchesInView = false
        self.view.addGestureRecognizer(tap)
        
        NotificationCenter.default.addObserver(self, selector: #selector(userDidSignedIn), name: Joyup.signedInNotification, object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(userDidSignedOut), name: Joyup.signedOutNotification, object: nil)
        
        Localization.startObserve(with: self, selector: #selector(localizationDidChange))
    }
    
    deinit {
        Localization.stopObserve(with: self)
        NotificationCenter.default.removeObserver(self)
    }
    
    open func localizationDidChange() {
        Log.d(Localization.preferredLanguage)
    }
    
    override open func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    open override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        view.endEditing(true)
    }
    
    @objc private func viewTapped() {
        self.view.endEditing(true)
    }
    
    @discardableResult public func showWebPage(_ url: String, present: Bool) -> Bool {
        if present {
            return self.presentWebPage(url)
        }
        return self.pushWebPage(url)
    }
    
    @discardableResult public func presentWebPage(_ url: String) -> Bool {
        if let url = URL(string: url) {
            let web = WebViewController()
            web.url = url
            web.hasDismissButton = true
            let nav = NavigationController(rootViewController: web)
            self.present(nav, animated: false, completion: nil)
            return true
        }
        return false
    }
    
    @discardableResult public func pushWebPage(_ url: String) -> Bool {
        if let url = URL(string: url), let nav = self.navigationController {
            let web = WebViewController()
            web.url = url
            web.hasDismissButton = false
            nav.pushViewController(web, animated: true)
            return true
        }
        return false
    }
    
    // MARK: - Picker 
    
    public enum ImageType: String {
        case original = "UIImagePickerControllerOriginalImage"
        case edited = "UIImagePickerControllerEditedImage"
    }
    
    public typealias PickerType = UIImagePickerControllerSourceType
    public typealias PickerCompletion = ((_ image: UIImage?) -> Swift.Void)
    
    private var pickerImageType: ImageType = .original
    private var pickerSource: PickerType = .savedPhotosAlbum
    private var pickerCompletion: PickerCompletion?
    private var pickerCancelled: VoidClosure?
    private let picker = UIImagePickerController()
    
    public func pickImage(_ type: ImageType = .original, from source: PickerType = .savedPhotosAlbum, completed closure: @escaping PickerCompletion, orCancelled cancel: VoidClosure?) {
        self.pickerImageType = type
        self.pickerSource = source
        self.pickerCompletion = closure
        self.pickerCancelled = cancel
        
        self.picker.sourceType = source
        self.picker.delegate = self
        self.picker.navigationBar.tintColor = self.navigationController?.navigationBar.tintColor
        self.picker.navigationBar.barTintColor = self.navigationController?.navigationBar.barTintColor
        self.picker.navigationBar.titleTextAttributes = self.navigationController?.navigationBar.titleTextAttributes
        self.picker.popoverPresentationController?.sourceView = self.view
        self.picker.popoverPresentationController?.sourceRect = self.view.bounds
        self.present(self.picker, animated: true, completion: nil)
    }
    
    public func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        if let cal = self.pickerCancelled {
            cal()
        }
        picker.dismiss(animated: true, completion: nil)
    }
    
    public func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        // 选择的图片参数
        /*
         指定用户选择的媒体类型 UIImagePickerControllerMediaType
         原始图片 UIImagePickerControllerOriginalImage
         修改后的图片 UIImagePickerControllerEditedImage
         裁剪尺寸 UIImagePickerControllerCropRect
         媒体的URL UIImagePickerControllerMediaURL
         原件的URL UIImagePickerControllerReferenceURL
         当来数据来源是照相机的时候这个值才有效 UIImagePickerControllerMediaMetadata
         */
        
        // 获取选择的原图
        if let com = self.pickerCompletion {
            if let pickedImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
                com(pickedImage)
            } else {
                com(nil)
            }
        }
        
        picker.dismiss(animated: true, completion: nil)
    }

}

extension BaseViewController: JoyupUserProtocol {
    public func userDidSignedIn() {
        
    }
    
    public func userDidSignedOut() {
        
    }
}
