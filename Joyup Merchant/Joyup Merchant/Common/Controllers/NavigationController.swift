//
//  NavigationController.swift
//  Joyup
//
//  Created by Meniny on 2017-05-09.
//  Copyright © 2017年 Meniny. All rights reserved.
//

import UIKit

open class NavigationController: UINavigationController {
    
    /// 注意： 聊天控制器不是继承自 BaseViewController
    open var baseViewControllers: [BaseViewController] {
        return self.viewControllers as! [BaseViewController]
    }
    
    /// 注意： 聊天控制器不是继承自 BaseViewController
//    open override var topViewController: BaseViewController? {
//        return super.topViewController as? BaseViewController
//    }
    
    open override func viewDidLoad() {
        super.viewDidLoad()
        
        let att = [
            NSForegroundColorAttributeName: Configuration.mainNavgationTintColor
        ]
        self.navigationBar.titleTextAttributes = att
//        let item = UIBarButtonItem.appearance()
//        item.setTitleTextAttributes(att, for: UIControlState.normal)
        self.navigationBar.tintColor = Configuration.mainNavgationTintColor
        self.navigationBar.barTintColor = Configuration.mainColor
    }
    
    public override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
    }
    
    public override init(rootViewController: UIViewController) {
        super.init(rootViewController: rootViewController)
        rootViewController.extendedLayoutIncludesOpaqueBars = false
        rootViewController.edgesForExtendedLayout = []
    }
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
}
