//
//  Joyup.swift
//  Joyup
//
//  Created by Meniny on 2017-07-30.
//  Copyright © 2017年 Meniny. All rights reserved.
//

import Foundation
import UIKit
import CocoaHelper
import ColaExpression
import InfoPlist

public var DEBUG: Bool {
    #if DEBUG
        return true
    #else
        return false
    #endif
}

/// Configurations Manager
open class Joyup: NSObject {
    @objc(sharedConfiguration)
    open static let shared: Joyup = Joyup()
}

public typealias Configuration = Joyup

// Define
extension Joyup {
    
    open static let tabBarControllerClasses: [String] = [
        HomeViewController.nameOfClass,
    ]
    
    open static let shouldHidesBottomBarWhenPushedKey = "JoyupShouldHidesBottomBarWhenPushedKey"
    
    open static var hidesBottomBarWhenPushed: Bool {
        get {
            return UserDefaults.standard.bool(forKey: Joyup.shouldHidesBottomBarWhenPushedKey)
        }
        set {
            UserDefaults.standard.set(newValue, forKey: Joyup.shouldHidesBottomBarWhenPushedKey)
            UserDefaults.standard.synchronize()
        }
    }
    
    open class func shouldHidesBottomBarWhenPushed(for controller: UIViewController) -> Bool {
        return false
//        return Joyup.hidesBottomBarWhenPushed
//        return !BaseViewController.tabBarControllerClasses.contains(controller.nameOfClass)
    }
    
    
    open static var projectName: String {
        return "Joyup Merchant".localized
    }
    
    open static var bundleIdentifier: String {
        return InfoPlist.bundleIndentifier
    }
    open static let fieldSize = CGSize(width: 260, height: 45)
    
    open static let defaultTextFieldHeight: CGFloat = 40
    
    open static let bottomBarHeight: CGFloat = 49
    
    open static let regExPatternUserName = "^[a-zA-Z\\d_]{4,}$"
    open static let regExPatternPassword = "[a-zA-Z\\d_\\-\\.]{5,}"
    open static let regExPatternEmail = ColaExpression.emailPattern.pattern
    open static let regExPatternScientificNotation = ColaExpression.scientificNotation.pattern
    
    open static let signedInNotification: Notification.Name = Notification.Name(rawValue: "JoyupSignedInNotification")
    open static let signedOutNotification: Notification.Name = Notification.Name(rawValue: "JoyupSignedOutNotification")
    open static let showOrderDetailNotification: Notification.Name = Notification.Name(rawValue: "JoyupShowOrderDetailNotification")
    
    open static let alipayPaymentCallbackNotification: Notification.Name = Notification.Name(rawValue: "JoyupAlipayPaymentCallbackNotification")
    
    open static let updateCartListNotification: Notification.Name = Notification.Name(rawValue: "JoyupUpdateCartListNotification")
    open static let updateCartItemCountNotification: Notification.Name = Notification.Name(rawValue: "JoyupUpdateCartItemCountNotification")
}

// UIColors
extension Joyup {
    /// 角标颜色
    open static var badgeColor: UIColor {
        return UIColor.valencia
        //UIColor(red:0.75, green:0.16, blue:0.20, alpha:1.00)// kind of red
    }
    
    /// 应用主色
    open static var mainColor: UIColor {
        return UIColor.shipGary
    }
    
    open static var submitButtonColor: UIColor {
        return UIColor.valencia
    }
    
    open static var submitTextColor: UIColor {
        return UIColor.white
    }
    
    /// 应用文字主色
    open static var mainTextColor: UIColor {
        return UIColor.white
    }
    
    /// 文字高亮色
    open static var mainTextHighlightedColor: UIColor {
        return UIColor.lightGray
    }
    
    /// 应用Bar文字主色
    open static var mainNavgationTintColor: UIColor {
        return UIColor.white
    }
    
    /// 应用TabBar未选中色
    open static var mainTabBarNormalColor: UIColor {
        return UIColor.lightGray
    }
    
    /// 应用TabBar选中色
    open static var mainTabBarHighlightColor: UIColor {
        return mainColor
    }
    
    /// 应用TabBar选中色
    open static var mainTabBarBackgroundColor: UIColor {
        return UIColor.white
    }
}

public extension Joyup {
    public static let returnPolicyURL = "https://joyuponline.com/common/returnpolicy.jhtml"
    public static let privacyURL = "https://joyuponline.com/common/privacy.jhtml"
    public static let aboutURL = "https://joyuponline.com/common/aboutus.jhtml"
    /// UserAgreement URL
    public static let userAgreementURL = "https://joyuponline.com/common/privacy.jhtml"
}

// Store Area
extension Joyup {
    private static let increaseAnimationKey = "JoyupIncreaseAnimationKey"
    
    open static var increaseAnimation: Bool {
        get {
            return UserDefaults.standard.bool(forKey: Joyup.increaseAnimationKey)
        }
        set {
            UserDefaults.standard.set(newValue, forKey: Joyup.increaseAnimationKey)
            UserDefaults.standard.synchronize()
        }
    }
}

