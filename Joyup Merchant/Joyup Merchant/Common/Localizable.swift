//
//  Localizable.swift
//  Joyup
//
//  Created by Meniny on 2017-05-10.
//  Copyright © 2017年 Meniny. All rights reserved.
//

import Foundation
import CocoaHelper
import Localization

public class Localizable {
    
    public class Generic {
        public class var all: String { return "All".localized() }
        public class var back: String { return "Back".localized() }
        public class var needToFillUp: String { return "NeedToFillUp".localized() }
        public class var expenses: String { return "Expenses".localized() }
        public class var unknownError: String { return "UnknownError".localized() }
        public class var done: String { return "Done".localized() }
        public class var yes: String { return "Yes".localized() }
        public class var no: String { return "No".localized() }
        public class var close: String { return "Close".localized() }
        public class var confirm: String { return "Confirm".localized() }
        public class var cancel: String { return "Cancel".localized() }
        public class var notNow: String { return "NotNow".localized() }
        public class var failed: String { return "Failed".localized() }
        public class var notificationTitle: String { return "NotificationTitle".localized() }
        public class var errorTitle: String { return "ErrorTitle".localized() }
        public class var warningTitle: String { return "WarningTitle".localized() }
        public class var loadingTitle: String { return "LoadingTitle".localized() }
        public class var successTitle: String { return "SuccessTitle".localized() }
        public class var pleaseWait: String { return "PleaseWait".localized() }
        public class var delete: String { return "Delete".localized() }
        public class var skip: String { return "Skip".localized() }
        public class var nextStep: String { return "NextStep".localized() }
        public class var lastStep: String { return "LastStep".localized() }
        public class var optional: String { return "Optional".localized() }
        public class var required: String { return "Required".localized() }
        public class var wrongParameters: String { return "WrongParameters".localized() }
        public class var networkError: String { return "NetworkError".localized() }
        
        public class var chooseArea: String { return "ChooseArea".localized() }
        
        public class var viewDetail: String { return "ViewDetail".localized() }
        public class var foot: String { return "Foot".localized() }
        
        public class var usernameExists: String { return "UsernameExists".localized() }
        public class var RefererExists: String { return "RefererExists".localized() }
        public class var placementExists: String { return "PlacementExists".localized() }
        public class var emailExists: String { return "EmailExists".localized() }
        
        public class var copy: String { return "Copy".localized() }
        public class var share: String { return "Share".localized() }
        public class var accept: String { return "Accept".localized() }
        
        public class var search: String { return "Search".localized() }
        
        public class var quantity: String { return "Quantity".localized() }
        
        public class var consignee: String { return "Consignee".localized() }
    }
    
    public class Home {
        public class var title: String {
            return "HomeTitle".localized()
        }
    }
    
    public class Profile {
        public class var title: String { return "ProfileTitle".localized() }
    }
    
    public class Settings {
        public class var title: String { return "SettingsTitle".localized() }
        public class var hidesBottomBarWhenPushed: String { return "HidesBottomBarWhenPushed".localized() }
        public class var cleanCaches: String { return "CleanCaches".localized() }
        public class var cachesCleaned: String { return "CachesCleaned".localized() }
        public class var increaseAnimation: String { return "IncreaseAnimation".localized() }
        
        public class var apnsFailed: String { return "APNsRegisterFailed".localized() }
        public class var gotoSettings: String { return "GotoSettings".localized() }
    }
    
    public class MemberCenter {
        public class var title: String { return "MemberCenterTitle".localized() }
        
        public class var memberRank: String { return "MemberCenterMemberRank".localized() }
        public class var amount: String { return "MemberCenterAmount.".localized() }
        public class var balance: String { return "MemberCenterBalance".localized() }
        public class var activeDate: String { return "MemberCenterActiveDate".localized() }
        public class var expireDate: String { return "MemberCenterExpireDate".localized() }
        public class var remainderPV: String { return "MemberCenterRemainderPV".localized() }
        public class var needPV: String { return "MemberCenterNeedPV".localized() }
    }
    
    public class Wallet {
        public class var title: String { return "WalletTitle".localized() }
        
        public class var memberBalance: String { return "MemberBalance".localized() }
        public class var totalFee: String { return "TotalFee".localized() }
        public class var freezeFee: String { return "FreezeFee".localized() }
    }
    
    public class MyOrders {
        public class var title: String { return "MyOrdersTitle".localized() }
        
        public class var orderProductsCountingFormat: String { return "OrderProductsCountingFormat".localized() }
        
        public class var pendingPayment: String { return "PendingPayment".localized() }
        public class var pendingReview: String { return "PendingReview".localized() }
        public class var pendingShipment: String { return "PendingShipment".localized() }
        public class var shipped: String { return "Shipped".localized() }
        public class var received: String { return "Received".localized() }
        public class var completed: String { return "Completed".localized() }
        public class var failed: String { return "Failed".localized() }
        public class var canceled: String { return "Canceled".localized() }
        public class var denied: String { return "Denied".localized() }
        public class var suspendShipment: String { return "SuspendShipment".localized() }
        public class var unknown: String { return "Unknown".localized() }
        
        //        public class var pending: String { return "Pending".localized() }
        //        public class var waitingForProcessing: String { return "WaitingForProcessing".localized() }
        //        public class var waitingForApproval: String { return "WaitingForApproval".localized() }
        //        public class var waitingForIDValidation: String { return "WaitingForIDValidation".localized() }
        //        public class var waitingForPayment: String { return "WaitingForPayment".localized() }
        //        public class var processingPayment: String { return "ProcessingPayment".localized() }
        //        public class var inProcessingQueue: String { return "InProcessingQueue".localized() }
        //        public class var processing: String { return "Processing".localized() }
        //        public class var waitingForStock: String { return "WaitingForStock".localized() }
        //        public class var waitingForAllocation: String { return "WaitingForAllocation".localized() }
        //        public class var pickUpOrPosting: String { return "PickUpOrPosting".localized() }
        //        public class var readyToPick: String { return "ReadyToPick".localized() }
        //        public class var picking: String { return "Picking".localized() }
        //        public class var readyToShip: String { return "ReadyToShip".localized() }
        //        public class var shippedOut: String { return "ShippedOut".localized() }
        //        public class var rejected: String { return "Rejected".localized() }
        
        public class var shippingMethod: String { return "ShippingMethod".localized() }
        public class var paymentMethod: String { return "PaymentMethod".localized() }
        public class var copyed: String { return "Copyed".localized() }
        public class var orderSN: String { return "OrderSN".localized() }
        public class var amount: String { return "Amount".localized() }
        
        public class PayPal {
            public class var shortDescription: String { return "PayPalPaymentShortDescription".localized() }
        }
    }
    
    public class About {
        public class var title: String { return "AboutTitle".localized() }
        public class var version: String { return "Version".localized() }
    }
    
    public class Categories {
        public class var title: String { return "CategoriesTitle".localized() }
    }
    
    public class Cart {
        public class var title: String { return "CartTitle".localized() }
        public class var total: String { return "Total".localized() }
        public class var useBalance: String { return "UseBalance".localized() }
        public class var submitOrder: String { return "SubmitOrder".localized() }
        public class var submit: String { return "Submit".localized() }
        public class var mustChooseAnAddress: String { return "MustChooseAnAddress".localized() }
        
        public class var dontHaveAnyAddress: String { return "DontHaveAnyAddress".localized() }
        
        public class var addToCart: String { return "AddToCart".localized() }
        public class var addToCartFailed: String { return "AddToCartFailed".localized() }
        public class var removeFromCartFailed: String { return "RemoveFromCartFailed".localized() }
        public class var changeQuantityFailed: String { return "ChangeQuantityFailed".localized() }
        
        
    }
    
    public class Chatting {
        public class var title: String { return "ChattingTitle".localized() }
        public class var systemMessages: String { return "SystemMessages".localized() }
        public class var IMMessages: String { return "IMMessages".localized() }
        public class var markAllAsRead: String { return "MarkAllAsRead".localized() }
    }
    
    public class User {
        public class var signUp: String { return "SignUp".localized() }
        public class var signIn: String { return "SignIn".localized() }
        public class var signOut: String { return "SignOut".localized() }
        public class var accoutPlaceholderText: String { return "AccoutPlaceholderText".localized() }
        public class var passwordPlaceholderText: String { return "PasswordPlaceholderText".localized() }
        public class var confirmPasswordPlaceholderText: String { return "ConfirmPasswordPlaceholderText".localized() }
        public class var getAuthCodeButtonTitle: String { return "GetAuthCodeButtonTitle".localized() }
        public class var goToSignUp: String { return "GoToSignUp".localized() }
        public class var goToResetPwd: String { return "GoToResetPwd".localized() }
        public class var goToSignIn: String { return "GoToSignIn".localized() }
        public class var changeAvatar: String { return "ChangeAvatar".localized() }
        public class var signInFailed: String { return "SignInFailed".localized() }
        public class var InvalidFormat: String { return "InvalidFormat".localized() }
        public class var emptyAccountOrPassword: String { return "EmptyAccountOrPassword".localized() }
        public class var completeProfileTitle: String { return "CompleteProfileTitle".localized() }
        public class var profileCompleted: String { return "ProfileCompleted".localized() }
        public class var changeNickname: String { return "ChangeNickname".localized() }
        
        public class var gender: String { return "Gender".localized() }
        public class var genderMale: String { return "Male".localized() }
        public class var genderFemale: String { return "Female".localized() }
        
        public class var typeMember: String { return "Member".localized() }
        public class var typeAssociate: String { return "Associate".localized() }
        public class var typePremiumAssociate: String { return "PremiumAssociate".localized() }
        
        public class var area: String { return "Area".localized() }
        public class var america: String { return "America".localized() }
        public class var australia: String { return "Australia".localized() }
        public class var china: String { return "China".localized() }
        public class var malaysia: String { return "Malaysia".localized() }
        public class var singapore: String { return "Singapore".localized() }
        public class var hongkong: String { return "HongKong".localized() }
        public class var taiwan: String { return "Taiwan".localized() }
        
        public class var site: String { return "Site".localized() }
        public class var americaEnglish: String { return "AmericaEnglish".localized() }
        public class var americaChinese: String { return "AmericaChinese".localized() }
        public class var chinaChinese: String { return "ChinaChinese".localized() }
        
        public class var brief: String { return "Brief".localized() }
        public class var connectToSinaWeibo: String { return "ConnectToSinaWeibo".localized() }
        public class var connectToWeChat: String { return "ConnectToWeChat".localized() }
        
        public class var accountAndPasswordTitle: String { return "AccountAndPasswordTitle".localized() }
        public class var refererAndPlacementTitle: String { return "RefererAndPlacementTitle".localized() }
        public class var accountTypeTitle: String { return "AccountTypeTitle".localized() }
        public class var accountTypeWarning: String { return "AccountTypeWarning".localized() }
        public class var nameAndEmailTitle: String { return "NameAndEmailTitle".localized() }
        public class var genderTitle: String { return "GenderTitle".localized() }
        public class var telAndMobileTitle: String { return "TelAndMobileTitle".localized() }
        
        public class var refererPlaceholder: String { return "RefererPlaceholder".localized() }
        public class var placementPlaceholder: String { return "PlacementPlaceholder".localized() }
        public class var namePlaceholder: String { return "NamePlaceholder".localized() }
        public class var emailPlaceholder: String { return "EmailPlaceholder".localized() }
        public class var telPlaceholder: String { return "TelPlaceholder".localized() }
        public class var mobilePlaceholder: String { return "MobilePlaceholder".localized() }
        
        public class var areaTitle: String { return "AreaTitle".localized() }
        public class var SSNTitle: String { return "SSNTitle".localized() }
        
        public class var signInLoadingTitle: String { return "SignInLoadingTitle".localized() }
        public class var signInLoading: String { return "SignInLoading".localized() }
        
        public class var signUpDoneTitle: String { return "SignUpDoneTitle".localized() }
        public class var signUpLoadingTitle: String { return "SignUpLoadingTitle".localized() }
        public class var signUpLoading: String { return "SignUpLoading".localized() }
        
        public class var forgottenPassword: String { return "ForgottenPassword".localized() }
        public class var pleaseCheckEmail: String { return "PleaseCheckYourEmail".localized() }
        
        public class var privacyPolicy: String { return "PrivacyPolicy".localized() }
        public class var returnAndRefundPolicy: String { return "ReturnAndRefundPolicy".localized() }
        
        public class var userAgreement: String { return "UserAgreement".localized() }
        public class var haveAcceptedUserAgreement: String { return "HaveAcceptedUserAgreement".localized() }
        
        public class var addressName: String { return "AddressName".localized() }
        public class var addressPhone: String { return "AddressPhone".localized() }
        public class var addressZip: String { return "AddressZip".localized() }
        public class var addressArea: String { return "AddressArea".localized() }
        public class var addressState: String { return "AddressState".localized() }
        public class var addressCity: String { return "AddressCity".localized() }
        public class var addressAddress: String { return "AddressAddress".localized() }
        public class var addressIsDefault: String { return "AddressisDefault".localized() }
    }
}
