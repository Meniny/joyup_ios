//
//  JoyupUser.swift
//  Joyup
//
//  Created by Meniny on 2017-07-13.
//  Copyright © 2017年 Meniny. All rights reserved.
//

import Foundation
import UIKit
import CocoaHelper
import Jsonify
import Logify
import ColaExpression
//import SwiftyRSA
//import Heimdall

public protocol JoyupUserProtocol {
    func userDidSignedIn()
    func userDidSignedOut()
}

/// see `https://joyuponline.com/register.jhtml`
public struct JoyupUser {
    
    public static let kKeychainStoreUserNameKey = "kKeychainStoreUserNameKey"
    public static let kKeychainStoreService = "kKeychainStoreService"
    
    public var token: String?
    
    public var userName: String
    public var email: String
    
    public var avatar: String?
    
    public var accoutType: AccoutType
    public var name: String
    
    public var gender: Gender?
    public var telephone: String?
    public var mobilePhone: String?
    public var referer: String?
    public var placement: String?
    
    public var area: Area
    
    public var SSN: String
    
    public init?(_ json: Jsonify) {
        if let info = json.arrayValue.first, let tk = json.arrayValue.last {
            userName = info["username"].stringValue
            name = info["name"].stringValue
            
            email = info["email"].stringValue
            avatar = info["avatar"].string
            accoutType = AccoutType(rawValue: info["type"].intValue) ?? .member
            gender = Gender(rawValue: info["sex"].intValue) ?? .male
            telephone = info["tel"].string
            mobilePhone = info["mobile"].string
            referer = info["referer"].stringValue
            placement = info["placement"].stringValue
            area = Area(rawValue: info["area"].intValue) ?? .america
            SSN = info["ssn"].stringValue
            
            token = tk["token"]["token"].string
        } else {
            return nil
        }
    }
    
    init(userName un: String,
         email em: String,
         avatar av: String?,
         accoutType at: AccoutType,
         name n: String,
         gender g: Gender,
         telephone tp: String,
         mobile mp: String,
         referer r: String,
         placement p: String,
         area a: Area,
         SSN s: String,
         token t: String) {
        
        userName = un
        email = em
        avatar = av
        accoutType = at
        name = n
        gender = g
        telephone = tp
        mobilePhone = mp
        referer = r
        placement = p
        area = a
        SSN = s
        token = t
    }
    
}

public extension JoyupUser {

    public static var main: JoyupUser?
    
    public static var signedIn: Bool {
        guard let _ = JoyupUser.main?.token else {
            return false
        }
        return true
    }
    
    public func getPassword() -> String? {
        return Keychain.getStoredPassword(for: userName)
    }
    
    @discardableResult public func setPassword(_ pwd: String) -> Bool {
        return Keychain.storePassword(pwd, for: userName)
    }
    
    public static func signIn(_ uname: String, password: String, modulus: String, exponent: String, completion: ((_ success: Bool, _ message: String?) -> Swift.Void)?) {
        guard let base64 = Encrypt(password, modulus: modulus, exponent: exponent) else {
            completion?(false, nil)
            return
        }
        if !JoyupUser.check(username: uname) {
            completion?(false, Localizable.User.emptyAccountOrPassword)
            return
        }
        Request.signIn(uname, base64).request(params: nil, completion: { (json, resp, error) in
            
            if let message = Request.checkResult(json) {
                completion?(false, message)
            } else {
                if json["errCode"].intValue == 259 {
                    completion?(false, json["msg"].string)
                } else {
                    JoyupUser.main = JoyupUser(json["data"])
                    Keychain.storeUsername(uname)
                    Keychain.storePassword(password, for: uname)
                    NotificationCenter.default.post(name: Joyup.signedInNotification, object: nil)
                    completion?(true, nil)
                }
            }
        }) { (_ resp, error) in
            completion?(false, error.localizedDescription)
        }
    }
    
    public static func autoSignIn(completion: @escaping (_ success: Bool) -> Swift.Void) {
        guard let storeduname = Keychain.getStoredUsername() else {
            completion(false)
            return
        }
        guard let storedpassword = Keychain.getStoredPassword(for: storeduname) else {
            completion(false)
            return
        }
        Request.getRSAPublicKey.request(params: nil, completion: { (json, _, _) in
            guard let modulus = json["modulus"].string, let exponent = json["exponent"].string else {
                completion(false)
                return
            }
            JoyupUser.signIn(storeduname, password: storedpassword, modulus: modulus, exponent: exponent, completion: { s, _ in
                completion(s)
            })
        }) { (_, _) in
            completion(false)
        }
    }

    public static func signOut(_ completion: ((_ success: Bool, _ error: String?) -> Swift.Void)?) {
        Request.signOut.request(params: nil, completion: { (json, _, _) in
            if let message = Request.checkResult(json) {
                completion?(false, message)
            } else {
                JoyupUser.signOutWithoutRequest()
                completion?(true, nil)
            }
        }) { (_, error) in
            completion?(false, error.localizedDescription)
        }
    }
    
    public static func signOutWithoutRequest() {
        if let uname = JoyupUser.main?.userName {
            let clean = Keychain.cleanStoredPassword(for: uname)
            Log.d(clean)
        }
        if let storeduname = Keychain.getStoredUsername() {
            let clean = Keychain.cleanStoredPassword(for: storeduname)
            Log.d(clean)
        }
        JoyupUser.main = nil
        NotificationCenter.default.post(name: Joyup.signedOutNotification, object: nil)
    }
    
    public static func signUp(_ user: JoyupUser, completion: ((_ success: Bool, _ error: String?) -> Swift.Void)?) {
    }
}

public extension JoyupUser {
//    public static func showSignInPanel(to controller: UIViewController, completion: VoidClosure?) {
//        let ctrl = SignInViewController()
//        ctrl.completion = completion
//        let nav = NavigationController(rootViewController: ctrl)
//        controller.present(nav, animated: true, completion: nil)
//    }
//    
//    public static func showSignUpPanel(to controller: UIViewController, completion: VoidClosure?) {
//        let ctrl = SignUpViewController()
//        ctrl.completion = completion
//        if controller is SignInViewController {
//            controller.navigationController?.pushViewController(ctrl, animated: true)
//        } else {
//            let nav = NavigationController(rootViewController: ctrl)
//            controller.present(nav, animated: true, completion: nil)
//        }
//    }
}

public extension JoyupUser {
    public static func check(username: String) -> Bool {
        return username.isMatch(pattern: Joyup.regExPatternUserName)
    }
    
    public static func check(password: String) -> Bool {
        return password.isMatch(pattern: Joyup.regExPatternPassword)
    }
    
    public static func check(email: String) -> Bool {
        return email.isMatch(pattern: Joyup.regExPatternEmail)
    }
}

/// For Signing Up
public class UserInfoHolder {
    
    public static let shared = UserInfoHolder()
    
    public var userName: String?
    public var password: String?
    public var email: String?
    
    public var accoutType: AccoutType?
    public var name: String?
    
    public var gender: Gender?
    public var telephone: String?
    public var mobilePhone: String?
    public var referer: String?
    public var placement: String?
    
    public var area: Area?
    public var subarea: Subarea?
    
    public var SSN: String?
    
    public func cleanUp() {
        userName = nil
        password = nil
        email = nil
        accoutType = nil
        name = nil
        gender = nil
        telephone = nil
        mobilePhone = nil
        referer = nil
        placement = nil
        area = nil
        SSN = nil
    }
    
    public func generateParams(publicKeyModulus: String, publicKeyExponent: String) -> Request.Params? {
        if let pwd = password {
            let base64 = Encrypt(pwd, modulus: publicKeyModulus, exponent: publicKeyExponent)
            
            var params: Request.Params = [:]
            
            if userName != nil && userName!.isNotEmpty { params["username"] = userName! } else { return nil }
            if email != nil && email!.isNotEmpty { params["email"] = email! } else { return nil }
            if accoutType != nil { params["rankid"] = accoutType!.rawValue } else { return nil }
            if referer != nil && referer!.isNotEmpty { params["referer"] = referer } else { params["referer"] = "liuqiang" }
            if placement != nil && placement!.isNotEmpty { params["placement"] = placement! } else { params["placement"] = "legend" }
            
            if name != nil && name!.isNotEmpty { params["memberAttribute_1"] = name! } else { return nil }
            if SSN != nil && SSN!.isNotEmpty { params["memberAttribute_11"] = SSN! } else { return nil }
            if base64 != nil && base64!.isNotEmpty { params["enPassword"] = base64! } else { return nil }
            if subarea != nil { params["memberAttribute_4"] = subarea!.rawValue } else { return nil }
            
            if gender != nil { params["memberAttribute_2"] = (gender! == .male ? "male" : "female") }
            if telephone != nil && telephone!.isNotEmpty { params["memberAttribute_7"] = telephone! }
            if mobilePhone != nil && mobilePhone!.isNotEmpty { params["memberAttribute_8"] = mobilePhone! }
            //"memberAttribute_3": brithdate,
            //"memberAttribute_5": address,
            //"memberAttribute_6": zip,
            //"memberAttribute_10": member id/number
            return params
        }
        return nil
    }
}

public extension Keychain {
    
    @discardableResult
    public static func storeUsername(_ uname: String) -> Bool {
        return Keychain.setPassword(uname, forAccount: JoyupUser.kKeychainStoreUserNameKey, service: JoyupUser.kKeychainStoreService)
    }
    
    @discardableResult
    public static func storePassword(_ password: String, for account: String) -> Bool {
        return Keychain.setPassword(password, forAccount: account, service: JoyupUser.kKeychainStoreService)
    }
    
    @discardableResult
    public static func getStoredUsername() -> String? {
        return Keychain.password(forAccount: JoyupUser.kKeychainStoreUserNameKey, service: JoyupUser.kKeychainStoreService)
    }
    
    @discardableResult
    public static func getStoredPassword(for account: String) -> String? {
        return Keychain.password(forAccount: account, service: JoyupUser.kKeychainStoreService)
    }
    
    @discardableResult
    public static func cleanStoredUsername() -> Bool {
        return Keychain.deletePassword(forAccount: JoyupUser.kKeychainStoreUserNameKey, service: JoyupUser.kKeychainStoreService)
    }
    
    @discardableResult
    public static func cleanStoredPassword(for account: String) -> Bool {
        return Keychain.deletePassword(forAccount: account, service: JoyupUser.kKeychainStoreService)
    }
}
