//
//  File.swift
//  Joyup
//
//  Created by Meniny on 2017-07-22.
//  Copyright © 2017年 Meniny. All rights reserved.
//

import Foundation
import UIKit

/**
 * 货币
 */
public enum Currency: String {
    
    /** 美元 */
    case USD = "USD"
    
    /** 澳大利亚元 */
    case AUD = "AUD"
    
    /** 加拿大元 */
    case CAD = "CAD"
    
    /** 人民币元 */
    case CNY = "CNY"
    
    /** 捷克克郎 */
    case CZK = "CZK"
    
    /** 丹麦克朗 */
    case DKK = "DKK"
    
    /** 欧元 */
    case EUR = "EUR"
    
    /** 港元 */
    case HKD = "HKD"
    
    /** 匈牙利福林 */
    case HUF = "HUF"
    
    /** 新西兰元 */
    case NZD = "NZD"
    
    /** 挪威克朗 */
    case NOK = "NOK"
    
    /** 波兰兹罗提 */
    case PLN = "PLN"
    
    /** 英镑 */
    case GBP = "GBP"
    
    /** 新加坡元 */
    case SGD = "SGD"
    
    /** 瑞典克朗 */
    case SEK = "SEK"
    
    /** 瑞士法郎 */
    case CHF = "CHF"
    
    /** 日元 */
    case JPY = "JPY"
}

public let kDefaultCurrency = Currency.USD

