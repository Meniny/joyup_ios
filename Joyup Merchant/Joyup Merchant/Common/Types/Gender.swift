//
//  Gender.swift
//  Joyup
//
//  Created by Meniny on 2017-08-02.
//  Copyright © 2017年 Meniny. All rights reserved.
//

import Foundation

public enum Gender: Int {
    case male = 0
    case female = 1
    
    public var localizedString: String {
        switch self {
        case .male:
            return Localizable.User.genderMale
        default:
            return Localizable.User.genderFemale
        }
    }
}
