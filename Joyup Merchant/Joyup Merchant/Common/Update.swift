//
//  Update.swift
//  Joyup Merchant
//
//  Created by Meniny on 2017-09-08.
//  Copyright © 2017年 Meniny. All rights reserved.
//

import Foundation
import UIKit
import InfoPlist
import Jsonify

open class UpdateManager {
    
    public enum UpdateResult: String {
        case disabled = "false"
        case forced = "force"
        case suggested = "suggest"
        
        public init(rawValue raw: String) {
            switch raw {
            case "force":
                self = .forced
                break
            case "suggest":
                self = .suggested
                break
            default:
                self = .disabled
                break
            }
        }
    }
    
    open class func check(_ completion: @escaping (_ update: Bool, _ force: Bool, _ error: String?) -> Swift.Void) {
        Request.checkVersion.request(params: nil, completion: { (json, _, _) in
            if let error = Request.checkResult(json) {
                completion(false, false, error)
            } else {
                if json["false"].boolValue {
                    completion(false, false, nil)
                } else {
                    let res = UpdateResult(rawValue: json["data"].stringValue)
                    completion(res != .disabled, res == .forced, nil)
                }
            }
        }) { (_, error) in
            completion(false, false, error.localizedDescription)
        }
    }
    
    open class func alert(force: Bool, completion: @escaping () -> Swift.Void) {
        var opts = ["Upgrade".localized()]
        if !force {
            opts.append(Localizable.Generic.notNow)
        }
        JoyupNotice.showOptions(nil, title: "New Version".localized(), options: opts) { (idx, _) in
            if idx == 0, let url = URL(string: "https://itunes.apple.com/us/app/joyup-merchant/id1280806301?l=zh&ls=1&mt=8") {
                UIApplication.shared.openURL(url)
            }
            completion()
        }
    }
}
