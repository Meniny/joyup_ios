//
//  BaseTableView.swift
//  Joyup
//
//  Created by Meniny on 2017-05-11.
//  Copyright © 2017年 Meniny. All rights reserved.
//

import UIKit

open class BaseTableView: UITableView {
    init() {
        super.init(frame: CGRect.zero, style: .plain)
        self.setup()
    }
    
    public override init(frame: CGRect, style: UITableViewStyle) {
        super.init(frame: frame, style: style)
        self.setup()
    }
    
    open func register(cell cls: UITableViewCell.Type, useNib: Bool = true, forCellReuseIdentifier identifier: String? = nil) {
        if useNib {
            if let id = identifier {
                let nib = UINib(nibName: id, bundle: Bundle.main)
                super.register(nib, forCellReuseIdentifier: id)
            } else {
                let id = cls.nameOfClass
                let nib = UINib(nibName: id, bundle: Bundle.main)
                super.register(nib, forCellReuseIdentifier: id)
            }
        } else {
            if let id = identifier {
                super.register(cls, forCellReuseIdentifier: id)
            } else {
                let id = cls.nameOfClass
                super.register(cls, forCellReuseIdentifier: id)
            }
        }
    }
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.setup()
    }
    
    open override func awakeFromNib() {
        super.awakeFromNib()
        self.setup()
    }
    
    private func setup() {
        self.backgroundColor = UIColor.clear
        self.showsVerticalScrollIndicator = false
        self.separatorStyle = .none
    }
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}
