//
//  Array.swift
//  Joyup
//
//  Created by Meniny on 2017-07-26.
//  Copyright © 2017年 Meniny. All rights reserved.
//

import Foundation

public extension Array {
    public func object(at index: Int) -> Element? {
        guard !isEmpty else {
            return nil
        }
        guard index >= 0 && index < count else {
            return nil
        }
        return self[index]
    }
}

public extension Dictionary {
    public var stringsValue: [String] {
        var result = [String]()
        for key in self.keys {
            if let value = self[key] {
                if let array = value as? Array<Any> {
                    result.append("\(key): \(array.stringsValue)")
                } else if let number = value as? NSNumber {
                    result.append("\(key): " + number.stringValue)
                } else if let str = value as? String {
                    result.append("\(key): " + str)
                } else if let str = value as? NSString {
                    result.append("\(key): " + (str as String))
                } else {
                    result.append("\(key): \(value)")
                }
            } else {
                result.append("\(key): nil")
            }
        }
        return result
    }
}

public extension Array {
    public var isNotEmpty: Bool {
        return !isEmpty
    }
    
    public var stringsValue: [String] {
        var result = [String]()
        for element in self {
            if element is Array {
                result.append(contentsOf: (element as! Array).stringsValue)
            } else {
                if let dict = element as?  Dictionary<AnyHashable, Any> {
                    result.append(contentsOf: dict.stringsValue)
                } else if let number = element as? NSNumber {
                    result.append(number.stringValue)
                } else if let str = element as? String {
                    result.append(str)
                } else if let str = element as? NSString {
                    result.append(str as String)
                } else {
                    result.append("\(element)")
                }
            }
        }
        return result
    }
}
