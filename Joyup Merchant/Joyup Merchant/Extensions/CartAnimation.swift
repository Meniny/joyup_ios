//
//  CartAnimation.swift
//  Joyup
//
//  Created by Meniny on 2017-08-09.
//  Copyright © 2017年 Meniny. All rights reserved.
//

import Foundation
import UIKit

extension CALayer {
    public func cartAnimation(from startPoint: CGPoint,
                              pass passPoint: CGPoint,
                              to endPoint: CGPoint,
                              duration: CFTimeInterval = 1,
                              delegate: CAAnimationDelegate?) {
        
        let animation = CAKeyframeAnimation(keyPath: "position")
        animation.delegate = delegate
        animation.duration = duration
        animation.isRemovedOnCompletion = true
        animation.fillMode = kCAFillModeForwards
        
        let path = CGMutablePath()
        path.move(to: startPoint)
        path.addQuadCurve(to: endPoint, control: passPoint)
        
        animation.path = path
        
        add(animation, forKey: nil)
    }
}
