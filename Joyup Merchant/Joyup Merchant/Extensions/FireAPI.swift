//
//  API.swift
//  Joyup
//
//  Created by Meniny on 2017-05-09.
//  Copyright © 2017年 Meniny. All rights reserved.
//

import Foundation
import Fire
import Jsonify
import CocoaHelper
import Logify
import NAIManager
import InfoPlist

public typealias Request = Fire.API
public typealias Status = Fire.ResponseStatus
public typealias Method = Fire.HTTPMethod

// MARK: -  Joyup ID
public let JoyupAppSecretMD5 = ""
public let JoyupAppID = "com.joyuponline.Joyup"

public let ReturnCodeKey = "retCode"
public let ErrorCodeKey = "errCode"

public enum RequestError: Error {
    case unknown
    case wrongStatusCode
    case noError
    
    public var localizedDescription: String {
        switch self {
        case .unknown:
            return "Unknown Error"
        case .wrongStatusCode:
            return "Wrong Status Code"
        default:
            return "Everything is fine"
        }
    }
}

public typealias RequestCompletion = ((_ json: Jsonify, _ response: HTTPURLResponse?, _ error: RequestError) -> Swift.Void)
public typealias RequestErrorClosure = FireErrorCallback

public extension Request {
    // TODO: Result Code
    public enum Result: Int {
        case success = 0
        case failed = 1
    }
    
    /// Check response codes
    ///
    /// - Parameter json: Jsonify data
    /// - Returns: Error Message, `nil` if there is no error
    public static func checkResult(_ json: Jsonify) -> String? {
        var noCodeKey = json[ErrorCodeKey].intValue == 0
        if !noCodeKey {
            return json["msg"].string ?? Localizable.Generic.unknownError
        }
        if let retCode = json[ReturnCodeKey].int {
            if retCode == 0 {
                return nil
            }
        } else {
            noCodeKey = true
        }
        if let msg = json["msg"].string {
            return msg
        } else {
            if noCodeKey {
                return Localizable.Generic.networkError
            }
        }
        return nil
    }
    
    public typealias Params = Fire.Params
    
    public static let baseURLStoredKey = "kJoyupBaseURLStoredKey"
    
    public static let DEBUGBaseURL = "https://joyuponline.co/api/"
    public static let RELEASEBaseURL = "https://joyuponline.com/api/"
    
    public static func initialize() {
        #if DEBUG
            self.baseURL = UserDefaults.standard.string(forKey: Request.baseURLStoredKey) ?? DEBUGBaseURL
        #else
            self.baseURL = RELEASEBaseURL
        #endif
    }
    
//    public var description: String {
//        return "<API \(self.method.rawValue) \(self.fullURL) \(self.successCode)(\(self.successCode.rawValue))\nAPI>"
//    }
    
    /// 发送网络请求
    ///
    /// - Parameters:
    ///   - params: 参数列表, 可空
    ///   - headers: HTTP 请求头, 可空
    ///   - callBackQueue: 执行回调闭包的 DispatchQueue
    ///   - completion: 请求完成回调闭包, 可空, 在主线程异步执行
    ///   - error: 请求错误回调闭包, 可空, 在主线程异步执行
    public func request(params: Fire.Params?,
                        headers: Fire.HeaderFields? = nil,
                        callBackQueue queue: DispatchQueue = DispatchQueue.main,
                        completion: RequestCompletion?,
                        error: RequestErrorClosure?) {
        
        var paramsString = ""
        var allParams: Fire.Params = params ?? [:]
        
        if let p = params {
            let keys = p.keys.sorted()
            paramsString.append(keys.reduce("") { (pre, cur) -> String in
                return "\(pre)\(cur)\(p[cur]!)"
            })
        }
        
        if let p = defaultParameters {
            let keys = p.keys.sorted()
            paramsString.append(keys.reduce("") { (pre, cur) -> String in
                return "\(pre)\(cur)\(p[cur]!)"
            })
        }
        
        let sign = "\(JoyupAppSecretMD5)\(paramsString)\(JoyupAppSecretMD5)".md5String.uppercased()
        
        allParams["timestamp"] = Date().timestampString
        allParams["appid"] = UUID().uuidString //JoyupAppID
        allParams["v"] = "\(InfoPlist.version).\(InfoPlist.build)"
        allParams["sign"] = sign
        
        var newHeader: Fire.HeaderFields = headers ?? [:]
        if let tk = JoyupUser.main?.token {
            newHeader["token"] = tk
        }
        
        NAIManager.operationStarted()
        // FireDefaults.defaultTimeout
        self.requestJSON(params: allParams, headers: newHeader, timeout: 30, dispatch: .asynchronously, callback: { (json, resp) in
            if let cp = completion {
                queue.async {
                    if resp?.statusCode == self.successCode.rawValue {
                        cp(json, resp, RequestError.noError)
                    } else {
                        cp(json, resp, RequestError.wrongStatusCode)
                    }
                }
            }
            // AppToken 失效
            if json["errCode"].stringValue == "152" {
                JoyupUser.signOutWithoutRequest()
            }
            NAIManager.operationFinished()
        }) { (r, e) in
            if let eb = error {
                queue.async {
                    eb(r, e)
                }
            }
            NAIManager.operationFinished()
        }
    }
}

public extension Request {
    /// 用户登录
    public static func signIn(_ username: String, _ enPassword: String) -> Request {
        var params: Request.Params = [
            "username": username,
            "enPassword": enPassword
        ]
        if let apns = ApplePushNotification.token, !apns.isEmpty {
            params["apnsToken"] = apns
        }
        return Request(appending: "merchantLogin/login.jhtml", HTTPMethod: .POST, params: params, successCode: .created)
    }
    
    /// 登出
    public static var signOut = Request(appending: "merchantLogin/logout.jhtml", HTTPMethod: .GET, successCode: .success)
    
    /// 主页
    public static var home = Request(appending: "merchant/index.jhtml", HTTPMethod: .GET, successCode: .success)
    
    /// 忘记密码
    public static func forgottenPassword(_ uname: String, _ email: String) -> Request {
        let params: Request.Params = [
            "username": uname,
            "email": email
        ]
        return Request(appending: "password/find.jhtml", HTTPMethod: .GET, params: params, successCode: .success)
    }
    
    public static var getRSAPublicKey = Request(appending: "common/public_key.jhtml", HTTPMethod: .GET, successCode: .success)
    
    public static var transferRecords = Request(appending: "merchantTransfer/transRecord.jhtml", HTTPMethod: .GET, successCode: .success)
    
    /// 检查新版本
    public static var checkVersion: Request {
        let params: Request.Params = [
            "currentVersion": InfoPlist.version + "." + InfoPlist.build,
            "bundleId": InfoPlist.bundleIndentifier,
            "userType": "2"
            // 1 用户, 2 商户
        ]
        return Request(appending: "versionUpgrade/check_version_upgrade.jhtml", HTTPMethod: .GET, params: params, successCode: .success)
    }
}
