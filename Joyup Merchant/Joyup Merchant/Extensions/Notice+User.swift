//
//  Notice.swift
//  Joyup
//
//  Created by Meniny on 2017-07-31.
//  Copyright © 2017年 Meniny. All rights reserved.
//

import Foundation
import UIKit
import Oops
import CocoaHelper
import EasyGCD

public extension JoyupNotice {
    
    @discardableResult
    public static func showFireError(_ response: HTTPURLResponse?, error: String?, action: JoyupNotice.IndexClosure? = nil) -> JoyupNotice {
        let status = response != nil ? " (\(response!.statusCode))" : ""
        
        return showError(error ?? Localizable.Generic.networkError,
                         title: Localizable.Generic.errorTitle + status,
                         buttons: nil,
                         action: action)
    }
    
    @discardableResult
    public static func checkingSignUpInfo() -> JoyupNotice {
        return showLoading("checking... please wait.", title: "Checking")
    }
    
    // MARK: - Loading
    @discardableResult
    public static func signInLoadingWithRequest(_ uname: String,
                                                password: String,
                                                completion: @escaping (_ success: Bool, _ message: String?) -> Swift.Void) -> JoyupNotice {
        let config = Oops.Configuration()
        config.shouldAutoDismiss = false
        config.showCloseButton = false
        let notice = JoyupNotice(alert: Oops(configuration: config))
        notice.alertView.show(.loading,
                              title: Localizable.User.signInLoadingTitle,
                              detail: Localizable.User.signInLoading,
                              icon: nil,
                              color: kSignButtonColor,
                              buttonTitleColor: kSignButtonTextColor,
                              completeText: nil,
                              timeout: nil,
                              animation: .topToBottom)
        Request.getRSAPublicKey.request(params: nil, completion: { (json, _, _) in
            
            if let modulus = json["modulus"].string, let exponent = json["exponent"].string {
                
                JoyupUser.signIn(uname, password: password, modulus: modulus, exponent: exponent, completion: { (s, msg) in
                    notice.alertView.hideView()
                    completion(s, msg)
                })
            } else {
                notice.hide()
                completion(false, Localizable.Generic.networkError)
            }
            
        }) { (resp, error) in
            notice.hide()
//            JoyupNotice.showFireError(resp, error: error.localizedDescription, action: { (n, _) in
//            })
            completion(false, error.localizedDescription)
        }
        
        return notice
    }
    
    @discardableResult
    public static func forgottenPasswordLoadingRequest(_ uname: String,
                                                       email: String,
                                                       completion: @escaping (_ success: Bool, _ message: String?) -> Swift.Void) -> JoyupNotice {
        let config = Oops.Configuration()
        config.shouldAutoDismiss = false
        config.showCloseButton = false
        let notice = JoyupNotice(alert: Oops(configuration: config))
        notice.alertView.show(.loading,
                              title: Localizable.Generic.loadingTitle,
                              detail: "",
                              icon: nil,
                              color: kSignButtonColor,
                              buttonTitleColor: kSignButtonTextColor,
                              completeText: nil,
                              timeout: nil,
                              animation: .topToBottom)
        Request.forgottenPassword(uname, email).request(params: nil, completion: { (json, _, _) in
            notice.alertView.hideView()
            let msg = json["msg"].stringValue
            completion(json["retCode"].stringValue == "0", msg.isEmpty ? nil : msg)
        }) { (_, error) in
            notice.alertView.hideView()
            completion(false, error.localizedDescription)
        }
        return notice
    }
    
    // MARK: - SignIn
    @discardableResult
    public static func signIn(_ onSignIn: @escaping VoidClosure = {}, onSignUp: @escaping VoidClosure = {}, onCancel: @escaping VoidClosure = {}) -> JoyupNotice {
        let config = Oops.Configuration()
        config.shouldAutoDismiss = false
        config.showCloseButton = false
        let notice = JoyupNotice(alert: Oops(configuration: config))
        
        let storedpassword: String?
        let storeduname: String?
        if let su = Keychain.getStoredUsername() {
            storedpassword = Keychain.getStoredPassword(for: su)
            storeduname = su
        } else {
            storedpassword = nil
            storeduname = nil
        }
        
        let uname = notice.alertView.addTextField(Localizable.User.accoutPlaceholderText, text: storeduname, secure: false)
        uname.keyboardType = .alphabet
        let pwd = notice.alertView.addTextField(Localizable.User.passwordPlaceholderText, text: storedpassword, secure: true)
        
        notice.alertView.addButton(Localizable.User.signIn, action: {
            
            let onError: VoidClosure = {
                if uname.text == nil || uname.text!.isEmpty {
                    onErrorField([uname])
                }
                if pwd.text == nil || pwd.text!.isEmpty {
                    onErrorField([pwd])
                }
            }
            
            if let u = uname.text, let p = pwd.text {
                if !u.isEmpty && !p.isEmpty {
                    notice.alertView.hideView()
                    JoyupNotice.signInLoadingWithRequest(u, password: p, completion: { (s, msg) in
                        if s {
                            onSignIn()
                        } else {
                            JoyupNotice.showError(msg, title: Localizable.User.signInFailed, buttons: nil, action: { n, _ in
                                n.hide()
                                JoyupNotice.signIn()
                            })
                        }
                    })
                    return
                }
            }
            onError()
        })
        
        notice.alertView.addButton(Localizable.User.forgottenPassword, backgroundColor: kOptionalButtonColor, textColor: kOptionalButtonTextColor, showTimeout: nil) {
            notice.alertView.hideView()
            JoyupNotice.forgottenPassword()
        }
        
//        notice.alertView.addButton(Localizable.Generic.notNow, backgroundColor: kOptionalButtonColor, textColor: kOptionalButtonTextColor, showTimeout: nil) {
//            onCancel()
//            notice.alertView.hideView()
//        }
        notice.alertView.show(.editor,
                              title: Localizable.User.signIn,
                              detail: Joyup.projectName,
                              icon: nil,
                              color: kSignButtonColor,
                              buttonTitleColor: kSignButtonTextColor,
                              completeText: nil,
                              timeout: nil,
                              animation: .topToBottom)
        return notice
    }
    
    // MARK: - Forgotten Password
    
    @discardableResult
    public static func forgottenPassword() -> JoyupNotice {
        let config = Oops.Configuration()
        config.shouldAutoDismiss = false
        config.showCloseButton = false
        let notice = JoyupNotice(alert: Oops(configuration: config))
        
        let storeduname: String? = Keychain.getStoredUsername()
        
        let uname = notice.alertView.addTextField(Localizable.User.accoutPlaceholderText, text: storeduname, secure: false)
        uname.keyboardType = .alphabet
        let mail = notice.alertView.addTextField(Localizable.User.emailPlaceholder, text: nil, secure: false)
        
        notice.alertView.addButton(Localizable.Generic.nextStep, action: {
            
            let onError: VoidClosure = {
                if uname.text == nil || uname.text!.isEmpty {
                    onErrorField([uname])
                }
                if mail.text == nil || !mail.text!.isMatch(pattern: Joyup.regExPatternEmail) {
                    onErrorField([mail])
                }
            }
            
            if let u = uname.text, let m = mail.text {
                // FIMME: EMail Pattern
                if !u.isEmpty && !m.isEmpty && m.isMatch(pattern: Joyup.regExPatternEmail) {
                    notice.alertView.hideView()
                    JoyupNotice.forgottenPasswordLoadingRequest(u, email: m, completion: { (success, msg) in
                        if success {
                            JoyupNotice.showMessage(msg ?? Localizable.User.pleaseCheckEmail)
//                            JoyupNotice.signIn({}, onSignUp: {}, onCancel: {})
                        } else {
                            JoyupNotice.showError(msg ?? Localizable.Generic.unknownError)
                        }
                    })
                    return
                }
            }
            onError()
        })
        
        /*
        notice.alertView.addButton(Localizable.User.goToSignIn, backgroundColor: kOptionalButtonColor, textColor: kOptionalButtonTextColor, showTimeout: nil) {
            notice.alertView.hideView()
        }*/
        
        notice.alertView.addButton(Localizable.Generic.cancel, backgroundColor: kOptionalButtonColor, textColor: kOptionalButtonTextColor, showTimeout: nil) {
            notice.alertView.hideView()
            JoyupNotice.signIn()
        }
        notice.alertView.show(.editor,
                              title: Localizable.User.forgottenPassword,
                              detail: Localizable.User.forgottenPassword,
                              icon: nil,
                              color: kSignButtonColor,
                              buttonTitleColor: kSignButtonTextColor,
                              completeText: nil,
                              timeout: nil,
                              animation: .topToBottom)
        return notice
    }
}

fileprivate extension JoyupNotice {
    
    fileprivate class func onErrorField(_ fields: [UITextField]) {
        for f in fields {
            f.layer.borderColor = UIColor.red.cgColor
            
        }
        EasyGCD.after(1) {
            for f in fields {
                f.layer.borderColor = kSignButtonColor.cgColor
            }
        }
    }
    
    fileprivate class func onErrorTextView(_ textViews: [UITextView]) {
        for t in textViews {
            t.layer.borderColor = UIColor.red.cgColor
            EasyGCD.after(1) {
                t.layer.borderColor = kSignButtonColor.cgColor
            }
        }
    }
}

