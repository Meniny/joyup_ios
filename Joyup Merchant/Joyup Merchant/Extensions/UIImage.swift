//
//  UIImage.swift
//  Joyup
//
//  Created by Meniny on 2017-07-19.
//  Copyright © 2017年 Meniny. All rights reserved.
//

import Foundation
import UIKit

extension UIImage {
    public convenience init?(color: UIColor, size: CGSize) {
        UIGraphicsBeginImageContext(size)
        let context = UIGraphicsGetCurrentContext()!
        context.setFillColor(color.cgColor)
        context.fill(CGRect(origin: CGPoint.zero, size: size))
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        guard let i = image, let data = UIImagePNGRepresentation(i) else {
            return nil
        }
        self.init(data: data, scale: i.scale)
    }
}
