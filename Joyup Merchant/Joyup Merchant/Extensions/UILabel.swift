//
//  UILabel.swift
//  Joyup Merchant
//
//  Created by Meniny on 2017-09-03.
//  Copyright © 2017年 Meniny. All rights reserved.
//

import Foundation
import UIKit

extension UILabel {
    public var localizedText: String? {
        set {
            text = newValue?.localized()
        }
        get {
            return text
        }
    }
}

extension UITextField {
    public var localizedText: String? {
        set {
            text = newValue?.localized()
        }
        get {
            return text
        }
    }
    
    public var localizedPlaceholder: String? {
        set {
            placeholder = newValue?.localized()
        }
        get {
            return placeholder
        }
    }
}

extension UIButton {
    public var localizedTitle: String? {
        set {
            setTitle(newValue?.localized(), for: .normal)
        }
        get {
            return title(for: .normal)
        }
    }
}

extension UIView {
    public var clipToRadius: CGFloat {
        get {
            return layer.cornerRadius
        }
        set {
            clipsToBounds = true
            layer.masksToBounds = true
            layer.cornerRadius = newValue
        }
    }
}


