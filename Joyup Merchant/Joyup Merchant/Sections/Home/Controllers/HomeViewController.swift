//
//  HomeViewController.swift
//  Joyup
//
//  Created by Meniny on 2017-07-12.
//  Copyright © 2017年 Meniny. All rights reserved.
//

import UIKit
import Jsonify
import EasyGCD
import SVPullToRefresh
import Localization
import Imagery
//import ImageFactory
import Settings
import CocoaHelper
import Essence
import QuickResponseCode
import Imager

func JoyupAutoSignIn() {
    if !JoyupUser.signedIn {
        JoyupUser.autoSignIn(completion: { success in if !success { JoyupNotice.signIn() } })
    }
}

public struct MerchantHomeData {
    public var balance: Float
    public var records: [TransferRecord] = []
    
    public init(json: Jsonify) {
        balance = json["acctBalance"].floatValue
        for js in json["transRecord"]["memberTransferList"].arrayValue {
            let rec = TransferRecord(json: js)
            records.append(rec)
        }
    }
}

class HomeViewController: BaseTableViewController {

    var homedata: MerchantHomeData?
    var userItem: UIBarButtonItem?
    var balanceItem: UIBarButtonItem?
    var codeItem: UIBarButtonItem?
    
    var recordsButton = UIButton(type: .custom)
    
    override func viewDidLoad() {
        
        let offset: CGFloat = 20
        let bottomHeight: CGFloat = 49
        self.tableInset = UIEdgeInsets(top: 0, left: 0, bottom: -(bottomHeight + offset * 2), right: 0)
        
        super.viewDidLoad()
        
        title = nil//Localizable.Home.title
        
        tableView.register(cell: TransferRecordTableViewCell.self)
        tableView.delegate = self
        tableView.dataSource = self
        
        tableView.addPullToRefresh {
            self.request()
        }
        
        recordsButton.isEnabled = false
        recordsButton.backgroundColor = .lightGray
        recordsButton.localizedTitle = "Transfer Records"
        recordsButton.setTitleColor(.white, for: .normal)
        recordsButton.clipToRadius = 5
        recordsButton.addTarget(self, action: #selector(showRecords), for: .touchUpInside)
        view.addSubview(recordsButton)
        recordsButton.snp.makeConstraints { (make) in
            make.bottom.equalToSuperview().offset(-offset)
            make.left.equalToSuperview().offset(offset)
            make.right.equalToSuperview().offset(-offset)
            make.top.equalTo(self.tableView.snp.bottom).offset(offset)
        }
        
        codeItem = UIBarButtonItem(image: #imageLiteral(resourceName: "qrcode"), style: .plain, target: self, action: #selector(showQRCodeImage))
    }
    
    func showRecords() {
        let next = TransferRecordsViewController()
        navigationController?.pushViewController(next, animated: true)
    }
    
    func showDebuggingPanel() {
        let next = DebugViewController()
        self.navigationController?.pushViewController(next, animated: true)
    }
    
    func didTapUserAvatar() {
        if JoyupUser.signedIn {
            JoyupNotice.showConfirm(Localizable.User.signOut, message: nil, leftButton: Localizable.User.signOut, leftAction: { 
                JoyupUser.signOut(nil)
            }, rightButton: Localizable.Generic.cancel, rightAction: nil)
        } else {
            JoyupNotice.signIn()
        }
    }
    
    func showQRCodeImage() {
        showQRCode(showError: true)
    }
    
    func generateQRCodeString(username: String, nickname: String) -> String {
        return "Joyup://" + username.URLEncodedString + "/" + nickname.URLEncodedString
    }
    
    func showQRCode(showError: Bool) {
        // FIXME: QRCode Rule
        guard let uname = JoyupUser.main?.userName, let name = JoyupUser.main?.name, let code = QuickResponseCode.generate(string: generateQRCodeString(username: uname, nickname: name), size: CGSize(width: 1024, height: 1024), foreground: .black, background: .white, correction: .medium).iconedImage(#imageLiteral(resourceName: "avatar"), radius: 5) else {
            if showError {
                JoyupNotice.showError()
            }
            return
        }
        Imager.show(images: [code], controller: self, delegate: self)
    }
    
    func setupBarItems(_ signIn: Bool) {
        if signIn {
            userItem = UIBarButtonItem(title: JoyupUser.main?.name ?? "AlreadSignedIn".localized(), style: .plain, target: self, action: #selector(didTapUserAvatar))
            let balance: Float = homedata?.balance ?? 0
            balanceItem = UIBarButtonItem(title: "Balance".localized() + ": US$\(balance.amountString)", style: .plain, target: nil, action: nil)
        } else {
            userItem = UIBarButtonItem(image: #imageLiteral(resourceName: "profile"), style: .plain, target: self, action: #selector(didTapUserAvatar))
            balanceItem = nil
        }
        navigationItem.rightBarButtonItems = [codeItem!, userItem!]
        navigationItem.leftBarButtonItem = balanceItem
    }
    
    override func userDidSignedOut() {
        super.userDidSignedOut()
        recordsButton.isEnabled = false
        recordsButton.backgroundColor = .lightGray
        homedata = nil
        tableView.reloadData()
        setupBarItems(false)
        JoyupNotice.signIn()
    }
    
    override func userDidSignedIn() {
        super.userDidSignedIn()
        recordsButton.isEnabled = true
        recordsButton.backgroundColor = #colorLiteral(red: 0.15, green:0.82, blue:0.55, alpha:1.00)
        setupBarItems(true)
        request()
        
        if let userInfo = ApplePushNotification.userInfo {
            handleUserInfo(userInfo)
            ApplePushNotification.userInfo = nil
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if !once("JoyupAutoSignInOnceToken", {
            UpdateManager.check { (update, forced, _) in
                if update {
                    UpdateManager.alert(force: forced, completion: {
                        JoyupAutoSignIn()
                    })
                } else {
                    JoyupAutoSignIn()
                }
            }
        }) {
            JoyupAutoSignIn()
            /*
            if ApplePushNotification.registerFailed && !ESDevice.isSimulator {
                once("JoyupAPNsRegisterFailed") {
                    main {
                        JoyupNotice.showConfirm(Localizable.Settings.apnsFailed, message: "", leftButton: Localizable.Settings.gotoSettings, leftAction: {
                            Settings.currentApp.open()
                        }, rightButton: Localizable.Generic.notNow, rightAction: nil)
                    }
                }
            }
            */
        }
    }
    
    func request() {
        
        if !JoyupUser.signedIn {
            self.tableView.pullToRefreshView.stopAnimating()
        }
        
        tableView.pullToRefreshView.startAnimating()
        
        Request.home.request(params: nil, completion: { [weak self] (json, resp, error) in
            self?.tableView.pullToRefreshView.stopAnimating()
            if let message = Request.checkResult(json) {
                JoyupNotice.showError(message)
            } else {
                self?.homedata = MerchantHomeData(json: json["data"])
                self?.tableView.reloadData()
                self?.setupBarItems(true)
            }
        }) { [weak self] (resp, error) in
            self?.tableView.pullToRefreshView.stopAnimating()
            JoyupNotice.showFireError(error, response: resp)
        }
    }
}

extension HomeViewController: ImagerDelegate {
    func imagerControllerWillDismiss(_ controller: ImagerController) {
        
    }

    func imagerController(_ controller: ImagerController, didMoveTo page: Int) {
        
    }
    
    func imagerController(_ controller: ImagerController, didLongPressedAt page: Int) {
        guard controller.images.isNotEmpty && (0..<controller.images.count).contains(page) else {
            return
        }
        guard let image = controller.images[page].image else {
            return
        }
        
        let alert = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "Save".localized(), style: .default, handler: { (action) in
            UIImageWriteToSavedPhotosAlbum(image, nil, nil, nil)
        }))
        
        alert.addAction(UIAlertAction(title: "Cancel".localized(), style: .cancel, handler: { (action) in }))
        alert.popoverPresentationController?.sourceView = controller.view
        alert.popoverPresentationController?.sourceRect = controller.view.bounds
        
        controller.present(alert, animated: true, completion: nil)
    }
}

extension HomeViewController: UITableViewDelegate, UITableViewDataSource {
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let id = self.cellIdentifier(at: indexPath)
        let cell = tableView.dequeueReusableCell(withIdentifier: id)
        self.config(cell: cell!, at: indexPath)
        return cell!
    }
    
    func cellIdentifier(at indexPath: IndexPath) -> String {
        return TransferRecordTableViewCell.nameOfClass
    }
    
    func config(cell aCell: UITableViewCell, at indexPath: IndexPath) {
        aCell.clipsToBounds = true
        aCell.selectionStyle = .none
        aCell.accessoryType = .none
        
        if aCell is TransferRecordTableViewCell {
            let cell = aCell as! TransferRecordTableViewCell
            cell.config(with: self.homedata?.records.object(at: indexPath.row))
        }
    }
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return homedata?.records.count ?? 0
    }
    
    public func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    public func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let id = self.cellIdentifier(at: indexPath)
        return tableView.fd_heightForCell(withIdentifier: id, configuration: { (cell) in
            self.config(cell: cell as! UITableViewCell, at: indexPath)
        })
    }
}
