//
//  TransferRecordsViewController.swift
//  Joyup Merchant
//
//  Created by Meniny on 2017-09-04.
//  Copyright © 2017年 Meniny. All rights reserved.
//

import UIKit

class TransferRecordsViewController: BaseTableViewController {

    var records: [TransferRecord] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()

        title = "Transfer Records".localized()
        
        tableView.register(cell: TransferRecordTableViewCell.self)
        tableView.delegate = self
        tableView.dataSource = self
        
        tableView.addPullToRefresh {
            self.request(loadMore: false)
        }
        
        tableView.addInfiniteScrolling { 
            self.request(loadMore: true)
        }
        
        request(loadMore: false)
    }
    
    func request(loadMore: Bool) {
        if loadMore {
            tableView.pullToRefreshView.startAnimating()
        } else {
            tableView.infiniteScrollingView.startAnimating()
        }
        Request.transferRecords.request(params: ["pageNumber": loadMore ? page + 1 : 0], completion: { [weak self] (json, _, _) in
            self?.tableView.pullToRefreshView.stopAnimating()
            self?.tableView.infiniteScrollingView.stopAnimating()
            if let message = Request.checkResult(json) {
                JoyupNotice.showError(message)
            } else {
                if loadMore {
                    self?.page += 1
                } else {
                    self?.page = 0
                    self?.records.removeAll()
                }
                for js in json["data"]["memberTransferList"].arrayValue {
                    let item = TransferRecord(json: js)
                    self?.records.append(item)
                }
                self?.tableView.reloadData()
            }
        }) { [weak self] (resp, error) in
            self?.tableView.pullToRefreshView.stopAnimating()
            self?.tableView.infiniteScrollingView.stopAnimating()
            JoyupNotice.showFireError(resp, error: error.localizedDescription)
        }
    }

}

extension TransferRecordsViewController: UITableViewDelegate, UITableViewDataSource {
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let id = self.cellIdentifier(at: indexPath)
        let cell = tableView.dequeueReusableCell(withIdentifier: id)
        self.config(cell: cell!, at: indexPath)
        return cell!
    }
    
    func cellIdentifier(at indexPath: IndexPath) -> String {
        return TransferRecordTableViewCell.nameOfClass
    }
    
    func config(cell aCell: UITableViewCell, at indexPath: IndexPath) {
        aCell.clipsToBounds = true
        aCell.selectionStyle = .none
        aCell.accessoryType = .none
        
        if aCell is TransferRecordTableViewCell {
            let cell = aCell as! TransferRecordTableViewCell
            cell.config(with: records.object(at: indexPath.row))
        }
    }
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return records.count
    }
    
    public func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    public func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let id = self.cellIdentifier(at: indexPath)
        return tableView.fd_heightForCell(withIdentifier: id, configuration: { (cell) in
            self.config(cell: cell as! UITableViewCell, at: indexPath)
        })
    }
}
