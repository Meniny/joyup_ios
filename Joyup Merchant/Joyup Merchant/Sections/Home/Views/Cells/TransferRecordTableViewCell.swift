//
//  TransferRecordTableViewCell.swift
//  Joyup Merchant
//
//  Created by Meniny on 2017-09-04.
//  Copyright © 2017年 Meniny. All rights reserved.
//

import UIKit

class TransferRecordTableViewCell: UITableViewCell {

    @IBOutlet weak var memoLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var amountLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    func config(with item: TransferRecord?) {
        memoLabel.localizedText = item?.memo.localized()
        dateLabel.text = item?.createDate
        if let amount = item?.amount.amountString {
            amountLabel.text = "US$ " + amount
        } else {
            amountLabel.text = nil
        }
    }
}
