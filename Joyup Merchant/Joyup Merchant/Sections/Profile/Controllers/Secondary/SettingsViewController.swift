//
//  SettingsViewController.swift
//  Joyup
//
//  Created by Meniny on 2017-07-13.
//  Copyright © 2017年 Meniny. All rights reserved.
//

import UIKit
//import SDWebImage
import Imagery
import Logify
import EasyGCD
import Oops
import Formal
import InfoPlist

class SettingsViewController: FormalViewController {
    
    override init(style: UITableViewStyle) {
        super.init(style: style)
        hidesBottomBarWhenPushed = Joyup.shouldHidesBottomBarWhenPushed(for: self)
    }
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        hidesBottomBarWhenPushed = Joyup.shouldHidesBottomBarWhenPushed(for: self)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        hidesBottomBarWhenPushed = Joyup.shouldHidesBottomBarWhenPushed(for: self)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        title = Localizable.Settings.title
        
        formal +++ FormalSection(Localizable.Settings.title)
            <<< FormalSwitchRow() {
                $0.title = Localizable.Settings.increaseAnimation
                $0.value = Joyup.increaseAnimation
                }.onChange({ (row) in
                    Joyup.increaseAnimation = row.value ?? false
                })
//            <<< FormalSwitchRow() {
//                $0.title = Localizable.Settings.hidesBottomBarWhenPushed
//                $0.value = Joyup.hidesBottomBarWhenPushed
//                }.onChange({ (row) in
//                    Joyup.hidesBottomBarWhenPushed = row.value ?? false
//                })
        
        
        formal +++ FormalSection(Localizable.Settings.cleanCaches)
            <<< FormalLabelRow() {
                $0.title = Localizable.Settings.cleanCaches
            }.onCellSelection({ (cell, row) in
                JoyupNotice.showConfirm(Localizable.Settings.cleanCaches, message: nil, leftButton: Localizable.Settings.cleanCaches, leftAction: {
//                    SDWebImageManager.shared().cancelAll()
//                    SDWebImageManager.shared().imageCache?.clearMemory()
                    ImageryCache.clearMemoryCache()
                    ImageryCache.clearDiskCache(completion: {
                        main {
                            JoyupNotice.showSuccess(Localizable.Settings.cachesCleaned)
                        }
                    })
                }, rightButton: nil, rightAction: nil)
        })
        
        formal +++ FormalSection(Localizable.About.title)
            <<< FormalLabelRow() {
                $0.title = Localizable.About.version
                #if DEBUG
                    $0.value = "DEBUG " + InfoPlist.version + "." + InfoPlist.build
                #else
                    $0.value = InfoPlist.version + "." + InfoPlist.build
                #endif
            }
            <<< FormalLabelRow() {
                $0.title = Localizable.About.title
                }.onCellSelection({ (_, _) in
                    self.show(url: Joyup.aboutURL)
                })
            <<< FormalLabelRow() {
                $0.title = Localizable.User.userAgreement
                }.onCellSelection({ (_, _) in
                    self.show(url: Joyup.userAgreementURL)
                })
            <<< FormalLabelRow() {
                $0.title = Localizable.User.returnAndRefundPolicy
                }.onCellSelection({ (_, _) in
                    self.show(url: Joyup.returnPolicyURL)
                })
            <<< FormalLabelRow() {
                $0.title = Localizable.User.privacyPolicy
                }.onCellSelection({ (_, _) in
                    self.show(url: Joyup.privacyURL)
                })
        
        formal +++ FormalSection()
            <<< FormalButtonRow() {
                $0.title = Localizable.User.signOut
            }.onCellSelection({ (cell, row) in
                self.signOut()
        })
        
        #if DEBUG
            formal +++ FormalSection()
                <<< FormalLabelRow() {
                    $0.title = "DEBUG"
            }.cellSetup({ (cell, _) in
                cell.accessoryType = .disclosureIndicator
            }).onCellSelection({ (_, _) in
                let next = DebugViewController()
                self.navigationController?.pushViewController(next, animated: true)
            })
            
        #endif
        
        self.tableView.separatorStyle = .singleLine
    }
    
    func show(url: String) {
        let next = WebViewController(urlString: url)
        self.navigationController?.pushViewController(next, animated: true)
    }
    
    func signOut() {
        JoyupNotice.showConfirm(Localizable.User.signOut, message: nil, leftButton: Localizable.User.signOut, leftAction: {
            let loading = JoyupNotice.showLoading(nil)
            JoyupUser.signOut({ (success, error) in
                loading.hide()
                if success {
                    self.navigationController?.popToRootViewController(animated: false)
                    self.tabBarController?.selectedIndex = 0
                } else {
                    JoyupNotice.showError(error ?? Localizable.Generic.unknownError)
                }
            })
        }, rightButton: nil, rightAction: nil)
    }
}
