//
//  SignInViewController.swift
//  Joyup Merchant
//
//  Created by Meniny on 2017-09-03.
//  Copyright © 2017年 Meniny. All rights reserved.
//

import UIKit
import CocoaHelper
import SnapKit

class SignInViewController: BaseViewController {

    let unameField = UITextField()
    let pwdField = UITextField()
    let actionButton = UIButton(type: .custom)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        title = Localizable.User.signIn
//        view.backgroundColor = #colorLiteral(red: 0.12, green:0.13, blue:0.16, alpha:1.00)

//        let topLabel = UILabel()
//        topLabel.textAlignment = .center
//        topLabel.textColor = UIColor.darkText//UIColor.white
//        topLabel.localizedText = "Joyup Merchant"
//        view.addSubview(topLabel)
//        topLabel.snp.makeConstraints { (make) in
//            make.left.equalToSuperview()
//            make.right.equalToSuperview()
//            make.top.equalTo(50)
//            make.height.greaterThanOrEqualTo(0)
//        }
        
        let logo = UIImageView(image: #imageLiteral(resourceName: "avatar"))
        logo.contentMode = .scaleAspectFit
        logo.backgroundColor = UIColor.clear
        logo.clipsToBounds = true
        view.addSubview(logo)
        logo.snp.makeConstraints { (make) in
            let length: CGFloat = 150
            make.width.equalTo(length)
            make.height.equalTo(length)
            make.centerX.equalToSuperview()
//            make.top.equalTo(topLabel.snp.bottom).offset(20)
            make.top.equalTo(20)
        }
        
        unameField.localizedPlaceholder = "UserName"
//        unameField.backgroundColor = #colorLiteral(red: 0.17, green:0.17, blue:0.20, alpha:1.00)
        unameField.borderStyle = .roundedRect
//        unameField.textColor = .white
//        unameField.tintColor = .white
        unameField.returnKeyType = .next
        unameField.keyboardAppearance = .dark
        unameField.clearButtonMode = .whileEditing
        view.addSubview(unameField)
        unameField.snp.makeConstraints { (make) in
            make.width.equalTo(200)
            make.height.equalTo(40)
            make.top.equalTo(logo.snp.bottom).offset(20)
            make.centerX.equalToSuperview()
        }
        
        pwdField.localizedPlaceholder = "Password"
        pwdField.backgroundColor = unameField.backgroundColor
        pwdField.borderStyle = unameField.borderStyle
//        pwdField.textColor = .white
//        pwdField.tintColor = .white
        pwdField.returnKeyType = .done
        pwdField.keyboardType = .asciiCapable
        pwdField.keyboardAppearance = unameField.keyboardAppearance
        pwdField.isSecureTextEntry = true
        pwdField.clearButtonMode = unameField.clearButtonMode
        view.addSubview(pwdField)
        pwdField.snp.makeConstraints { (make) in
            make.width.equalTo(self.unameField.snp.width)
            make.height.equalTo(self.unameField.snp.height)
            make.top.equalTo(self.unameField.snp.bottom).offset(8)
            make.centerX.equalToSuperview()
        }
        
        unameField.delegate = self
        pwdField.delegate = self
        
        actionButton.localizedTitle = "SignIn"
        actionButton.setTitleColor(.white, for: .normal)
        actionButton.setTitleColor(.lightText, for: .highlighted)
        actionButton.backgroundColor = #colorLiteral(red: 0.15, green:0.82, blue:0.55, alpha:1.00)
        actionButton.clipToRadius = 3
        view.addSubview(actionButton)
        actionButton.snp.makeConstraints { (make) in
            let length: CGFloat = 200
            make.width.equalTo(length)
            make.height.equalTo(self.pwdField.snp.height)
            make.top.equalTo(self.pwdField.snp.bottom).offset(20)
            make.centerX.equalToSuperview()
        }
        
        actionButton.addTarget(self, action: #selector(signInAction), for: .touchUpInside)
        
        #if DEBUG
            unameField.text = "shanghuapp"
            pwdField.text = "11111"
        #endif
        
//        navigationItem.leftBarButtonItem = UIBarButtonItem(barButtonSystemItem: .cancel, target: self, action: #selector(close))
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        unameField.becomeFirstResponder()
    }
    
    func signInAction() {
        guard let uname = unameField.text, !uname.isEmpty else {
            self.warning(Localizable.User.InvalidFormat)
            return
        }
        guard let pwd = pwdField.text, !pwd.isEmpty else {
            self.warning(Localizable.User.InvalidFormat)
            return
        }
        Request.getRSAPublicKey.request(params: nil, completion: { (json, _, _) in
            guard let modulus = json["modulus"].string, let exponent = json["exponent"].string else {
                self.error()
                return
            }
            JoyupUser.signIn(uname, password: pwd, modulus: modulus, exponent: exponent, completion: { s, m in
                if s {
                    self.close()
                } else {
                    self.error(m)
                }
            })
        }) { (_, e) in
            self.error(e.localizedDescription)
        }
    }
    
    func warning(_ message: String? = nil) {
        JoyupNotice.showWarning(message ?? Localizable.User.signInFailed)
    }
    
    func error(_ message: String? = nil) {
        JoyupNotice.showError(message ?? Localizable.User.signInFailed)
    }
    
    func close() {
        dismiss(animated: true, completion: nil)
    }

}

extension SignInViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        textField.resignFirstResponder()
        
        if textField == unameField {
            pwdField.becomeFirstResponder()
        } else if textField == pwdField {
            signInAction()
        }
        return true
    }
}
