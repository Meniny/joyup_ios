//
//  NormalSwitchTableViewCell.swift
//  Joyup
//
//  Created by Meniny on 2017-08-15.
//  Copyright © 2017年 Meniny. All rights reserved.
//

import UIKit

class NormalSwitchTableViewCell: UITableViewCell {

    @IBOutlet weak var `switch`: UISwitch!
    @IBOutlet weak var leftLabel: UILabel!
    
    private var changeAction: ((_ isOn: Bool) -> Void)?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    @IBAction func switchChanged(_ sender: UISwitch) {
        changeAction?(sender.isOn)
    }
    
    public func config(useBalance: Bool, balance: Float, total: Float, onChange: @escaping (_ isOn: Bool) -> Void) {
        changeAction = onChange
//        if self.switch.isOn {
            leftLabel.text = "UseBalance".localized() + " (US$ " + total.amountString + "): US$ " + balance.amountString
//        } else {
//            leftLabel.text = "UseBalance".localized() + ": US$ 0"
//        }

        self.`switch`.isOn = useBalance
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
