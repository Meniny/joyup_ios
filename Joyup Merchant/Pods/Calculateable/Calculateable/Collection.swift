//
//  CGRectUtils.swift
//  Crucio
//
//  Created by Meniny on 2017-08-19.
//  Copyright © 2017年 Sky Platanus. All rights reserved.
//

import Foundation
import CoreGraphics

// MARK: - Int -> Array

public extension Int {
    /// An array contains all the integer values from 0 to `self`
    public var arrayFromZero: [Int] {
        return self.array
    }
    
    /// An array contains all the integer values from `self` to 0
    public var arrayToZero: [Int] {
        return self.array
    }
    
    /// An array contains all the integer values between 0 and `self`
    public var array: [Int] {
        guard self != 0 else { return [0] }
        var result = [Int]()
        let range = (self > 0) ? 0...self : self...0
        for i in range { result.append(i) }
        return result
    }
}

public protocol ObjectRepeating {
    func repeating(_ time: Int) -> [Self]
}

public func GenericRepeating<T>(_ obj: T, time: Int) -> [T] {
    guard time != 0 else {
        return [obj]
    }
    var result = [T]()
    for _ in 0..<abs(time) {
        result.append(obj)
    }
    return result
}

public extension ObjectRepeating where Self: Any {
    public func repeating(_ time: Int) -> [Self] {
        return GenericRepeating(self, time: time)
    }
}

public extension ObjectRepeating where Self: AnyObject {
    public func repeating(_ time: Int) -> [Self] {
        return GenericRepeating(self, time: time)
    }
}

public extension ObjectRepeating where Self: NSObject {
    public func repeating(_ time: Int) -> [Self] {
        return GenericRepeating(self, time: time)
    }
}

public extension Array {
    public var firstIndex: Int {
        return 0
    }
    
    public var lastIndex: Int {
        return (count > 0) ? (count - 1) : 0
    }
}

public extension Array {
    
    /// Get an element at specific index
    ///
    /// - Parameter index: Index of the element
    /// - Returns: The element at specific index, return `nil` if `index` is out of bounds
    public func element(at index: Int) -> Element? {
        guard index >= 0 && count >= index else {
            return nil
        }
        return self[index]
    }
    
    /// Get an object at specific index
    ///
    /// - Parameter index: Index of the object
    /// - Returns: The object at specific index, return `nil` if `index` is out of bounds
    public func object(at index: Int) -> Element? {
        return element(at: index)
    }
    
    /// Get a value at specific index
    ///
    /// - Parameter index: Index of the value
    /// - Returns: The value at specific index, return `nil` if `index` is out of bounds
    public func value(at index: Int) -> Element? {
        return element(at: index)
    }
    
    /// If this array is not empty
    public var isFilled: Bool {
        return !isEmpty
    }
    
    /// If this array is not empty
    public var hasElement: Bool {
        return !isEmpty
    }
    
}

public extension Array {
    /// Returns a new array by convert the elements of the sequence to `String` type.
    ///
    /// The following example shows how an `[Int]` array can be converted to `[String]` array:
    ///
    ///     let cast = [123, 456, 789]
    ///     let list = cast.toStringArray()
    ///     print(list)
    ///     // Prints ["123", "456", "789"]
    ///
    /// - Returns: A new array.
    public func toStringArray() -> [String] {
        if self is [String] {
            return self as! [String]
        }
        return self.map({ (e) -> String in
            if e is CustomStringConvertible {
                return (e as! CustomStringConvertible).description
            }
            return "\(e)"
        })
    }
    
    /// Returns a new string by concatenating the elements of the sequence, adding the given separator between each element.
    ///
    /// The following example shows how an `[Int]` array can be joined to a single comma-separated string:
    ///
    ///     let cast = ["Vivien", "Marlon", "Kim", "Karl"]
    ///     let list = cast.toString(separatedBy: ", ")
    ///     print(list)
    ///     // Prints "Vivien, Marlon, Kim, Karl"
    ///
    /// - Parameter separator: A string to insert between each of the elements in this sequence. The default separator is an empty string.
    /// - Returns: A single, concatenated string.
    public func toString(separatedBy separator: String = "") -> String {
        return toStringArray().joined(separator: separator)
    }
    
}
