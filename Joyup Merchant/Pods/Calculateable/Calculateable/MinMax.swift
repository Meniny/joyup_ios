//
//  CGRectUtils.swift
//  Crucio
//
//  Created by Meniny on 2017-08-19.
//  Copyright © 2017年 Sky Platanus. All rights reserved.
//

import Foundation
import CoreGraphics

// MARK: - min/max

public func min(_ lhs: CGVector, _ rhs: CGVector) -> CGVector {
    return CGVector(dx: fmin(lhs.dx, rhs.dx), dy: fmin(lhs.dy, rhs.dy))
}

public func min(_ lhs: CGPoint, _ rhs: CGPoint) -> CGPoint {
    return CGPoint(x: fmin(lhs.x, rhs.x), y: fmin(lhs.y, rhs.y))
}

public func min(_ lhs: CGSize, _ rhs: CGSize) -> CGSize {
    return CGSize(width: fmin(lhs.width, rhs.width), height: fmin(lhs.height, rhs.height))
}

public func min(_ lhs: CGRect, _ rhs: CGRect) -> CGRect {
    return CGRect(origin: min(lhs.origin, rhs.origin), size: min(lhs.size, rhs.size))
}

public func max(_ lhs: CGVector, _ rhs: CGVector) -> CGVector {
    return CGVector(dx: fmax(lhs.dx, rhs.dx), dy: fmax(lhs.dy, rhs.dy))
}

public func max(_ lhs: CGPoint, _ rhs: CGPoint) -> CGPoint {
    return CGPoint(x: fmax(lhs.x, rhs.x), y: fmax(lhs.y, rhs.y))
}

public func max(_ lhs: CGSize, _ rhs: CGSize) -> CGSize {
    return CGSize(width: fmax(lhs.width, rhs.width), height: fmax(lhs.height, rhs.height))
}

public func max(_ lhs: CGRect, _ rhs: CGRect) -> CGRect {
    return CGRect(origin: max(lhs.origin, rhs.origin), size: max(lhs.size, rhs.size))
}

public extension CGVector {
    public var min: CGFloat {
        return fmin(dx, dy)
    }
    
    public var max: CGFloat {
        return fmax(dx, dy)
    }
}

public extension CGPoint {
    public var min: CGFloat {
        return fmin(x, y)
    }
    
    public var max: CGFloat {
        return fmax(x, y)
    }
}

public extension CGSize {
    public var min: CGFloat {
        return fmin(width, width)
    }
    
    public var max: CGFloat {
        return fmax(width, height)
    }
}

public extension CGRect {
    public var originMin: CGFloat {
        return origin.min
    }
    
    public var sizeMin: CGFloat {
        return size.min
    }
    
    public var originMax: CGFloat {
        return origin.max
    }
    
    public var sizeMax: CGFloat {
        return size.max
    }
}
