//
//  CGRectUtils.swift
//  Crucio
//
//  Created by Meniny on 2017-08-19.
//  Copyright © 2017年 Sky Platanus. All rights reserved.
//

import Foundation
import CoreGraphics

// MARK: - Operators

public func * (lhs: CGVector, rhs: CGFloat) -> CGVector {
    return CGVector(dx: lhs.dx * rhs, dy: lhs.dy * rhs)
}

public func * (lhs: CGPoint, rhs: CGFloat) -> CGPoint {
    return CGPoint(x: lhs.x * rhs, y: lhs.y * rhs)
}

public func * (lhs: CGSize, rhs: CGFloat) -> CGSize {
    return CGSize(width: lhs.width * rhs, height: lhs.height * rhs)
}

public func * (lhs: CGRect, rhs: CGFloat) -> CGRect {
    return CGRect(origin: lhs.origin * rhs, size: lhs.size * rhs)
}

// ---

public func * (lhs: CGVector, rhs: Double) -> CGVector {
    return CGVector(dx: lhs.dx * CGFloat(rhs), dy: lhs.dy * CGFloat(rhs))
}

public func * (lhs: CGPoint, rhs: Double) -> CGPoint {
    return CGPoint(x: lhs.x * CGFloat(rhs), y: lhs.y * CGFloat(rhs))
}

public func * (lhs: CGSize, rhs: Double) -> CGSize {
    return CGSize(width: lhs.width * CGFloat(rhs), height: lhs.height * CGFloat(rhs))
}

public func * (lhs: CGRect, rhs: Double) -> CGRect {
    return CGRect(origin: lhs.origin * CGFloat(rhs), size: lhs.size * CGFloat(rhs))
}

// ---

public func * (lhs: CGVector, rhs: Float) -> CGVector {
    return CGVector(dx: lhs.dx * CGFloat(rhs), dy: lhs.dy * CGFloat(rhs))
}

public func * (lhs: CGPoint, rhs: Float) -> CGPoint {
    return CGPoint(x: lhs.x * CGFloat(rhs), y: lhs.y * CGFloat(rhs))
}

public func * (lhs: CGSize, rhs: Float) -> CGSize {
    return CGSize(width: lhs.width * CGFloat(rhs), height: lhs.height * CGFloat(rhs))
}

public func * (lhs: CGRect, rhs: Float) -> CGRect {
    return CGRect(origin: lhs.origin * CGFloat(rhs), size: lhs.size * CGFloat(rhs))
}

// ---

public func * (lhs: CGVector, rhs: Int) -> CGVector {
    return CGVector(dx: lhs.dx * CGFloat(rhs), dy: lhs.dy * CGFloat(rhs))
}

public func * (lhs: CGPoint, rhs: Int) -> CGPoint {
    return CGPoint(x: lhs.x * CGFloat(rhs), y: lhs.y * CGFloat(rhs))
}

public func * (lhs: CGSize, rhs: Int) -> CGSize {
    return CGSize(width: lhs.width * CGFloat(rhs), height: lhs.height * CGFloat(rhs))
}

public func * (lhs: CGRect, rhs: Int) -> CGRect {
    return CGRect(origin: lhs.origin * CGFloat(rhs), size: lhs.size * CGFloat(rhs))
}

// MARK: - Strings

public func - (lhs: String, rhs: String) -> String {
    return lhs.replacingOccurrences(of: rhs, with: "")
}

public func -= (lhs: inout String, rhs: String) {
    lhs = lhs - rhs
}

public func += (lhs: inout String, rhs: String) {
    lhs.append(rhs)
}

public func * (lhs: String, rhs: Int) -> String {
    guard rhs > 0 else {
        return lhs
    }
    var array = [String]()
    for _ in 0..<rhs {
        array.append(lhs)
    }
    return array.joined()
}

public func *= (lhs: inout String, rhs: Int) {
    guard rhs > 0 else {
        return
    }
    lhs = lhs * rhs
}

// MARK: - Array

// Append an element
public func +=<T> (lhs: inout [T], rhs: T) {
    lhs.append(rhs)
}

// Append an array
public func +=<T> (lhs: inout [T], rhs: [T]) {
    lhs.append(contentsOf: rhs)
}

// Insert to an array
public func +=<T> (lhs: T, rhs: inout [T]) {
    rhs.insert(lhs, at: 0)
}

// Insert to an array, return the new array
public func +<T> (lhs: T, rhs: [T]) -> [T] {
    var new = rhs
    new.insert(lhs, at: 0)
    return new
}

// Append an element, return the new array
public func +<T> (lhs: [T], rhs: T) -> [T] {
    var new = lhs
    new.append(rhs)
    return new
}

// Append an array, return the new array
public func +<T> (lhs: [T], rhs: [T]) -> [T] {
    var new = lhs
    new.append(contentsOf: rhs)
    return new
}
