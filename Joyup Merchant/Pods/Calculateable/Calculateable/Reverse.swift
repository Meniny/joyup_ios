//
//  Reverse.swift
//  Pods
//
//  Created by Meniny on 2017-08-19.
//
//

import Foundation

public extension Int {
    /// Resersed integer
    ///     
    /// ```
    /// let num = 12345.reversed
    /// // num: 54321
    /// ```
    public var reversed: Int {
        
        var module = 0
        var result = 0
        
        var number = self
        
        while (number / 10 != 0) {// 当num为一位数时，跳出循环
            
            module = number % 10
            number = number / 10
            result = result * 10 + module
        }
        
        return result * 10 + number// 当num为一位数时，返回结果
    }
}

public extension String {
    // Reverse characters in range
    public static func reverse(characters chars: inout [Character], from: Int, to: Int) {
        
        var startIndex = min(from, to)
        var endIndex = max(from, to)
        
        if startIndex <= endIndex {
            let tempChar = chars[endIndex]
            chars[endIndex] = chars[startIndex]
            chars[startIndex] = tempChar
            
            startIndex += 1
            endIndex -= 1
            
            String.reverse(characters: &chars, from: startIndex, to: endIndex)
        }
    }
    
    /// All characters resersed string
    ///
    /// ```
    /// let new = "the sky is blue".reversed
    /// // new: "eulb si yks eht"
    /// ```
    public var charactersReversed: String {
        
        var chars = [Character](characters)
        
        // reverse all characters: "the sky is blue" -> "eulb si yks eht"
        String.reverse(characters: &chars, from: 0, to: chars.count - 1)
        
        return String(chars)
    }
    
    /// All characters resersed string
    ///
    /// ```
    /// let new = "the sky is blue".reversed
    /// // new: "eulb si yks eht"
    /// ```
    public var reversed: String {
        return charactersReversed
    }
    
    /// All words resersed string
    ///
    /// ```
    /// let new = "the sky is blue".reversed
    /// // new: "blue is sky the"
    /// ```
    public var wordsReversed: String {
        
        // reverse all characters: "the sky is blue" -> "eulb si yks eht"
        var chars = [Character](charactersReversed.characters)
        
        // reverse each words: "eulb si yks eht" -> "blue is sky the"
        var startIndex = 0
        for endIndex in 0 ..< chars.count {
            if endIndex == chars.count - 1 || chars[endIndex + 1] == " " {
                String.reverse(characters: &chars, from: startIndex, to: endIndex)
                startIndex = endIndex + 2
            }
        }
        
        return String(chars)
    }
    
}
