
#if os(iOS) || os(OSX)
    #if os(OSX)
        import Cocoa
        #else
        import Foundation
        import UIKit
    #endif

    public extension AppleView {
        
        public var height: CGFloat {
            get {
                return frame.height
            }
            set {
                frame.size.height = newValue
            }
        }
        
        public var width: CGFloat {
            get {
                return frame.width
            }
            set {
                frame.size.width = newValue
            }
        }
        
        public var x: CGFloat {
            get {
                return frame.minX
            }
            set {
                frame.origin.x = newValue
            }
        }
        
        public var y: CGFloat {
            get {
                return frame.minY
            }
            set {
                frame.origin.y = newValue
            }
        }
        
        public var maxX: CGFloat {
            get {
                return frame.maxX
            }
        }
        
        public var maxY: CGFloat {
            get {
                return frame.maxY
            }
        }
        
        #if !os(OSX)
            public var centerX: CGFloat {
                get {
                    return center.x
                }
                set {
                    center.x = newValue
                }
            }
            
            public var centerY: CGFloat {
                get {
                    return center.y
                }
                set {
                    center.y = newValue
                }
            }
        #endif
    }
#endif
