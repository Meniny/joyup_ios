
import Foundation
import UIKit

extension UIView {
    /// Enable Shadow
    ///
    /// - Parameters:
    ///   - color: `UIColor` object, default is black(nil)
    ///   - opacity: default is 0.05
    ///   - radius: default is 3
    ///   - x: default is 0
    ///   - y: default is 1
    public func enableShadow(_ color: UIColor? = nil,
                             opacity: Float = 0.05,
                             radius: CGFloat = 3,
                             offset x: CGFloat = 0, y: CGFloat = 1) {
        self.clipsToBounds = false
        self.layer.shadowOffset = CGSize(width: x, height: y)
        self.layer.shadowOpacity = opacity
        self.layer.shadowColor = (color ?? UIColor.black).cgColor
        self.layer.shadowRadius = radius
    }
    
    /// Enable Border
    ///
    /// - Parameters:
    ///   - color: `UIColor` object, default is black(nil)
    ///   - width: Border width, default is 1
    ///   - radius: Corner radius, default is 3
    public func enableBorder(_ color: UIColor? = nil,
                             width: CGFloat = 1,
                             radius: CGFloat = 3) {
        self.layer.borderColor = (color ?? UIColor.black).cgColor
        self.layer.borderWidth = width
        self.layer.cornerRadius = radius
    }
}

@IBDesignable extension UIView {
    
    @IBInspectable var borderColor: UIColor? {
        set {
            if let n = newValue {
                layer.borderColor = n.cgColor
            } else {
                layer.borderColor = nil
            }
        }
        get {
            if let color = layer.borderColor {
                return UIColor(cgColor: color)
            }
            return nil
        }
    }
    
    @IBInspectable var borderWidth: CGFloat {
        set {
            layer.borderWidth = newValue
        }
        get {
            return layer.borderWidth
        }
    }
    
    @IBInspectable var cornerRadius: CGFloat {
        set {
            layer.cornerRadius = newValue
        }
        get {
            return layer.cornerRadius
        }
    }
    
    @IBInspectable var shadowOffset: CGSize {
        set {
            layer.shadowOffset = newValue
        }
        get {
            return layer.shadowOffset
        }
    }
    
    @IBInspectable var shadowColor: UIColor? {
        set {
            if let n = newValue {
                layer.shadowColor = n.cgColor
            } else {
                layer.shadowColor = nil
            }
        }
        get {
            if let c = layer.shadowColor {
                return UIColor(cgColor: c)
            }
            return nil
        }
    }
    
    @IBInspectable var shadowRadius: CGFloat {
        set {
            layer.shadowRadius = newValue
        }
        get {
            return layer.shadowRadius
        }
    }
    
    @IBInspectable var shadowOpacity: Float {
        set {
            layer.shadowOpacity = newValue
        }
        get {
            return layer.shadowOpacity
        }
    }
    
}
