
import Foundation

public typealias Dispatcher = Dispatch.DispatchQueue

public enum DispatchQueueType {
    case main, interactive, initiated, utility, background, custom(Dispatch.DispatchQueue)
}

private func getQueue(_ queueType: DispatchQueueType = .main) -> Dispatch.DispatchQueue {
    let queue: Dispatcher
    
    switch queueType {
    case .main:
        queue = Dispatcher.main
    case .interactive:
        queue = Dispatcher.global(qos: DispatchQoS.QoSClass.userInteractive)
    case .initiated:
        queue = Dispatcher.global(qos: DispatchQoS.QoSClass.userInitiated)
    case .utility:
        queue = Dispatcher.global(qos: DispatchQoS.QoSClass.utility)
    case .background:
        queue = Dispatcher.global(qos: DispatchQoS.QoSClass.background)
    case .custom(let userQueue):
        queue = userQueue
    }
    
    return queue
}

public func delay(_ delay: TimeInterval, queue queueType: DispatchQueueType = .main, closure: @escaping () -> Void) {
    getQueue(queueType).asyncAfter(
        deadline: DispatchTime.now() + Double(Int64(delay * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC),
        execute: closure
    )
}

public func dispatch(queue queueType: DispatchQueueType = .main, closure: @escaping () -> Void) {
    getQueue(queueType).async(execute: {
        closure()
    })
}
