//
//  Operator-Dictionary.swift
//  Pods
//
//  Created by Meniny on 2017-05-16.
//
//

import Foundation

/// append to dictionary
public func + <K, V> (lhs: [K: V], rhs: [K: V]) -> [K: V] {
    if rhs.keys.isEmpty {
        return lhs
    }
    var cp = lhs
    for (k, v) in rhs {
        cp[k] = v
    }
    return cp
}

/// append to dictionary
public func += <K, V> (lhs: inout [K: V], rhs: [K: V]) {
    for (k, v) in rhs {
        lhs[k] = v
    }
}

/// remove a key from dictionary
public func - <K, V> (lhs: [K: V], rhs: K) -> [K: V] {
    var cp = lhs
    cp.removeValue(forKey: rhs)
    return cp
}

/// remove a key from dictionary
public func -= <K, V> (lhs: inout [K: V], rhs: K) {
    _ = lhs.removeValue(forKey: rhs)
}

/// remove keys from dictionary
public func - <K, V> (lhs: [K: V], rhs: [K]) -> [K: V] {
    var cp = lhs
    for k in rhs {
        cp.removeValue(forKey: k)
    }
    return cp
}

/// remove keys from dictionary
public func -= <K, V> (lhs: inout [K: V], rhs: [K]) {
    for k in rhs {
        _ = lhs.removeValue(forKey: k)
    }
}

/// repeat dictionary, return a dictionary array
public func * <K, V> (lhs: [K: V], rhs: Int) -> [[K: V]] {
    if rhs <= 1 {
        return [lhs]
    }
    var res = [[K: V]]()
    for _ in 1...rhs {
        res.append(lhs)
    }
    return res
}
