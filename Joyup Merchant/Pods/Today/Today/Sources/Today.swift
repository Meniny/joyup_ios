//
//  Today.swift
//  Pods
//
//  Created by Meniny on 2017-08-01.
//
//

import Foundation

/*
public func > (lhs: Date, rhs: Date) -> Bool {
    return lhs.isEarlier(than: rhs)
}

public func >= (lhs: Date, rhs: Date) -> Bool {
    return lhs.isEarlierThanOrEqualTo(rhs)
}

public func < (lhs: Date, rhs: Date) -> Bool {
    return lhs.isLater(than: rhs)
}

public func <= (lhs: Date, rhs: Date) -> Bool {
    return lhs.isLaterThanOrEqualTo(rhs)
}

public func == (lhs: Date, rhs: Date) -> Bool {
    return lhs.isEqualTo(rhs)
}

public func != (lhs: Date, rhs: Date) -> Bool {
    return !lhs.isEqualTo(rhs)
}
*/

open class Today {
    
    open static var calendar: Calendar = Calendar.current
    
    open var calendar: Calendar
    public init(calendar c: Calendar) {
        calendar = c
    }
    
    // MARK: - Leap Year
    
    public class func checkLeapYear(_ year: Int) -> Bool {
        return (year % 400 == 0) || ((year % 4 == 0) && (year % 100 != 0))
    }
    
    public func checkLeapYear(_ year: Int) -> Bool {
        return Today.checkLeapYear(year)
    }
    
    // MARK: - Compare
    
    // Class
    
    public class func compare(date: Date, ifEarlier another: Date) -> Bool {
        return date.isEarlier(than: another)
    }
    
    public class func compare(date: Date, ifEarlierThanOrEqualTo another: Date) -> Bool {
        return date.isEarlierThanOrEqualTo(another)
    }
    
    public class func compare(date: Date, ifLater another: Date) -> Bool {
        return date.isLater(than: another)
    }
    
    public class func compare(date: Date, ifLaterThanOrEqualTo another: Date) -> Bool {
        return date.isLaterThanOrEqualTo(another)
    }
    
    public class func compare(date: Date, ifEqualTo another: Date) -> Bool {
        return date.isEqualTo(another)
    }
    
    public class func compare(date: Date, ifNotEqualTo another: Date) -> Bool {
        return !date.isEqualTo(another)
    }
    
    // Instance
    
    public func compare(date: Date, ifEarlier another: Date) -> Bool {
        return date.isEarlier(than: another)
    }
    
    public func compare(date: Date, ifEarlierThanOrEqualTo another: Date) -> Bool {
        return date.isEarlierThanOrEqualTo(another)
    }
    
    public func compare(date: Date, ifLater another: Date) -> Bool {
        return date.isLater(than: another)
    }
    
    public func compare(date: Date, ifLaterThanOrEqualTo another: Date) -> Bool {
        return date.isLaterThanOrEqualTo(another)
    }
    
    public func compare(date: Date, ifEqualTo another: Date) -> Bool {
        return date.isEqualTo(another)
    }
    
    public func compare(date: Date, ifNotEqualTo another: Date) -> Bool {
        return !date.isEqualTo(another)
    }
    
    // MARK: - Calculating
    
    public class func dateBy(_ calculating: TimeCalculatingOperation, _ value: Int, of type: TimePeriodSize, to date: Date) -> Date? {
        return Today.calendar.date(by: calculating, value, of: type, to:date)
    }
    
    public func dateBy(_ calculating: TimeCalculatingOperation, _ value: Int, of type: TimePeriodSize, to date: Date) -> Date? {
        return calendar.date(by: calculating, value, of: type, to:date)
    }
    
    // MARK: Adding components to dates
    public class func dateByAdding(_ value: Int, of type: TimePeriodSize, to date: Date) -> Date? {
        return Today.calendar.date(by: .adding, value, of: type, to: date)
    }
    
    public func dateByAdding(_ value: Int, of type: TimePeriodSize, to date: Date) -> Date? {
        return Today.calendar.date(by: .adding, value, of: type, to: date)
    }
    
    public class func dateByAdding(years: Int, to date: Date) -> Date? {
        return Today.calendar.date(byAdding: years, of: .year, to: date)
    }
    
    public class func dateByAdding(months: Int, to date: Date) -> Date? {
        return Today.calendar.date(byAdding: months, of: .month, to: date)
    }
    
    public class func dateByAdding(weeks: Int, to date: Date) -> Date? {
        return Today.calendar.date(byAdding: weeks, of: .week, to: date)
    }
    
    public class func dateByAdding(days: Int, to date: Date) -> Date? {
        return Today.calendar.date(byAdding: days, of: .day, to: date)
    }
    
    public class func dateByAdding(hours: Int, to date: Date) -> Date? {
        return Today.calendar.date(byAdding: hours, of: .hour, to: date)
    }
    
    public class func dateByAdding(minutes: Int, to date: Date) -> Date? {
        return Today.calendar.date(byAdding: minutes, of: .minute, to: date)
    }
    
    public class func dateByAdding(seconds: Int, to date: Date) -> Date? {
        return Today.calendar.date(byAdding: seconds, of: .second, to: date)
    }
    
    // MARK: Subtracting components from dates
    public class func dateBySubtracting(_ value: Int, of type: TimePeriodSize, to date: Date) -> Date? {
        return Today.calendar.date(by: .subtracting, value, of: type, to: date)
    }
    
    public func dateBySubtracting(_ value: Int, of type: TimePeriodSize, to date: Date) -> Date? {
        return calendar.date(by: .subtracting, value, of: type, to: date)
    }
    
    public class func dateBySubtracting(years: Int, from date: Date) -> Date? {
        return Today.calendar.date(bySubtracting: years, of: .year, to: date)
    }
    
    public class func dateBySubtracting(months: Int, from date: Date) -> Date? {
        return Today.calendar.date(bySubtracting: months, of: .month, to: date)
    }
    
    public class func dateBySubtracting(weeks: Int, from date: Date) -> Date? {
        return Today.calendar.date(bySubtracting: weeks, of: .week, to: date)
    }
    
    public class func dateBySubtracting(days: Int, from date: Date) -> Date? {
        return Today.calendar.date(bySubtracting: days, of: .day, to: date)
    }
    
    public class func dateBySubtracting(hours: Int, from date: Date) -> Date? {
        return Today.calendar.date(bySubtracting: hours, of: .hour, to: date)
    }
    
    public class func dateBySubtracting(minutes: Int, from date: Date) -> Date? {
        return Today.calendar.date(bySubtracting: minutes, of: .minute, to: date)
    }
    
    public class func dateBySubtracting(seconds: Int, from date: Date) -> Date? {
        return Today.calendar.date(bySubtracting: seconds, of: .second, to: date)
    }
    
    // MARK: - Counting components between dates
    
    public class func counting(_ expected: DateCountingUnit, from firstDate: Date, to secondDate: Date) -> Int? {
        return Today.calendar.counting(expected, from: firstDate, to: secondDate)
    }
    
    public class func years(from firstDate: Date, to secondDate: Date) -> Int? {
        return Today.calendar.counting(.years, from: firstDate, to: secondDate)
    }
    
    public class func months(from firstDate: Date, to secondDate: Date) -> Int? {
        return Today.calendar.counting(.months, from: firstDate, to: secondDate)
    }
    
    public class func weeks(from firstDate: Date, to secondDate: Date) -> Int? {
        return Today.calendar.counting(.weeks, from: firstDate, to: secondDate)
    }
    
    public class func days(from firstDate: Date, to secondDate: Date) -> Int? {
        return Today.calendar.counting(.days, from: firstDate, to: secondDate)
    }
}
