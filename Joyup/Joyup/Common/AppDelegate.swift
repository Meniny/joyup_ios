//
//  AppDelegate.swift
//  Joyup
//
//  Created by Meniny on 2017-07-12.
//  Copyright © 2017年 Meniny. All rights reserved.
//

import UIKit
import Fire
import Logify
import OhCrap
import CocoaHelper
//import SDWebImage
import Imagery
import EasyGCD
import Localization

public let kDefaultURL = "https://joyuponline.com/"

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, CAAnimationDelegate {
    
    var window: UIWindow?
    var splitController = UISplitViewController()
    var tabBar = TabBarController()
    //    var webView = WebViewController()
    /// 主要的四个控制器的导航栏
    static var controllers = [NavigationController]()
    
    // MARK: 启动完成
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        
        // MARK: PayPal
        JoyupPayment.initiatePayPalSDK()
        
        // MARK: OhCrap
        OhCrap.isEnabled = true
        OhCrap.delegate = self
        
        // 初始化 API baseURL
        Fire.API.initialize()
        
        // 初始化窗口, SB 中没有设置箭头
        self.window = UIWindow(frame: UIScreen.main.bounds)
        
        // 初始化四个主要控制器
        let home = HomeViewController()
        let homeNav = NavigationController(rootViewController: home)
        
        let categroies = CategoriesViewController()
        let categroiesNav = NavigationController(rootViewController: categroies)
        
        let cart = CartViewController()
        let cartNav = NavigationController(rootViewController: cart)
        
        let profile = ProfileViewController()
        let profileNav = NavigationController(rootViewController: profile)
        
//        let editor = BaseViewController()
//        let webNav = NavigationController(rootViewController: editor)
        
        home.tabBarItem = TabBarItem(
            JoyupTabBarContentView(),
            title: Localizable.Home.title,
            image: UIImage(named: "inedx"),//store"),
            selectedImage: nil
        )
        categroies.tabBarItem = TabBarItem(
            JoyupTabBarContentView(),
            title: Localizable.Categories.title,
            image: UIImage(named: "sort"),//categories"),
            selectedImage: nil
        )
        cart.tabBarItem = TabBarItem(
            JoyupTabBarContentView(),
            title: Localizable.Cart.title,
            image: UIImage(named: "shopping_cars"),//cart"),
            selectedImage: nil
        )
        profile.tabBarItem = TabBarItem(
            JoyupTabBarContentView(),
            title: Localizable.Profile.title,
            image: UIImage(named: "my"),//profile"),
            selectedImage: nil
        )
        
//        editor.tabBarItem = TabBarItem(
//            IrregularityContentView(),
//            title: " ",
//            image: UIImage(named: "Tabbar_Circle"),
//            selectedImage: UIImage(named: "Tabbar_Circle")
//        )
        // MARK: Tabbar background Image
        self.tabBar.tabBar.backgroundImage = UIImage(color: Configuration.mainTabBarBackgroundColor, size: self.tabBar.tabBar.bounds.size)
        
        self.tabBar.shouldHijackHandler = { (tab, vc, idx) in
            return (idx == 2 || idx == 3) && !JoyupUser.signedIn
        }

        self.tabBar.didHijackHandler = { (tab, vc, idx) in
            JoyupNotice.signIn({ }, onSignUp: { }, onCancel: { })
//                JoyupUser.showSignInPanel(to: self, completion: {
//                    self.request(forMore: false)
//                })
        }
        
        // 初始化iPad分栏控制器或TabBar控制器
//        if UIApplication.onPad {
//            self.splitController.preferredDisplayMode = .allVisible
//            self.splitController.viewControllers = [self.tabBar, webNav]
//            
//            let temp = UIViewController(nibName: nil, bundle: nil)
//            temp.tabBarItem = TabBarItem(IrregularityContentView(), image: UIImage(named: "Tabbar_Circle"), selectedImage: UIImage(named: "Tabbar_Circle"))
//            
//            AppDelegate.controllers.append(contentsOf: [homeNav, cartNav, NavigationController(rootViewController: temp), categroiesNav, profileNav])
//        } else {
            AppDelegate.controllers.append(contentsOf: [homeNav/*, webNav*/, categroiesNav, cartNav, profileNav])
//        }
        
        self.tabBar.viewControllers = AppDelegate.controllers
        
//        if UIApplication.onPad {
//            self.window!.rootViewController = self.splitController
//        } else {
            self.window!.rootViewController = self.tabBar
//        }
        
        self.window?.makeKeyAndVisible()
        
        // MARK: 注册通知
        //ApplePushNotification.register(categories: nil)
        
        Localization.startObserve(with: self, selector: #selector(localizationDidChange))
        
        NotificationCenter.default.addObserver(self, selector: #selector(updateCartItemCount), name: Joyup.updateCartItemCountNotification, object: nil)
        return true
    }
    
    deinit {
        Localization.stopObserve(with: self)
        NotificationCenter.default.removeObserver(self)
    }
    
    open func localizationDidChange() {
        for navigation in AppDelegate.controllers {
            var title: String = ""
            if navigation.viewControllers.first! is HomeViewController {
                title = Localizable.Home.title
            } else if navigation.viewControllers.first! is CategoriesViewController {
                title = Localizable.Categories.title
            } else if navigation.viewControllers.first! is CartViewController {
                title = Localizable.Cart.title
            } else if navigation.viewControllers.first! is ProfileViewController {
                title = Localizable.Profile.title
            } else {
                continue
            }
            (navigation.viewControllers.first!.tabBarItem as! TabBarItem).title = title
        }
    }
    
    func updateCartItemCount() {
        let count = CartManager.shared.products.count
        if count > 0 {
            tabBar.setBadgeValue("\(count)", atIndex: 2)
            addingAnimation()
        } else {
            tabBar.setBadgeValue(nil, atIndex: 2)
        }
        
    }
    
    var animationView: UIImageView?
    
    func addingAnimation() {
        guard let win = window else {
            return
        }
        
        animationView = UIImageView(image: #imageLiteral(resourceName: "shopping_cars").withRenderingMode(.alwaysTemplate))
        
        guard let animateView = animationView else {
            return
        }
        
        let animationViewLength: CGFloat = Joyup.bottomBarHeight * 0.5
        
        animateView.contentMode = .scaleAspectFit
        let startFrame = CGRect(x: win.bounds.size.width - 75 - animationViewLength * 0.5,
                                y: win.bounds.size.height - 49 - Joyup.bottomBarHeight,
                                width: animationViewLength,
                                height: animationViewLength)
        animateView.frame = startFrame
        
//        animationView?.frame = CGRect(origin: addingButton.superview!.convert(addingButton.frame.origin, to: view), size: addingButton.bounds.size)
        win.addSubview(animateView)
        
        let endPoint = CGPoint(x: win.bounds.width * 0.75 - win.bounds.width * 0.125,
                               y: win.bounds.height - 49 * 0.25)
        //        let endPoint = cartButton.superview!.convert(cartButton.center, to: view)
        
//        let passPoint = CGPoint(x: (animationView!.center.x - endPoint.x) / 2, y: endPoint.y - 100)
        let passPoint = CGPoint(x: endPoint.x + (startFrame.midX - endPoint.x) * 0.5,
                                y: startFrame.minY - Joyup.bottomBarHeight)
        
        
        animateView.tintColor = Configuration.mainColor
        
        animateView.layer.cartAnimation(from: animateView.center,
                                        pass: passPoint,
                                        to: endPoint,
                                        duration: 1,
                                        delegate: self)
    }
    
    func animationDidStop(_ anim: CAAnimation, finished flag: Bool) {
        animationView?.removeFromSuperview()
    }
    
    // MARK: 注册推送通知成功, 设置 token 并登录
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        ApplePushNotification.registerFailed = false
        ApplePushNotification.setToken(data: deviceToken)
    }
    
    // MARK: 注册推送通知失败
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        ApplePushNotification.registerFailed = true
    }
    
    func applicationDidReceiveMemoryWarning(_ application: UIApplication) {
        ImageryCache.clearMemoryCache()
    }
    
    func application(_ app: UIApplication, open url: URL, options: [UIApplicationOpenURLOptionsKey : Any] = [:]) -> Bool {
        if let host = url.host, ["safepay", "platformapi"].contains(host) {
            AlipaySDK.defaultService().processOrder(withPaymentResult: url, standbyCallback: { (result: [AnyHashable : Any]?) in
                Log.d(result)
                if let resultStatus = result?["resultStatus"] as? String, resultStatus == "9000"{
                    // TODO: if sign=“xxx”
                    JoyupNotice.showSuccess()
                    NotificationCenter.default.post(name: Joyup.alipayPaymentCallbackNotification, object: "9000")
                    
//                    Localizable.Generic.successTitle

                } else {
                    JoyupNotice.showError(result?["memo"] as? String)
                    NotificationCenter.default.post(name: Joyup.alipayPaymentCallbackNotification, object: result?["memo"])

                }
            })
        }
        return true
    }
}

extension AppDelegate: OhCrapDelegate {
    func ohCrapDidCatch(_ exception: NSException, forType type: OhCrap.CrashType) {
        let strings: [String] = [
            "Name" + exception.name.rawValue,
            "Reason" + (exception.reason ?? "N/A"),
            "CallStackSymbols" + exception.callStackSymbols.joined(separator: "\n"),
            "CallStackReturnAddresses" + exception.callStackReturnAddresses.stringsValue.joined(separator: "\n"),
            "UserInfo" + (exception.userInfo?.stringsValue.joined(separator: "\n") ?? "N/A")
        ]
        let result = strings.joined(separator: "\n###########################\n")
        let documentUrl = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
        let now = Date()
        let path = documentUrl.appendingPathComponent("OhCrap_\(now.timestamp)_\(now.formattedString("yyyyMMdd_HHmmssSSS")).log")
//        JoyupNotice.showError(result, title: nil, buttons: [Localizable.Generic.done]) { (notice, _) in
//            notice.hide()
            if (try? result.write(to: path, atomically: true, encoding: .utf8)) != nil {
                exit(0)
            } else {
                exit(0)
            }
//        }
    }
}
