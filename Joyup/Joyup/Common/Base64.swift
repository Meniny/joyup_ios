//
//  Base64.swift
//  Joyup
//
//  Created by Meniny on 2017-08-07.
//  Copyright © 2017年 Meniny. All rights reserved.
//

import Foundation
import SwiftyRSA
import CocoaHelper
import Logify

public func Encrypt(_ string: String, modulus: String, exponent: String) -> String? {
    if let modData = Data(base64Encoded: modulus),
        let expData = Data(base64Encoded: exponent),
        let combinedData = PublicKeyRSA.generatePublicKey(withModulus: modData, exponent: expData),
        let key = try? PublicKey(data: combinedData),
        let message = try? ClearMessage(string: string, using: .utf8),
        let result = try? message.encrypted(with: key, padding: .PKCS1) {
        return result.base64String
    }
    return nil
}
