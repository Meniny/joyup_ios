//
//  BaseTableViewController.swift
//  Joyup
//
//  Created by Meniny on 2017-05-11.
//  Copyright © 2017年 Meniny. All rights reserved.
//

import UIKit
import CocoaHelper
import SnapKit
import Fire
import Jsonify
//import SDWebImage
import EasyGCD
import ColaExpression
import Imager
import Oops
//import SCLAlertView
import Logify

open class BaseCollectionViewController: BaseViewController {

    
    override init() {
        self.layout = UICollectionViewFlowLayout()
        self.collectionView = UICollectionView(frame: CGRect.zero, collectionViewLayout: self.layout)
        super.init()
    }
    
    
    required public init?(coder aDecoder: NSCoder) {
        self.layout = UICollectionViewFlowLayout()
        self.collectionView = UICollectionView(frame: CGRect.zero, collectionViewLayout: self.layout)
        super.init(coder: aDecoder)
    }
    
    open var layout: UICollectionViewFlowLayout
    
    open var collectionView: UICollectionView
    
    open var collectionInset: UIEdgeInsets = UIEdgeInsets.zero
    
    override open func viewDidLoad() {
        super.viewDidLoad()
        self.view.addSubview(self.collectionView)
        
        self.collectionView.backgroundColor = UIColor.clear
        self.layout.scrollDirection = .vertical
        
        self.collectionView.snp.makeConstraints { [weak self] (make) in
            make.top.equalTo(self!.collectionInset.top)
            make.bottom.equalTo(self!.collectionInset.bottom)
            make.left.equalTo(self!.collectionInset.left)
            make.right.equalTo(self!.collectionInset.right)
        }
    }

    open override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
