//
//  BaseTableViewController.swift
//  Joyup
//
//  Created by Meniny on 2017-05-11.
//  Copyright © 2017年 Meniny. All rights reserved.
//

import UIKit
import CocoaHelper
import SnapKit
import Fire
import Jsonify
//import SDWebImage
import EasyGCD
import ColaExpression
import Imager
import Oops
//import SCLAlertView
import Logify
import UITableView_FDTemplateLayoutCell

open class BaseTableViewController: BaseViewController {
    
    open var tableView: BaseTableView! = BaseTableView()
    
    /// Set this before calling `super.viewDidLoad()`
    open var tableInset: UIEdgeInsets = UIEdgeInsets.zero
    
    public override func userDidSignedIn() {
        super.userDidSignedIn()
    }
    
    override open func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.addSubview(self.tableView)
        self.tableView.tableFooterView = UIView()
        
        self.tableView.snp.makeConstraints { [weak self] (make) in
            make.top.equalTo(self!.tableInset.top)
            make.bottom.equalTo(self!.tableInset.bottom)
            make.left.equalTo(self!.tableInset.left)
            make.right.equalTo(self!.tableInset.right)
        }
    }
}
