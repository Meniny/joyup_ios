//
//  PageViewController.swift
//  BeerCheers
//
//  Created by Meniny on 2017-05-09.
//  Copyright © 2017年 Meniny. All rights reserved.
//

import UIKit

open class PageViewController: UIPageViewController {

    init() {
        super.init(transitionStyle: .scroll, navigationOrientation: .horizontal, options: nil)
    }
    
    required public init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
