//
//  WebViewController.swift
//  Meniny
//
//  Created by Meniny on 2017-04-20.
//  Copyright © 2017年 Meniny. All rights reserved.
//

import UIKit
import WebKit
import SnapKit
import CocoaHelper

public class WebViewController: BaseViewController, WKNavigationDelegate {
    
    let kEstimatedProgress = "estimatedProgress";
    let kHomePage = "https://meniny.cn/";
    
    public var openInNewPage: Bool = false
    public var url: URL?
    public var hasDismissButton: Bool = false
    private var dismissItem: UIBarButtonItem?
    var webView: WKWebView = WKWebView()
    var progressView = UIProgressView(progressViewStyle: .default)
    let kProgressHeight = 4
    
    override init() {
        super.init()
        self.title = ""
    }
    
    init(url u: URL) {
        url = u
        super.init()
        self.title = ""
    }
    
    init(urlString s: String) {
        url = URL(string: s)
        super.init()
        self.title = ""
    }
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.title = ""
    }
    
    override public func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        self.extendedLayoutIncludesOpaqueBars = false
        self.edgesForExtendedLayout = []
        
        self.view.addSubview(self.progressView)
        self.progressView.snp.makeConstraints { (maker) in
            maker.top.equalTo(0)
            maker.left.equalTo(0)
            maker.right.equalTo(0)
            maker.height.equalTo(kProgressHeight)
        }
        
        self.view.addSubview(self.webView)
        self.webView.snp.makeConstraints { (maker) in
            maker.top.equalTo(self.progressView.snp.bottom)
            maker.left.equalTo(0)
            maker.right.equalTo(0)
            maker.bottom.equalTo(0)
        }
        self.webView.navigationDelegate = self
        
        self.webView.addObserver(self, forKeyPath: kEstimatedProgress, options: .new, context: nil)
        
        if let u = self.url {
            self.webView.load(URLRequest(url: u))
        } else {
            if let u = URL(string: kHomePage) {
                self.webView.load(URLRequest(url: u))
            }
        }
        
        let refreshItem = UIBarButtonItem(barButtonSystemItem: .refresh, target: self, action: #selector(refresh))
        let shareItem = UIBarButtonItem(barButtonSystemItem: .action, target: self, action: #selector(sharePage(_:)))
        self.navigationItem.rightBarButtonItems = [refreshItem, shareItem]
        
        if self.hasDismissButton {
            self.dismissItem = UIBarButtonItem(image: UIImage(named: "down-chevron")!, style: .done, target: self, action: #selector(dismissController))
            self.navigationItem.leftBarButtonItem = self.dismissItem
        }
        
        let agent = "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_4) AppleWebKit/603.1.30 (KHTML, like Gecko) Version/10.1 Safari/603.1.30"
        webView.customUserAgent = agent
    }
    
    func dismissController() {
        self.dismiss(animated: true, completion: nil)
    }
    
    open func openURL(_ u: URL, newPage: Bool = true, title: String? = nil) {
        self.loadViewIfNeeded()
        self.url = u
        if let top = self.navigationController?.topViewController {
            if self != top {
                if let topWeb = top as? WebViewController {
                    topWeb.openURL(u, newPage: newPage, title: title)
                    return
                }
            }
        }
        if let current = self.webView.url?.absoluteString {
            if current == "about:blank" || current == u.absoluteString {
                self.webView.load(URLRequest(url: u))
                return
            }
        }
        if newPage {
            if let nav = self.navigationController {
                let web = WebViewController()
                web.url = u
                web.title = title ?? u.absoluteString
                nav.pushViewController(web, animated: false)
            } else {
                self.webView.load(URLRequest(url: u))
            }
        } else {
            self.webView.load(URLRequest(url: u))
        }
    }
    
    func sharePage(_ sender: UIBarButtonItem) {
        if let url = self.webView.url {
            self.share(items: [url], barButtonItem: sender, completion: nil)
        } else {
            JoyupNotice.showError(Localizable.Generic.unknownError)
        }
    }
    
    func refresh() {
        let current = self.webView.url?.absoluteString
        if current != nil && current != "about:blank" {
            if let u = self.webView.url {
                self.webView.load(URLRequest(url: u))
            }
        } else {
            if let u = self.url {
                self.webView.load(URLRequest(url: u))
            }
        }
    }
    
    deinit {
        self.webView.removeObserver(self, forKeyPath: kEstimatedProgress)
    }
    
    override public func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if keyPath == kEstimatedProgress {
            let progress = (change?[NSKeyValueChangeKey.newKey] as! Float) * 100
            self.progressView.progress = progress
            if progress >= 100 {
                self.progressView.progress = 0
            }
        }
    }
    
    public func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction, decisionHandler: @escaping (WKNavigationActionPolicy) -> Void) {
        if navigationAction.navigationType == WKNavigationType.linkActivated {
            if let u = navigationAction.request.url {
                self.openURL(u, newPage: openInNewPage)
                decisionHandler(.cancel)
            } else {
                decisionHandler(.allow)
            }
        } else {
            decisionHandler(.allow)
        }
//        JJHUD.hide()
    }
    
    public func webView(_ webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation!) {
//        JJHUD.showLoading()
        self.progressView.progress = 0
    }
    
    public func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
//        if let url = webView.url?.absoluteString {
//            let version = UIDevice.current.systemVersion.replacingOccurrences(of: ".", with: "_")
//
//            let agent = "Mozilla/5.0 (iPhone; CPU iPhone OS \(version) like Mac OS X) AppleWebKit/602.4.6 (KHTML, like Gecko) Version/10.0 Mobile/14D27 Safari/602.1"
//            if url.contains("itunes.apple.com") {
//                webView.customUserAgent = agent.replacingOccurrences(of: "iPhone", with: "Meniny")
//            } else {
//                webView.customUserAgent = agent
//            }
//        }
        self.progressView.progress = 0
        self.title = webView.title// .stringByEvaluatingJavaScript(from: "document.title")
//        JJHUD.hide()
    }
    
    public func webView(_ webView: WKWebView, didFail navigation: WKNavigation!, withError error: Error) {
        self.progressView.progress = 0
//        MBProgressHUD.showError(error.localizedDescription)
        JoyupNotice.showError(error.localizedDescription)
    }
    
    override public func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
