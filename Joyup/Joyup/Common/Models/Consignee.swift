//
//  Consignee.swift
//  Joyup
//
//  Created by Meniny on 2017-08-01.
//  Copyright © 2017年 Meniny. All rights reserved.
//

import Foundation
import Jsonify

open class Consignee: Equatable {
    /// Returns a Boolean value indicating whether two values are equal.
    ///
    /// Equality is the inverse of inequality. For any values `a` and `b`,
    /// `a == b` implies that `a != b` is `false`.
    ///
    /// - Parameters:
    ///   - lhs: A value to compare.
    ///   - rhs: Another value to compare.
    public static func ==(lhs: Consignee, rhs: Consignee) -> Bool {
        return lhs.id == rhs.id && lhs.name == rhs.name && lhs.phone == rhs.phone && lhs.address == rhs.address &&
        lhs.zip == rhs.zip
    }

    open var id: Int?
    open var name: String
    open var phone: String
    open var zip: String?
    open var address: String
    open var state: String?
    open var city: String?
    open var areaID: Int?
    open var areaName: String?
    open var isDefault: Bool = false
    
    public init(_ consignee: String,
                phone ph: String,
                zip code: String? = nil,
                city ct: String? = nil,
                state st: String? = nil,
                address ads: String,
                areaID ai: Int? = nil) {
        name = consignee
        phone = ph
        zip = code
        address = ads
        areaID = ai
        state = st
        city = ct
    }
    
    public init(receiver json: Jsonify) {
        id = json["recId"].int
        phone = json["recPhone"].stringValue
        name = json["recConsignee"].string ?? json["recconsignee"].stringValue
        address = json["recAddress"].stringValue
        areaID = json["areaId"].int
        state = json["state"].string
        city = json["city"].string
        areaName = json["recAreaName"].string
        zip = json["zipCode"].string
        isDefault = json["isDefault"].boolValue
    }
}
