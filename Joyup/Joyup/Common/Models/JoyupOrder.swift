//
//  Order.swift
//  Joyup
//
//  Created by Meniny on 2017-07-13.
//  Copyright © 2017年 Meniny. All rights reserved.
//

import Foundation
import Jsonify

public class JoyupOrder {
    
    public class OrderItem {
        
        public var name: String
        public var productType: String
        public var productID: Int
        public var image: String
        public var sn: String
        public var quantity: Int
        public var price: Float
        
        init(json: Jsonify) {
            name = json["orderItemName"].stringValue
            productType = json["goodsType"].stringValue
            productID = json["productId"].intValue
            image = json["orderItemThumbnail"].stringValue
            sn = json["orderItemSn"].stringValue
            quantity = json["orderItemQuantity"].intValue
            price = json["orderItemPrice"].floatValue
        }
    }
    
    public var paymentMethod: PaymentMethod
    public var amount: Float
    public var pv: Int
    public var id: Int
    public var consignee: String
    public var sn: String
    public var createdDate: Date?
//    public var createdDate: String
    
    public var orderItems: [JoyupOrder.OrderItem]
    
    public enum Status: Int {
        case any = -2 // 任意, 所有, 服务器无此状态
        case unknown = -1 // 未知, 服务器无此状态
        case pendingPayment = 0 // 等待付款
        case pendingReview = 1 // 等待审核
        case pendingShipment = 2 // 等待发货
        case shipped = 3 // 已发货
        case received = 4 // 已收货
        case completed = 5 // 已完成
        case failed = 6 // 已失败
        case canceled = 7 // 已取消
        case denied = 8 // 已拒绝
        case suspendShipment = 9 // 暂缓发货
//        case pending // 等待处理(1)
//        case waitingForProcessing // 等待处理(2)
//        case waitingForApproval // 等待验证
//        case waitingForIDValidation // 等待身份证验证
//        case waitingForPayment // 等待支付数据确认
//        case processingPayment // 付款完成，等待处理
//        case inProcessingQueue // 等待工作人员上班处理
//        case processing // 处理中
//        case waitingForStock // 等待库存
//        case waitingForAllocation // 装箱备货中
//        case pickUpOrPosting // 拣获
//        case readyToPick // 装箱准备
//        case picking // 装箱中
//        case readyToShip // 装箱完毕，准备运出
//        case shippedOut // 运出
//        case rejected // 订单被拒绝
        
        public var localized: String {
            switch self {
            case .pendingPayment:
                return Localizable.MyOrders.pendingPayment
            case .pendingReview:
                return Localizable.MyOrders.pendingReview
            case .pendingShipment:
                return Localizable.MyOrders.pendingShipment
            case .shipped:
                return Localizable.MyOrders.shipped
            case .received:
                return Localizable.MyOrders.received
            case .completed:
                return Localizable.MyOrders.completed
            case .failed:
                return Localizable.MyOrders.failed
            case .canceled:
                return Localizable.MyOrders.canceled
            case .denied:
                return Localizable.MyOrders.denied
            case .suspendShipment:
                return Localizable.MyOrders.suspendShipment
            case .unknown:
                return Localizable.MyOrders.unknown
            case .any:
                return Localizable.Generic.all
//            case .pending:
//                return Localizable.MyOrders.pending
//            case .waitingForProcessing:
//                return Localizable.MyOrders.waitingForProcessing
//            case .waitingForApproval:
//                return Localizable.MyOrders.waitingForApproval
//            case .waitingForIDValidation:
//                return Localizable.MyOrders.waitingForIDValidation
//            case .waitingForPayment:
//                return Localizable.MyOrders.waitingForPayment
//            case .processingPayment:
//                return Localizable.MyOrders.processingPayment
//            case .inProcessingQueue:
//                return Localizable.MyOrders.inProcessingQueue
//            case .processing:
//                return Localizable.MyOrders.processing
//            case .waitingForStock:
//                return Localizable.MyOrders.waitingForStock
//            case .waitingForAllocation:
//                return Localizable.MyOrders.waitingForAllocation
//            case .pickUpOrPosting:
//                return Localizable.MyOrders.pickUpOrPosting
//            case .readyToPick:
//                return Localizable.MyOrders.readyToPick
//            case .picking:
//                return Localizable.MyOrders.picking
//            case .readyToShip:
//                return Localizable.MyOrders.readyToShip
//            case .shippedOut:
//                return Localizable.MyOrders.shippedOut
//            case .rejected:
//                return Localizable.MyOrders.rejected
            }
        }
        
        public static let all: [JoyupOrder.Status] = [
            .any,
            .pendingPayment,
            .pendingReview,
            .pendingShipment,
            .shipped,
            .received,
            .completed,
            .failed,
            .canceled,
            .denied,
            .suspendShipment
//            .pending,
//            .waitingForProcessing,
//            .waitingForApproval,
//            .waitingForIDValidation,
//            .waitingForPayment,
//            .processingPayment,
//            .inProcessingQueue,
//            .processing,
//            .waitingForStock,
//            .waitingForAllocation,
//            .pickUpOrPosting,
//            .readyToPick,
//            .picking,
//            .readyToShip,
//            .shippedOut,
//            .rejected
        ]
        
        public var key: String {
            switch self {
            case .pendingPayment:
                return "pendingPayment"
            case .pendingReview:
                return "pendingReview"
            case .pendingShipment:
                return "pendingShipment"
            case .shipped:
                return "shipped"
            case .received:
                return "received"
            case .completed:
                return "completed"
            case .failed:
                return "failed"
            case .canceled:
                return "canceled"
            case .denied:
                return "denied"
            case .suspendShipment:
                return "suspendShipment"
            case .unknown:
                return "unknown"
            case .any:
                return "all"
            }
        }
        
        public init(key: String) {
            switch key {
            case "pendingPayment":
                self = .pendingPayment
                break
            case "pendingReview":
                self = .pendingReview
                break
            case "pendingShipment":
                self = .pendingShipment
                break
            case "shipped":
                self = .shipped
                break
            case "received":
                self = .received
                break
            case "completed":
                self = .completed
                break
            case "failed":
                self = .failed
                break
            case "canceled":
                self = .canceled
                break
            case "denied":
                self = .denied
                break
            case "suspendShipment":
                self = .suspendShipment
                break
            case "all":
                self = .any
                break
            default:
                self = .unknown
                break
            }
        }
    }
    
    public var status: JoyupOrder.Status
    public var products: [JoyupProduct]
    
    public init(json: Jsonify) {
        amount = json["orderAmount"].floatValue
        pv = json["orderPvPoint"].intValue
        status = JoyupOrder.Status(key: json["orderStatus"].string ?? json["status"].stringValue)
        id = json["orderId"].intValue
        consignee = json["consignee"].stringValue
        sn = json["orderSn"].stringValue
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyyMMddHHmmss"
        createdDate = formatter.date(from: json["createDate"].stringValue)
        
        var pros: [JoyupProduct] = []
        for js in json["products"].arrayValue {
            pros.append(JoyupProduct(good: js))
        }
        products = pros
        
        var items: [JoyupOrder.OrderItem] = []
        for js in json["orderItems"].arrayValue {
            items.append(JoyupOrder.OrderItem(json: js))
        }
        orderItems = items
        
        paymentMethod = PaymentMethod(json: json["paymentMethod"])
        if let i = json["getPaymentMethodId"].int, let n = json["getPaymentMethodName"].string {
            paymentMethod.id = i
            paymentMethod.name = n
        }
    }
}
