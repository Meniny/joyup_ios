//
//  Product.swift
//  Joyup
//
//  Created by Meniny on 2017-07-13.
//  Copyright © 2017年 Meniny. All rights reserved.
//

import Foundation
import Jsonify

public enum ProductType: String {
    case kind = "kind"
    case fictitious = "fictitious"
}

public struct JoyupProduct: HomeListItem {
    public var name: String?

    public var url: String

    public var itemType: HomeListItemType = .products
    
    public struct Description {
        public var content: String
        public var images: [String] {
            var results = [String]()
            let matches = content.matches(pattern: "<img.+?/>", options: .dotMatchesLineSeparators)
            for img in matches {
                let i = img.stringByReplacingMatches(with: "(<img src=\"|\" style=\\\"\\\"/>|\"/>)")
                if i.hasPrefix("http") && !i.hasSuffix("\"") {
                    results.append(i)
                }
            }
            return results
        }
        
        public var attributedString: NSAttributedString {
            return attributedString(from: content)
        }
        
        public var attributedStringWithoutImages: NSAttributedString {
            let replaced = content.stringByReplacingMatches(with: "<img[^<>]+>")
            return attributedString(from: replaced)
        }
        
        public func attributedString(from: String) -> NSAttributedString {
            if let data = content.data(using: .unicode, allowLossyConversion: true),
                let result = try? NSAttributedString(data: data, options: [NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType], documentAttributes: nil) {
                return result
            }
            return NSAttributedString(string: content)
        }
    }
    
    public struct Review {
        public var content: String
    }
    
    public struct Consultations {
        public var content: String
    }
    
    public var id: String
    public var goodsID: String
    public var title: String
    public var price: Float
    public var images: [String] = []
    public var preview: String?
    public var image: String {
        set {
            
        }
        get {
            return images.first ?? (preview ?? "")
        }
    }
    
    public var quantity: Int = 1
    public var PVPoint: Int
    public var serialNumber: String
    public var desc: JoyupProduct.Description?
    public var reviews: [JoyupProduct.Review] = []
    public var consultations: [JoyupProduct.Consultations] = []
    
    public var checked: Bool = true
    public var bannerData: [BannerData] = []
    
    // Cart
    public var cartID: String?
    public var cartItemID: String?
    public var subtotal: Int?
    
    public var productType: ProductType = .kind
  
    public var productsStandards: [JoyupProduct] = []
    
    public class Specification {
        public var id: Int
        public var value: String
        
        public init(json: Jsonify) {
            id = json["id"].intValue
            value = json["value"].stringValue
        }
    }
    
    public var specifications: [Specification] = []
    
    public var domain: Domain?
    
    public init(good json: Jsonify) {
        goodsID = json["goodsId"].stringValue
        id = json["productId"].string ?? json["goodsId"].stringValue
        PVPoint = json["goodsPv"].intValue
        price = json["goodsPrice"].floatValue
        var imgs = [String]()
        for img in json["goodsImgs"].arrayValue {
            if let i = img["large"].string, !i.isEmpty {
                imgs.append(i)
            }
        }
        if imgs.isEmpty {
            for img in json["productImages"].arrayValue {
                if let i = img["thumbnail"].string, !i.isEmpty {
                    imgs.append(i)
                }
            }
        }
        url = json["goodsUrl"].stringValue
        preview = imgs.first ?? json["goodsImage"].string
        let d = JoyupProduct.Description(content: json["intruduction"].stringValue)
        desc = d
        imgs.append(contentsOf: d.images)
        images = imgs
        title = json["goodsName"].stringValue
        for i in images {
            bannerData.append(BannerData(id: id, title: title, image: i, url: url))
        }
        if bannerData.isNotEmpty {
            bannerData.append(BannerData(id: id, title: title, image: preview ?? (imgs.first ?? ""), url: url))
        }
        serialNumber = json["sn"].stringValue
        
        productType = ProductType(rawValue: json["productType"].stringValue) ?? .kind
        domain = Domain(rawValue: json["productDomain"].stringValue)
        
        for standard in json["products"].arrayValue {
            productsStandards.append(JoyupProduct(standard: standard))
        }
    }
    
    public init(standard json: Jsonify) {
        goodsID = json["goodsId"].stringValue
        id = json["productId"].stringValue
        PVPoint = json["productPv"].intValue
        price = json["productPrice"].floatValue
        url = json["productUrl"].stringValue
        images = []
        title = ""
        serialNumber = json["productSn"].stringValue
        
        for specification in json["specifications"].arrayValue {
            specifications.append(Specification(json: specification))
        }
        
        
    }
    
    public init(json: Jsonify) {
        if let cid = json["cartId"].int {
            cartID = "\(cid)"
        }
        if let citid = json["cartItemId"].int {
            cartItemID = "\(citid)"
        }
        subtotal = json["subtotal"].int
        
        if let i = json["productId"].int {
            id = "\(i)"
            goodsID = "\(i)"
        } else {
            id = ""
            goodsID = ""
        }
        url = json["url"].stringValue
        productType = ProductType(rawValue: json["productType"].stringValue) ?? .kind
        title = json["productName"].stringValue
        price = json["price"].floatValue
        if let thumbnail = json["thumbnail"].string {
            images.append(thumbnail)
        } else {
            for i in json["thumbnail"].arrayValue {
                images.append(i.stringValue)
            }
        }
        var data: [BannerData] = []
        for i in images {
            data.append(BannerData(id: id, title: title, image: i, url: json["url"].stringValue))
        }
        bannerData.append(contentsOf: data)
        quantity = json["quantity"].intValue
        PVPoint = json["pvPoint"].intValue
        serialNumber = json["sn"].stringValue
        let d = JoyupProduct.Description(content: json["intruduction"].stringValue)
        desc = d
        images.append(contentsOf: d.images)
        for js in json["reviews"].arrayValue {
            reviews.append(JoyupProduct.Review(content: js["content"].stringValue))
        }
        for js in json["consultations"].arrayValue {
            consultations.append(JoyupProduct.Consultations(content: js["content"].stringValue))
        }
        domain = Domain(rawValue: json["productDomain"].stringValue)
        
        for standard in json["products"].arrayValue {
            productsStandards.append(JoyupProduct(standard: standard))
        }
    }
}
