//
//  TableItem.swift
//  Joyup
//
//  Created by Meniny on 2017-07-13.
//  Copyright © 2017年 Meniny. All rights reserved.
//

import Foundation
import UIKit
import CocoaHelper
import Logify

public struct TableItem {
    public var title: String
    public var subtitle: String
    public var target: Any?
    public var action: Selector?
    public var closure: VoidClosure?
    public var selectionStyle: UITableViewCellSelectionStyle
    public var accessoryType: UITableViewCellAccessoryType
    
    init(_ t: String,
         subtitle s: String?,
         action a: Selector?,
         toTarget to: Any?,
         selectionStyle ss: UITableViewCellSelectionStyle = .none,
         accessoryType at: UITableViewCellAccessoryType = .none) {
        title = t
        subtitle = s ?? ""
        target = to
        action = a
        closure = nil
        selectionStyle = ss
        accessoryType = at
    }
    
    init(_ t: String,
         subtitle s: String?,
         closure c: VoidClosure?,
         selectionStyle ss: UITableViewCellSelectionStyle = .none,
         accessoryType at: UITableViewCellAccessoryType = .none) {
        title = t
        subtitle = s ?? ""
        target = nil
        action = nil
        closure = c
        selectionStyle = ss
        accessoryType = at
    }
    
    public func scheduleIfNeeded() {
        if let t = target, let a = action {
            let timer = Timer(timeInterval: 0, target: t, selector: a, userInfo: nil, repeats: false)
            timer.fire()
//            Thread.detachNewThreadSelector(a, toTarget: t, with: nil)
            return
        }
        if let c = closure {
            c()
        }
    }
}
