//
//  Alert.swift
//  Joyup
//
//  Created by Meniny on 2017-05-11.
//  Copyright © 2017年 Meniny. All rights reserved.
//

import Foundation
//import SCLAlertView
import Oops
import EasyGCD
import CocoaHelper

public let kOptionalButtonColor = UIColor(red:0.45, green:0.45, blue:0.46, alpha:1.00)
public let kOptionalButtonTextColor = UIColor.white

public let kSignButtonColor = Configuration.mainColor
public let kSignButtonTextColor = Configuration.mainTextColor


//open class LoadingActivityManager: NSObject {
//    
//    private static var loadingCount: Int = 0
//    
//    open class func loadingStarted() {
//        if loadingCount == 0 {
//            
//        }
//        loadingCount += 1
//    }
//    
//    open class func loadingFinished() {
//        if loadingCount > 0 {
//            loadingCount -= 1
//        }
//        if loadingCount == 0 {
//            
//        }
//    }
//}

public class JoyupNotice {
    
    public func hide() {
        alertView.hideView()
    }
    
    public let alertView: Oops
    
    public typealias IndexClosure = (_ notice: JoyupNotice, _ idx: Int) -> Swift.Void
    
    public static func debug(style: Oops.PopUpStyle,
                             message: String?,
                             title: String? = nil,
                             buttons: [String]? = nil,
                             action: JoyupNotice.IndexClosure? = nil) {
        #if DEBUG
        self.showNotice(message, title: title ?? "DEBUG", buttons: buttons, style: .notice, action: action)
        #endif
    }
    
    @discardableResult
    public static func showWarning(_ message: String?,
                                 title: String? = nil,
                                 buttons: [String]? = nil,
                                 action: JoyupNotice.IndexClosure? = nil) -> JoyupNotice {
        return self.showNotice(message, title: title ?? Localizable.Generic.warningTitle, buttons: buttons, style: .warning, action: action)
    }
    
    @discardableResult
    public static func showError(_ message: String? = nil,
                                 title: String? = nil,
                                 buttons: [String]? = nil,
                                 action: JoyupNotice.IndexClosure? = nil) -> JoyupNotice {
        return self.showNotice(message, title: title ?? Localizable.Generic.errorTitle, buttons: buttons, style: .error, action: action)
    }
    
    @discardableResult
    public static func showFireError(_ error: Error?,
                                     response: HTTPURLResponse?,
                                     action: JoyupNotice.IndexClosure? = nil) -> JoyupNotice {
        
        let title = response != nil ? "\(response!.statusCode)" : nil
        
        return self.showNotice(error?.localizedDescription ?? Localizable.Generic.networkError,
                               title: title ?? Localizable.Generic.errorTitle,
                               buttons: nil,
                               style: .error,
                               action: action)
    }
    
    @discardableResult
    public static func showMessage(_ message: String?,
                                   title: String? = nil,
                                   buttons: [String]? = nil,
                                   action: JoyupNotice.IndexClosure? = nil) -> JoyupNotice {
        return self.showNotice(message, title: title, buttons: buttons, style: .info, action: action)
    }
    
    @discardableResult
    public static func showSuccess(_ message: String? = nil,
                                   title: String? = nil,
                                   buttons: [String]? = nil,
                                   action: JoyupNotice.IndexClosure? = nil) -> JoyupNotice {
        return self.showNotice(message, title: title ?? Localizable.Generic.successTitle, buttons: buttons, style: .success, action: action)
    }
    
    @discardableResult
    public static func showLoading(_ message: String? = nil,
                                 title: String? = nil) -> JoyupNotice {
        let config = Oops.Configuration()
        config.shouldAutoDismiss = false
        config.showCloseButton = false
        let notice = JoyupNotice(alert: Oops(configuration: config))
        
        notice.alertView.show(.loading,
                              title: (title ?? Localizable.Generic.loadingTitle).localized,
                              detail: (message ?? Localizable.Generic.pleaseWait),
                              icon: nil,
                              color: kSignButtonColor,
                              buttonTitleColor: kSignButtonTextColor,
                              completeText: nil,
                              timeout: nil,
                              animation: .topToBottom)
        return notice
    }
    
    @discardableResult
    public static func showNotice(_ message: String?,
                                 title: String? = nil,
                                 buttons: [String]? = nil,
                                 icon: UIImage? = nil,
                                 style: Oops.PopUpStyle = .info,
                                 action: JoyupNotice.IndexClosure? = nil) -> JoyupNotice {
        return self.show(message, title: title, buttons: buttons ?? [], icon: icon, style: style, action: action)
    }
    
    @discardableResult
    fileprivate static func show(_ message: String?,
                                 title: String? = nil,
                                 buttons: [String] = [],
                                 icon: UIImage? = nil,
                                 style: Oops.PopUpStyle = .info,
                                 action: JoyupNotice.IndexClosure? = nil) -> JoyupNotice {
        let config = Oops.Configuration()
        config.showCloseButton = false//buttons.isEmpty
        config.shouldAutoDismiss = false//config.showCloseButton
        config.dynamicAnimatorActive = Joyup.increaseAnimation ? (style != .loading) : false
        let notice = JoyupNotice(alert: Oops(configuration: config))
        for t in buttons {
            notice.alertView.addButton(t.localized, action: {
                if let act = action {
                    if let idx = buttons.index(of: t) {
                        act(notice, idx)
                    }
                } else {
                    notice.hide()
                }
            })
        }
        if notice.alertView.buttons.isEmpty {
            notice.alertView.addButton(Localizable.Generic.done, action: {
                if let act = action {
                    act(notice, 0)
                } else {
                    notice.hide()
                }
            })
        }
        notice.alertView.show(style,
                              title: (title ?? style.rawValue.localized()).localized,
                              detail: (message ?? "").localized,
                              icon: icon,
                              color: nil,
                              buttonTitleColor: nil,
                              completeText: nil,
                              timeout: nil,
                              animation: .topToBottom)
        return notice
    }
    
    init(alert: Oops) {
        alertView = alert
    }
    
    init() {
        fatalError("Must use `init(alert:)`.")
    }
}

public extension JoyupNotice {
    
    @discardableResult
    public static func showConfirm(_ title: String,
                                   message: String?,
                                   leftButton: String?,
                                   leftAction: VoidClosure?,
                                   rightButton: String?,
                                   rightAction: VoidClosure?) -> JoyupNotice {
        let config = Oops.Configuration()
        config.showCloseButton = false
        config.buttonsLayout = .horizontal
        config.dynamicAnimatorActive = Joyup.increaseAnimation
        let notice = JoyupNotice(alert: Oops(configuration: config))
        notice.alertView.addButton(leftButton ?? Localizable.Generic.confirm, action: {
            main {
                leftAction?()
            }
        })
        
        notice.alertView.addButton(rightButton ?? Localizable.Generic.cancel, backgroundColor: kOptionalButtonColor, textColor: kOptionalButtonTextColor, showTimeout: nil) {
            main {
                rightAction?()
            }
        }
        
        notice.alertView.show(.question,
                              title: title.localized,
                              detail: message ?? "",
                              icon: nil,
                              color: kSignButtonColor,
                              buttonTitleColor: kSignButtonTextColor,
                              completeText: nil,
                              timeout: nil,
                              animation: .topToBottom)
        return notice
    }
    
    @discardableResult
    public static func showOptions(_ title: String,
                                   message: String?,
                                   optionsWithImage options: [(opt: String, img: UIImage?)],
                                   allowCancel: Bool = true,
                                   action: ((_ index: Int?, _ option: String?) -> Swift.Void)?) -> JoyupNotice {
        let config = Oops.Configuration()
        config.showCloseButton = false
        config.hideWhenBackgroundViewIsTapped = true
        config.shouldAutoDismiss = true
        config.dynamicAnimatorActive = Joyup.increaseAnimation
        
        let notice = JoyupNotice(alert: Oops(configuration: config))
        
        for i in 0..<options.count {
            let o = options[i]
            let btn = notice.alertView.addButton(o.opt, action: {
                main {
                    action?(i, o.opt)
                    notice.hide()
                }
            })
            btn.setImage(o.img, for: .normal)
        }
        
        if allowCancel {
            notice.alertView.addButton(Localizable.Generic.cancel, backgroundColor: kOptionalButtonColor, textColor: kOptionalButtonTextColor, showTimeout: nil) {
                main {
                    action?(nil, nil)
                    notice.hide()
                }
            }
        }
        
        notice.alertView.show(.question,
                              title: title.localized,
                              detail: message ?? "",
                              icon: nil,
                              color: kSignButtonColor,
                              buttonTitleColor: kSignButtonTextColor,
                              completeText: nil,
                              timeout: nil,
                              animation: .topToBottom)
        return notice
    }
    
    @discardableResult
    public static func showOptions(_ message: String?,
                                   title: String,
                                   options: [String],
                                   allowCancel: Bool = true,
                                   action: ((_ index: Int?, _ option: String?) -> Swift.Void)?) -> JoyupNotice {
        
        var opts = [(opt: String, img: UIImage?)]()
        for o in options {
            opts.append((opt: o, img: nil))
        }
        
        return showOptions(title,
                           message: message,
                           optionsWithImage: opts,
                           allowCancel: allowCancel,
                           action: action)
    }
    
    // MARK: - Editor
    
    public class FieldConfiguration {
        public var text: String?
        public var placeholder: String
        public var isSecurityInput: Bool
        public var keyboardType: UIKeyboardType
        
        public init(_ text: String? = nil, placeholder: String, secure: Bool = false, keyboard: UIKeyboardType = .default) {
            self.text = text
            self.placeholder = placeholder
            isSecurityInput = secure
            keyboardType = keyboard
        }
    }
    
    @discardableResult
    public static func showFields(_ message: String? = nil,
                                  title: String,
                                  fields: [FieldConfiguration],
                                  buttons: [String],
                                  allowCancel: Bool = true,
                                  action: ((_ index: Int?, _ oops: Oops) -> Swift.Void)?,
                                  cancel: ((_ oops: Oops) -> Swift.Void)? = nil) -> JoyupNotice {
        let config = Oops.Configuration()
        config.shouldAutoDismiss = false
        config.showCloseButton = false
        let notice = JoyupNotice(alert: Oops(configuration: config))
        
        for f in fields {
            let textField = notice.alertView.addTextField(f.placeholder, text: f.text, secure: f.isSecurityInput)
            textField.keyboardType = f.keyboardType
        }
        
        for b in buttons {
            notice.alertView.addButton(b, backgroundColor: kSignButtonColor, textColor: kSignButtonTextColor, showTimeout: nil) {
                action?(buttons.index(of: b), notice.alertView)
            }
        }
        
        if allowCancel {
            notice.alertView.addButton(Localizable.Generic.cancel, backgroundColor: kOptionalButtonColor, textColor: kOptionalButtonTextColor, showTimeout: nil) {
                cancel?(notice.alertView)
                notice.alertView.hideView()
            }            
        }
        notice.alertView.show(.editor,
                              title: title.localized(),
                              detail: message ?? "",
                              icon: nil,
                              color: kSignButtonColor,
                              buttonTitleColor: kSignButtonTextColor,
                              completeText: nil,
                              timeout: nil,
                              animation: .topToBottom)
        return notice
    }
}
