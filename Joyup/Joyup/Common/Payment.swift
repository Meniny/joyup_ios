//
//  Payment.swift
//  Joyup
//
//  Created by Meniny on 2017-07-13.
//  Copyright © 2017年 Meniny. All rights reserved.
//

import Foundation
import UIKit
import Logify
import Fire
import Jsonify
import CocoaHelper
import EasyGCD
import InfoPlist

public class JoyupPayment {
    
    public static let manager = JoyupPayment()
    
    // FIXME: AliPay App ID
    public static let AliPayAppID = ""
    public static let AliPayPublicKey = ""
    
    public enum Platform: String {
        case alipay = "AliPay"
        case payPal = "PayPal"
        
        public var name: String {
            return rawValue
        }
        
        public var plugin: String {
            if self == .alipay {
                return "alipayDirectPaymentPlugin"
            }
            return "paypalPaymentPlugin"
        }
    }
    
    @discardableResult
    public static func usePaypal(future amount: Float, controller: BaseViewController, delegate: PayPalFuturePaymentDelegate) -> Bool {
        let config = PayPalConfiguration.joyupConfiguration()
        if let paypalController = PayPalFuturePaymentViewController(configuration: config, delegate: delegate) {
            configNavigationController(paypalController)
            controller.present(paypalController, animated: true, completion: nil)
            return true
        }
        return false
    }
    
    @discardableResult
    public static func usePaypal(pay amount: Float, brief: String?, controller: BaseViewController, delegate: PayPalPaymentDelegate) -> Bool {
        let payment = PayPalPayment()
        payment.amount = NSDecimalNumber(string: amount.amountString)
        payment.currencyCode = Currency.USD.rawValue
        payment.shortDescription = brief ?? Localizable.MyOrders.PayPal.shortDescription
        payment.intent = PayPalPaymentIntent.sale
        
        if payment.processable {
            let config = PayPalConfiguration.joyupConfiguration()
            if let paypalController = PayPalPaymentViewController(payment: payment, configuration: config, delegate: delegate) {
                configNavigationController(paypalController)
                controller.present(paypalController, animated: true, completion: nil)
                return true
            }
        }
        return false
    }
    
    public static func useAlipay(sn: String, completion: @escaping (_ success: Bool, _ error: String?) -> Swift.Void) {
        guard sn.isNotEmpty, let scheme = InfoPlist.mainScheme else {
            completion(false, Localizable.Generic.unknownError)
            return
        }
        
        Request.getOrderString(sn).request(params: nil, completion: { (json, resp, _) in
            if let error = Request.checkResult(json) {
                completion(false, error)
            } else {
                if let signstr = json["data"]["signstr"].string {
                    let orderString = signstr//.URLEncodedString
                    AlipaySDK.defaultService().payOrder(orderString, fromScheme: scheme) { (result) in
                        Log.d(result)
                        if let resultStatus = result?["resultStatus"] as? String, resultStatus == "9000"{

                            completion(true, nil)
                            
                        } else {
                            completion(false, (result?["memo"] as? String) ?? Localizable.Generic.unknownError)
                        }
                    }
                } else {
                    completion(false, Localizable.Generic.unknownError)
                }
            }
        }) { (_, error) in
            completion(false, error.localizedDescription)
        }
        
//        String(format: "%@&sign=\"%@\"&sign_type=\"%@\"", orderSpec, signedString, "RSA")
        // partner="2088101568358171"&out_trade_no="0819145412-6177"&subject="测试"&body="测试测试"&total_fee="0.01"&notify_url="http://notify.msp.hk/notify.htm"&service="mobile.securitypay.pay"&payment_type="1"&_input_charset="utf-8"&it_b_pay="30m"&sign="lBBK%2F0w5LOajrMrji7DUgEqNjIhQbidR13GovA5r3TgIbNqv231yC1NksLdw%2Ba3JnfHXoXuet6XNNHtn7VE%2BeCoRO1O%2BR1KugLrQEZMtG5jmJIe2pbjm%2F3kb%2FuGkpG%2BwYQYI51%2BhA3YBbvZHVQBYveBqK%2Bh8mUyb7GM1HxWs9k4%3D"&sign_type="RSA"
    }
    
    private static func configNavigationController(_ navigationController: UINavigationController) {
        navigationController.navigationBar.tintColor = Joyup.mainColor
        navigationController.navigationBar.barTintColor = Joyup.mainColor
        let att = [
            NSForegroundColorAttributeName: Joyup.mainNavgationTintColor
        ]
        navigationController.navigationBar.titleTextAttributes = att
    }
    
    public static var PaypalMetadataID: String {
        return PayPalMobile.clientMetadataID()
    }
    
    public static let kPayPalSandboxClientID = "Abh5s2CxomtqS2Cdkx5lb4MXAet1tudQ68gdyjnbm2ARntibE25AMXosCuZlfqSXkwEPKuSRpMMNk3s5"
    public static let kPayPalProductionClientID = "AQEDnwFbbRbS6m8VX3m4vRNJUCvMB5ztZhj5mrsHi2KUfSfD0aoZSTIQwl6jlwtl2VDLMfTYX7Me8kHJ"
    
    #if DEBUG
    public static let kPayPalEnvironment = PayPalEnvironmentSandbox
    #else
    public static let kPayPalEnvironment = PayPalEnvironmentProduction
    #endif
    
    public static func initiatePayPalSDK() {
        let clientIds: [AnyHashable: Any] = [
            PayPalEnvironmentSandbox: kPayPalSandboxClientID,
            PayPalEnvironmentProduction: kPayPalProductionClientID,
        ]
        PayPalMobile.initializeWithClientIds(forEnvironments: clientIds)
    }
    
    public static func clientMetadataID() -> String {
        return PayPalMobile.clientMetadataID()
    }
}

public extension PayPalConfiguration {
    public static func joyupConfiguration() -> PayPalConfiguration {
        let payPalConfiguration: PayPalConfiguration = PayPalConfiguration()
        payPalConfiguration.acceptCreditCards = true
        payPalConfiguration.rememberUser = true
        payPalConfiguration.merchantName = "Joyup Online".lowercased()
        payPalConfiguration.merchantPrivacyPolicyURL = URL(string: "https://joyuponline.com/common/privacy.jhtml")
        payPalConfiguration.merchantUserAgreementURL = URL(string: "https://joyuponline.com/common/privacy.jhtml")
        payPalConfiguration.payPalShippingAddressOption = .both
        payPalConfiguration.languageOrLocale = NSLocale.preferredLanguages.first
        return payPalConfiguration
    }
}
