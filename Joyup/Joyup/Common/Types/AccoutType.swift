//
//  AccoutType.swift
//  Joyup
//
//  Created by Meniny on 2017-08-02.
//  Copyright © 2017年 Meniny. All rights reserved.
//

import Foundation

public enum AccoutType: Int {
    case member = 1
    case associate = 2
    case premiumassociate = 3
    
    public var localizedString: String {
        switch self {
        case .member:
            return Localizable.User.typeMember
        case .associate:
            return Localizable.User.typeAssociate
        default:
            return Localizable.User.typePremiumAssociate
        }
    }
}
