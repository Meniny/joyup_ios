//
//  Area.swift
//  Joyup
//
//  Created by Meniny on 2017-08-01.
//  Copyright © 2017年 Meniny. All rights reserved.
//

import Foundation
import Localization
import EnumCollection

public enum Subarea: Int, EnumCollection {
    case contiguousAmerica = 3228
    case greaterChina = 3231
    case hongkong = 3232
    case taiwan = 3236
    case singapore = 3237
    case malaysia = 3238
    case australia = 3235
    
    public var localizedName: String {
        switch self {
        case .contiguousAmerica:
            return Localizable.User.america
        case .greaterChina:
            return Localizable.User.china
        case .hongkong:
            return Localizable.User.hongkong
        case .taiwan:
            return Localizable.User.taiwan
        case .singapore:
            return Localizable.User.singapore
        case .malaysia:
            return Localizable.User.malaysia
        default:
            return Localizable.User.australia
        }
    }
    
    public init?(name: String) {
        switch name {
        case Localizable.User.america:
            self = .contiguousAmerica
            break
        case Localizable.User.china:
            self = .greaterChina
            break
        case Localizable.User.hongkong:
            self = .hongkong
            break
        case  Localizable.User.taiwan:
            self = .taiwan
            break
        case Localizable.User.singapore:
            self = .singapore
            break
        case Localizable.User.malaysia:
            self = .malaysia
            break
        case Localizable.User.australia:
            self = .australia
            break
        default:
            return nil
        }
    }
}

public enum Area: Int, EnumCollection {
    case america = 0
    case china = 1
    case australia = 2
    
    public var localizedName: String {
        switch self {
        case .america:
            return Localizable.User.america
        case .china:
            return Localizable.User.china
        default:
            return Localizable.User.australia
        }
    }
    
    public static var allNames: [String] {
        return allValues.map({ (a) -> String in
            return a.localizedName
        })
    }
    
    public var subarea: [Subarea] {
        switch self {
        case .china:
            return [.greaterChina, .hongkong, .taiwan, .singapore, .malaysia]
        case .america:
            return [.contiguousAmerica]
        default:
            return [.australia]
        }
    }
}

public extension Array where Element == Subarea {
    public var allNames: [String] {
        return map({ (s) -> String in
            return s .localizedName
        })
    }
}

