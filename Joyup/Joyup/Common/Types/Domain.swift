//
//  Domain.swift
//  Joyup
//
//  Created by Meniny on 2017-08-07.
//  Copyright © 2017年 Meniny. All rights reserved.
//

import Foundation
import UIKit
import Localization

public enum Domain: String {
    case americaEnglish = "uen"
    case americaChinese = "ucn"
    case chinaChinese = "cn"
    
    public var localizedName: String {
        switch self {
        case .americaChinese:
            return Localizable.User.americaChinese
        case .chinaChinese:
            return Localizable.User.chinaChinese
        default:
            return Localizable.User.americaEnglish
        }
    }
    
    public var image: UIImage {
        switch self {
        case .americaEnglish:
            return #imageLiteral(resourceName: "uen").withRenderingMode(.alwaysOriginal)
        case .chinaChinese:
            return #imageLiteral(resourceName: "cn").withRenderingMode(.alwaysOriginal)
        default:
            return #imageLiteral(resourceName: "ucn").withRenderingMode(.alwaysOriginal)
        }
    }
    
    public var language: LocalizationLanguage {
        switch self {
        case .americaEnglish:
            return .english
        default:
            return .simplifiedChinese
        }
    }
    
    public var area: Area {
        switch self {
        case .americaEnglish:
            return .america
        case .chinaChinese:
            return .china
        default:
            return .america
        }
    }
    
    public var regionNotice: String {
        return String(format: "RegionNoticeFormat".localized(), self.area.localizedName)
    }
}


