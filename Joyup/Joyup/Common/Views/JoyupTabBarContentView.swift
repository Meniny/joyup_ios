//
//  JoyupTabBarContentView.swift
//  Joyup
//
//  Created by Meniny on 2017-07-19.
//  Copyright © 2017年 Meniny. All rights reserved.
//

import Foundation
import UIKit

public class JoyupTabBarContentView: BouncesContentView {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        tintColor = Configuration.mainColor//mainTintColor
        
        textColor = Configuration.mainTabBarNormalColor
        highlightTextColor = Configuration.mainTabBarHighlightColor
        
        iconColor = Configuration.mainTabBarNormalColor
        highlightIconColor = Configuration.mainTabBarHighlightColor
        
        backdropColor = UIColor.clear
        highlightBackdropColor = backdropColor
        
        backgroundColor = UIColor.clear
        badgeColor = Configuration.badgeColor
    }
    
    public required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
