//
//  IrregularityContentView.swift
//  TabBarControllerExample
//
//  Created by Meniny on 2016/8/9.
//  Copyright © 2017年 Meniny. All rights reserved.
//

import UIKit

open class IrregularityContentView: TabBarItemContentView {

    override init(frame: CGRect) {
        super.init(frame: frame)
        self.renderingMode = .alwaysOriginal
        
        self.imageView.backgroundColor = Configuration.mainColor
        self.imageView.contentMode = .scaleAspectFit
        self.imageView.image = UIImage(named: "Tabbar_Circle")?.withRenderingMode(.alwaysOriginal)
        self.imageView.layer.cornerRadius = 35
        self.superview?.bringSubview(toFront: self)
        
        textColor = UIColor.clear
        highlightTextColor = UIColor.clear
        iconColor = Configuration.mainColor
        highlightIconColor = iconColor
        backdropColor = Configuration.mainColor
        highlightBackdropColor = backgroundColor!
    }
    
    public required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override open func point(inside point: CGPoint, with event: UIEvent?) -> Bool {
        let p = CGPoint.init(x: point.x - imageView.frame.origin.x, y: point.y - imageView.frame.origin.y)
        return sqrt(pow(imageView.bounds.size.width / 2.0 - p.x, 2) + pow(imageView.bounds.size.height / 2.0 - p.y, 2)) < imageView.bounds.size.width / 2.0
    }
    
    open override func selectAnimation(animated: Bool, completion: (() -> ())?) {
        let view = UIView.init(frame: CGRect.init(origin: CGPoint.zero, size: CGSize(width: 2.0, height: 2.0)))
        view.layer.cornerRadius = 1.0
        view.layer.opacity = 0.5
        view.backgroundColor = Configuration.mainColor//UIColor.init(red: 10/255.0, green: 66/255.0, blue: 91/255.0, alpha: 1.0)
        self.addSubview(view)
    }
    
    open override func reselectAnimation(animated: Bool, completion: (() -> ())?) {
        completion?()
    }
    
    open override func deselectAnimation(animated: Bool, completion: (() -> ())?) {
        completion?()
    }
    
    open override func highlightAnimation(animated: Bool, completion: (() -> ())?) {
        UIView.beginAnimations("small", context: nil)
        UIView.setAnimationDuration(0.2)
        let transform = self.imageView.transform.scaledBy(x: 0.8, y: 0.8)
        self.imageView.transform = transform
        UIView.commitAnimations()
        completion?()
    }
    
    open override func dehighlightAnimation(animated: Bool, completion: (() -> ())?) {
        UIView.beginAnimations("big", context: nil)
        UIView.setAnimationDuration(0.2)
        let transform = CGAffineTransform.identity
        self.imageView.transform = transform
        UIView.commitAnimations()
        completion?()
    }
}
