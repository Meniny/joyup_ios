//
//  API.swift
//  Joyup
//
//  Created by Meniny on 2017-05-09.
//  Copyright © 2017年 Meniny. All rights reserved.
//

import Foundation
import Fire
import Jsonify
import CocoaHelper
import Logify
import NAIManager
import InfoPlist

public typealias Request = Fire.API
public typealias Status = Fire.ResponseStatus
public typealias Method = Fire.HTTPMethod

// MARK: -  Joyup ID
public let JoyupAppSecretMD5 = ""
public let JoyupAppID = "com.joyuponline.Joyup"

public let ReturnCodeKey = "retCode"
public let ErrorCodeKey = "errCode"

public enum RequestError: Error {
    case unknown
    case wrongStatusCode
    case noError
    
    public var localizedDescription: String {
        switch self {
        case .unknown:
            return "Unknown Error"
        case .wrongStatusCode:
            return "Wrong Status Code"
        default:
            return "Everything is fine"
        }
    }
}

public typealias RequestCompletion = ((_ json: Jsonify, _ response: HTTPURLResponse?, _ error: RequestError) -> Swift.Void)
public typealias RequestErrorClosure = FireErrorCallback

public extension Request {
    // TODO: Result Code
    public enum Result: Int {
        case success = 0
        case failed = 1
    }
    
    /// Check response codes
    ///
    /// - Parameter json: Jsonify data
    /// - Returns: Error Message, `nil` if there is no error
    public static func checkResult(_ json: Jsonify) -> String? {
        var noCodeKey = json[ErrorCodeKey].intValue == 0
        if !noCodeKey {
            return json["msg"].string ?? Localizable.Generic.unknownError
        }
        if let retCode = json[ReturnCodeKey].int {
            if retCode == 0 {
                return nil
            }
        } else {
            noCodeKey = true
        }
        if let msg = json["msg"].string {
            return msg
        } else {
            if noCodeKey {
                return Localizable.Generic.networkError
            }
        }
        return nil
    }
    
    public typealias Params = Fire.Params
    
    public static let baseURLStoredKey = "kJoyupBaseURLStoredKey"
    
    public static let DEBUGBaseURL = "https://joyuponline.co/api/"
    public static let RELEASEBaseURL = "https://joyuponline.com/api/"
    
    public static func initialize() {
        #if DEBUG
            self.baseURL = UserDefaults.standard.string(forKey: Request.baseURLStoredKey) ?? DEBUGBaseURL
        #else
            self.baseURL = RELEASEBaseURL
        #endif
    }
    
//    public var description: String {
//        return "<API \(self.method.rawValue) \(self.fullURL) \(self.successCode)(\(self.successCode.rawValue))\nAPI>"
//    }
    
    /// 发送网络请求
    ///
    /// - Parameters:
    ///   - params: 参数列表, 可空
    ///   - headers: HTTP 请求头, 可空
    ///   - callBackQueue: 执行回调闭包的 DispatchQueue
    ///   - completion: 请求完成回调闭包, 可空, 在主线程异步执行
    ///   - error: 请求错误回调闭包, 可空, 在主线程异步执行
    public func request(params: Fire.Params?,
                        headers: Fire.HeaderFields? = nil,
                        callBackQueue queue: DispatchQueue = DispatchQueue.main,
                        completion: RequestCompletion?,
                        error: RequestErrorClosure?) {
        
        var paramsString = ""
        var allParams: Fire.Params = params ?? [:]
        
        if let p = params {
            let keys = p.keys.sorted()
            paramsString.append(keys.reduce("") { (pre, cur) -> String in
                return "\(pre)\(cur)\(p[cur]!)"
            })
        }
        
        if let p = defaultParameters {
            let keys = p.keys.sorted()
            paramsString.append(keys.reduce("") { (pre, cur) -> String in
                return "\(pre)\(cur)\(p[cur]!)"
            })
        }
        
        let sign = "\(JoyupAppSecretMD5)\(paramsString)\(JoyupAppSecretMD5)".md5String.uppercased()
        
        allParams["timestamp"] = Date().timestampString
        allParams["appid"] = UUID().uuidString //JoyupAppID
        allParams["v"] = "\(InfoPlist.version).\(InfoPlist.build)"
        allParams["sign"] = sign
        allParams["domain"] = Joyup.domain().rawValue
        
        var newHeader: Fire.HeaderFields = headers ?? [:]
        if let tk = JoyupUser.main?.token {
            newHeader["token"] = tk
        }
        
        NAIManager.operationStarted()
        // FireDefaults.defaultTimeout
        self.requestJSON(params: allParams, headers: newHeader, timeout: 30, dispatch: .asynchronously, callback: { (json, resp) in
            if let cp = completion {
                queue.async {
                    if resp?.statusCode == self.successCode.rawValue {
                        cp(json, resp, RequestError.noError)
                    } else {
                        cp(json, resp, RequestError.wrongStatusCode)
                    }
                }
            }
            // AppToken 失效
            if json["errCode"].stringValue == "152" {
                JoyupUser.signOutWithoutRequest()
            }
            NAIManager.operationFinished()
        }) { (r, e) in
            if let eb = error {
                queue.async {
                    eb(r, e)
                }
            }
            NAIManager.operationFinished()
        }
    }
}

public extension Request {
    /// 用户登录
    public static func signIn(_ username: String, _ enPassword: String) -> Request {
        let params: Request.Params = [
            "username": username,
            "enPassword": enPassword
        ]
        return Request(appending: "userLogin/login.jhtml", HTTPMethod: .POST, params: params, successCode: .created)
    }
    
    /// 检查用户名是否被禁用或已存在
    public static func checkUsername(_ username: String) -> Request {
        let params: Request.Params = [
            "username": username
            ]
        return Request(appending: "register/check_username.jhtml", HTTPMethod: .GET, params: params, successCode: .success)
    }
    
    /// 检查E-mail是否存在
    public static func checkEmail(_ email: String) -> Request {
        let params: Request.Params = [
            "email": email
        ]
        return Request(appending: "register/check_email.jhtml", HTTPMethod: .GET, params: params, successCode: .success)
    }
    
    /// 检查referer是否存在，并且是激活的
    /// 检查安置人是否被禁用或已存在，安置人可以为空，如果为空则返回true
    public static func checkRefererAndPlacement(_ referer: String, _ placement: String) -> Request {
        let params: Request.Params = [
            "referer": referer,
            "placement": placement
        ]
        return Request(appending: "register/check_referer_placement.jhtml", HTTPMethod: .GET, params: params, successCode: .success)
    }
    
    /// 注册会员
    public static var signUp = Request(appending: "register/submit.jhtml", HTTPMethod: .POST, successCode: .created)
    
    /// 登出
    public static var signOut = Request(appending: "userLogin/logout.jhtml", HTTPMethod: .GET, successCode: .success)
    
    /// 获取用户信息
    public static var userInfo = Request(appending: "memberCenter/userInfo.jhtml", HTTPMethod: .GET, successCode: .success)
    
    /// 主页
    public static var home = Request(appending: "index/index.jhtml", HTTPMethod: .GET, successCode: .success)
    
    /// 添加地址
    public static func addingConsignee(_ consignee: Consignee) -> Request {
        let params: Request.Params = [
            "consignee": consignee.name,
            "areaId": consignee.areaID ?? 0,
            "state": consignee.state ?? "",
            "city": consignee.city ?? "",
            "address": consignee.address,
            "zipCode": consignee.zip ?? "",
            "phone": consignee.phone,
            "isDefault": consignee.isDefault,
        ]
        return Request(appending: "member/receiver/save.jhtml", HTTPMethod: .GET, params: params, successCode: .success)
    }
    
    public static var consigneeList = Request(appending: "member/receiver/list.jhtml", HTTPMethod: .GET, successCode: .success)
    
    /// 购物车
    public static var cart = Request(appending: "cart/cartList.jhtml", HTTPMethod: .GET, successCode: .success)
    
    /// 购物车结算
    public static var orderCheckout = Request(appending: "order/checkout.jhtml", HTTPMethod: .GET, successCode: .success)
    
    /// 计算费用
    public static func orderCalculate(_ receiverID: Int, _ payment: Int, _ shipping: Int, _ balance: Float) -> Request {
        let params: Request.Params = [
            "receiverId": receiverID,
            "paymentMethodId": payment,
            "shippingMethodId": shipping,
            "balance": balance.amountString
        ]
        return Request(appending: "order/calculate.jhtml", HTTPMethod: .GET, params: params, successCode: .success)
    }
    
    /// 创建订单
    public static func orderCreate(_ cartToken: String, _ receiverID: Int, _ payment: Int, _ shipping: Int, _ balance: Float) -> Request {
        let params: Request.Params = [
            "cartToken": cartToken,
            "receiverId": receiverID,
            "paymentMethodId": payment,
            "shippingMethodId": shipping,
            "balance": balance.amountString
        ]
        return Request(appending: "order/create.jhtml", HTTPMethod: .GET, params: params, successCode: .success)
    }
    
    /// 订单列表
    public static func orderList(_ status: JoyupOrder.Status) -> Request {
        return Request(appending: "memberCenter/myOrderListByStatus.jhtml",
                       HTTPMethod: .GET,
                       params: status == .any ? nil : ["status": status.key],
                       successCode: .success)
    }
    
    /// 订单详情
    public static func orderDetail(_ sn: String) -> Request {
        let params: Request.Params = [
            "sn": sn,
        ]
        return Request(appending: "member/order/view.jhtml", HTTPMethod: .GET, params: params, successCode: .success)
    }
    
    /// 加入购物车
    public static func addToCart(_ productID: String, _ quantity: Int) -> Request {
        let params: Request.Params = [
            "productId": productID,
            "quantity": "\(quantity)",
        ]
        return Request(appending: "cart/add.jhtml", HTTPMethod: .GET, params: params, successCode: .success)
    }
    
    /// 编辑购物车
    public static func editCart(_ cartItemID: String, _ quantity: Int) -> Request {
        let params: Request.Params = [
            "id": cartItemID,
            "quantity": quantity
        ]
        return Request(appending: "cart/edit.jhtml", HTTPMethod: .GET, params: params, successCode: .success)
    }
    
    /// 从购物车移除
    public static func removeFromCart(_ cartItemID: String) -> Request {
        let params: Request.Params = [
            "id": cartItemID
        ]
        return Request(appending: "cart/delete.jhtml", HTTPMethod: .GET, params: params, successCode: .success)
    }
    
    /// 返回我的收藏列表
    public static var collections = Request(appending: "member/favorite/list.jhtml", HTTPMethod: .GET, successCode: .success)

    /// 删除收藏商品 goodsId
    public static var collectionDeletion = Request(appending: "member/favorite/delete.jhtml", HTTPMethod: .GET, successCode: .success)
    /// 添加收藏商品 goodsId
    public static var collectionAddition = Request(appending: "member/favorite/add.jhtml", HTTPMethod: .GET, successCode: .success)
    
    /// 获取分类列表和子分类列表
    public static var categories = Request(appending: "product_category/menuList.jhtml", HTTPMethod: .GET, successCode: .success)
    
    /// 根据分类ID返回产品列表
    public static func products(_ categoriesID: String) -> Request {
        return Request(appending: "product_category/goodsListByCategory.jhtml", HTTPMethod: .GET, params: ["proCategoryId": categoriesID], successCode: .success)
    }
    
    /// 商品详情
    public static func productDetail(_ productID: String, _ fromOrder: Bool) -> Request {
        let params = fromOrder ? ["productId": productID] : ["goodsId": productID]
        return Request(appending: "goods/goodsContent.jhtml", HTTPMethod: .GET, params: params, successCode: .success)
    }
    
    /// 会员中心
    public static var memberCenter = Request(appending: "memberCenter/index.jhtml", HTTPMethod: .GET, successCode: .success)
    
    /// 我的钱包
    public static var wallet = Request(appending: "member/wallet/index.jhtml", HTTPMethod: .GET, successCode: .success)

    /// OrderString
    public static func orderString(_ outtradeno: String) -> Request {
        let params: Request.Params = [
            "APP_ID": JoyupPayment.AliPayAppID,
            "outtradeno": outtradeno,
            "CHARSET": "utf8",
            "ALIPAY_PUBLIC_KEY": JoyupPayment.AliPayPublicKey,
            "APP_PRIVATE_KEY": ""
        ]
        return Request(appending: "order/crateOrderString.jhtml", HTTPMethod: .GET, params: params, successCode: .success)
    }
    
    /// 支付结束通知服务端
    public static var orderFinishNotify = Request(appending: "order/orderStringNotify.jhtml", HTTPMethod: .POST, successCode: .created)
    
    public static func orderSubmit(_ plugin: JoyupPayment.Platform, _ sn: String, _ amount: Float) -> Request {
        let params: Request.Params = [
            "type": "payment",
            "paymentPluginId": plugin.plugin,
            "sn": sn,
            "amount": "\(amount.amountString)",
        ]
        return Request(appending: "payment/plugin_submit.jhtml", HTTPMethod: .GET, params: params, successCode: .success)
    }
    
    public static func paymentPluginNotify(_ plugin: JoyupPayment.Platform, _ sn: String) -> Request {
        let params: Request.Params = [
            "pluginId": plugin.plugin,
            "sn": sn,
            ]
        return Request(appending: "payment/plugin_notify.jhtml", HTTPMethod: .GET, params: params, successCode: .success)
    }
    
    public static func forgottenPassword(_ uname: String, _ email: String) -> Request {
        let params: Request.Params = [
            "username": uname,
            "email": email
        ]
        return Request(appending: "password/find.jhtml", HTTPMethod: .GET, params: params, successCode: .success)
    }
    
    public static func searchProduct(_ keyword: String) -> Request {
        let params: Request.Params = [
            "keyword": keyword,
        ]
        return Request(appending: "goods/search.jhtml", HTTPMethod: .GET, params: params, successCode: .success)
    }
    
    public static var getRSAPublicKey = Request(appending: "common/public_key.jhtml", HTTPMethod: .GET, successCode: .success)

    public static func getOrderString(_ sn: String) -> Request {
        let params: Request.Params = [
            "sn": sn,
        ]
        return Request(appending: "payment/alipay_sign.jhtml", HTTPMethod: .GET, params: params, successCode: .success)
    }
    
    public static func transferToMerchant(_ receiver: String, _ amount: Float, memo: String?) -> Request {
        var params: Request.Params = [
            "receiver": receiver,
            "amount": amount.amountString
            ]
        if let memo = memo {
            params["memo"] = memo
        }
        return Request(appending: "memberTransfer/toMerchant.jhtml", HTTPMethod: .GET, params: params, successCode: .success)
    }
    
    public static var transferRecords = Request(appending: "memberTransfer/transRecord.jhtml", HTTPMethod: .GET, successCode: .success)
    
    /// 检查新版本
    public static var checkVersion: Request {
        let params: Request.Params = [
            "currentVersion": InfoPlist.version + "." + InfoPlist.build,
            "bundleId": InfoPlist.bundleIndentifier,
            "userType": "1"
            // 1 用户, 2 商户
        ]
        return Request(appending: "versionUpgrade/check_version_upgrade.jhtml", HTTPMethod: .GET, params: params, successCode: .success)
    }
}
