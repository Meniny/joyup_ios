//
//  Notice.swift
//  Joyup
//
//  Created by Meniny on 2017-07-31.
//  Copyright © 2017年 Meniny. All rights reserved.
//

import Foundation
import UIKit
import Oops
import CocoaHelper
import EasyGCD

public extension JoyupNotice {
    
    @discardableResult
    public static func showFireError(_ response: HTTPURLResponse?, error: String?, action: JoyupNotice.IndexClosure? = nil) -> JoyupNotice {
        let status = response != nil ? " (\(response!.statusCode))" : ""
        
        return showError(error ?? Localizable.Generic.networkError,
                         title: Localizable.Generic.errorTitle + status,
                         buttons: nil,
                         action: action)
    }
    
    @discardableResult
    public static func checkingSignUpInfo() -> JoyupNotice {
        return showLoading("checking... please wait.", title: "Checking")
    }
    
    @discardableResult
    public static func signUpInfoError(_ error: String,
                                       nextStep: JoyupNotice.SignUpStep,
                                       onAction: @escaping (_ nextStep: JoyupNotice.SignUpStep) -> Swift.Void,
                                       onCancel: @escaping VoidClosure) -> JoyupNotice {
        return showError(error, title: nil, buttons: [Localizable.Generic.done], action: { (notice, index) in
            notice.hide()
            JoyupNotice.signUp(nextStep, onAction: onAction, onCancel: onCancel)
        })
    }
    
    // MARK: - Loading
    @discardableResult
    public static func signInLoadingWithRequest(_ uname: String,
                                                password: String,
                                                completion: @escaping (_ success: Bool, _ message: String?) -> Swift.Void) -> JoyupNotice {
        let config = Oops.Configuration()
        config.shouldAutoDismiss = false
        config.showCloseButton = false
        let notice = JoyupNotice(alert: Oops(configuration: config))
        notice.alertView.show(.loading,
                              title: Localizable.User.signInLoadingTitle,
                              detail: Localizable.User.signInLoading,
                              icon: nil,
                              color: kSignButtonColor,
                              buttonTitleColor: kSignButtonTextColor,
                              completeText: nil,
                              timeout: nil,
                              animation: .topToBottom)
        Request.getRSAPublicKey.request(params: nil, completion: { (json, _, _) in
            
            if let modulus = json["modulus"].string, let exponent = json["exponent"].string {
                
                JoyupUser.signIn(uname, password: password, modulus: modulus, exponent: exponent, completion: { (s, msg) in
                    notice.alertView.hideView()
                    completion(s, msg)
                })
            } else {
                notice.hide()
                completion(false, Localizable.Generic.networkError)
            }
            
        }) { (resp, error) in
            notice.hide()
            JoyupNotice.showFireError(error, response: resp)
        }
        
        return notice
    }
    
    @discardableResult
    public static func forgottenPasswordLoadingRequest(_ uname: String,
                                                       email: String,
                                                       completion: @escaping (_ success: Bool, _ message: String?) -> Swift.Void) -> JoyupNotice {
        let config = Oops.Configuration()
        config.shouldAutoDismiss = false
        config.showCloseButton = false
        let notice = JoyupNotice(alert: Oops(configuration: config))
        notice.alertView.show(.loading,
                              title: Localizable.Generic.loadingTitle,
                              detail: "",
                              icon: nil,
                              color: kSignButtonColor,
                              buttonTitleColor: kSignButtonTextColor,
                              completeText: nil,
                              timeout: nil,
                              animation: .topToBottom)
        Request.forgottenPassword(uname, email).request(params: nil, completion: { (json, _, _) in
            notice.alertView.hideView()
            let msg = json["msg"].stringValue
            completion(json["retCode"].stringValue == "0", msg.isEmpty ? nil : msg)
        }) { (_, error) in
            notice.alertView.hideView()
            completion(false, error.localizedDescription)
        }
        return notice
    }
    
    // MARK: - SignIn
    @discardableResult
    public static func signIn(_ onSignIn: @escaping VoidClosure = {}, onSignUp: @escaping VoidClosure = {}, onCancel: @escaping VoidClosure = {}) -> JoyupNotice {
        let config = Oops.Configuration()
        config.shouldAutoDismiss = false
        config.showCloseButton = false
        let notice = JoyupNotice(alert: Oops(configuration: config))
        
        let storedpassword: String?
        let storeduname: String?
        if let su = Keychain.getStoredUsername() {
            storedpassword = Keychain.getStoredPassword(for: su)
            storeduname = su
        } else {
            storedpassword = nil
            storeduname = nil
        }
        
        
        let uname = notice.alertView.addTextField(Localizable.User.accoutPlaceholderText, text: storeduname, secure: false)
        uname.keyboardType = .alphabet
        let pwd = notice.alertView.addTextField(Localizable.User.passwordPlaceholderText, text: storedpassword, secure: true)
        
        notice.alertView.addButton(Localizable.User.signIn, action: {
            
            let onError: VoidClosure = {
                if uname.text == nil || uname.text!.isEmpty {
                    onErrorField([uname])
                }
                if pwd.text == nil || pwd.text!.isEmpty {
                    onErrorField([pwd])
                }
            }
            
            if let u = uname.text, let p = pwd.text {
                if !u.isEmpty && !p.isEmpty {
                    notice.alertView.hideView()
                    JoyupNotice.signInLoadingWithRequest(u, password: p, completion: { (s, msg) in
                        if s {
                            onSignIn()
                        } else {
                            JoyupNotice.showError(msg, title: Localizable.User.signInFailed, buttons: nil, action: nil)
                        }
                    })
                    return
                }
            }
            onError()
        })
        
        notice.alertView.addButton(Localizable.User.goToSignUp, backgroundColor: kOptionalButtonColor, textColor: kOptionalButtonTextColor, showTimeout: nil) {
            onSignUp()
            //            JoyupUser.showSignUpPanel(to: controller, completion: nil)
            notice.alertView.hideView()
            JoyupNotice.signUp(.accountAndPassword, onAction: { (step) in
                if step == .done {
                    
                }
            }, onCancel: onCancel)
        }
        
        notice.alertView.addButton(Localizable.User.forgottenPassword, backgroundColor: kOptionalButtonColor, textColor: kOptionalButtonTextColor, showTimeout: nil) {
            notice.alertView.hideView()
            JoyupNotice.forgottenPassword()
        }
        
        notice.alertView.addButton(Localizable.Generic.notNow, backgroundColor: kOptionalButtonColor, textColor: kOptionalButtonTextColor, showTimeout: nil) {
            onCancel()
            notice.alertView.hideView()
        }
        notice.alertView.show(.editor,
                              title: Localizable.User.signIn,
                              detail: Joyup.projectName,
                              icon: nil,
                              color: kSignButtonColor,
                              buttonTitleColor: kSignButtonTextColor,
                              completeText: nil,
                              timeout: nil,
                              animation: .topToBottom)
        return notice
    }
    
    // MARK: - Forgotten Password
    
    @discardableResult
    public static func forgottenPassword() -> JoyupNotice {
        let config = Oops.Configuration()
        config.shouldAutoDismiss = false
        config.showCloseButton = false
        let notice = JoyupNotice(alert: Oops(configuration: config))
        
        let storeduname: String? = Keychain.getStoredUsername()
        
        let uname = notice.alertView.addTextField(Localizable.User.accoutPlaceholderText, text: storeduname, secure: false)
        uname.keyboardType = .alphabet
        let mail = notice.alertView.addTextField(Localizable.User.emailPlaceholder, text: nil, secure: false)
        
        notice.alertView.addButton(Localizable.Generic.nextStep, action: {
            
            let onError: VoidClosure = {
                if uname.text == nil || uname.text!.isEmpty {
                    onErrorField([uname])
                }
                if mail.text == nil || !mail.text!.isMatch(pattern: Joyup.regExPatternEmail) {
                    onErrorField([mail])
                }
            }
            
            if let u = uname.text, let m = mail.text {
                // FIMME: EMail Pattern
                if !u.isEmpty && !m.isEmpty && m.isMatch(pattern: Joyup.regExPatternEmail) {
                    notice.alertView.hideView()
                    JoyupNotice.forgottenPasswordLoadingRequest(u, email: m, completion: { (success, msg) in
                        if success {
                            JoyupNotice.showMessage(msg ?? Localizable.User.pleaseCheckEmail)
//                            JoyupNotice.signIn({}, onSignUp: {}, onCancel: {})
                        } else {
                            JoyupNotice.showError(msg ?? Localizable.Generic.unknownError)
                        }
                    })
                    return
                }
            }
            onError()
        })
        
        /*
        notice.alertView.addButton(Localizable.User.goToSignIn, backgroundColor: kOptionalButtonColor, textColor: kOptionalButtonTextColor, showTimeout: nil) {
            notice.alertView.hideView()
        }*/
        
        notice.alertView.addButton(Localizable.Generic.cancel, backgroundColor: kOptionalButtonColor, textColor: kOptionalButtonTextColor, showTimeout: nil) {
            notice.alertView.hideView()
            JoyupNotice.signIn({
                
            }, onSignUp: {
                
            }, onCancel: {
                
            })
        }
        notice.alertView.show(.editor,
                              title: Localizable.User.forgottenPassword,
                              detail: Localizable.User.forgottenPassword,
                              icon: nil,
                              color: kSignButtonColor,
                              buttonTitleColor: kSignButtonTextColor,
                              completeText: nil,
                              timeout: nil,
                              animation: .topToBottom)
        return notice
    }
    
    // MARK: - SignUp
    
    public enum SignUpStep: Int {
        case accountAndPassword = 0
        case refererAndPlacement = 1
        case accountType = 2
        case nameAndEmail = 3
        case gender = 4
        case telAndMobile = 5
        case area = 6
        case subarea = 7
        case SSN = 8
        case done = 9
        
        public var required: Bool {
            switch self {
            case .accountAndPassword:
                return true
            case .refererAndPlacement:
                return false
            case .accountType:
                return true
            case .nameAndEmail:
                return true
            case .gender:
                return false
            case .telAndMobile:
                return false
            case .area:
                return true
            case .subarea:
                return true
            case .SSN:
                return true
            default:
                return true
            }
        }
        
        public var text: (title: String, detail: String) {
            let detail = self.required ? Localizable.Generic.required : Localizable.Generic.optional
            switch self {
            case .accountAndPassword:
                return (Localizable.User.accountAndPasswordTitle, detail)
            case .refererAndPlacement:
                return (Localizable.User.refererAndPlacementTitle, detail)
            case .accountType:
                return (Localizable.User.accountTypeTitle, detail + "\n" + Localizable.User.accountTypeWarning)
            case .nameAndEmail:
                return (Localizable.User.nameAndEmailTitle, detail)
            case .gender:
                return (Localizable.User.genderTitle, detail)
            case .telAndMobile:
                return (Localizable.User.telAndMobileTitle, detail)
            case .area:
                return (Localizable.User.areaTitle, detail)
            case .subarea:
                return (Localizable.User.areaTitle, detail)
            case .SSN:
                return (Localizable.User.SSNTitle, detail)
            default:
                return (Localizable.User.signUpLoadingTitle, Localizable.User.signUpLoading)
            }
        }
    }
    
    @discardableResult
    public static func signUp(_  step: JoyupNotice.SignUpStep,
                              onAction: @escaping (_ nextStep: JoyupNotice.SignUpStep) -> Swift.Void,
                              onCancel: @escaping VoidClosure) -> JoyupNotice {
        
        let nextStep: JoyupNotice.SignUpStep = JoyupNotice.SignUpStep(rawValue: step.rawValue + 1) ?? .done
        let lastStep: JoyupNotice.SignUpStep = JoyupNotice.SignUpStep(rawValue: step.rawValue - 1) ?? .accountAndPassword
        let style: Oops.PopUpStyle = (step == .done) ? .loading : .editor
        
        let config = Oops.Configuration()
        config.shouldAutoDismiss = false
        config.showCloseButton = false
        let notice = JoyupNotice(alert: Oops(configuration: config))
        
        switch step {
        case .accountAndPassword:
            
            let uname = notice.alertView.addTextField(Localizable.User.accoutPlaceholderText, text: UserInfoHolder.shared.userName)
//            uname.keyboardType = .asciiCapableNumberPad
            uname.keyboardType = .alphabet
            let pwd = notice.alertView.addTextField(Localizable.User.passwordPlaceholderText, text: nil, secure: true)
            let confirm = notice.alertView.addTextField(Localizable.User.confirmPasswordPlaceholderText, text: nil, secure: true)
            let accept = notice.alertView.addButton(Localizable.User.haveAcceptedUserAgreement, backgroundColor: UIColor(red:0.99, green:0.34, blue:0.31, alpha:1.00), font: UIFont.systemFont(ofSize: 10), action: {
                if let u = uname.text, let p = pwd.text, let c = confirm.text {
                    if !u.isEmpty && !p.isEmpty && c == p {
                        
                        notice.hide()
                        
                        let loading = checkingSignUpInfo()
                        Request.checkUsername(u).request(params: nil, completion: { (json, _, _) in
                            loading.hide()
                            if let error = Request.checkResult(json) {
                                JoyupNotice.signUpInfoError(error, nextStep: step, onAction: onAction, onCancel: onCancel)
                            } else {
                                // `isExist` is true, means not exists...
                                if json["data"]["isExist"].boolValue {
                                    UserInfoHolder.shared.userName = u
                                    UserInfoHolder.shared.password = p
                                    JoyupNotice.signUp(nextStep, onAction: onAction, onCancel: onCancel)
                                } else {
                                    JoyupNotice.signUpInfoError(Localizable.Generic.usernameExists, nextStep: step, onAction: onAction, onCancel: onCancel)
                                }
                            }
                        }) { (_, error) in
                            loading.hide()
                            JoyupNotice.signUpInfoError(error.localizedDescription, nextStep: step, onAction: onAction, onCancel: onCancel)
                        }
                        return
                    }
                }
                onErrorField([uname, pwd, confirm])
            })
            accept.titleLabel?.numberOfLines = 0
            
            break
        case .refererAndPlacement:
            
            let referer = notice.alertView.addTextField(Localizable.User.refererPlaceholder, text: UserInfoHolder.shared.referer)
            let placement = notice.alertView.addTextField(Localizable.User.placementPlaceholder, text: UserInfoHolder.shared.placement)
            notice.alertView.addButton(Localizable.Generic.nextStep, action: {
                
                if let re = referer.text, let pl = placement.text {
                    if !re.isEmpty && !pl.isEmpty {
                        notice.hide()
                        
                        let loading = checkingSignUpInfo()
                        Request.checkRefererAndPlacement(re, pl).request(params: nil, completion: { (json, _, _) in
                            loading.hide()
                            if let error = Request.checkResult(json) {
                                JoyupNotice.signUpInfoError(error, nextStep: step, onAction: onAction, onCancel: onCancel)
                            } else {
                                // `isExist` is true, means not exists...
                                if json["data"]["isExist"].boolValue {
                                    UserInfoHolder.shared.referer = referer.text
                                    UserInfoHolder.shared.placement = placement.text
                                    JoyupNotice.signUp(nextStep, onAction: onAction, onCancel: onCancel)
                                } else {
                                    JoyupNotice.signUpInfoError(Localizable.Generic.RefererExists, nextStep: step, onAction: onAction, onCancel: onCancel)
                                }
                            }
                        }) { (_, error) in
                            loading.hide()
                            JoyupNotice.signUpInfoError(error.localizedDescription, nextStep: step, onAction: onAction, onCancel: onCancel)
                        }
                        return
                    }
                }
                UserInfoHolder.shared.referer = referer.text
                UserInfoHolder.shared.placement = placement.text
                notice.alertView.hideView()
                JoyupNotice.signUp(nextStep, onAction: onAction, onCancel: onCancel)
            })
            
            break
        case .accountType:
            
            let allTypes: [AccoutType] = [
                AccoutType.member,
                AccoutType.associate,
                AccoutType.premiumassociate
            ]
            for t in allTypes {
                notice.alertView.addButton(t.localizedString, action: {
                    UserInfoHolder.shared.accoutType = t
                    notice.alertView.hideView()
                    JoyupNotice.signUp(nextStep, onAction: onAction, onCancel: onCancel)
                })
            }
            
            break
        case .nameAndEmail:
            
            let name = notice.alertView.addTextField(Localizable.User.namePlaceholder, text: UserInfoHolder.shared.name)
            let email = notice.alertView.addTextField(Localizable.User.emailPlaceholder, text: UserInfoHolder.shared.email)
            email.keyboardType = .emailAddress
            notice.alertView.addButton(Localizable.Generic.nextStep, action: {
                if let n = name.text, let e = email.text {
                    // FIMME: EMail Pattern
                    if !n.isEmpty {//&& e.isMatch(pattern: "") {
                        notice.hide()
                        
                        let loading = checkingSignUpInfo()
                        Request.checkEmail(e).request(params: nil, completion: { (json, _, _) in
                            loading.hide()
                            if let error = Request.checkResult(json) {
                                JoyupNotice.signUpInfoError(error, nextStep: step, onAction: onAction, onCancel: onCancel)
                            } else {
                                // `isExist` is true, means not exists...
                                if json["data"]["isBlock"].boolValue {
                                    UserInfoHolder.shared.name = n
                                    UserInfoHolder.shared.email = e
                                    JoyupNotice.signUp(nextStep, onAction: onAction, onCancel: onCancel)
                                } else {
                                    JoyupNotice.signUpInfoError(Localizable.Generic.emailExists, nextStep: step, onAction: onAction, onCancel: onCancel)
                                }
                            }
                        }) { (_, error) in
                            loading.hide()
                            JoyupNotice.signUpInfoError(error.localizedDescription, nextStep: step, onAction: onAction, onCancel: onCancel)
                        }
                        return
                    }
                }
                onErrorField([name, email])
            })
            
            break
        case .gender:
            
            for g in [Gender.male, Gender.female] {
                notice.alertView.addButton(g.localizedString, action: {
                    UserInfoHolder.shared.gender = g
                    notice.alertView.hideView()
                    JoyupNotice.signUp(nextStep, onAction: onAction, onCancel: onCancel)
                })
            }
            
            break
        case .telAndMobile:
            
            let tel = notice.alertView.addTextField(Localizable.User.telPlaceholder, text: UserInfoHolder.shared.telephone)
            let mobile = notice.alertView.addTextField(Localizable.User.mobilePlaceholder, text: UserInfoHolder.shared.mobilePhone)
            tel.keyboardType = .phonePad
            mobile.keyboardType = .phonePad
            notice.alertView.addButton(Localizable.Generic.nextStep, action: {
                UserInfoHolder.shared.telephone = tel.text
                UserInfoHolder.shared.mobilePhone = mobile.text
                notice.alertView.hideView()
                JoyupNotice.signUp(nextStep, onAction: onAction, onCancel: onCancel)
            })
            
            break
        case .area:
            
            for a in [Area.america, Area.china, Area.australia] {
                notice.alertView.addButton(a.localizedName, action: {
                    UserInfoHolder.shared.area = a
                    notice.alertView.hideView()
                    JoyupNotice.signUp(nextStep, onAction: onAction, onCancel: onCancel)
                })
            }
            
            break
            
        case .subarea:
            
            for a in UserInfoHolder.shared.area!.subarea {
                notice.alertView.addButton(a.localizedName, action: {
                    UserInfoHolder.shared.subarea = a
                    notice.alertView.hideView()
                    JoyupNotice.signUp(nextStep, onAction: onAction, onCancel: onCancel)
                })
            }
            
            break
            
        case .SSN:
            
            let ssnView = notice.alertView.addTextView(text: UserInfoHolder.shared.SSN)
            //            ssnView.keyboardType = .alphabet
            notice.alertView.addButton(Localizable.Generic.nextStep, action: {
                if let ssn = ssnView.text {
                    if !ssn.isEmpty {
                        UserInfoHolder.shared.SSN = ssn
                        notice.alertView.hideView()
                        JoyupNotice.signUp(nextStep, onAction: onAction, onCancel: onCancel)
                        return
                    }
                }
                onErrorTextView([ssnView])
            })
            
            break
        default: // .done
            notice.hide()
            let loading = showLoading(Localizable.User.signUpLoading, title: Localizable.User.signUpLoadingTitle)
            
            Request.getRSAPublicKey.request(params: nil, completion: { (pubJson, _, _) in
                if let modulus = pubJson["modulus"].string,
                    let exponent = pubJson["exponent"].string,
                    let params = UserInfoHolder.shared.generateParams(publicKeyModulus: modulus, publicKeyExponent: exponent) {
                    
                    Request.signUp.request(params: params, completion: { (json, _, _) in
                        loading.hide()
                        if let message = Request.checkResult(json) {
                            JoyupNotice.showError(message)
                        } else {
                            JoyupNotice.showSuccess(nil)
                            if let u = UserInfoHolder.shared.userName {
                                Keychain.storeUsername(u)
                                
                                if let p = UserInfoHolder.shared.password {
                                    Keychain.storePassword(p, for: u)
                                    JoyupUser.autoSignIn()
                                }
                            }
                            UserInfoHolder.shared.cleanUp()
                        }
                    }, error: { (resp, error) in
                        loading.hide()
                        JoyupNotice.showFireError(error, response: resp)
                    })
                    
                } else {
                    loading.hide()
                    JoyupNotice.showError(Localizable.Generic.unknownError)
                }
            }, error: { (resp, error) in
                loading.hide()
                JoyupNotice.showFireError(error, response: resp)
            })
            break
        }
        
        if step == .accountAndPassword {
            
            let btns = [
                (Localizable.User.userAgreement, Joyup.userAgreementURL),
//                (Localizable.User.privacyPolicy, Joyup.privacyURL),
//                (Localizable.User.returnAndRefundPolicy, Joyup.returnPolicyURL),
            ]
            for (t, u) in btns {
                notice.alertView.addButton(t, backgroundColor: kOptionalButtonColor, textColor: kOptionalButtonTextColor, showTimeout: nil) {
                    notice.alertView.hideView()
                    let next = WebViewController(urlString: u)
                    next.hasDismissButton = true
                    let navi = NavigationController(rootViewController: next)
                    let root = UIApplication.shared.keyWindow?.rootViewController
                    root?.present(navi, animated: true, completion: nil)
                }
            }
            
            notice.alertView.addButton(Localizable.User.goToSignIn, backgroundColor: kOptionalButtonColor, textColor: kOptionalButtonTextColor, showTimeout: nil) {
                notice.alertView.hideView()
                JoyupNotice.signIn({
                    
                }, onSignUp: {
                    
                }, onCancel: onCancel)
            }
        }
        
        if step != .accountAndPassword && step != .done {
            notice.alertView.addButton(Localizable.Generic.lastStep, backgroundColor: kOptionalButtonColor, textColor: kOptionalButtonTextColor, showTimeout: nil) {
                notice.alertView.hideView()
                JoyupNotice.signUp(lastStep, onAction: onAction, onCancel: onCancel)
            }
        }
        
        if !step.required {
            notice.alertView.addButton(Localizable.Generic.skip, backgroundColor: kOptionalButtonColor, textColor: kOptionalButtonTextColor, showTimeout: nil) {
                notice.alertView.hideView()
                JoyupNotice.signUp(nextStep, onAction: onAction, onCancel: onCancel)
            }
        }
        
        if step != .done {
            notice.alertView.addButton(Localizable.Generic.cancel, backgroundColor: kOptionalButtonColor, textColor: kOptionalButtonTextColor, showTimeout: nil) {
                onCancel()
                notice.alertView.hideView()
            }
        }
        
        notice.alertView.show(style,
                              title: step.text.title,
                              detail: step.text.detail,
                              icon: nil,
                              color: kSignButtonColor,
                              buttonTitleColor: kSignButtonTextColor,
                              completeText: nil,
                              timeout: nil,
                              animation: .topToBottom)
        return notice
    }
}

fileprivate extension JoyupNotice {
    
    fileprivate class func onErrorField(_ fields: [UITextField]) {
        for f in fields {
            f.layer.borderColor = UIColor.red.cgColor
            
        }
        EasyGCD.after(1) {
            for f in fields {
                f.layer.borderColor = kSignButtonColor.cgColor
            }
        }
    }
    
    fileprivate class func onErrorTextView(_ textViews: [UITextView]) {
        for t in textViews {
            t.layer.borderColor = UIColor.red.cgColor
            EasyGCD.after(1) {
                t.layer.borderColor = kSignButtonColor.cgColor
            }
        }
    }
}

