//
//  Float.swift
//  Joyup
//
//  Created by Meniny on 2017-08-03.
//  Copyright © 2017年 Meniny. All rights reserved.
//

import Foundation

extension Int {
    public func boolValue() -> Bool {
        return self != 0
    }
}

extension Double {
    public func boolValue() -> Bool {
        return self != 0
    }
}

extension Float {
    public func boolValue() -> Bool {
        return self != 0
    }
}

extension CGFloat {
    public func boolValue() -> Bool {
        return self != 0
    }
}

extension UInt {
    public func boolValue() -> Bool {
        return self != 0
    }
}

extension Float {
    public var amountString: String {
        return amountString()
    }
    
    public func amountString(keepZero: Bool = false) -> String {
        let amount = String(format: "%0.02f", self)
        return keepZero ? amount : amount.replace(".00", with: "")
    }
}

extension Double {
    public var amountString: String {
        return amountString()
    }
    
    public func amountString(keepZero: Bool = false) -> String {
        let amount = String(format: "%0.02f", self)
        return keepZero ? amount : amount.replace(".00", with: "")
    }
}
