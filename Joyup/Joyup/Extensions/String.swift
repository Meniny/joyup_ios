//
//  String.swift
//  Joyup
//
//  Created by Meniny on 2017-07-30.
//  Copyright © 2017年 Meniny. All rights reserved.
//

import Foundation
import ColaExpression

@discardableResult
public func +<T> (lhs: T, rhs: String) -> String {
    if let left = lhs as? String {
        return "\(left)\(rhs)"
    }
    return "\(lhs)\(rhs)"
}

@discardableResult
public func +<T> (lhs: String, rhs: T) -> String {
    if let right = rhs as? String {
        return "\(lhs)\(right)"
    }
    return "\(lhs)\(rhs)"
}

extension String {
    
    public var isNotEmpty: Bool {
        return !isEmpty
    }
    
    public var isJoyupEmail: Bool {
        return self.isMatch(pattern: Joyup.regExPatternEmail)
    }
    
    public var isJoyupUsername: Bool {
        return self.isMatch(pattern: Joyup.regExPatternUserName)
    }
    
    public var isJoyupPassword: Bool {
        return self.isMatch(pattern: Joyup.regExPatternPassword)
    }
    
    public var isScientificNotation: Bool {
        return self.isMatch(pattern: Joyup.regExPatternScientificNotation)
    }
}

extension String {
    
    public func replaceCharacters(characterSet: CharacterSet, with replacement: String) -> String {
        let components = self.components(separatedBy: characterSet)
        let result = components.joined(separator: "")
        return result
    }
    
    public func removeWhitespacesAndNewlines() -> String {
        return self.replaceCharacters(characterSet: CharacterSet.whitespacesAndNewlines, with: "")
    }
}
