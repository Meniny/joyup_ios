//
//  UIColor.swift
//  Joyup
//
//  Created by Meniny on 2017-07-30.
//  Copyright © 2017年 Meniny. All rights reserved.
//

import Foundation
import UIKit

extension UIColor {
    public static var valencia: UIColor {
        return UIColor(red:0.87, green:0.29, blue:0.22, alpha:1.00)
    }
    
    public static var shipGary: UIColor {
        return UIColor(red:0.24, green:0.24, blue:0.27, alpha:1.00)
    }
    
    public static var sunglow: UIColor {
        return UIColor(red:1.00, green:0.82, blue:0.20, alpha:1.00)
    }
    
    public static var raven: UIColor {
        return UIColor(red:0.90, green:0.90, blue:0.90, alpha:1.00)
    }
    
    public static var java: UIColor {
        return UIColor(red:0.18, green:0.77, blue:0.73, alpha:1.00)
    }
    
    public static var pictonBlue: UIColor {
        return UIColor(red:0.30, green:0.71, blue:0.97, alpha:1.00)
    }
    
    public static var moonMist: UIColor {
        return UIColor(red:0.86, green:0.87, blue:0.80, alpha:1.00)
    }
    
    public static var athensGary: UIColor {
        return UIColor(red:0.94, green:0.94, blue:0.96, alpha:1.00)
    }
    
    public static var cerulean: UIColor {
        return UIColor(red:0.10, green:0.62, blue:0.90, alpha:1.00)
    }
}

