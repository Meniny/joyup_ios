//
//  UIImageVIew.swift
//  Joyup
//
//  Created by Meniny on 2017-05-09.
//  Copyright © 2017年 Meniny. All rights reserved.
//

import Foundation
import Imagery

public extension UIImageView {
    
    /// 设置网络图片
    ///
    /// - Parameters:
    ///   - url: 网络图片地址
    ///   - placeholder: 占位图名称, 可以为空
    public func webImage(url: String, placeholder: String? = nil) {
        webImage(url: url, placeholderImage: UIImage(named: placeholder ?? "avatar"))
    }
    
    /// 设置网络图片
    ///
    /// - Parameters:
    ///   - url: 网络图片地址
    ///   - placeholder: 占位图, 可以为空
    public func webImage(url: String, placeholderImage: UIImage?) {
        let holder = placeholderImage ?? #imageLiteral(resourceName: "Icon_clear_bg")
        if let u = URL(string: url) {
            imagery.setImage(with: u, placeholder: holder)
        } else {
            image = holder
        }
    }
}
