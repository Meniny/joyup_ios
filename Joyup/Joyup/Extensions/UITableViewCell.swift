//
//  UITableViewCell.swift
//  Joyup
//
//  Created by Meniny on 2017-05-11.
//  Copyright © 2017年 Meniny. All rights reserved.
//

import Foundation
import UIKit

public extension UITableViewCell {
    public func setup() {
        self.backgroundColor = UIColor.clear
        self.contentView.backgroundColor = self.backgroundColor
    }
}

