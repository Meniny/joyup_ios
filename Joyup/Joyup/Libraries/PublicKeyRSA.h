//
//  PublicKeyItems.h
//  Joyup
//
//  Created by Meniny on 2017-08-08.
//  Copyright © 2017年 Meniny. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <OpenSSL/rsa.h>
#import "NSData+Base64.h"

@interface PublicKeyRSA : NSObject
//+ (RSA * __nullable)rsaFromExponent:(NSString * __nonnull)exponent modulus:(NSString * __nonnull)modulus;
//+ (NSString * __nullable)cleanString:(NSString * __nonnull)input;
+ (NSString * __nullable)encrypt:(NSString * __nonnull)string exponentB64:(NSString * __nonnull)exponentB64 modulusB64:(NSString * __nonnull)modulusB64;
+ (NSData * __nullable)generateRSAPublicKeyWithModulus:(NSData * __nonnull)modulus exponent:(NSData * __nonnull)exponent;
@end
