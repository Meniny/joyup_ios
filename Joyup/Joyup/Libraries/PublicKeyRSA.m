//
//  PublicKeyItems.m
//  Joyup
//
//  Created by Meniny on 2017-08-08.
//  Copyright © 2017年 Meniny. All rights reserved.
//

#import "PublicKeyRSA.h"
//#import "OpenSSL/rsa.h"
//#import "NSData+Base64.h"

@implementation PublicKeyRSA

+ (RSA *)rsaFromExponent:(NSString *)exponent modulus:(NSString *)modulus {
    RSA *rsa_pub = RSA_new();
    
    const char *N = [modulus UTF8String];
    const char *E = [exponent UTF8String];
    
    if (!BN_hex2bn(&rsa_pub->n, N))
    {
        // TODO
    }
    printf("N: %s\n", N);
    printf("n: %s\n", BN_bn2dec(rsa_pub->n));
    
    if (!BN_hex2bn(&rsa_pub->e, E))
    {
        // TODO
    }
    printf("E: %s\n", E);
    printf("e: %s\n", BN_bn2dec(rsa_pub->e));
    
    return rsa_pub;
}

+ (NSString *)cleanString:(NSString *)input {
    NSString *output = input;
    output = [output stringByReplacingOccurrencesOfString:@"<" withString:@""];
    output = [output stringByReplacingOccurrencesOfString:@">" withString:@""];
    output = [output stringByReplacingOccurrencesOfString:@" " withString:@""];
    return output;
}

+ (NSString *)encrypt:(NSString *)string exponentB64:(NSString *)exponentB64 modulusB64:(NSString *)modulusB64 {
    
    // 1. decode base64
    NSData *exponent = [NSData dataFromBase64String: exponentB64];
    NSData *modulus = [NSData dataFromBase64String: modulusB64];
    
    NSString *exponentHex = [self cleanString: [exponent description]];
    NSString *modulusHex = [self cleanString: [modulus description]];
    
    // 2. create RSA public key
    RSA *rsa_pub = [self rsaFromExponent: exponentHex modulus: modulusHex];
    
    // 3. encode base 64
    NSData *data = [string dataUsingEncoding: NSASCIIStringEncoding];
    NSString *b64String = [data base64EncodedString];
    
    // 4. encrypt
    const unsigned char *from = (const unsigned char *)[b64String cStringUsingEncoding: NSASCIIStringEncoding];
    int flen = strlen((const char *)from);
    unsigned char *to = (unsigned char *) malloc(RSA_size(rsa_pub));
    int padding = RSA_PKCS1_PADDING;
    int result = RSA_public_encrypt(flen, from, to, rsa_pub, padding);
    if (-1 == result) {
        return nil;
    }
//        NSLog(@"from: %s", from); // echo VEVTVA==
//        NSLog(@"to: %s", to); // echo something strange with characters like: ~™Ÿû—...
    // 5. encode base 64
    NSData *cipherData = [NSData dataWithBytes: (const void *)to length: result];
    NSString *cipherDataB64 = [cipherData base64EncodedString];
    NSLog(@"user encrypted b64: %@", cipherDataB64); // now echo the expected value
    return  cipherDataB64;
}

//NSData* bytesFromHexString(NSString * aString) {
//    NSString *theString = [[aString componentsSeparatedByCharactersInSet: [NSCharacterSet whitespaceAndNewlineCharacterSet]] componentsJoinedByString: @""];
//    
//    NSMutableData* data = [NSMutableData data];
//    int idx;
//    for (idx = 0; idx+2 <= theString.length; idx+=2) {
//        NSRange range = NSMakeRange(idx, 2);
//        NSString* hexStr = [theString substringWithRange:range];
//        NSScanner* scanner = [NSScanner scannerWithString:hexStr];
//        unsigned int intValue;
//        if ([scanner scanHexInt:&intValue])
//        [data appendBytes:&intValue length:1];
//    }
//    return data;
//}

+ (NSData *)generateRSAPublicKeyWithModulus:(NSData *)modulus exponent:(NSData *)exponent {
    const uint8_t DEFAULT_EXPONENT[] = {0x01, 0x00, 0x01,}; //default: 65537
    const uint8_t UNSIGNED_FLAG_FOR_BYTE = 0x81;
    const uint8_t UNSIGNED_FLAG_FOR_BYTE2 = 0x82;
    const uint8_t UNSIGNED_FLAG_FOR_BIGNUM = 0x00;
    const uint8_t SEQUENCE_TAG = 0x30;
    const uint8_t INTEGER_TAG = 0x02;
    
    uint8_t* modulusBytes = (uint8_t*)[modulus bytes];
    uint8_t* exponentBytes = (uint8_t*)(exponent == nil ? DEFAULT_EXPONENT : [exponent bytes]);
    
    //(1) calculate lengths
    //- length of modulus
    int lenMod = (int)[modulus length];
    if(modulusBytes[0] >= 0x80)
    lenMod ++;  //place for UNSIGNED_FLAG_FOR_BIGNUM
    int lenModHeader = 2 + (lenMod >= 0x80 ? 1 : 0) + (lenMod >= 0x0100 ? 1 : 0);
    //- length of exponent
    int lenExp = exponent == nil ? sizeof(DEFAULT_EXPONENT) : (int)[exponent length];
    int lenExpHeader = 2;
    //- length of body
    int lenBody = lenModHeader + lenMod + lenExpHeader + lenExp;
    //- length of total
    int lenTotal = 2 + (lenBody >= 0x80 ? 1 : 0) + (lenBody >= 0x0100 ? 1 : 0) + lenBody;
    
    int index = 0;
    uint8_t* byteBuffer = malloc(sizeof(uint8_t) * lenTotal);
    memset(byteBuffer, 0x00, sizeof(uint8_t) * lenTotal);
    
    //(2) fill up byte buffer
    //- sequence tag
    byteBuffer[index ++] = SEQUENCE_TAG;
    //- total length
    if(lenBody >= 0x80)
    byteBuffer[index ++] = (lenBody >= 0x0100 ? UNSIGNED_FLAG_FOR_BYTE2 : UNSIGNED_FLAG_FOR_BYTE);
    if(lenBody >= 0x0100)
    {
        byteBuffer[index ++] = (uint8_t)(lenBody / 0x0100);
        byteBuffer[index ++] = lenBody % 0x0100;
    }
    else
    byteBuffer[index ++] = lenBody;
    //- integer tag
    byteBuffer[index ++] = INTEGER_TAG;
    //- modulus length
    if(lenMod >= 0x80)
    byteBuffer[index ++] = (lenMod >= 0x0100 ? UNSIGNED_FLAG_FOR_BYTE2 : UNSIGNED_FLAG_FOR_BYTE);
    if(lenMod >= 0x0100)
    {
        byteBuffer[index ++] = (int)(lenMod / 0x0100);
        byteBuffer[index ++] = lenMod % 0x0100;
    }
    else
    byteBuffer[index ++] = lenMod;
    //- modulus value
    if(modulusBytes[0] >= 0x80)
    byteBuffer[index ++] = UNSIGNED_FLAG_FOR_BIGNUM;
    memcpy(byteBuffer + index, modulusBytes, sizeof(uint8_t) * [modulus length]);
    index += [modulus length];
    //- exponent length
    byteBuffer[index ++] = INTEGER_TAG;
    byteBuffer[index ++] = lenExp;
    //- exponent value
    memcpy(byteBuffer + index, exponentBytes, sizeof(uint8_t) * lenExp);
    index += lenExp;
    
    if(index != lenTotal)
    NSLog(@"lengths mismatch: index = %d, lenTotal = %d", index, lenTotal);
    
    NSMutableData* buffer = [NSMutableData dataWithBytes:byteBuffer length:lenTotal];
    free(byteBuffer);
    
    return buffer;
}

@end
