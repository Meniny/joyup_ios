function rsaEncrypt(string, modulus, exponent) {
  var rsaKey = new RSAKey();
  rsaKey.setPublic(b64tohex(modulus), b64tohex(exponent));
  var enPassword = hex2b64(rsaKey.encrypt(string));
  return enPassword
}
