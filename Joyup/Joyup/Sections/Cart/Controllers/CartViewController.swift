//
//  CartViewController.swift
//  Joyup
//
//  Created by Meniny on 2017-07-12.
//  Copyright © 2017年 Meniny. All rights reserved.
//

import UIKit
import EasyGCD
import CocoaHelper

class CartViewController: BaseTableViewController {
    
    var bottomBar: UIView = UIView()
//    var checkButton: UIButton = UIButton(type: .custom)
    var priceLabel: UILabel = UILabel()
    var payButton: UIButton = UIButton(type: .custom)

    public var hasDismissButton = false
    
    override func viewDidLoad() {
        
        self.tableInset = UIEdgeInsets(top: 0, left: 0, bottom: -Joyup.bottomBarHeight, right: 0)
        super.viewDidLoad()
        
        self.title = Localizable.Cart.title
        
        self.view.clipsToBounds = true
        
        self.view.addSubview(bottomBar)
//        bottomBar.addSubview(checkButton)
        bottomBar.addSubview(priceLabel)
        bottomBar.addSubview(payButton)
        
        bottomBar.backgroundColor = UIColor.white
        bottomBar.enableShadow(offset: 0, y: -1)
//        checkButton.setImage(#imageLiteral(resourceName: "cart_check_n"), for: .normal)
//        checkButton.setImage(#imageLiteral(resourceName: "cart_check_p"), for: .selected)
        priceLabel.font = UIFont.systemFont(ofSize: 17)
        payButton.backgroundColor = Configuration.submitButtonColor
        payButton.setTitleColor(Configuration.submitTextColor, for: .normal)
        
        bottomBar.snp.makeConstraints { (make) in
            make.left.equalTo(self.tableView.snp.left)
            make.right.equalTo(self.tableView.snp.right)
            make.bottom.equalTo(0)
            make.top.equalTo(self.tableView.snp.bottom)
        }
        
//        checkButton.snp.makeConstraints { (make) in
//            make.left.equalTo(8)
//            make.width.equalTo(20)
//            make.height.equalTo(20)
//            make.centerY.equalTo(self.bottomBar.snp.centerY)
//        }
        
        payButton.snp.makeConstraints { (make) in
            make.top.equalTo(0)
            make.bottom.equalTo(0)
            make.width.equalTo(100)
            make.right.equalTo(0)
        }
        
        priceLabel.snp.makeConstraints { (make) in
            make.left.equalTo(8)//self.checkButton.snp.right).offset(8)
            make.height.greaterThanOrEqualTo(10)
            make.right.equalTo(self.payButton.snp.left)
            make.centerY.equalTo(self.bottomBar.snp.centerY)
        }
        
        payButton.setTitle(Localizable.Cart.submitOrder, for: .normal)
        payButton.setTitleColor(Joyup.submitTextColor, for: .normal)
//        payButton.setTitleColor(UIColor.black, for: .disabled)
        payButton.backgroundColor = Joyup.submitButtonColor
        
        self.tableView.separatorStyle = .singleLine
        self.tableView.register(cell: CartTableViewCell.self)
        self.tableView.delegate = self
        self.tableView.dataSource = self
        
//        checkButton.addTarget(self, action: #selector(checkAllAction), for: .touchUpInside)
        payButton.addTarget(self, action: #selector(submitOrder), for: .touchUpInside)
        
        NotificationCenter.default.addObserver(self, selector: #selector(userDidSignedIn), name: Joyup.signedInNotification, object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(pullCartData), name: Joyup.updateCartListNotification, object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(showOrderDetail(_:)), name: Joyup.showOrderDetailNotification, object: nil)
        
        self.updatePrice()
        
        navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .refresh, target: self, action: #selector(pullCartData))
        
        if hasDismissButton {
            navigationItem.leftBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "down-chevron"), style: .done, target: self, action: #selector(dismissCart))
        }
    }
    
    var reloadWhenViewAppears = true
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    func dismissCart() {
        dismiss(animated: true, completion: nil)
    }
    
    func showOrderDetail(_ sender: Notification) {
        reloadWhenViewAppears = false
        self.navigationController?.popToRootViewController(animated: false)
        if let sn = sender.object as? String {
            let next = OrderDetailViewController(sn: sn)
            self.navigationController?.pushViewController(next, animated: true)
        }
        reloadWhenViewAppears = true
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if JoyupUser.signedIn && reloadWhenViewAppears {
            self.pullCartData()
        }
    }
    
    var loading: JoyupNotice?
    
    func pullCartData() {
        if self.tabBarController?.selectedIndex == 2 && self == self.navigationController!.topViewController {
            loading = JoyupNotice.showLoading(nil)
        } else {
            loading = nil
        }
        CartManager.pullCartList { [weak self] (success, error) in
            self?.updatePrice()
            if success {
                self?.tableView.reloadData()
                self?.loading?.hide()
            } else {
                self?.loading?.hide()
                JoyupNotice.showError(error ?? Localizable.Generic.unknownError)
            }
            self?.loading = nil
            let c = CartManager.shared.products.count
            let filled = c > 0
            self?.payButton.isEnabled = filled
            self?.payButton.backgroundColor = filled ? Joyup.submitButtonColor : UIColor.lightGray
            
            self?.tabBarItem.badgeValue = filled ? "\(c)" : nil
        }
    }
    
    override func localizationDidChange() {
        super.localizationDidChange()
        self.title = Localizable.Cart.title
        payButton.setTitle(Localizable.Cart.submitOrder, for: .normal)
        pullCartData()
    }
    
    override func userDidSignedIn() {
        super.userDidSignedIn()
        if tabBarController?.selectedIndex == 2 {
            pullCartData()
        }
    }
    
    func checkAllAction(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        CartManager.shared.set(allCheck: sender.isSelected)
        self.tableView.reloadData()
        self.updatePrice()
    }
    
    func submitOrder() {
        let next = SubmitOrderViewController()
        self.navigationController?.pushViewController(next, animated: true)
    }
    
    func updatePrice() {
        CartManager.shared.update()
        priceLabel.text = String(format: "%@: US$%@", Localizable.Cart.total, CartManager.shared.totalPrice.amountString)
    }
}

extension CartViewController: UITableViewDelegate, UITableViewDataSource {
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let id = self.cellIdentifier(at: indexPath)
        let cell = tableView.dequeueReusableCell(withIdentifier: id)
        self.config(cell: cell!, at: indexPath)
        return cell!
    }
    
    func cellIdentifier(at indexPath: IndexPath) -> String {
        return CartTableViewCell.nameOfClass
    }
    
    func config(cell aCell: UITableViewCell, at indexPath: IndexPath) {
        aCell.clipsToBounds = true
        aCell.selectionStyle = .none
        aCell.accessoryType = .none
        
        let cell = aCell as! CartTableViewCell
        let p = CartManager.shared.products[indexPath.row]
        cell.titleLabel.text = "\(p.title)"
        cell.priceLabel.text = "US$\(p.price.amountString)"
        cell.countField.text = "\(p.quantity)"
//        cell.checkButton.isSelected = p.checked
        cell.previewView.webImage(url: p.image, placeholder: nil)
        cell.noticeLabel.text = p.domain?.regionNotice
        cell.onQuantityChangeClosure = { (quantity) in
            if let cid = p.cartItemID {
                let loading = JoyupNotice.showLoading(nil)
                CartManager.shared.changingRequest(cartItemID: cid, quantity: quantity, completion: { (s, error) in
                    loading.alertView.hideView()
                    if s {
                        CartManager.shared.set(quantity: quantity, at: indexPath.row)
                        self.tableView.reloadData()
                        self.updatePrice()
                    } else {
                        JoyupNotice.showError(error ?? Localizable.Cart.changeQuantityFailed)
                    }
                })
            } else {
                JoyupNotice.showError(Localizable.Generic.wrongParameters)
            }
            return true
        }
//        cell.onCheckStatusChangeClosure = { (checked) in
//            CartManager.shared.set(check: checked, at: indexPath.row)
//            self.updatePrice()
//        }
    }
    
    public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let p = CartManager.shared.products[indexPath.row]
        let next = ProductDetailViewController(id: p.id, fromOrder: true)
        navigationController?.pushViewController(next, animated: true)
    }
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return CartManager.shared.products.count
    }
    
    public func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let id = self.cellIdentifier(at: indexPath)
        return tableView.fd_heightForCell(withIdentifier: id, configuration: { (cell) in
            self.config(cell: cell as! UITableViewCell, at: indexPath)
        })
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        let delete = UITableViewRowAction(style: .destructive, title: Localizable.Generic.delete) { [weak self] (act, idx) in
            let p = CartManager.shared.products[idx.row]
            guard let cid = p.cartItemID else {
                JoyupNotice.showError(Localizable.Generic.wrongParameters)
                return
            }
            let loading = JoyupNotice.showLoading(nil)
            CartManager.shared.removeRequest(cartItemID: cid, completion: { (s, error) in
                loading.alertView.hideView()
                if s {
                    main { [weak self] in
                        self?.updatePrice()
                        self?.tableView.reloadData()
                    }
                } else {
                    JoyupNotice.showError(error ?? Localizable.Cart.removeFromCartFailed)
                }
            })
        }
        return [delete]
    }
}
