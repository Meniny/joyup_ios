//
//  ConsigneeSelectionViewController.swift
//  Joyup
//
//  Created by Meniny on 2017-08-07.
//  Copyright © 2017年 Meniny. All rights reserved.
//

import UIKit

class ConsigneeSelectionViewController: BaseTableViewController {
    
    var consignees: [Consignee] = []
    var callback: (_ consignees: [Consignee], _ consignee: Consignee) -> Void
    
    public init(consignees cs: [Consignee], callback cbk: @escaping (_ consignees: [Consignee], _ consignee: Consignee) -> Void) {
        consignees.append(contentsOf: cs)
        callback = cbk
        super.init()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        title = Localizable.Generic.consignee
        
        self.tableView.register(cell: OrderDetailConsigneeTableViewCell.self)
        
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.separatorStyle = .none
        
        navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(adding))
        
        request()
    }
    
    override func userDidSignedOut() {
        super.userDidSignedOut()
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    func request() {
        let loading = JoyupNotice.showLoading()
        Request.consigneeList.request(params: nil, completion: { (json, _, _) in
            loading.hide()
            if let error = Request.checkResult(json) {
                JoyupNotice.showError(error)
            } else {
                self.consignees.removeAll()
                for js in json["data"]["receiverList"].arrayValue {
                    self.consignees.append(Consignee(receiver: js))
                }
                self.tableView.reloadData()
            }
        }) { (resp, error) in
            loading.hide()
            JoyupNotice.showFireError(error, response: resp)
        }
    }
    
    func adding() {
        let next = ConsigneeCreationViewController {
            self.request()
        }
        navigationController?.pushViewController(next, animated: true)
    }
}

extension ConsigneeSelectionViewController: UITableViewDelegate, UITableViewDataSource {
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let id = self.cellIdentifier(at: indexPath)
        let cell = tableView.dequeueReusableCell(withIdentifier: id)
        self.config(cell: cell!, at: indexPath)
        return cell!
    }
    
    func cellIdentifier(at indexPath: IndexPath) -> String {
        return OrderDetailConsigneeTableViewCell.nameOfClass
    }
    
    func config(cell aCell: UITableViewCell, at indexPath: IndexPath) {
        aCell.clipsToBounds = true
        aCell.selectionStyle = .none
        aCell.accessoryType = .none
        
        let cell = aCell as! OrderDetailConsigneeTableViewCell
        cell.config(with: consignees.object(at: indexPath.section))
    }
    
    public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let consignee = consignees.object(at: indexPath.section) {
            callback(consignees, consignee)
            navigationController?.popViewController(animated: true)
        }
    }
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    public func numberOfSections(in tableView: UITableView) -> Int {
        return consignees.count
    }
    
    public func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let id = self.cellIdentifier(at: indexPath)
        return tableView.fd_heightForCell(withIdentifier: id, configuration: { (cell) in
            self.config(cell: cell as! UITableViewCell, at: indexPath)
        })
    }
}
