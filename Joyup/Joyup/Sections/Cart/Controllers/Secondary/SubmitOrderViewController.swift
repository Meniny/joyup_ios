//
//  SubmitOrderViewController.swift
//  Joyup
//
//  Created by Meniny on 2017-07-15.
//  Copyright © 2017年 Meniny. All rights reserved.
//

import UIKit
import Fire
import Today
import CocoaHelper
import Jsonify

open class PaymentMethod {
    open var name: String
    open var id: Int
    open var image: String
    open var brief: String
    
    public init(json: Jsonify) {
        name = json["payMethodName"].stringValue
        id = json["payMethodId"].intValue
        image = json["payMethodImage"].stringValue
        brief = json["payMethodDesc"].stringValue
    }
}

open class ShippingMethod {
    open var name: String
    open var id: Int
    open var image: String
    open var brief: String
    
    public init(json: Jsonify) {
        image = json["shippingIcon"].stringValue
        name = json["shippingName"].stringValue
        brief = json["shippingDesc"].stringValue
        id = json["shippingId"].intValue
    }
}

open class OrderCheckoutInfo {
    open var cartToken: String
    open var balance: Float
    
    open var paymentMethods: [PaymentMethod] = []
    open var productType: ProductType
    open var consignee: [Consignee] = []
    open var shippingMethods: [ShippingMethod] = []
    
    open class OrderItem {
        open var name: String
        open var goodsType: String
        open var productId: Int
        open var goodsId: Int
        open var thumbnail: String
        open var SN: String
        open var quantity: Int
        open var price: Float
        
        public init(json: Jsonify) {
            name = json["orderItemName"].stringValue
            goodsType = json["goodsType"].stringValue
            productId = json["productId"].intValue
            goodsId = json["goodsId"].intValue
            thumbnail = json["orderItemThumbnail"].stringValue
            SN = json["orderItemSn"].stringValue
            quantity = json["orderItemQuantity"].intValue
            price = json["orderItemPrice"].floatValue
        }
    }
    
    open var orderItems: [OrderCheckoutInfo.OrderItem] = []
    
    public init(json: Jsonify) {
        cartToken = json["cartToken"].stringValue
        for cjs in json["defaultReceiver"].arrayValue {
            consignee.append(Consignee(receiver: cjs))
        }
        productType = ProductType(rawValue: json["<##>"].stringValue) ?? .kind
        for js in json["paymentMethods"].arrayValue {
            let payment = PaymentMethod(json: js)
            paymentMethods.append(payment)
        }
        for js in json["shippingMethods"].arrayValue {
            shippingMethods.append(ShippingMethod(json: js))
        }
        if let o = json["order"].arrayValue.first {
            for js in o["orderItems"].arrayValue {
                orderItems.append(OrderItem(json: js))
            }
        }
        balance = json["balance"].floatValue
    }
}

open class OrderPriceInfo {
    open var amount: Float
    open var amountPayable: Float
    open var price: Float
    open var tax: String
    open var freight: Float
    open var promotionDiscount: Float
    open var fee: Float
    open var couponDiscount: Float
    open var balance: Float
    
    public init(json: Jsonify) {
        amount = json["amount"].floatValue
        amountPayable = json["amountPayable"].floatValue
        price = json["price"].floatValue
        tax = json["tax"].stringValue
        freight = json["freight"].floatValue
        promotionDiscount = json["promotionDiscount"].floatValue
        fee = json["fee"].floatValue
        balance = json["balance"].floatValue
        couponDiscount = json["couponDiscount"].floatValue
    }
    
    public func update(json: Jsonify) {
        amount = json["amount"].floatValue
        amountPayable = json["amountPayable"].floatValue
        price = json["price"].floatValue
        tax = json["tax"].stringValue
        freight = json["freight"].floatValue
        promotionDiscount = json["promotionDiscount"].floatValue
        fee = json["fee"].floatValue
        balance = json["balance"].floatValue
        couponDiscount = json["couponDiscount"].floatValue
    }
}

class SubmitOrderViewController: BaseTableViewController {
    
    var checkedout: OrderCheckoutInfo?
    
    var selectedConsigneeIndex: Int = 0
    var selectedPaymentMethodIndex: Int = 0
    var selectedShippingMethodIndex: Int = 0
    
    var bottomBar: UIView = UIView()
    var priceLabel: UILabel = UILabel()
    var payButton: UIButton = UIButton(type: .custom)
    
    var price: OrderPriceInfo?
    var SN: String?
    var paymentSN: String?
    var balance: Float = 0
    var useBalance = false
    
    override func viewDidLoad() {
        
        self.tableInset = UIEdgeInsets(top: 0, left: 0, bottom: -Joyup.bottomBarHeight, right: 0)
        
        super.viewDidLoad()
        
        title = Localizable.Cart.submitOrder
        
        PayPalMobile.preconnect(withEnvironment: JoyupPayment.kPayPalEnvironment)
        
        self.view.clipsToBounds = true
        
        self.view.addSubview(bottomBar)
        bottomBar.addSubview(priceLabel)
        bottomBar.addSubview(payButton)
        
        bottomBar.backgroundColor = UIColor.white
        bottomBar.enableShadow(offset: 0, y: -1)
        
        priceLabel.font = UIFont.systemFont(ofSize: 14)
        priceLabel.textColor = UIColor.valencia
        priceLabel.textAlignment = .right
        
        payButton.backgroundColor = Configuration.submitButtonColor
        payButton.setTitleColor(Configuration.submitTextColor, for: .normal)
        
        bottomBar.snp.makeConstraints { (make) in
            make.left.equalTo(self.tableView.snp.left)
            make.right.equalTo(self.tableView.snp.right)
            make.bottom.equalTo(0)
            make.top.equalTo(self.tableView.snp.bottom)
        }
        
        payButton.snp.makeConstraints { (make) in
            make.top.equalTo(0)
            make.bottom.equalTo(0)
            make.width.equalTo(100)
            make.right.equalTo(0)
        }
        
        priceLabel.snp.makeConstraints { (make) in
            make.top.equalTo(0)
            make.bottom.equalTo(0)
            make.left.equalTo(8)
            make.right.equalTo(self.payButton.snp.left).offset(-8)
        }
        
        priceLabel.text = "AmountPayable".localized() + ": US$ ---"
        
        payButton.setTitle(Localizable.Cart.submit, for: .normal)
        payButton.addTarget(self, action: #selector(createOrder), for: .touchUpInside)
        
        self.tableView.register(cell: NormalSwitchTableViewCell.self)
        self.tableView.register(cell: AddConsigneeTableViewCell.self)
        self.tableView.register(cell: OrderCheckoutConsigneeTableViewCell.self)
        self.tableView.register(cell: CheckboxTableViewCell.self)
        self.tableView.register(cell: NormalDetailedTableViewCell.self)
        self.tableView.register(cell: ProductListTableViewCell.self)
        
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.separatorStyle = .singleLine
        
        orderCheckout(autoCalculating: true)
        
        NotificationCenter.default.addObserver(self, selector: #selector(alipayAppCallbackNotificationHandler), name: Joyup.alipayPaymentCallbackNotification, object: nil)
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    override func userDidSignedOut() {
        super.userDidSignedOut()
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    func createOrder() {
        guard price != nil else {
            calculate(consigneeIndex: selectedConsigneeIndex, paymentIndex: selectedPaymentMethodIndex, shippingIndex: selectedShippingMethodIndex, completion: {
                self.createOrder()
            })
            return
        }
        
        guard let cartTK = checkedout?.cartToken else {
            JoyupNotice.showError(Localizable.Generic.unknownError)
            return
        }
        
        guard let consigneeID = checkedout?.consignee.object(at: selectedConsigneeIndex)?.id else {
            if let _ = checkedout?.consignee.first {
                JoyupNotice.showError(Localizable.Generic.unknownError)
            } else {
                JoyupNotice.showWarning(Localizable.Cart.mustChooseAnAddress)
            }
            return
        }
        
        guard let paymentID = checkedout?.paymentMethods.object(at: selectedPaymentMethodIndex)?.id,
            let shippingID = checkedout?.shippingMethods.object(at: selectedShippingMethodIndex)?.id else {
                JoyupNotice.showError(Localizable.Generic.unknownError)
                return
        }
        let loading = JoyupNotice.showLoading(nil)
        Request.orderCreate(cartTK, consigneeID, paymentID, shippingID, balance).request(params: nil, completion: { (json, _, _) in
            if let error = Request.checkResult(json) {
                loading.hide()
                JoyupNotice.showError(error)
            } else {
                loading.hide()
                self.SN = json["data"]["sn"].string
                //                NotificationCenter.default.post(name: Joyup.updateCartListNotification, object: nil)
                if let amount = self.price?.amountPayable {
                    if amount > 0 {
                        switch self.selectedPaymentMethodIndex {
                        case 0:
                            self.payWithPaypal(amount: amount)
                            break
                        case 1:
                            self.payWithAlipay(amount: amount)
                            break
                        default:
                            JoyupNotice.showSuccess()
                            self.doneAction()
                            break
                        }
                    } else {
                        JoyupNotice.showSuccess()
                        self.doneAction()
                    }
                } else {
                    JoyupNotice.showSuccess()
                    self.doneAction()
                }
            }
        }) { (resp, error) in
            loading.hide()
            JoyupNotice.showFireError(error, response: resp)
        }
    }
    
    func calculate(consigneeIndex: Int, paymentIndex: Int, shippingIndex: Int, noAlert: Bool = false, completion: VoidClosure? = nil) {
        guard let consigneeID = checkedout?.consignee.object(at: consigneeIndex)?.id else {
            if noAlert { return }
            if let _ = checkedout?.consignee.first {
                JoyupNotice.showError(Localizable.Generic.unknownError)
            } else {
                JoyupNotice.showWarning(Localizable.Cart.mustChooseAnAddress)
            }
            return
        }
        guard let paymentID = checkedout?.paymentMethods.object(at: paymentIndex)?.id,
            let shippingID = checkedout?.shippingMethods.object(at: shippingIndex)?.id else {
                if noAlert { return }
                JoyupNotice.showError(Localizable.Generic.unknownError)
                return
        }
        
        let loading = JoyupNotice.showLoading(nil)
        Request.orderCalculate(consigneeID, paymentID, shippingID, balance).request(params: nil, completion: { (json, _, _) in
            if let error = Request.checkResult(json) {
                loading.hide()
                JoyupNotice.showError(error)
            } else {
                loading.hide()
                self.selectedConsigneeIndex = consigneeIndex
                self.selectedPaymentMethodIndex = paymentIndex
                self.selectedShippingMethodIndex = shippingIndex
                if self.price == nil {
                    self.price = OrderPriceInfo(json: json["data"])
                } else {
                    self.price?.update(json: json["data"])
                }
                if let priceString = self.price?.amountPayable.amountString {
                    self.priceLabel.text = "AmountPayable".localized() + ": US$ " + priceString
                } else {
                    self.priceLabel.text = "AmountPayable".localized() + ": US$ ---"
                }
                self.tableView.reloadData()
                completion?()
            }
        }) { (resp, error) in
            loading.hide()
            JoyupNotice.showFireError(error, response: resp)
        }
    }
    
    override func localizationDidChange() {
        super.localizationDidChange()
        tableView.reloadData()
    }
    
    func orderCheckout(autoCalculating: Bool = false) {
        let loading = JoyupNotice.showLoading(nil)
        Request.orderCheckout.request(params: nil, completion: { (json, _, _) in
            if let error = Request.checkResult(json) {
                loading.hide()
                JoyupNotice.showError(error, title: nil, buttons: [Localizable.Generic.cancel], action: { (notice, _) in
                    notice.hide()
                    self.navigationController?.popViewController(animated: true)
                })
            } else {
                self.checkedout = OrderCheckoutInfo(json: json["data"])
                loading.hide()
                self.tableView.reloadData()
                if autoCalculating {
                    self.calculate(consigneeIndex: self.selectedConsigneeIndex,
                                   paymentIndex: self.selectedPaymentMethodIndex,
                                   shippingIndex: self.selectedShippingMethodIndex,
                                   noAlert: true)
                }
            }
        }) { (_, error) in
            loading.hide()
            JoyupNotice.showError(error.localizedDescription, title: nil, buttons: [Localizable.Generic.cancel], action: { (notice, _) in
                notice.hide()
                self.navigationController?.popViewController(animated: true)
            })
        }
    }
    
    /// Pay with PayPal
    func payWithPaypal(amount: Float) {
        if let sn = SN {
            let loading = JoyupNotice.showLoading()
            Request.orderSubmit(.payPal, sn, amount).request(params: nil, completion: { (json, _, _) in
                loading.hide()
                if let error = Request.checkResult(json) {
                    JoyupNotice.showError(error)
                    self.doneAction()
                } else {
                    if let psn = json["data"]["paymentSN"].string {
                        self.paymentSN = psn
                        JoyupPayment.usePaypal(pay: amount, brief: sn, controller: self, delegate: self)
                    } else {
                        JoyupNotice.showError(Localizable.Generic.unknownError)
                        self.doneAction()
                    }
                }
            }, error: { (resp, error) in
                loading.hide()
                JoyupNotice.showFireError(error, response: resp)
                self.doneAction()
            })
        }
    }
    
    /// Pay with PayPal
    func payWithAlipay(amount: Float) {
        if let sn = SN {
            let loading = JoyupNotice.showLoading()
            Request.orderSubmit(.alipay, sn, amount).request(params: nil, completion: { (json, _, _) in
                loading.hide()
                if let error = Request.checkResult(json) {
                    JoyupNotice.showError(error)
                    self.doneAction()
                } else {
                    if let psn = json["data"]["paymentSN"].string {
                        self.paymentSN = psn
                        JoyupPayment.useAlipay(sn: psn, completion: { (s, e) in
                            if s {
                                self.doneAction()
                            } else {
                                JoyupNotice.showError(Localizable.Generic.unknownError)
                                self.doneAction()
                            }
                        })
                    } else {
                        JoyupNotice.showError(Localizable.Generic.unknownError)
                        self.doneAction()
                    }
                }
            }, error: { (resp, error) in
                loading.hide()
                JoyupNotice.showFireError(error, response: resp)
                self.doneAction()
            })
        }
    }
    
    public func doneAction() {
        NotificationCenter.default.post(name: Joyup.showOrderDetailNotification, object: SN)
        //        self.navigationController?.popViewController(animated: false)
    }
  
    public func alipayAppCallbackNotificationHandler() {
        
        self.navigationController?.popViewController(animated: false)
    }
}

extension SubmitOrderViewController: PayPalPaymentDelegate {
    public func payPalPaymentViewController(_ paymentViewController: PayPalPaymentViewController, didComplete completedPayment: PayPalPayment) {
        paymentViewController.dismiss(animated: true, completion: nil)
        // FIXME: API
        if let sn = paymentSN {
            let loading = JoyupNotice.showLoading()
            Request.paymentPluginNotify(.payPal, sn).request(params: nil, completion: { (json, _, _) in
                loading.hide()
                if let error = Request.checkResult(json) {
                    JoyupNotice.showError(error)
                } else {
                    JoyupNotice.showSuccess()
                }
                self.doneAction()
            }, error: { (resp, error) in
                loading.hide()
                JoyupNotice.showFireError(error, response: resp)
                self.doneAction()
            })
        } else {
            JoyupNotice.showError(Localizable.Generic.unknownError)
            self.doneAction()
        }
    }
    
    public func payPalPaymentDidCancel(_ paymentViewController: PayPalPaymentViewController) {
        paymentViewController.dismiss(animated: true, completion: nil)
        self.doneAction()
    }
}
/*
 extension SubmitOrderViewController: PayPalFuturePaymentDelegate {
 func payPalFuturePaymentViewController(_ futurePaymentViewController: PayPalFuturePaymentViewController, didAuthorizeFuturePayment futurePaymentAuthorization: [AnyHashable : Any]) {
 futurePaymentViewController.dismiss(animated: true, completion: nil)
 JoyupNotice.debug(style: .success, message: "\(futurePaymentAuthorization)")
 }
 
 func payPalFuturePaymentDidCancel(_ futurePaymentViewController: PayPalFuturePaymentViewController) {
 futurePaymentViewController.dismiss(animated: true, completion: nil)
 JoyupNotice.debug(style: .warning, message: "Failed")
 }
 }
 */

extension SubmitOrderViewController: UITableViewDelegate, UITableViewDataSource {
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let id = self.cellIdentifier(at: indexPath)
        let cell = tableView.dequeueReusableCell(withIdentifier: id)
        self.config(cell: cell!, at: indexPath)
        return cell!
    }
    
    func cellIdentifier(at indexPath: IndexPath) -> String {
        switch indexPath.section {
        case 0:
            if let _ = checkedout?.consignee.first {
                return OrderCheckoutConsigneeTableViewCell.nameOfClass
            } else {
                return AddConsigneeTableViewCell.nameOfClass
            }
        case 1:
            if indexPath.row >= (checkedout?.paymentMethods.count ?? 0) {
                return NormalSwitchTableViewCell.nameOfClass
            }
            return CheckboxTableViewCell.nameOfClass
        case 2:
            return CheckboxTableViewCell.nameOfClass
        case 3:
            return NormalDetailedTableViewCell.nameOfClass
        default:
            return ProductListTableViewCell.nameOfClass
        }
    }
    
    func config(cell aCell: UITableViewCell, at indexPath: IndexPath) {
        aCell.clipsToBounds = true
        aCell.selectionStyle = .none
        aCell.accessoryType = .none
        
        if aCell is OrderCheckoutConsigneeTableViewCell {
            let cell = aCell as! OrderCheckoutConsigneeTableViewCell
            if let consignee = checkedout?.consignee.object(at: selectedConsigneeIndex > 0 ? selectedConsigneeIndex : 0) {
                cell.config(with: consignee)
            } else {
                cell.clear()
            }
        } else if aCell is AddConsigneeTableViewCell {
        } else if aCell is ProductListTableViewCell {
            let cell = aCell as! ProductListTableViewCell
            let item = checkedout?.orderItems.object(at: indexPath.row)
            cell.config(with: item)
            
        } else if aCell is NormalDetailedTableViewCell {
            let cell = aCell as! NormalDetailedTableViewCell
            if indexPath.row == 0 {
                cell.leftLabel.text = "AmountPrice".localized()
                cell.rightLabel.text = "US$ " + (price?.price.amountString ?? "---")
            } else {
                cell.leftLabel.text = "Freight".localized()
                cell.rightLabel.text = "US$ " + (price?.freight.amountString ?? "---")
            }
        } else if aCell is NormalSwitchTableViewCell {
            let cell = aCell as! NormalSwitchTableViewCell
            cell.config(useBalance: useBalance, balance: balance, total: checkedout?.balance ?? 0) { isOn in
                if isOn {
                    self.balance = self.checkedout?.balance ?? 0
                    self.setBalance() // contains calculate
                } else {
                    self.useBalance = false
                    self.balance = 0
                    self.tableView.reloadData()
                    self.calculate(consigneeIndex: self.selectedConsigneeIndex,
                                   paymentIndex: self.selectedPaymentMethodIndex,
                                   shippingIndex: self.selectedShippingMethodIndex)
                }
            }
        } else {
            let cell = aCell as! CheckboxTableViewCell
            if indexPath.section == 1 {
                let method = checkedout?.paymentMethods.object(at: indexPath.row)
                cell.briefLabel.text = method?.name
                cell.checkbox.isSelected = (indexPath.row == selectedPaymentMethodIndex)
            } else {
                let method = checkedout?.shippingMethods.object(at: indexPath.row)
                cell.briefLabel.text = method?.name
                cell.checkbox.isSelected = (indexPath.row == selectedShippingMethodIndex)
            }
        }
    }
    
    func setBalance() {
        let max = fmin((price?.price ?? 0) + (price?.freight ?? 0), self.checkedout?.balance ?? 0)
        let field = JoyupNotice.FieldConfiguration(max.amountString, placeholder: Localizable.Cart.useBalance, keyboard: .decimalPad)
        
        JoyupNotice.showFields(Localizable.Cart.useBalance, title: Localizable.Cart.useBalance, fields: [field], buttons: [Localizable.Generic.done], action: { (index, oops) in
            let input = oops.textFields.first?.text?.floatValue ?? 0
            let currentBalance = self.checkedout?.balance ?? 0
            self.balance = fmin(fmin(currentBalance, max), input)
            self.useBalance = true
            oops.hideView()
            self.tableView.reloadData()
            self.calculate(consigneeIndex: self.selectedConsigneeIndex,
                           paymentIndex: self.selectedPaymentMethodIndex,
                           shippingIndex: self.selectedShippingMethodIndex)
        }) { (oops) in
            self.balance = 0
            self.useBalance = false
            self.tableView.reloadData()
        }
    }
    
    public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch indexPath.section {
        case 0:
            if let cns = checkedout?.consignee, let _ = cns.first {
                let next = ConsigneeSelectionViewController(consignees: cns, callback: { (cons, c) in
                    self.checkedout?.consignee.removeAll()
                    self.checkedout?.consignee.append(contentsOf: cons)
                    let list = self.checkedout?.consignee ?? []
                    for cs in list {
                        if let oid = cs.id, let cid = c.id, oid == cid, let idx = list.index(of: cs) {
                            self.selectedConsigneeIndex = idx
                            self.tableView.reloadData()
                            self.calculate(consigneeIndex: idx,
                                           paymentIndex: self.selectedPaymentMethodIndex,
                                           shippingIndex: self.selectedShippingMethodIndex)
                            break
                        }
                    }
                })
                navigationController?.pushViewController(next, animated: true)
            } else {
                let next = ConsigneeCreationViewController(onCreation: {
                    self.checkedout = nil
                    self.orderCheckout(autoCalculating: true)
                })
                navigationController?.pushViewController(next, animated: true)
            }
            break
        case 1:
            if indexPath.row >= (checkedout?.paymentMethods.count ?? 0) {
                
            } else {
                calculate(consigneeIndex: selectedConsigneeIndex,
                          paymentIndex: indexPath.row,
                          shippingIndex: selectedShippingMethodIndex)
            }
            break
        case 2:
            balance = 0
            useBalance = false
            calculate(consigneeIndex: selectedConsigneeIndex,
                      paymentIndex: selectedPaymentMethodIndex,
                      shippingIndex: indexPath.row)
            break
        default:
            
            break
        }
    }
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0:
            return checkedout != nil ? 1 : 0
        case 1:
            return (checkedout?.paymentMethods.count ?? 0) + 1
        case 2:
            return checkedout?.shippingMethods.count ?? 0
        case 3:
            return 2
        default:
            return checkedout?.orderItems.count ?? 0
        }
    }
    
    public func numberOfSections(in tableView: UITableView) -> Int {
        return 5
    }
    
    //    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
    //        return 8
    //    }
    
    //    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
    //        let header = UIView()
    //        header.backgroundColor = UIColor.groupTableViewBackground
    //        return header
    //    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        switch section {
        case 1:
            return "Payment Method".localized()
        case 2:
            return (checkedout?.shippingMethods.count ?? 0) > 0 ? "Shipping Method".localized(): nil
        case 3:
            return Localizable.Generic.expenses
        case 4:
            return Localizable.Cart.title
        default:
            return nil
        }
    }
    
    public func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let id = self.cellIdentifier(at: indexPath)
        return tableView.fd_heightForCell(withIdentifier: id, configuration: { (cell) in
            self.config(cell: cell as! UITableViewCell, at: indexPath)
        })
    }
}
