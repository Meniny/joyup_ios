//
//  CartManager.swift
//  Joyup
//
//  Created by Meniny on 2017-07-15.
//  Copyright © 2017年 Meniny. All rights reserved.
//

import Foundation
import UIKit
import Jsonify
import CocoaHelper
import EasyGCD

public class CartManager {
    
    public typealias ResultClosure = (_ success: Bool, _ error: String?) -> Swift.Void
    
    public static var shared: CartManager = CartManager()
    
    public var totalPVPoint: Int = 0
    public var totalPrice: Float = 0
    public var totalQuantity: Int = 0
    public var products: [JoyupProduct] = []
    
    init() {
    }
    
//    public static var pullSemaphore = EasyGCD.semaphore(1)
//    public static var addingSemaphore = EasyGCD.semaphore(2)
//    public static var removingSemaphore = EasyGCD.semaphore(3)
//    public static var changingSemaphore = EasyGCD.semaphore(4)
    
    public static func containsProduct(id: String) -> (cartItemID: String?, quantity: Int)? {
        for p in shared.products {
            if p.id == id {
                return (cartItemID: p.cartItemID, quantity: p.quantity)
            }
        }
        return nil
    }
    
    public static func pullCartList(_ completion: CartManager.ResultClosure?) {
        
//        pullSemaphore.wait()
        
        guard let _ = JoyupUser.main?.token else {
            completion?(false, nil)
//            pullSemaphore.signal()
            return
        }
        Request.cart.request(params: nil, completion: { (json, _, _) in
            if let error = Request.checkResult(json) {
                completion?(false, error)
            } else {
                shared.removeAll()
                for dj in json["data"].arrayValue {
                    shared.add(product: JoyupProduct(json: dj))
                }
                shared.update()
                completion?(true, nil)
            }
//            pullSemaphore.signal()
        }) { (_, error) in
            completion?(false, error.localizedDescription)
//            pullSemaphore.signal()
        }
    }
    
    func set(quantity: Int, at index: Int) {
        if index >= 0 && index < products.count {
            if quantity > 0 {
                totalQuantity -= products[index].quantity
                products[index].quantity = quantity
                totalQuantity += quantity
                totalPrice += products[index].price * Float(quantity)
                totalPVPoint += products[index].PVPoint * quantity
            } else {
                remove(productAt: index)
            }
        }
    }
    
    func add(product: JoyupProduct, quantity: Int = 1) {
        self.products.append(product)
        self.set(quantity: quantity, at: product.quantity - 1)
    }
    
    func addRequest(product: JoyupProduct, quantity: Int = 1, completion: CartManager.ResultClosure?) {
        
//        CartManager.addingSemaphore.wait()
        
        guard let _ = JoyupUser.main?.token else {
            completion?(false, nil)
//            CartManager.addingSemaphore.signal()
            return
        }
        Request.addToCart(product.id, quantity).request(params: nil, completion: { (json, _, _) in
            if let error = Request.checkResult(json) {
                completion?(false, error)
            } else {
                self.add(product: product, quantity: quantity)
                completion?(true, nil)
                NotificationCenter.default.post(name: Joyup.updateCartListNotification, object: nil)
            }
//            CartManager.addingSemaphore.signal()
        }) { (_, error) in
            completion?(false, error.localizedDescription)
//            CartManager.addingSemaphore.signal()
        }
        
    }
    
    private func remove(productAt index: Int) {
        if index >= 0 && index < products.count {
            products.remove(at: index)
            update()
        }
    }
    
    func remove(cartItemID: String) {
        for i in 0..<products.count {
            if let id = products.object(at: i)?.cartItemID {
                if id == cartItemID {
                    products.remove(at: i)
                    update()
                    break
                }
            }
        }
    }
    
    func removeRequest(cartItemID: String, completion: CartManager.ResultClosure?) {
//        CartManager.removingSemaphore.wait()
        
        guard let _ = JoyupUser.main?.token else {
            completion?(false, nil)
//            CartManager.removingSemaphore.signal()
            return
        }
        
        Request.removeFromCart(cartItemID).request(params: nil, completion: { (json, _, _) in
            if let error = Request.checkResult(json) {
                completion?(false, error)
            } else {
                CartManager.shared.remove(cartItemID: cartItemID)
                completion?(true, nil)
            }
//            CartManager.removingSemaphore.signal()
        }) { (_, error) in
            completion?(false, error.localizedDescription)
//            CartManager.removingSemaphore.signal()
        }
    }
    
    func removeAll() {
        products.removeAll()
        update()
    }
    
    func changingRequest(cartItemID: String, quantity: Int, completion: CartManager.ResultClosure?) {
//        CartManager.changingSemaphore.wait()
        
        guard let _ = JoyupUser.main?.token else {
            completion?(false, nil)
//            CartManager.changingSemaphore.signal()
            return
        }
        
        Request.editCart(cartItemID, quantity).request(params: nil, completion: { (json, _, _) in
            if let error = Request.checkResult(json) {
                completion?(false, error)
            } else {
                completion?(true, nil)
            }
//            CartManager.changingSemaphore.signal()
        }) { (_, error) in
            completion?(false, error.localizedDescription)
//            CartManager.changingSemaphore.signal()
        }
    }
    
    func set(check status: Bool, at index: Int) {
        products[index].checked = status
        let quantity = products[index].quantity
        let price = products[index].price * Float(quantity)
        let pv = products[index].PVPoint * quantity
        if status {
            totalPrice += price
            totalQuantity += quantity
            totalPVPoint += pv
        } else {
            totalPrice -= price
            totalQuantity -= quantity
            totalPVPoint -= pv
        }
    }
    
    func set(allCheck status: Bool) {
        for i in 0..<products.count {
            products[i].checked = status
        }
        if status {
            update()
        } else {
            totalPrice = 0
            totalQuantity = 0
            totalPVPoint = 0
        }
    }
    
    func update() {
        var price: Float = 0
        var quantity: Int = 0
        var pvpoint: Int = 0
        for i in 0..<products.count {
            if products[i].checked {
                let q = products[i].quantity
                let p = products[i].price
                let pv = products[i].PVPoint
                price += (p * Float(q))
                quantity += q
                pvpoint += (pv * q)
            }
        }
        totalPrice = price
        totalQuantity = quantity
        totalPVPoint = pvpoint
    }
}
