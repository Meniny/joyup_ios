//
//  CartTableViewCell.swift
//  Joyup
//
//  Created by Meniny on 2017-07-15.
//  Copyright © 2017年 Meniny. All rights reserved.
//

import UIKit
import ColaExpression

public class CartTableViewCell: UITableViewCell {

//    @IBOutlet public weak var checkButton: UIButton!
    @IBOutlet public weak var previewView: UIImageView!
    @IBOutlet public weak var titleLabel: UILabel!
    @IBOutlet public weak var priceLabel: UILabel!
    @IBOutlet public weak var countField: UITextField!
    @IBOutlet public weak var reduceButton: UIButton!
    @IBOutlet public weak var addButton: UIButton!
    
    @IBOutlet weak var noticeLabel: UILabel!
//    public var onCheckStatusChangeClosure: ((_ checked: Bool) -> Void)?
    public var onQuantityChangeClosure: ((_ quantity: Int) -> Bool)?
    
//    public var checked: Bool {
//        return checkButton.isSelected
//    }
    
    public override func setup() {
        super.setup()
        reduceButton.imageView?.contentMode = .scaleToFill
        addButton.imageView?.contentMode = .scaleToFill
        countField.isEnabled = false
    }
    
    override public func awakeFromNib() {
        super.awakeFromNib()
    }

//    @IBAction private func checkAction(_ sender: UIButton) {
//        sender.isSelected = !sender.isSelected
//        onCheckStatusChangeClosure?(checked)
//    }
    
    @IBAction private func reduceAction(_ sender: UIButton) {
        let count = getQuantity()
        if count > 1 {
            _ = onQuantityChangeClosure?(count - 1)
        }
    }
    
    @IBAction private func addAction(_ sender: UIButton) {
        let count = getQuantity()
        _ = onQuantityChangeClosure?(count + 1)
    }
    
    public func getQuantity() -> Int {
        if let quantity = countField.text?.integerValue {
            return quantity
        } else {
            return 1
        }
    }
    
}
