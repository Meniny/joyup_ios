//
//  CategoriesViewController.swift
//  Joyup
//
//  Created by Meniny on 2017-07-12.
//  Copyright © 2017年 Meniny. All rights reserved.
//

import UIKit
import Jsonify
import CocoaHelper
import EasyGCD

class CategoriesViewController: BaseViewController {

    open var categoriesTableView: BaseTableView = BaseTableView()
    open var subCategoriesTableView: BaseTableView = BaseTableView()
    
    var categories: [JoyupCategory] = []
    
    var selectedCategoryIndex: Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()

        title = Localizable.Categories.title
        
        view.addSubview(categoriesTableView)
        categoriesTableView.tableFooterView = UIView()
        categoriesTableView.snp.makeConstraints { (make) in
            make.top.equalTo(0)
            make.bottom.equalTo(0)
            make.left.equalTo(0)
            make.width.equalTo(self.view.snp.width).multipliedBy(0.25)
        }
        
        view.addSubview(subCategoriesTableView)
        subCategoriesTableView.tableFooterView = UIView()
        subCategoriesTableView.snp.makeConstraints { (make) in
            make.top.equalTo(0)
            make.bottom.equalTo(0)
            make.left.equalTo(self.categoriesTableView.snp.right)
            make.right.equalTo(0)
        }
        
        categoriesTableView.separatorStyle = .none
        categoriesTableView.register(cell: CategoryTableViewCell.self)
        categoriesTableView.dataSource = self
        categoriesTableView.delegate = self
        
        subCategoriesTableView.separatorStyle = .singleLine
        subCategoriesTableView.register(cell: SubCategoriesTableViewCell.self)
        subCategoriesTableView.dataSource = self
        subCategoriesTableView.delegate = self
        
        view.bringSubview(toFront: categoriesTableView)
        categoriesTableView.enableShadow(.black, offset: 1, y: 0)
        
        request()
        
        navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .refresh, target: self, action: #selector(request))
    }
    
    func request() {
//        let loading = JoyupNotice.showLoading(nil)
        Request.categories.request(params: nil, completion: { (json, _, _) in
            if let message = Request.checkResult(json) {
//                loading.hide()
                JoyupNotice.showError(message)
            } else {
                self.categories.removeAll()
                for js in json["data"].arrayValue {
                    self.categories.append(JoyupCategory(js))
                }
                main {
                    self.categoriesTableView.reloadData()
                    self.subCategoriesTableView.reloadData()
//                    loading.hide()
                }
            }
        }) { (resp, error) in
//            loading.hide()
            JoyupNotice.showFireError(error, response: resp)
        }
        
        self.selectedCategoryIndex = 0
        self.categoriesTableView.reloadData()
        self.subCategoriesTableView.reloadData()
        
        self.categoriesTableView.selectRow(at: IndexPath(row: selectedCategoryIndex, section: 0), animated: true, scrollPosition: .none)
    }
    
    override func localizationDidChange() {
        super.localizationDidChange()
        title = Localizable.Categories.title
        request()
    }
}

extension CategoriesViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if tableView == categoriesTableView {
            selectedCategoryIndex = indexPath.row
            tableView.reloadData()
            subCategoriesTableView.reloadData()
        } else {
            if let p = categories.object(at: selectedCategoryIndex)?.sublist.object(at: indexPath.row),
                let id = p.productCategoryID {
                let detail = ProductsListViewController(categoriesID: "\(id)")
                detail.title = p.name
                self.navigationController?.pushViewController(detail, animated: true)
            }
        }
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let id = self.cellIdentifier(at: indexPath, of: tableView)
        let cell = tableView.dequeueReusableCell(withIdentifier: id)
        self.config(cell: cell!, at: indexPath)
        return cell!
    }
    
    func cellIdentifier(at indexPath: IndexPath, of tableView: UITableView) -> String {
        if tableView == categoriesTableView {
            return CategoryTableViewCell.nameOfClass
        }
        return SubCategoriesTableViewCell.nameOfClass
    }
    
    func config(cell aCell: UITableViewCell, at indexPath: IndexPath) {
        aCell.clipsToBounds = true
        aCell.accessoryType = .none
        aCell.selectionStyle = .none
        
        if aCell is CategoryTableViewCell {
            
            let cate = categories[indexPath.row]
            let cell = aCell as! CategoryTableViewCell
            cell.categoryLabel.text = cate.name
            cell.isSelected = indexPath.row == selectedCategoryIndex
        } else {
            
            let cell = aCell as! SubCategoriesTableViewCell
            if let p = categories.object(at: selectedCategoryIndex)?.sublist.object(at: indexPath.row) {
                cell.previewView.webImage(url: p.image, placeholderImage: nil)
                cell.nameLabel.text = p.name
            } else {
                cell.previewView.image = nil
                cell.nameLabel.text = ""
            }
        }
    }
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == categoriesTableView {
            return categories.count
        }
        return categories.object(at: selectedCategoryIndex)?.sublist.count ?? 0
    }
    
    public func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let id = self.cellIdentifier(at: indexPath, of: tableView)
        return tableView.fd_heightForCell(withIdentifier: id, configuration: { (cell) in
            self.config(cell: cell as! UITableViewCell, at: indexPath)
        })
    }
}
