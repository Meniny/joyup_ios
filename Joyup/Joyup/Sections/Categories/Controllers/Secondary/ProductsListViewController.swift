//
//  ProductsListViewController.swift
//  Joyup
//
//  Created by Meniny on 2017-07-29.
//  Copyright © 2017年 Meniny. All rights reserved.
//

import UIKit
import CocoaHelper
import EasyGCD
import Jsonify
import Fire
import SVPullToRefresh

class ProductsListViewController: BaseTableViewController {

    let categoriesID: String
    
    public init(categoriesID id: String) {
        categoriesID = id
        super.init()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    var products: [JoyupProduct] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if title == nil {
            title = Localizable.Categories.title
        }
        
        tableView.separatorStyle = .singleLine
        tableView.register(cell: ProductListTableViewCell.self)
        tableView.dataSource = self
        tableView.delegate = self
        
        
        tableView.addPullToRefresh {
            self.request()
        }
        request()
      
//        tableView.addInfiniteScrolling {
//            self.request(loadMore: true)
//        }
    }
    
    func request(loadMore: Bool = false) {
        self.tableView.pullToRefreshView.startAnimating()
//        let loading = JoyupNotice.showLoading(nil)
        Request.products(categoriesID).request(params: ["page": loadMore ? page + 1: firstPage, "pageSize": pageSize], completion: { (json, _, _) in
            self.tableView.pullToRefreshView.stopAnimating()
//            self.tableView.infiniteScrollingView.stopAnimating()
            if let message = Request.checkResult(json) {
//                loading.hide()
                JoyupNotice.showError(message)
            } else {
                if loadMore {
                    self.page += 1
                } else {
                    self.products.removeAll()
                    self.page = self.firstPage
                }
                for js in json["data"].arrayValue {
                    self.products.append(JoyupProduct(good: js))
                }
                main {
                    self.tableView.reloadData()
//                    loading.hide()
                }
            }
        }) { (resp, error) in
            self.tableView.pullToRefreshView.stopAnimating()
//            self.tableView.infiniteScrollingView.stopAnimating()
//            loading.hide()
            JoyupNotice.showFireError(error, response: resp)
        }
    }
}

extension ProductsListViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let p = products.object(at: indexPath.row) {
            let next = ProductDetailViewController(id: p.id)
            navigationController?.pushViewController(next, animated: true)
        }
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let id = self.cellIdentifier(at: indexPath, of: tableView)
        let cell = tableView.dequeueReusableCell(withIdentifier: id)
        self.config(cell: cell!, at: indexPath)
        return cell!
    }
    
    func cellIdentifier(at indexPath: IndexPath, of tableView: UITableView) -> String {
        return ProductListTableViewCell.nameOfClass
    }
    
    func config(cell aCell: UITableViewCell, at indexPath: IndexPath) {
        aCell.clipsToBounds = true
        aCell.accessoryType = .none
        aCell.selectionStyle = .none
        
        let cell = aCell as! ProductListTableViewCell
        cell.config(with: products.object(at: indexPath.row))
    }
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return products.count
    }
    
    public func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let id = self.cellIdentifier(at: indexPath, of: tableView)
        return tableView.fd_heightForCell(withIdentifier: id, configuration: { (cell) in
            self.config(cell: cell as! UITableViewCell, at: indexPath)
        })
    }
}
