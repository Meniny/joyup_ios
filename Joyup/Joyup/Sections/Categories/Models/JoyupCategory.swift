//
//  JoyupCategory.swift
//  Joyup
//
//  Created by Meniny on 2017-07-18.
//  Copyright © 2017年 Meniny. All rights reserved.
//

import Foundation
import Jsonify
import CocoaHelper
import ColaExpression

public struct JoyupCategory {
    public let id: String
    public let name: String
    public let image: String
    public let sublist: [JoyupCategory]
    public let productCategoryID: Int?
    
    init(_ json: Jsonify) {
        id = json["id"].stringValue
        image = json["image"].stringValue
        name = json["name"].stringValue.replace("（", with: "\n(").replace("）", with: ")")
        productCategoryID = json["productCategoryId"].int
        var sub = [JoyupCategory]()
        for js in json["children"].arrayValue {
            sub.append(JoyupCategory(js))
        }
        sublist = sub
    }
}
