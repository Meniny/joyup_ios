//
//  CategoryTableViewCell.swift
//  Joyup
//
//  Created by Meniny on 2017-07-18.
//  Copyright © 2017年 Meniny. All rights reserved.
//

import UIKit

class CategoryTableViewCell: UITableViewCell {

    @IBOutlet weak var categoryLabel: UILabel!
    @IBOutlet weak var selectionView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    
    override var isSelected: Bool {
        didSet {
            categoryLabel.textColor = isSelected ? UIColor.cerulean : UIColor.black
            selectionView.backgroundColor = isSelected ? UIColor.cerulean : UIColor.clear
        }
    }
}
