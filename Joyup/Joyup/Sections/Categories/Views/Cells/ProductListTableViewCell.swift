//
//  ProductListTableViewCell.swift
//  Joyup
//
//  Created by Meniny on 2017-07-29.
//  Copyright © 2017年 Meniny. All rights reserved.
//

import UIKit

class ProductListTableViewCell: UITableViewCell {
    @IBOutlet weak var previewView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var pvLabel: UILabel!
    @IBOutlet weak var pvTitleLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    public func config(with item: OrderCheckoutInfo.OrderItem?) {
        previewView.webImage(url: item?.thumbnail ?? "")
        nameLabel.text = item?.name ?? "N/A"
        priceLabel.text = item?.price.amountString ?? "N/A"
        pvLabel.text = "\(item?.quantity ?? 1)"
        pvTitleLabel.text = "×"
    }
    
    public func config(with product: JoyupOrderDetail.OrderItem?) {
        previewView.webImage(url: product?.thumbnail ?? "")
        nameLabel.text = product?.name ?? "N/A"
        priceLabel.text = product?.price.amountString ?? "N/A"
        let pv = product?.pv ?? 0
        if pv > 0 {
            pvLabel.text = "\(pv)"
            pvTitleLabel.text = "PV"
        } else {
            pvLabel.text = ""
            pvTitleLabel.text = ""
        }
    }
    
    public func config(with product: JoyupProduct?) {
        previewView.webImage(url: product?.image ?? "")
        nameLabel.text = product?.title ?? "N/A"
        priceLabel.text = product?.price.amountString ?? "N/A"
        let pv = product?.PVPoint ?? 0
        if pv > 0 {
            pvLabel.text = "\(pv)"
            pvTitleLabel.text = "PV"
        } else {
            pvLabel.text = ""
            pvTitleLabel.text = ""
        }
    }
    
}
