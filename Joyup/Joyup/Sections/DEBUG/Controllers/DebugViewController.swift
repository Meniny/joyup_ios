//
//  ProductDetailViewController.swift
//  Joyup
//
//  Created by Meniny on 2017-07-13.
//  Copyright © 2017年 Meniny. All rights reserved.
//

import UIKit
import Imager
import Formal
import Fire
import Jsonify
import EasyGCD
import ColaExpression
import InfoPlist
import CocoaHelper

open class DebugViewController: FormalViewController {

    var paramsHolder: Request.Params = [:]
    
    public init() {
        super.init(style: .grouped)
    }
    
    public override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
    }
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    var baseURL: URL? = URL(string: Request.baseURL!)
    
    override open func viewDidLoad() {
        super.viewDidLoad()

        title = "DEBUG"
        
        formal +++ FormalSection("环境")
            <<< FormalLabelRow() {
                #if DEBUG
                $0.title = "DEBUG"
                #else
                $0.title = "RELEASE"
                #endif
                $0.value = InfoPlist.version + " (" + InfoPlist.build + ")"
        }
        
        formal +++ FormalSection()
            <<< FormalURLFloatingFieldRow() {
                $0.placeholder = "Base URL Prefix"
                $0.value = self.baseURL
                }.onChange({ (row) in
                    self.baseURL = row.value
                })
            <<< FormalButtonRow() {
                $0.title = "Use this URL"
                }.onCellSelection({ (_, _) in
                    self.setNewBaseURL()
                })
            <<< FormalButtonRow() {
                $0.title = "Use [DEBUG] \(Request.DEBUGBaseURL)"
                }.onCellSelection({ (_, _) in
                    self.baseURL = URL(string: Request.DEBUGBaseURL)
                    self.setNewBaseURL()
                })
            <<< FormalButtonRow() {
                $0.title = "Use [RELEASE] \(Request.RELEASEBaseURL)"
                }.onCellSelection({ (_, _) in
                    self.baseURL = URL(string: Request.RELEASEBaseURL)
                    self.setNewBaseURL()
                })
        
        let documentUrl = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
        if let contents = try? FileManager.default.contentsOfDirectory(atPath: documentUrl.path) {
            var logs = [String]()
            for item in contents {
                if item.hasPrefix("OhCrap") {
                    logs.append(item)
                }
            }
            if !logs.isEmpty {
                let section = FormalSection("Crash Logs (\(logs.count))")
                formal +++ section
                for item in logs {
                    if item.hasPrefix("OhCrap") {
                        section <<< FormalLabelRow() {
                            $0.title = item
                            }.onCellSelection({ (cell, _) in
                                if let str = try? String(contentsOf: documentUrl.appendingPathComponent(item), encoding: .utf8) {
                                    JoyupNotice.showOptions(str, title: item, options: ["Copy", "Share"], action: { (idx, opt) in
                                        if opt == "Copy" {
                                            UIPasteboard.general.string = str
                                        } else if opt == "Share" {
                                            self.share(items: [item, str], sourceView: cell, sourceRect: cell.frame, completion: nil)
                                        }
                                    })
                                    
                                } else {
                                    JoyupNotice.showError(Localizable.Generic.unknownError)
                                }
                            })
                    }
                }
            }
        }
        
    }
    
    func setNewBaseURL() {
        if let u = self.baseURL {
            UserDefaults.standard.set(u.absoluteString, forKey: Request.baseURLStoredKey)
            UserDefaults.standard.synchronize()
            Request.baseURL = u.absoluteString
            JoyupNotice.showConfirm(Localizable.Generic.successTitle, message: "Better quit the app now", leftButton: Localizable.Generic.yes, leftAction: {
                exit(0)
            }, rightButton: Localizable.Generic.notNow, rightAction: nil)
        } else {
            JoyupNotice.showError(Localizable.Generic.unknownError)
        }
    }
}
