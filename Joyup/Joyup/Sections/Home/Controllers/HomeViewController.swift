//
//  HomeViewController.swift
//  Joyup
//
//  Created by Meniny on 2017-07-12.
//  Copyright © 2017年 Meniny. All rights reserved.
//

import UIKit
import Jsonify
import EasyGCD
import SVPullToRefresh
import Localization
import Imagery
import ImageFactory
import Settings
import CocoaHelper
import Essence

public enum HomeListItemType {
    case banner, icons, products
}

public protocol HomeListItem {
    var itemType: HomeListItemType { get set }
    var id: String { get set }
    var image: String { get set }
    var url: String { get set }
    var title: String { get set }
    var name: String? { get set }
}

class HomeViewController: BaseTableViewController {

    var searchBar: UISearchBar = UISearchBar(frame: CGRect(x: 0, y: 0, width: 200, height: 44))
    
    var dataSource: [[HomeListItem]] = []
    
    var semaphore = EasyGCD.semaphore(1)
    
    var languageItem: UIBarButtonItem?
    var userItem: UIBarButtonItem?
    var scannerItem: UIBarButtonItem?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        title = Localizable.Home.title
        
        searchBar.barStyle = .blackTranslucent
        searchBar.tintColor = UIColor.clear
        navigationItem.titleView = searchBar
        searchBar.placeholder = Localizable.Generic.search
        searchBar.delegate = self
        
        tableView.register(cell: BannerTableViewCell.self, useNib: false, forCellReuseIdentifier: BannerTableViewCell.nameOfClass)
        tableView.register(cell: ScrollingProductsTableViewCell.self)
        tableView.register(cell: HomeIconsTableViewCell.self)
        tableView.delegate = self
        tableView.dataSource = self
        
        tableView.addPullToRefresh {
            self.request(forMore: false)
        }
        
        request(forMore: false)
        
        languageItem = UIBarButtonItem(image: Joyup.domain().image, style: .plain, target: self, action: #selector(selectJoyupArea))
        userItem = UIBarButtonItem(image: #imageLiteral(resourceName: "profile"), style: .plain, target: self, action: #selector(didTapUserAvatar))
        navigationItem.rightBarButtonItems = [userItem!, languageItem!]
        
        scannerItem = UIBarButtonItem(image: #imageLiteral(resourceName: "qrcode"), style: .plain, target: self, action: #selector(showQRCodeScanner))
        #if DEBUG
            navigationItem.leftBarButtonItems = [
                UIBarButtonItem(title: "DEBUG", style: .plain, target: self, action: #selector(showDebuggingPanel)),
                scannerItem!
            ]
        #else
            navigationItem.leftBarButtonItem = scannerItem
        #endif
    }
    
    func showDebuggingPanel() {
        let next = DebugViewController()
        self.navigationController?.pushViewController(next, animated: true)
    }
    
    func showQRCodeScanner() {
        guard JoyupUser.signedIn else {
            JoyupNotice.signIn()
            return
        }
        var style = MXScanViewStyle()
        style.centerUpOffset = 44;
        style.photoframeAngleStyle = MXScanViewPhotoframeAngleStyle.inner;
        style.photoframeLineW = 3;
        style.photoframeAngleW = 18;
        style.photoframeAngleH = 18;
        style.isNeedShowRetangle = false;
        style.anmiationStyle = MXScanViewAnimationStyle.lineMove;
        style.animationImage = #imageLiteral(resourceName: "qrcode_scan_light_green")
        
        let scanner = MXScanViewController();
        scanner.scanStyle = style
        scanner.scanResultDelegate = self
        
        let next = NavigationController(rootViewController: scanner)
        self.navigationController?.present(next, animated: true, completion: nil)
    }
    
    func didTapUserAvatar() {
        if JoyupUser.signedIn {
            tabBarController?.selectedIndex = 3
        } else {
            JoyupNotice.signIn()
        }
    }
    
    override func userDidSignedOut() {
        super.userDidSignedOut()
        userItem = UIBarButtonItem(image: #imageLiteral(resourceName: "profile"), style: .plain, target: self, action: #selector(didTapUserAvatar))
        navigationItem.rightBarButtonItems = [userItem!, languageItem!]
    }
    
    override func userDidSignedIn() {
        super.userDidSignedIn()
        userItem = UIBarButtonItem(title: JoyupUser.main?.name ?? "AlreadSignedIn".localized(), style: .plain, target: self, action: #selector(didTapUserAvatar))
        navigationItem.rightBarButtonItems = [userItem!, languageItem!]
//        if let avatar = JoyupUser.main?.avatar, avatar.isNotEmpty, let url = URL(string: avatar)  {
//            ImageryManager.shared.downloader.downloadImage(with: url, retrieveImageTask: nil, options: nil, progressBlock: nil, completionHandler: { (img, error, _, _) in
//                if let image = img {
//                    self.setAvatar(image)
//                }
//            })
//        } else {
//            let text: String
//            if let name = JoyupUser.main?.name {
//                text = name.isEmpty ? "J" : name.firstCharacter().uppercased()
//            } else {
//                text = "J"
//            }
////            self.setAvatar(IconCreator.create(with: text, length: 300))
//            let size = CGSize(width: 22, height: 22)
//            UIGraphicsBeginImageContext(size)
////            let context = UIGraphicsGetCurrentContext()
//            let rect = CGRect(origin: .zero, size: size)
////            context?.addRect(rect)
////            context?.setFillColor(UIColor.white.cgColor)
////            context?.fillPath()
//            (text as NSString).draw(in: rect, withAttributes: [
//                NSFontAttributeName: UIFont.boldSystemFont(ofSize: size.width),
//                NSForegroundColorAttributeName: UIColor.black
//                ])
//            let image = UIGraphicsGetImageFromCurrentImageContext()
//            UIGraphicsEndImageContext()
//            if let image = image {
//                setAvatar(image)
//            }
//        }
    }
    
    func setAvatar(_ avatar: UIImage) {
        if avatar.size.width > 22 {
            userItem?.image = avatar.scale(to: CGSize(width: 22, height: 22)) ?? #imageLiteral(resourceName: "profile")
        } else {
            userItem?.image = avatar.addBorder(width: 2, color: UIColor.white) ?? avatar
        }
    }
    
    func selectJoyupArea() {
        Request.getRSAPublicKey.request(params: nil, completion: nil, error: nil)
        
        let options = [
            Localizable.User.americaEnglish,
            Localizable.User.americaChinese,
            Localizable.User.chinaChinese
        ]
        JoyupNotice.showOptions(nil, title: Localizable.Generic.chooseArea, options: options) { (index, option) in
            if let _ = index, let opt = option {
                switch opt {
                case Localizable.User.chinaChinese:
                    let l = Domain.chinaChinese
                    Joyup.store(domain: l)
                    Localization.preferredLanguage = l.language.code
                    break
                case Localizable.User.americaChinese:
                    let l = Domain.americaChinese
                    Joyup.store(domain: l)
                    Localization.preferredLanguage = l.language.code
                    break
                default:
                    let l = Domain.americaEnglish
                    Joyup.store(domain: l)
                    Localization.preferredLanguage = l.language.code
                    break
                }
                NotificationCenter.default.post(name: Joyup.siteChangeNotification, object: nil)
            }
        }
    }
    
    override func joyupAreaDidChange() {
        super.joyupAreaDidChange()
        request(forMore: false)
        languageItem?.image = Joyup.domain().image
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if !once("JoyupAutoSignInOnceToken", {
            JoyupUser.autoSignIn()
            UpdateManager.check { (update, forced, _) in
                if update { UpdateManager.alert(force: forced, completion: { }) }
            }
        }) {
            JoyupUser.autoSignIn()
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        /*
        if ApplePushNotification.registerFailed && !ESDevice.isSimulator {
            once("JoyupAPNsRegisterFailed") {
                main {
                    JoyupNotice.showConfirm(Localizable.Settings.apnsFailed, message: "", leftButton: Localizable.Settings.gotoSettings, leftAction: {
                        Settings.currentApp.open()
                    }, rightButton: Localizable.Generic.notNow, rightAction: nil)
                }
            }
        }
         */
    }
    
    func request(forMore loadMore: Bool) {
        
        semaphore.wait()
        
        tableView.pullToRefreshView.startAnimating()
//        let loading = JoyupNotice.showLoading(nil)
        
        Request.home.request(params: nil, completion: { (json, resp, error) in
            if let message = Request.checkResult(json) {
//                loading.alertView.hideView()
                self.tableView.pullToRefreshView.stopAnimating()
                self.semaphore.signal()
                JoyupNotice.showError(message)
            } else {
                EasyGCD.async(.global(), closure: {
                    self.dataSource.removeAll()
                    let data = json["data"].arrayValue
                    // TODO: THIS JSON DATA IS JUST A MESS, PAIN IN THE ASS
                    
                    // banner
                    if let bannerJSON = data.first {
                        var banner = [HomeListItem]()
                        for js in bannerJSON.arrayValue {
                            let b = BannerData(id: "\(js["adId"].intValue)", title: js["title"].stringValue, image: js["path"].stringValue, url: js["url"].stringValue)
                            banner.append(b)
                        }
                        self.dataSource.append(banner)
                    }
                    
                    // icons
                    if let iconsJSON = data.object(at: 1) {
                        var categories = [HomeListItem]()
                        for js in iconsJSON.arrayValue {
                            var c = BannerData(id: "\(js["id"].int ?? js["adId"].intValue)", title: js["title"].stringValue, image: js["path"].stringValue, url: js["url"].stringValue)
                            c.itemType = .icons
                            categories.append(c)
                        }
                        self.dataSource.append(categories)
                    }
                    
                    // group
                    if data.count > 2 {
                        for idx in 2..<data.count {
                            guard let groupJSON = data.object(at: idx) else {
                                break
                            }
                            var bannerGroup = [HomeListItem]()
                            var goodsGroups = [[HomeListItem]]()
                            
                            for js in groupJSON.arrayValue {
                                if let goods = js["goods"].array {
                                    var group = [HomeListItem]()
                                    for g in goods {
                                        var p = JoyupProduct(good: g)
                                        p.itemType = .products
                                        p.name = js["name"].string
                                        group.append(p)
                                    }
                                    if group.isNotEmpty {
                                        goodsGroups.append(group)
                                    }
                                } else {
                                    var c = BannerData(id: "\(js["adId"].intValue)", title: js["title"].stringValue, image: js["path"].stringValue, url: js["url"].stringValue)
                                    c.name = js["name"].string
                                    bannerGroup.append(c)
                                }
                            }
                            if bannerGroup.isNotEmpty {
                                self.dataSource.append(bannerGroup)
                            }
                            if goodsGroups.isNotEmpty {
                                self.dataSource.append(contentsOf: goodsGroups)
                            }
                        }
                    }
                    self.semaphore.signal()
                    main {
                        self.tableView.pullToRefreshView.stopAnimating()
                        self.tableView.reloadData()
//                        loading.alertView.hideView()
                    }
                })
            }
        }) { (resp, error) in
//            loading.alertView.hideView()
            self.tableView.pullToRefreshView.stopAnimating()
            JoyupNotice.showFireError(error, response: resp)
            self.semaphore.signal()
        }
    }
}

extension HomeViewController: UISearchBarDelegate {
    func searchBarShouldBeginEditing(_ searchBar: UISearchBar) -> Bool {
        let next = SearchViewController()
        navigationController?.pushViewController(next, animated: false)
        return false
    }
}

extension HomeViewController: MXScanViewControllerDelegate {
    func scanViewController(_ scanViewController: MXScanViewController, didFinishScan result: MXScanResult, error: String?) {
        let prefix = "Joyup://"
        guard let string = result.strScanned else {
            scanViewController.scanObj?.start()
            return
        }
        guard string.isNotEmpty else {
            scanViewController.scanObj?.start()
            return
        }
        guard string.hasPrefix(prefix) else {
            JoyupNotice.showMessage(string, action: { [weak scanViewController] (n, _) in
                n.hide()
                scanViewController?.scanObj?.start()
            })
            return
        }
        let info = string.replace(prefix, with: "").components(separatedBy: "/")
        guard info.count == 2, let receiver = info.first, let name = info.last, receiver.isNotEmpty, name.isNotEmpty else {
            JoyupNotice.showMessage(string, action: { [weak scanViewController] (n, _) in
                n.hide()
                scanViewController?.scanObj?.start()
            })
            return
        }
        let next = TransferViewController(receiver: receiver.URLDecodedString, name: name.URLDecodedString)
        scanViewController.navigationController?.pushViewController(next, animated: true)
    }
}

extension HomeViewController: UITableViewDelegate, UITableViewDataSource {
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let id = self.cellIdentifier(at: indexPath)
        let cell = tableView.dequeueReusableCell(withIdentifier: id)
        self.config(cell: cell!, at: indexPath)
        return cell!
    }
    
    func cellIdentifier(at indexPath: IndexPath) -> String {
        if let group = dataSource.object(at: indexPath.section), let item = group.first  {
            switch item.itemType {
            case .banner:
                return BannerTableViewCell.nameOfClass
            case .icons:
                return HomeIconsTableViewCell.nameOfClass
            default:
                break
            }
        }
        return ScrollingProductsTableViewCell.nameOfClass
    }
    
    func config(cell aCell: UITableViewCell, at indexPath: IndexPath) {
        aCell.clipsToBounds = true
        aCell.selectionStyle = .none
        aCell.accessoryType = .none
        
        if let group = dataSource.object(at: indexPath.section), let item = group.first {
            switch item.itemType {
            case .banner:
                let cell = aCell as! BannerTableViewCell
                cell.config(with: group as! [BannerData], action: { [weak self] (b) in
                    self?.nextPage(fromURL: b.url, value: b.id, title: item.title)
                })
                break
            case .icons:
                let cell = aCell as! HomeIconsTableViewCell
                cell.config(with: group, action: { [weak self] (icon) in  
                    self?.nextPage(fromURL: icon.url, value: icon.id, title: icon.title)
                })
                break
            default:
                let cell = aCell as! ScrollingProductsTableViewCell
                cell.config(with: group as! [JoyupProduct], action: { [weak self] (p) in
                    self?.nextPage(fromURL: p.url, value: p.id, title: p.title)
                })
                break
            }
        }
    }
    
    func nextPage(fromURL url: String, value: String, title pageTitle: String?) {
        guard url.isNotEmpty else {
            return
        }
        
        if url.contains("/goods/list/") { // https://us.joyuponline.com/goods/list/287.jhtml
            
            let id = url.matches(pattern: "(?<=/goods/list/)[\\d]+").first ?? value
            let detail = ProductsListViewController(categoriesID: "\(id)")
            detail.title = pageTitle
            self.navigationController?.pushViewController(detail, animated: true)
            
        } else if url.contains("/goods/search.jhtml") { // https://joyuponline.com/goods/search.jhtml?keyword=四川1号
            
            let next = SearchViewController()
            let key = url.matches(pattern: "(?<=keyword=).+").first ?? value
            next.defaultKeyword = key.URLDecodedString
            navigationController?.pushViewController(next, animated: false)
            
        } else if url.contains("/goods/content/") { // https://joyuponline.com/goods/content/201705/551.html
            
            if let match = url.matches(pattern: "\\d+").last, match != url {
                let detail = ProductDetailViewController(id: match)
                self.navigationController?.pushViewController(detail, animated: true)
            }
            
        } else if url.hasSuffix("joyuponline.com") || url.hasSuffix("joyuponline.com/") { // https://us.joyuponline.com/
            // MARK: Goto WebView
//            let next = WebViewController(urlString: url)
//            self.navigationController?.pushViewController(next, animated: true)
        }
        
    }
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    public func numberOfSections(in tableView: UITableView) -> Int {
        return dataSource.count
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if let _ = dataSource.object(at: section)?.first?.name {
            return 16
        }
        return section > 0 ? 8 : 0
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if let groupName = dataSource.object(at: section)?.first?.name {
            let label = UILabel()
            label.text = "  " + groupName
            label.font = UIFont.systemFont(ofSize: 12)
            label.backgroundColor = UIColor.groupTableViewBackground
            label.textColor = UIColor.lightGray
            return label
        }
        if section == 0 {
            let v = UIView()
            v.backgroundColor = UIColor.groupTableViewBackground
            return v
        }
        return nil
    }
    
    public func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if let group = dataSource.object(at: indexPath.section), let item = group.first {
            if item.itemType == .banner {
                return BannerTableViewCell.cellHeight
            }
            if item.itemType == .icons {
                return HomeIconsTableViewCell.collectionViewHeight()
            }
        }
        return ScrollingProductsTableViewCell.cellHeight
    }
}
