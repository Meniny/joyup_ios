//
//  SearchViewController.swift
//  Joyup
//
//  Created by Meniny on 2017-07-22.
//  Copyright © 2017年 Meniny. All rights reserved.
//

import UIKit
import CocoaHelper
import ColaExpression

class SearchViewController: BaseTableViewController {

    var searchBar: UISearchBar = UISearchBar(frame: CGRect(x: 0, y: 0, width: 200, height: 44))
    
    var results: [JoyupProduct] = []
    
    public var defaultKeyword: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        title = Localizable.Generic.search
        
        searchBar.barStyle = .blackTranslucent
        searchBar.tintColor = UIColor.clear
        navigationItem.titleView = searchBar
        searchBar.placeholder = Localizable.Generic.search
        searchBar.delegate = self

        navigationItem.hidesBackButton = true
        navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .cancel, target: self, action: #selector(didmissSearchPanel))
        
        tableView.separatorStyle = .singleLine
        tableView.register(cell: ProductListTableViewCell.self)
        tableView.dataSource = self
        tableView.delegate = self
        
        if let dk = defaultKeyword {
            searchBar.text = dk
            search(dk)
        }
    }
    
    func search(_ text: String?) {
        searchBar.resignFirstResponder()
        let loading = JoyupNotice.showLoading(nil)
        guard let keyword = text, keyword.isNotEmpty else {
            results.removeAll()
            tableView.reloadData()
            loading.hide()
            return
        }
        Request.searchProduct(keyword).request(params: nil, completion: { (json, _, _) in
            if let error = Request.checkResult(json) {
                loading.hide()
                JoyupNotice.showError(error)
            } else {
                self.results.removeAll()
                if let data = json["data"]["page"].arrayValue.first {
                    for js in data["goodsList"].arrayValue {
                        self.results.append(JoyupProduct(good: js))
                    }
                }
                self.tableView.reloadData()
                loading.hide()
            }
        }) { (resp, error) in
            loading.hide()
            JoyupNotice.showFireError(error, response: resp)
        }
    }
    
    var already = false
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if !already && defaultKeyword == nil {
            searchBar.becomeFirstResponder()
            already = true
        }
    }
    
    func didmissSearchPanel() {
        searchBar.resignFirstResponder()
        navigationController?.popViewController(animated: false)
    }

}

extension SearchViewController: UISearchBarDelegate {
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        search(searchBar.text)
    }
}

extension SearchViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let p = results.object(at: indexPath.row) {
            let next = ProductDetailViewController(id: p.id)
            navigationController?.pushViewController(next, animated: true)
        }
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let id = self.cellIdentifier(at: indexPath, of: tableView)
        let cell = tableView.dequeueReusableCell(withIdentifier: id)
        self.config(cell: cell!, at: indexPath)
        return cell!
    }
    
    func cellIdentifier(at indexPath: IndexPath, of tableView: UITableView) -> String {
        return ProductListTableViewCell.nameOfClass
    }
    
    func config(cell aCell: UITableViewCell, at indexPath: IndexPath) {
        aCell.clipsToBounds = true
        aCell.accessoryType = .none
        aCell.selectionStyle = .none
        
        let cell = aCell as! ProductListTableViewCell
        cell.config(with: results.object(at: indexPath.row))
    }
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return results.count
    }
    
    public func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let id = self.cellIdentifier(at: indexPath, of: tableView)
        return tableView.fd_heightForCell(withIdentifier: id, configuration: { (cell) in
            self.config(cell: cell as! UITableViewCell, at: indexPath)
        })
    }
}
