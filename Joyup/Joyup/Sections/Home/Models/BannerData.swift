//
//  BannerData.swift
//  Joyup
//
//  Created by Meniny on 2017-07-18.
//  Copyright © 2017年 Meniny. All rights reserved.
//

import Foundation
import UIKit
import CocoaHelper
import ColaExpression

public struct BannerData: HomeListItem {
    public var name: String?
    public var url: String
    public var itemType: HomeListItemType = .banner
    public var id: String
    public var title: String
    public var image: String
    
    init(product: JoyupProduct) {
        id = product.id
        title = product.title
        image = product.image
        url = product.url
    }
    
    init(id i: String, title t: String, image im: String, url u: String) {
        id = i
        title = t
        image = im
        url = u
    }
    
    public static func empty() -> BannerData {
        return BannerData(id: "", title: "N/A", image: "", url: "")
    }
}
