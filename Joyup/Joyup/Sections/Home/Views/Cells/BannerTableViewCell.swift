//
//  BannerTableViewCell.swift
//  Joyup
//
//  Created by Meniny on 2017-07-18.
//  Copyright © 2017年 Meniny. All rights reserved.
//

import UIKit
import FSPagerView
import SnapKit
import EasyGCD
import CocoaHelper

public typealias BannerClosure = ((_ item: BannerData) -> Swift.Void)
let kBannerHeight: CGFloat = 150


open class BannerTableViewCell: UITableViewCell {
    
    open static var cellHeight: CGFloat {
        return fmin(UIScreen.main.bounds.width, UIScreen.main.bounds.height) / 3.660
    }
    
    lazy open var bannerView:FSPagerView = {
        let b = FSPagerView(frame: CGRect(origin: CGPoint.zero, size: CGSize(width: 0, height: BannerTableViewCell.cellHeight)))
        return b
    }()
    
    var items = [BannerData]()
    
    init() {
//    open override func didMoveToSuperview() {
//        super.didMoveToSuperview()
        super.init(style: .default, reuseIdentifier: BannerTableViewCell.nameOfClass)
        setupViews()
    }
    
    public override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupViews()
    }
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupViews()
    }
    
    func setupViews() {
        self.contentView.addSubview(self.bannerView)
        self.bannerView.register(FSPagerViewCell.self, forCellWithReuseIdentifier: FSPagerViewCell.nameOfClass)
        
        self.bannerView.snp.makeConstraints { (make) in
            make.top.equalTo(0)
            make.left.equalTo(0)
            make.right.equalTo(0)
            make.height.equalTo(BannerTableViewCell.cellHeight)
            make.bottom.greaterThanOrEqualTo(0)
        }
        
        self.bannerView.isInfinite = true
        self.bannerView.delegate = self
        self.bannerView.dataSource = self
    }
    
    var action: BannerClosure?
    
    open func config(with data: [BannerData], action act: @escaping BannerClosure) {
        self.items.removeAll()
        self.items.append(contentsOf: data)
        self.action = act
        main { [weak self] in
            self?.bannerView.reloadData()
        }
    }
}

extension BannerTableViewCell: FSPagerViewDelegate, FSPagerViewDataSource {
    
    // MARK:- FSPagerViewDataSource
    
    public func numberOfItems(in pagerView: FSPagerView) -> Int {
        return self.items.count
    }
    
    public func pagerView(_ pagerView: FSPagerView, cellForItemAt index: Int) -> FSPagerViewCell {
        let cell = pagerView.dequeueReusableCell(withReuseIdentifier: FSPagerViewCell.nameOfClass, at: index)
        let item = items[index]
        cell.imageView?.webImage(url: item.image, placeholder: nil)
        cell.imageView?.contentMode = .scaleAspectFill
        cell.imageView?.clipsToBounds = true
        return cell
    }
    
    // MARK: - FSPagerViewDelegate
    
    public func pagerView(_ pagerView: FSPagerView, didSelectItemAt index: Int) {
        pagerView.deselectItem(at: index, animated: true)
        pagerView.scrollToItem(at: index, animated: true)
        if let act = self.action {
            act(self.items[index])
        }
    }
    
    public func pagerViewDidScroll(_ pagerView: FSPagerView) {}
    
}


