//
//  HomeIconCollectionViewCell.swift
//  Joyup
//
//  Created by Meniny on 2017-07-18.
//  Copyright © 2017年 Meniny. All rights reserved.
//

import UIKit

class HomeIconCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var widthConstraint: NSLayoutConstraint!
    @IBOutlet weak var iconView: UIImageView!
}
