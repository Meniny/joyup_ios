//
//  HomeIconsTableViewCell.swift
//  Joyup
//
//  Created by Meniny on 2017-07-18.
//  Copyright © 2017年 Meniny. All rights reserved.
//

import UIKit

class HomeIconsTableViewCell: UITableViewCell {

    @IBOutlet weak var collectionView: UICollectionView!
    
    typealias HomeIcon = (id: String, image: String)
    
    var action: ((_ icon: HomeListItem) -> Swift.Void)?
    
    var icons: [HomeListItem] = []
    
    open func config(with data: [HomeListItem], action act: @escaping (_ icon: HomeListItem) -> Swift.Void) {
        icons.removeAll()
        icons.append(contentsOf: data)
        action = act
        collectionView.reloadData()
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        let nib = UINib(nibName: HomeIconCollectionViewCell.nameOfClass, bundle: Bundle.main)
        self.collectionView.register(nib, forCellWithReuseIdentifier: HomeIconCollectionViewCell.nameOfClass)
        self.collectionView.bounces = false
        
        self.collectionView.delegate = self
        self.collectionView.dataSource = self
    }    
}

extension HomeIconsTableViewCell: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    public func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        action?(icons[indexPath.item])
    }
    
    public func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: HomeIconCollectionViewCell.nameOfClass, for: indexPath) as! HomeIconCollectionViewCell
        let icon = icons[indexPath.item]
        cell.iconView.webImage(url: icon.image, placeholder: nil)
        return cell
    }
    
    public func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    public func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.icons.count
    }
    
    public func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let l = HomeIconsTableViewCell.itemLength()
        return CGSize(width: l, height: l)
    }
    
    public func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return HomeIconsTableViewCell.margin()
    }
    
    public func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return HomeIconsTableViewCell.margin()
    }
    
    public static func margin(times: UInt = 1) -> CGFloat {
        return CGFloat(times)
    }
    
    public static func itemLength() -> CGFloat {
        let bounds = UIApplication.shared.keyWindow?.bounds ?? UIScreen.main.bounds
        return (fmin(bounds.width, bounds.height) - HomeIconsTableViewCell.margin(times: 5)) / 4
    }
    
    public static func collectionViewHeight() -> CGFloat {
        return itemLength() * 2 + margin(times: 3)
    }
}

