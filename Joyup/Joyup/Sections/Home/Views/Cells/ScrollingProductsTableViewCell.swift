//
//  ScrollingProductsTableViewCell.swift
//  Joyup
//
//  Created by Meniny on 2017-07-18.
//  Copyright © 2017年 Meniny. All rights reserved.
//

import UIKit
import CocoaHelper
//import SDWebImage

public typealias ProductClosure = ((_ product: JoyupProduct) -> Swift.Void)

class ScrollingProductsTableViewCell: UITableViewCell {

    @IBOutlet weak var collectionView: UICollectionView!
    
    var action: ProductClosure?
    var products: [JoyupProduct] = []
    
    override func awakeFromNib() {
        super.awakeFromNib()
        let nib = UINib(nibName: ScrollingProductCollectionViewCell.nameOfClass, bundle: Bundle.main)
        self.collectionView.register(nib, forCellWithReuseIdentifier: ScrollingProductCollectionViewCell.nameOfClass)
        self.collectionView.backgroundColor = UIColor.groupTableViewBackground
        self.collectionView.delegate = self
        self.collectionView.dataSource = self
        self.collectionView.bounces = false
    }
    
    func config(with products: [JoyupProduct], action: @escaping ProductClosure) {
        self.products.removeAll()
        self.products.append(contentsOf: products)
        self.action = action
        collectionView.reloadData()
    }
}

extension ScrollingProductsTableViewCell: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    public func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        action?(products[indexPath.item])
    }
    
    public func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: ScrollingProductCollectionViewCell.nameOfClass, for: indexPath) as! ScrollingProductCollectionViewCell
        let product = products[indexPath.item]
        cell.imageView.webImage(url: product.preview ?? product.image, placeholder: nil)
        cell.nameLabel.text = product.title
        cell.priceLabel.text = String(format: "$%@", product.price.amountString)
        cell.pvLabel.text = "PV\(product.PVPoint)"
        return cell
    }
    
    public func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    public func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.products.count
    }
    
    public func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        //collectionView.bounds.size.height
        return CGSize(width: ScrollingProductsTableViewCell.cellWidth, height: ScrollingProductsTableViewCell.cellHeight)
    }
    
    public static var cellWidth: CGFloat {
        return UIScreen.main.bounds.width / 3
    }
    
    public static var cellHeight: CGFloat {
        return 148
    }
    
    public func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 2
    }
    
    public func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 2
    }
}

