//
//  ProfileViewController.swift
//  Joyup
//
//  Created by Meniny on 2017-07-12.
//  Copyright © 2017年 Meniny. All rights reserved.
//

import UIKit
import SVPullToRefresh

class ProfileViewController: BaseTableViewController {
    
    var tableItems = [TableItem]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.title = Localizable.Profile.title
        
        tableItems.append(contentsOf: [
            TableItem(Localizable.MemberCenter.title, subtitle: nil, action: #selector(gotoMemberCenter), toTarget: self, selectionStyle: .none, accessoryType: .disclosureIndicator),
            TableItem(Localizable.Wallet.title, subtitle: nil, action: #selector(gotoWallet), toTarget: self, selectionStyle: .none, accessoryType: .disclosureIndicator),
            TableItem(Localizable.MyOrders.title, subtitle: nil, action: #selector(gotoOrderList), toTarget: self, selectionStyle: .none, accessoryType: .disclosureIndicator),
            TableItem(Localizable.MyTransferRecords.title, subtitle: nil, action: #selector(gotoTransferRecordsList), toTarget: self, selectionStyle: .none, accessoryType: .disclosureIndicator)
            ]
        )
        
        self.tableView.register(cell: ProfileHeaderTableViewCell.self)
        self.tableView.register(cell: UniversalTextTableViewCell.self)
        
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.separatorStyle = .singleLine
        
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "install"), style: .done, target: self, action: #selector(gotoSettings))
        
        tableView.addPullToRefresh {
            self.request()
        }
        
        request()
    }
    
    override func userDidSignedIn() {
        super.userDidSignedIn()
        request()
    }
    
    override func localizationDidChange() {
        super.localizationDidChange()
        self.title = Localizable.Profile.title
        tableView.reloadData()
    }
    
    func request() {
        Request.userInfo.request(params: nil, completion: { (json, _, _) in
            if let message = Request.checkResult(json) {
                self.tableView.pullToRefreshView.stopAnimating()
                JoyupNotice.showError(message)
            } else {
                JoyupUser.main?.avatar = json["data"]["avatar"].string
                if let name = json["data"]["name"].string {
                    JoyupUser.main?.name = name
                }
                self.tableView.pullToRefreshView.stopAnimating()
                self.tableView.reloadData()
            }
        }) { (resp, error) in
            self.tableView.pullToRefreshView.stopAnimating()
            JoyupNotice.showFireError(error, response: resp)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if !JoyupUser.signedIn {
            self.tabBarController?.selectedIndex = 0
        }
    }
    
    func gotoSettings() {
        let next = SettingsViewController()
        self.navigationController?.pushViewController(next, animated: true)
    }
    
    func gotoMemberCenter() {
        let next = MemberCenterViewController()
        self.navigationController?.pushViewController(next, animated: true)
    }
    
    func gotoWallet() {
        let next = WalletViewController()
        self.navigationController?.pushViewController(next, animated: true)
    }
    
    func gotoOrderList() {
        let next = MyOrdersViewController()
        self.navigationController?.pushViewController(next, animated: true)
    }
    
    func gotoTransferRecordsList() {
        let next = TransferRecordsViewController()
        self.navigationController?.pushViewController(next, animated: true)
    }
}

extension ProfileViewController: UITableViewDelegate, UITableViewDataSource {
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let id = self.cellIdentifier(at: indexPath)
        let cell = tableView.dequeueReusableCell(withIdentifier: id)
        self.config(cell: cell!, at: indexPath)
        return cell!
    }
    
    func cellIdentifier(at indexPath: IndexPath) -> String {
        if indexPath.section == 0 {
            return ProfileHeaderTableViewCell.nameOfClass
        }
        return UniversalTextTableViewCell.nameOfClass
    }
    
    func config(cell aCell: UITableViewCell, at indexPath: IndexPath) {
        aCell.clipsToBounds = true
        
        if indexPath.section == 0 {
            let cell = aCell as! ProfileHeaderTableViewCell
            if let avatar = JoyupUser.main?.avatar {
                cell.avatarView.webImage(url: avatar)
            } else {
                if let name = JoyupUser.main?.name {
                    cell.avatarView.image = IconCreator.create(with: name.isEmpty ? "J" : name.firstCharacter().uppercased())
                } else {
                    cell.avatarView.image = IconCreator.create(with: "J")
                }
            }
            cell.nameLabel.text = JoyupUser.main?.name
            cell.selectionStyle = .none
            cell.accessoryType = .none
        } else {
            let cell = aCell as! UniversalTextTableViewCell
            
            let item = tableItems[indexPath.row]
            
            cell.leftLabel.text = item.title
            cell.rightLabel.text = item.subtitle
            cell.selectionStyle = item.selectionStyle
            cell.accessoryType = item.accessoryType
        }
    }
    
    public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section > 0 {
            let item = tableItems[indexPath.row]
            item.scheduleIfNeeded()            
        }
    }
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 1 {
            return tableItems.count
        }
        return 1
    }
    
    public func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    public func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let id = self.cellIdentifier(at: indexPath)
        return tableView.fd_heightForCell(withIdentifier: id, configuration: { (cell) in
            self.config(cell: cell as! UITableViewCell, at: indexPath)
        })
    }
}
