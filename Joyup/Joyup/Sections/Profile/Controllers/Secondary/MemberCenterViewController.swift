//
//  MemberCenterViewController.swift
//  Joyup
//
//  Created by Meniny on 2017-07-13.
//  Copyright © 2017年 Meniny. All rights reserved.
//

import UIKit
import Formal
import Jsonify
import Today

open class MemberInfo {
    
    open var amount: Float
    open var expireDate: Date?
    open var activeDate: Date?
    open var balance: Float
    open var memberRank: String//AccoutType
    open var remainderPV: Int
    open var needPV: Int
    
    public init(json: Jsonify) {
        memberRank = json["memberRank"].stringValue//AccoutType(rawValue: json["type"].intValue) ?? .member
        amount = json["amount"].floatValue
        let format = "yyyyMMddHHmmss"
        expireDate = Date.date(from: json["expireDate"].stringValue, format: format)
        activeDate = Date.date(from: json["activeDate"].stringValue, format: format)
        balance = json["balance"].floatValue
        remainderPV = json["remainderPV"].intValue
        needPV = json["needPv"].intValue
    }
}

class MemberCenterViewController: FormalViewController {

    var info: MemberInfo?
    
    public init() {
        super.init(style: .grouped)
        hidesBottomBarWhenPushed = Joyup.shouldHidesBottomBarWhenPushed(for: self)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        hidesBottomBarWhenPushed = Joyup.shouldHidesBottomBarWhenPushed(for: self)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.title = Localizable.MemberCenter.title
        
        request()
    }

    func request() {
        Request.memberCenter.request(params: nil, completion: { (json, _, _) in
            if let message = Request.checkResult(json) {
                JoyupNotice.showError(message)
            } else {
                self.info = MemberInfo(json: json["data"])
                self.loadFormal()
            }
        }) { (resp, error) in
            JoyupNotice.showFireError(error, response: resp)
        }
    }
    
    func loadFormal() {
        formal +++ FormalSection()
            <<< FormalLabelRow() {
                $0.title = Localizable.MemberCenter.memberRank
                $0.value = self.info?.memberRank ?? "N/A"
            }
            <<< FormalLabelRow() {
                $0.title = Localizable.MemberCenter.amount
                $0.value = self.info?.amount.amountString ?? "N/A"
            }
            <<< FormalLabelRow() {
                $0.title = Localizable.MemberCenter.balance
                $0.value = self.info?.balance.amountString ?? "N/A"
            }
            <<< FormalLabelRow() {
                $0.title = Localizable.MemberCenter.activeDate
                $0.value = self.info?.activeDate?.formattedString() ?? "N/A"
        }
            <<< FormalLabelRow() {
                $0.title = Localizable.MemberCenter.expireDate
                $0.value = self.info?.expireDate?.formattedString() ?? "N/A"
        }
            <<< FormalLabelRow() {
                $0.title = Localizable.MemberCenter.remainderPV
                $0.value = "\(self.info?.remainderPV ?? 0)"
        }
            <<< FormalLabelRow() {
                $0.title = Localizable.MemberCenter.needPV
                $0.value = "\(self.info?.needPV ?? 0)"
        }
    }

}
