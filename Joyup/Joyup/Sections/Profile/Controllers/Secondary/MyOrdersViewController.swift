//
//  MyOrdersViewController.swift
//  Joyup
//
//  Created by Meniny on 2017-07-13.
//  Copyright © 2017年 Meniny. All rights reserved.
//

import UIKit
import Segmentio
import EasyGCD
import CocoaHelper
import SnapKit

class MyOrdersViewController: BaseViewController {

    var segmentioView: Segmentio!
    
    open let pageController = PageViewController()
    var controllers: [OrderListViewController] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.title = Localizable.MyOrders.title
        
        let height: CGFloat = 49
        let segmentioViewRect = CGRect(x: 0, y: 0, width: view.width, height: height)
        segmentioView = Segmentio(frame: segmentioViewRect)
        view.addSubview(segmentioView)
        
        var content: [SegmentioItem] = []
        
        for s in JoyupOrder.Status.all {
            let tornadoItem = SegmentioItem(title: s.localized, image: nil)
            content.append(tornadoItem)
        }
        
        let indicatorOpts = SegmentioIndicatorOptions(
            type: .bottom,
            ratio: 1,
            height: 5,
            color: .orange
        )
        
        let horizontalSepOpts = SegmentioHorizontalSeparatorOptions(
            type: .topAndBottom,
            height: 1,
            color: .clear
        )
        
        let verticalSepOpts = SegmentioVerticalSeparatorOptions(
            ratio: 0.6,
            color: .clear
        )
        
        let stateOpts = SegmentioStates(
            defaultState: SegmentioState(
                backgroundColor: .clear,
                titleFont: UIFont.systemFont(ofSize: UIFont.smallSystemFontSize),
                titleTextColor: .black
            ),
            selectedState: SegmentioState(
                backgroundColor: .white,
                titleFont: UIFont.systemFont(ofSize: UIFont.smallSystemFontSize),
                titleTextColor: .black
            ),
            highlightedState: SegmentioState(
                backgroundColor: UIColor.lightGray.withAlphaComponent(0.6),
                titleFont: UIFont.boldSystemFont(ofSize: UIFont.smallSystemFontSize),
                titleTextColor: .black
            )
        )
        
        segmentioView.setup(
            content: content,
            style: .onlyLabel,
            options: SegmentioOptions(backgroundColor: .white,
                                      maxVisibleItems: 4,
                                      scrollEnabled: true,
                                      indicatorOptions: indicatorOpts,
                                      horizontalSeparatorOptions: horizontalSepOpts,
                                      verticalSeparatorOptions: verticalSepOpts,
                                      imageContentMode: .scaleAspectFit,
                                      labelTextAlignment: .center,
                                      labelTextNumberOfLines: 0,
                                      segmentStates: stateOpts,
                                      animationDuration: 0.25)
        )
        
        segmentioView.selectedSegmentioIndex = 0
        
        segmentioView.valueDidChange = { (segmentio, idx) in
            if let current = (self.pageController.viewControllers?.last as? OrderListViewController)?.tag {
                guard current != idx else {
                    return
                }
                if let controller = self.controllers.object(at: idx) {
                    main {
                        self.pageController.setViewControllers([controller], direction: current < idx ? .forward : .reverse, animated: true, completion: nil)
                    }
                }
            }
        }
        
        segmentioView.enableShadow()
        
        segmentioView.snp.makeConstraints { (make) in
            make.top.equalTo(0)
            make.left.equalTo(0)
            make.right.equalTo(0)
            make.height.equalTo(height)
        }
        
        segmentioView.isUserInteractionEnabled = false
        
        addChildViewController(pageController)
        view.addSubview(pageController.view)
        
        pageController.view.snp.makeConstraints { (make) in
            make.top.equalTo(height)
            make.left.equalTo(0)
            make.right.equalTo(0)
            make.bottom.equalTo(0)
        }
        
        for s in JoyupOrder.Status.all {
            let list = OrderListViewController(status: s)
            controllers.append(list)
        }
        
        for ctrl in controllers {
            ctrl.tag = controllers.index(of: ctrl)!
        }
        
        self.pageController.setViewControllers([controllers.first!], direction: .forward, animated: false, completion: nil)
        self.pageController.dataSource = self
        self.pageController.delegate = self
    }

}

extension MyOrdersViewController: UIPageViewControllerDataSource, UIPageViewControllerDelegate {
    // MARK: - UIPageViewControllerDataSource
    
    public func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        let tag = (viewController as! OrderListViewController).tag
        if tag <= 0 {
            return controllers.last!
        }
        return controllers[tag - 1]
    }
    
    public func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        let tag = (viewController as! OrderListViewController).tag
        if tag == controllers.count - 1 || tag < 0 {
            return controllers.first!
        }
        return controllers[tag + 1]
    }
    
    // MARK: - UIPageViewControllerDelegate
    
    public func pageViewController(_ pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool) {
        if let tag = (pageController.viewControllers?.last as? OrderListViewController)?.tag {
            segmentioView.selectedSegmentioIndex = tag
        }
    }
}
