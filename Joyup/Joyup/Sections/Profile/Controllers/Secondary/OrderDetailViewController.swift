//
//  OrderDetailViewController.swift
//  Joyup
//
//  Created by Meniny on 2017-07-30.
//  Copyright © 2017年 Meniny. All rights reserved.
//

import UIKit
import CocoaHelper
import Jsonify

open class JoyupOrderDetail {
    
    open class OrderItem {
        open var id: Int
        open var productID: Int
        open var type: String
        open var name: String
        open var price: Float
        open var pv: Int
        open var thumbnail: String
        open var quantity: Int
        open var sn: String
        
        open class RedeemCode {
            open var code: String
            open var date: Date?
            
            public init?(json: Jsonify) {
                let components = json.stringValue.components(separatedBy: ";")
                if components.count == 2 {
                    code = components.first!
                    let d = components.last!
                    if d == "null" {
                        let formatter = DateFormatter()
                        formatter.dateFormat = "yyyyMMddHHmmss"
                        date = formatter.date(from: d)
                    } else {
                        date = nil
                    }
                } else {
                    return nil
                }
            }
        }
        
        open var uCode: [RedeemCode] = []
        
        public init(json: Jsonify) {
            id = json["goodsId"].intValue
            productID = json["productId"].intValue
            type = json["goodsType"].stringValue
            name = json["orderItemName"].stringValue
            price = json["orderItemPrice"].floatValue
            thumbnail = json["orderItemThumbnail"].stringValue
            quantity = json["orderItemQuantity"].intValue
            sn = json["orderItemSn"].stringValue
            pv = json["orderItemPvPoint"].intValue
            for u in json["uCode"].arrayValue {
                if let c = RedeemCode(json: u) {
                    uCode.append(c)
                }
            }
        }
    }
    
    open var id: Int
    open var isValid: Bool
//    open var isKuaidi100Enabled: Bools
//    open var isFictitious: String
    open var orderItems: [JoyupOrderDetail.OrderItem]
    open var orderSN: String
    open var consignee: Consignee
    open var isInvoiceEnabled: Bool
    open var isDelivery: Bool
    open var amount: Float
    open var pvPoint: Int
    open var shippingMethod: Int
    open var shippingMethodName: String
    open var paymentMethod: Int
    open var paymentMethodName: String
    open var status: JoyupOrder.Status
    
    public init(json: Jsonify) {
        
        isValid = json["isValidOrder"].boolValue
        
        if let order = json["order"].arrayValue.first {
            status = JoyupOrder.Status(key: order["orderStatus"].stringValue)
            id = order["orderId"].intValue
            orderSN = order["orderSn"].stringValue
            isInvoiceEnabled = order["isInvoiceEnabled"].boolValue
            isDelivery = order["isDelivery"].boolValue
            amount = order["orderAmount"].floatValue
            pvPoint = order["orderPvPoint"].intValue
            shippingMethod = order["shippingMethodId"].intValue
            shippingMethodName = order["shippingMethodName"].stringValue
            paymentMethod = order["getPaymentMethodId"].intValue
            paymentMethodName = order["getPaymentMethodName"].stringValue
            
            var items: [JoyupOrderDetail.OrderItem] = []
            for i in order["orderItems"].arrayValue {
                items.append(OrderItem(json: i))
            }
            orderItems = items
            
            consignee = Consignee(order["consignee"].stringValue,
                                  phone: order["phone"].stringValue,
                                  zip: order["zipCode"].stringValue,
                                  address: order["address"].stringValue)
        } else {
            id = 0
            status = .unknown
            orderSN = "N/A"
            isInvoiceEnabled = false
            isDelivery = false
            amount = 0
            pvPoint = 0
            shippingMethod = 0
            shippingMethodName = "N/A"
            paymentMethod = 0
            paymentMethodName = "N/A"
            
            orderItems = []
            
            consignee = Consignee("N/A",
                                  phone: "N/A",
                                  zip: "N/A",
                                  address: "N/A")
        }
    }
}

class OrderDetailViewController: BaseTableViewController {

    var serialNumber: String
    
    var orderInfo: JoyupOrderDetail?
    
    public init(sn: String) {
        serialNumber = sn
        super.init()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        title = ""
        
        tableView.register(cell: OrderDetailProductListTableViewCell.self)
        tableView.register(cell: OrderDetailStatusTableViewCell.self)
        tableView.register(cell: OrderDetailConsigneeTableViewCell.self)
        tableView.register(cell: OrderDetailAmountTableViewCell.self)
        tableView.register(cell: OrderDetailInformationsTableViewCell.self)
        
        tableView.delegate = self
        tableView.dataSource = self
        
        request()
    }
    
    func request() {
        let loading = JoyupNotice.showLoading(nil)
        Request.orderDetail(serialNumber).request(params: nil, completion: { (json, _, _) in
            if let message = Request.checkResult(json) {
                loading.hide()
                JoyupNotice.showError(message)
            } else {
                loading.hide()
                self.orderInfo = JoyupOrderDetail(json: json["data"])
                self.tableView.reloadData()
            }
        }) { (resp, error) in
            loading.hide()
            JoyupNotice.showFireError(error, response: resp)
        }
    }
}

extension OrderDetailViewController: UITableViewDelegate, UITableViewDataSource {
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let id = self.cellIdentifier(at: indexPath)
        let cell = tableView.dequeueReusableCell(withIdentifier: id)
        self.config(cell: cell!, at: indexPath)
        return cell!
    }
    
    func cellIdentifier(at indexPath: IndexPath) -> String {
        // Infos
        if indexPath.section == 0 {
            switch indexPath.row {
            case 0:
                return OrderDetailStatusTableViewCell.nameOfClass
            case 1:
                return OrderDetailConsigneeTableViewCell.nameOfClass
            default:
                return OrderDetailInformationsTableViewCell.nameOfClass
            }
            // Items
        } else if indexPath.section == 1 {
            return OrderDetailProductListTableViewCell.nameOfClass
        }
        // Amount
        return OrderDetailAmountTableViewCell.nameOfClass
    }
    
    func config(cell aCell: UITableViewCell, at indexPath: IndexPath) {
        aCell.clipsToBounds = true
        aCell.selectionStyle = .none
        aCell.accessoryType = .none
        
        if aCell is OrderDetailStatusTableViewCell {
            let cell = aCell as! OrderDetailStatusTableViewCell
            cell.statusLabel.text = orderInfo?.status.localized ?? JoyupOrder.Status.unknown.localized
            return
        }
        
        if aCell is OrderDetailProductListTableViewCell {
            let cell = aCell as! OrderDetailProductListTableViewCell
            cell.config(with: orderInfo?.orderItems.object(at: indexPath.row))
            return
        }
        
        if aCell is OrderDetailAmountTableViewCell {
            let cell = aCell as! OrderDetailAmountTableViewCell
            cell.amountLabel.text = orderInfo?.amount.amountString ?? "N/A"
            return
        }
        
        if aCell is OrderDetailConsigneeTableViewCell {
            let cell = aCell as! OrderDetailConsigneeTableViewCell
            cell.config(with: orderInfo?.consignee)
            return
        }
        
        if aCell is OrderDetailInformationsTableViewCell {
            let cell = aCell as! OrderDetailInformationsTableViewCell
            cell.config(with: orderInfo)
            return
        }
    }
    
    public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section == 1 {
            if let item = orderInfo?.orderItems.object(at: indexPath.row) {
                let next = ProductDetailViewController(id: "\(item.productID)", fromOrder: true)
                navigationController?.pushViewController(next, animated: true)
            }
        }
    }
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // Infos
        if section == 0 {
            return 3
        // Items
        } else if section == 1 {
            return orderInfo?.orderItems.count ?? 0
        }
        // Amount
        return 1
    }
    
    public func numberOfSections(in tableView: UITableView) -> Int {
        return 3
    }
    
    public func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let id = self.cellIdentifier(at: indexPath)
        return tableView.fd_heightForCell(withIdentifier: id, configuration: { (cell) in
            self.config(cell: cell as! UITableViewCell, at: indexPath)
        })
    }
}
