//
//  OrderListViewController.swift
//  Joyup
//
//  Created by Meniny on 2017-07-20.
//  Copyright © 2017年 Meniny. All rights reserved.
//

import UIKit
import SVPullToRefresh
import EasyGCD

class OrderListViewController: BaseTableViewController {

    var status: JoyupOrder.Status
    var orders: [JoyupOrder] = []
    var semaphore = EasyGCD.semaphore(1)
    var footingIndex: Int = 0
    var OrderSn:String = ""
    
    
    init(status s: JoyupOrder.Status) {
        status = s
        super.init()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.register(cell: OrderTableViewCell.self)
        
        PayPalMobile.preconnect(withEnvironment: JoyupPayment.kPayPalEnvironment)
        
        tableView.dataSource = self
        tableView.delegate = self
        
        tableView.addPullToRefresh {
            self.request()
        }
        
        // TODO: Loading More
        tableView.addInfiniteScrolling {
            self.request(loadMore: true)
        }
        
        request()
        
        
        NotificationCenter.default.addObserver(self, selector: #selector(alipayAppCallbackNotificationHandlerWith(_:)), name: Joyup.alipayPaymentCallbackNotification, object: nil)

    }
    //支付结果
    func alipayAppCallbackNotificationHandlerWith(_ sn:Notification){
        let info = sn.object as! String;
        if(info == "9000"){
            self.tableView.triggerPullToRefresh()

            self.alipayAppCallbackNotificationHandler(sn: OrderSn)
        }
        
    }
    func alipayAppCallbackNotificationHandler(sn:String){
        let next = OrderDetailViewController(sn: sn)
        self.navigationController?.pushViewController(next, animated: true)
    }
    func request(loadMore: Bool = false) {
        EasyGCD.async(.global(), closure: { () -> (Void) in
            self.semaphore.wait()
        })
        let params: Request.Params = [
            "page": loadMore ? page + 1 : firstPage,
            "size": pageSize
        ]
        tableView.pullToRefreshView.startAnimating()
//        let loading = JoyupNotice.showLoading(nil)
        Request.orderList(status).request(params: params, completion: { (json, _, _) in
            if let message = Request.checkResult(json) {
                self.tableView.pullToRefreshView.stopAnimating()
//                self.tableView.infiniteScrollingView.stopAnimating()
//                loading.hide()
                JoyupNotice.showError(message)
                self.semaphore.signal()
            } else {
                if !loadMore {
                    self.orders.removeAll()
                }
                
                let orderJSONs = json["data"]["newOrders"].arrayValue
                for ojs in orderJSONs {
                    self.orders.append(JoyupOrder(json: ojs))
                }
                
                if loadMore {
                    self.page += 1
                } else {
                    self.page = self.firstPage
                }
                main {
                    self.tableView.pullToRefreshView.stopAnimating()
//                    self.tableView.infiniteScrollingView.stopAnimating()
                    self.tableView.reloadData()
//                    loading.hide()
                }
                self.semaphore.signal()
            }
        }) { (resp, error) in
            self.tableView.pullToRefreshView.stopAnimating()
//            self.tableView.infiniteScrollingView.stopAnimating()
//            loading.hide()
            JoyupNotice.showFireError(error, response: resp)
            self.semaphore.signal()
        }
    }
    
}

extension OrderListViewController: UITableViewDelegate, UITableViewDataSource {
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let id = self.cellIdentifier(at: indexPath)
        let cell = tableView.dequeueReusableCell(withIdentifier: id)
        self.config(cell: cell!, at: indexPath)
        return cell!
    }
    
    func cellIdentifier(at indexPath: IndexPath) -> String {
        return OrderTableViewCell.nameOfClass
    }
    
    func config(cell aCell: UITableViewCell, at indexPath: IndexPath) {
        aCell.clipsToBounds = true
        aCell.selectionStyle = .none
        aCell.accessoryType = .none
        
        let cell = aCell as! OrderTableViewCell
        if let order = orders.object(at: indexPath.row) {
            cell.config(with: order, showStatus: status == .any, left: { status, method in
                if status == .pendingPayment {
                    if method.id == 4 {
                        JoyupPayment.usePaypal(pay: order.amount, brief: order.sn, controller: self, delegate: self)
                    } else  if method.id == 8{
                        // TODO: AliPay
                        let loading = JoyupNotice.showLoading()
                        Request.orderSubmit(.alipay, order.sn, order.amount).request(params: nil, completion: { (json, _, _) in
                            loading.hide()
                            if let error = Request.checkResult(json) {
                                JoyupNotice.showError(error)
                            } else {
                                if let psn = json["data"]["paymentSN"].string {
                                    self.OrderSn = order.sn
                                    JoyupPayment.useAlipay(sn: psn, completion: { (s, e) in
                                        if s {
                                            JoyupNotice.showSuccess()
                                            self.tableView.triggerPullToRefresh()
                                            self.alipayAppCallbackNotificationHandler(sn: order.sn)

                                        } else {

                                            JoyupNotice.showError(Localizable.Generic.unknownError)

                                        }
                                    })
                                } else {
                                    JoyupNotice.showError(Localizable.Generic.unknownError)
                                }
                            }
                        }, error: { (resp, error) in
                            loading.hide()
                            JoyupNotice.showFireError(error, response: resp)
                        })

                        
                    }
                }
            }, right: { sn in
                let next = OrderDetailViewController(sn: sn)
                self.navigationController?.pushViewController(next, animated: true)
            })
        }
    }
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return orders.count
    }
    
    public func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let id = self.cellIdentifier(at: indexPath)
        return tableView.fd_heightForCell(withIdentifier: id, configuration: { (cell) in
            self.config(cell: cell as! UITableViewCell, at: indexPath)
        })
    }
}

extension OrderListViewController: PayPalPaymentDelegate {
    public func payPalPaymentViewController(_ paymentViewController: PayPalPaymentViewController, didComplete completedPayment: PayPalPayment) {
        paymentViewController.dismiss(animated: true, completion: nil)
        if let order = orders.object(at: footingIndex) {
            let loading = JoyupNotice.showLoading()
            Request.orderSubmit(.payPal, order.sn, order.amount).request(params: nil, completion: { (json, _, _) in
                loading.hide()
                if let error = Request.checkResult(json) {
                    JoyupNotice.showError(error)
                } else {
                    JoyupNotice.showSuccess()
                }
                self.request()
            }, error: { (resp, error) in
                loading.hide()
                JoyupNotice.showFireError(error, response: resp)
            })
        }
    }
    
    public func payPalPaymentDidCancel(_ paymentViewController: PayPalPaymentViewController) {
        paymentViewController.dismiss(animated: true, completion: nil)
    }
}
