//
//  WalletViewController.swift
//  Joyup
//
//  Created by Meniny on 2017-07-13.
//  Copyright © 2017年 Meniny. All rights reserved.
//

import UIKit
import Jsonify
import SnapKit
import Formal

public struct Wallet {
    public var freezeFee: Float
    public var totalFee: Float
    public var memberBalance: Float
    
    public init(json: Jsonify) {
        freezeFee = json["freezeFee"].floatValue
        totalFee = json["totalFee"].floatValue
        memberBalance = json["memberBalance"].floatValue
    }
    
    public init(balance: Float, total: Float, freeze: Float) {
        freezeFee = freeze
        totalFee = total
        memberBalance = balance
    }
    
    public static func zero() -> Wallet {
        return Wallet(balance: 0, total: 0, freeze: 0)
    }
}

class WalletViewController: FormalViewController {

    var wallet: Wallet?
    
    public init() {
        super.init(style: .grouped)
        hidesBottomBarWhenPushed = Joyup.shouldHidesBottomBarWhenPushed(for: self)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        hidesBottomBarWhenPushed = Joyup.shouldHidesBottomBarWhenPushed(for: self)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        title = Localizable.Wallet.title
        
        request()
    }
    
    func refresh() {
        formal.removeAll()
        formal +++ FormalSection()
            <<< FormalLabelRow() {
                $0.title = Localizable.Wallet.memberBalance
                if let wallet = self.wallet {
                    $0.value = String(format: "US$%@", wallet.memberBalance.amountString)
                } else {
                    $0.value = "N/A"
                }
            }
            <<< FormalLabelRow() {
                $0.title = Localizable.Wallet.totalFee
                if let wallet = self.wallet {
                    $0.value = String(format: "US$%@", wallet.totalFee.amountString)
                } else {
                    $0.value = "N/A"
                }
            }
            <<< FormalLabelRow() {
                $0.title = Localizable.Wallet.freezeFee
                if let wallet = self.wallet {
                    $0.value = String(format: "US$%@", wallet.freezeFee.amountString)
                } else {
                    $0.value = "N/A"
                }
        }
    }
    
    func request() {
        Request.wallet.request(params: nil, completion: { (json, _, _) in
            if let message = Request.checkResult(json) {
                JoyupNotice.showError(message)
            } else {
                self.wallet = Wallet(json: json["data"])
                self.refresh()
            }
        }) { (resp, error) in
            JoyupNotice.showFireError(error, response: resp)
        }
    }
}
