//
//  TransferRecord.swift
//  Joyup Merchant
//
//  Created by Meniny on 2017-09-04.
//  Copyright © 2017年 Meniny. All rights reserved.
//

import Foundation
import Jsonify
import CocoaHelper

public struct TransferRecord {
    public let amount: Float
//    public let amountbefore: Float
//    public let amountafter: Float
    public let incoming: Bool
    public let receiver: String
    public let memo: String
    public let id: Int
    public let createDate: String
    
    public init(json: Jsonify) {
        let date = (json["createDate"].intValue / 1000).stringValue()
        createDate = Date(timestampString: date).formattedString()
        amount = json["amount"].floatValue
        let m = json["memo"].stringValue
        memo = m.isEmpty ? "Transfer" : m
        incoming = json["income"].stringValue == "income"
        receiver = json["receiver"].stringValue
        id = json["transferId"].intValue
    }
}
