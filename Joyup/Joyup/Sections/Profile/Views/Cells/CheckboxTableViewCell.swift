//
//  CheckboxTableViewCell.swift
//  Joyup
//
//  Created by Meniny on 2017-08-06.
//  Copyright © 2017年 Meniny. All rights reserved.
//

import UIKit

class CheckboxTableViewCell: UITableViewCell {

    @IBOutlet weak var checkbox: UIButton!
    @IBOutlet weak var briefLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
