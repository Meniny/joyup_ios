//
//  OrderDetailAmountTableViewCell.swift
//  Joyup
//
//  Created by Meniny on 2017-08-02.
//  Copyright © 2017年 Meniny. All rights reserved.
//

import UIKit

class OrderDetailAmountTableViewCell: UITableViewCell {

    @IBOutlet weak var amountTitleLabel: UILabel!
    @IBOutlet weak var amountLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        amountTitleLabel.text = Localizable.MyOrders.amount
    }
    
}
