//
//  OrderDetailConsigneeTableViewCell.swift
//  Joyup
//
//  Created by Meniny on 2017-08-02.
//  Copyright © 2017年 Meniny. All rights reserved.
//

import UIKit

class OrderDetailConsigneeTableViewCell: UITableViewCell {
    
    @IBOutlet weak var consigneeLabel: UILabel!
    @IBOutlet weak var phoneLabel: UILabel!
    @IBOutlet weak var addressLabel: UILabel!

    func config(with consignee: Consignee?) {
        consigneeLabel.text = consignee?.name
        if let area = consignee?.areaName {
            addressLabel.text = "[\(area)] \(consignee?.address ?? "")"
        } else {
            addressLabel.text = consignee?.address
        }
        phoneLabel.text = consignee?.phone
    }
    
}
