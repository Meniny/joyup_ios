//
//  OrderDetailInformationsTableViewCell.swift
//  Joyup
//
//  Created by Meniny on 2017-08-02.
//  Copyright © 2017年 Meniny. All rights reserved.
//

import UIKit

class OrderDetailInformationsTableViewCell: UITableViewCell {
    
    @IBOutlet weak var copyButton: UIButton!
    
    @IBOutlet weak var leftLabel01: UILabel!
    @IBOutlet weak var leftLabel02: UILabel!
    @IBOutlet weak var leftLabel03: UILabel!
    
    @IBOutlet weak var rightLabel01: UILabel!
    @IBOutlet weak var rightLabel02: UILabel!
    @IBOutlet weak var rightLabel03: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        copyButton.setTitle(Localizable.Generic.copy, for: .normal)
        leftLabel01.text = Localizable.MyOrders.orderSN + ":"
        leftLabel02.text = Localizable.MyOrders.shippingMethod + ":"
        leftLabel03.text = Localizable.MyOrders.paymentMethod + ":"
    }
    
    @IBAction func copySN(_ sender: UIButton) {
        if let sn = rightLabel01.text {
            UIPasteboard.general.string = sn
            JoyupNotice.showSuccess(Localizable.MyOrders.copyed)
        }
    }
    
    func config(with detail: JoyupOrderDetail?) {
        rightLabel01.text = detail?.orderSN ?? "N/A"
        rightLabel02.text = detail?.shippingMethodName ?? "N/A"
        rightLabel03.text = detail?.paymentMethodName ?? "N/A"
    }
    
}
