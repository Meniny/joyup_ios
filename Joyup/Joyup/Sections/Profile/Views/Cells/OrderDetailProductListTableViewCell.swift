//
//  OrderDetailProductListTableViewCell.swift
//  Joyup
//
//  Created by Meniny on 2017-07-29.
//  Copyright © 2017年 Meniny. All rights reserved.
//

import UIKit

class OrderDetailProductListTableViewCell: UITableViewCell {
    @IBOutlet weak var codeLabel: UILabel!
    @IBOutlet weak var lineView: UIView!
    @IBOutlet weak var upLineView: UIView!
    @IBOutlet weak var previewView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var countLabel: UILabel!

    public func config(with product: JoyupOrderDetail.OrderItem?) {
        previewView.webImage(url: product?.thumbnail ?? "")
        nameLabel.text = product?.name ?? "N/A"
        priceLabel.text = product?.price.amountString ?? "N/A"
        countLabel.text = "× \(product?.quantity ?? 1)"
        if let codes = product?.uCode, codes.isNotEmpty {
            let bold = UIFont.boldSystemFont(ofSize: 14)
            let font = UIFont.systemFont(ofSize: 14)
            let strings = codes.map({ (c) -> NSAttributedString in
                if let d = c.date {
                    let s = c.code + " [" + d.formattedString() + "]"
                    return NSAttributedString(string: s, attributes: [
                        NSFontAttributeName: font,
                        NSForegroundColorAttributeName: UIColor.lightGray
                        ])
                }
                return NSAttributedString(string: c.code, attributes: [
                    NSFontAttributeName: bold,
                    NSForegroundColorAttributeName: UIColor(red:0.21, green:0.78, blue:0.75, alpha:1.00)
                    ])
            })
            let br = NSAttributedString(string: "\n", attributes: [NSFontAttributeName: font])
            let result = NSMutableAttributedString(string: "RedeemCode".localized() + ":\n", attributes: [
                NSFontAttributeName: font,
                NSForegroundColorAttributeName: UIColor.black
                ])
            for i in 0..<strings.count {
                if let a = strings.object(at: i) {
                    result.append(a)
                    if i < strings.count - 1 {
                        result.append(br)
                    }
                }
            }
            codeLabel.attributedText = result
            upLineView.isHidden = true
        } else {
            codeLabel.attributedText = nil
            upLineView.isHidden = false
        }
        lineView.isHidden = !upLineView.isHidden
    }
}
