//
//  OrderDetailStatusTableViewCell.swift
//  Joyup
//
//  Created by Meniny on 2017-08-02.
//  Copyright © 2017年 Meniny. All rights reserved.
//

import UIKit

class OrderDetailStatusTableViewCell: UITableViewCell {

    @IBOutlet weak var statusLabel: UILabel!
    
}
