//
//  OrderTableViewCell.swift
//  Joyup
//
//  Created by Meniny on 2017-07-20.
//  Copyright © 2017年 Meniny. All rights reserved.
//

import UIKit
import CocoaHelper

class OrderTableViewCell: UITableViewCell {
    @IBOutlet weak var topLabel: UILabel!
    @IBOutlet weak var leftButton: UIButton!
    @IBOutlet weak var rightButton: UIButton!
    @IBOutlet weak var amoutLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var previewView: UIImageView!
    @IBOutlet weak var statusLabel: UILabel!
    
    @IBOutlet weak var countLabel: UILabel!
    var rightAction: ((_ SN: String) -> Void)?
    var leftAction: ((_ status: JoyupOrder.Status, _ method: PaymentMethod) -> Void)?
    var SN: String?
    var status: JoyupOrder.Status?
    public var paymentMethod: PaymentMethod?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        rightButton.setTitle(Localizable.Generic.viewDetail, for: .normal)
        rightButton.setTitleColor(UIColor.pictonBlue, for: .normal)
        
        leftButton.setTitle(Localizable.Generic.foot, for: .normal)
        leftButton.setTitle("OfflinePayment".localized(), for: .disabled)
        leftButton.setTitleColor(UIColor.pictonBlue, for: .normal)
        leftButton.setTitleColor(UIColor.lightGray, for: .disabled)
        leftButton.titleLabel?.numberOfLines = 0
        leftButton.titleLabel?.textAlignment = .center
    }
    
    public func config(with order: JoyupOrder,
                       showStatus: Bool,
                       left: @escaping (_ status: JoyupOrder.Status, _ method: PaymentMethod) -> Void,
                       right: @escaping (_ sn: String) -> Void) {
        SN = order.sn
        status = order.status
        paymentMethod = order.paymentMethod
        previewView.webImage(url: order.orderItems.first?.image ?? "")
        topLabel.text = "SN: " + order.sn
        nameLabel.text = order.orderItems.first?.name ?? "N/A"
        amoutLabel.text = String(format: "US$ %@", order.amount.amountString)
        countLabel.text = String(format: Localizable.MyOrders.orderProductsCountingFormat, order.orderItems.count)
        leftAction = left
        rightAction = right
        leftButton.isHidden = order.status != .pendingPayment
        statusLabel.isHidden = !showStatus
        statusLabel.text = "○ " + order.status.localized
        // TODO: AliPay
        leftButton.isEnabled = !(order.status != .pendingPayment || (order.status == .pendingPayment && order.paymentMethod.id != 4 && order.paymentMethod.id != 8))
        leftButton.layer.borderColor = leftButton.isEnabled ? rightButton.layer.borderColor : UIColor.lightGray.cgColor
    }
    
    @IBAction func leftButtonAction(_ sender: UIButton) {
        if let s = status, let m = paymentMethod {
            leftAction?(s, m)
        }
    }
    
    @IBAction func rightButtonAction(_ sender: UIButton) {
        if let sn = SN {
            rightAction?(sn)
        }
    }
}
