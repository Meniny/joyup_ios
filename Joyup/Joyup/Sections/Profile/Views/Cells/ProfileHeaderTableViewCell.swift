//
//  ProfileHeaderTableViewCell.swift
//  Joyup
//
//  Created by Meniny on 2017-07-13.
//  Copyright © 2017年 Meniny. All rights reserved.
//

import UIKit

class ProfileHeaderTableViewCell: UITableViewCell {
    
    @IBOutlet weak var avatarView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        if let user = JoyupUser.main {
            if let avatar = user.avatar {
                avatarView.webImage(url: avatar, placeholder: nil)
            } else {
                avatarView.image = nil
            }
            nameLabel.text = user.userName
        } else {
            avatarView.image = nil
            nameLabel.text = ""
        }
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
