//
//  ConsigneeCreationViewController.swift
//  Joyup
//
//  Created by Meniny on 2017-08-12.
//  Copyright © 2017年 Meniny. All rights reserved.
//

import UIKit
import Formal

class ConsigneeCreationViewController: FormalViewController {
    
    let callback: () -> Void
    
    public init(onCreation: @escaping () -> Void) {
        callback = onCreation
        super.init(style: .grouped)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    var consignee: Consignee = Consignee("", phone: "", zip: "", address: "")
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = Localizable.Cart.dontHaveAnyAddress
        animationSettings.setAll(to: .fade)
        reloadFromal()
    }
    
    func reloadFromal() {
        formal.removeAll()
        
        let must = "* "
        
        formal +++ FormalSection()
            <<< FormalTextFloatingFieldRow() {
                $0.placeholder = must + Localizable.User.addressName
                $0.value = self.consignee.name
                }.onChange({ (row) in
                    self.consignee.name = row.value ?? ""
                })
            <<< FormalTextFloatingFieldRow() {
                $0.placeholder = must + Localizable.User.addressPhone
                $0.textType = .phone
                $0.value = self.consignee.phone
                }.onChange({ (row) in
                    self.consignee.phone = row.value ?? ""
                })
            <<< FormalLabelRow() {
                $0.title = must + Localizable.User.addressArea
                if let id = self.consignee.areaID, let subarea = Subarea(rawValue: id) {
                    $0.value = subarea.localizedName
                } else {
                    $0.value = "N/A"
                }
                }.onCellSelection({ (_, row) in
                    JoyupNotice.showOptions(nil, title: Localizable.User.area, options: Area.allNames, action: { (idx, _) in
                        if let idx = idx {
                            switch idx {
                            case 0:
                                self.consignee.areaID = Subarea.contiguousAmerica.rawValue
                                self.reloadFromal()
                                break
                            case 1:
                                JoyupNotice.showOptions(nil, title: Localizable.User.area, options: Area.china.subarea.allNames, action: { (index, option) in
                                    if let opt = option, let sub = Subarea(name: opt) {
                                        self.consignee.areaID = sub.rawValue
                                        self.reloadFromal()
                                    }
                                })
                                break
                            default:
                                self.consignee.areaID = Subarea.australia.rawValue
                                self.reloadFromal()
                                break
                            }
                            
                        }
                    })
                })
            <<< FormalTextFloatingFieldRow() {
                $0.placeholder = Localizable.User.addressState
                $0.value = self.consignee.state
                }.onChange({ (row) in
                    self.consignee.state = row.value
                })
            <<< FormalTextFloatingFieldRow() {
                $0.placeholder = Localizable.User.addressCity
                $0.value = self.consignee.city
                }.onChange({ (row) in
                    self.consignee.city = row.value
                })
            <<< FormalTextFloatingFieldRow() {
                $0.placeholder = must + Localizable.User.addressAddress
                $0.value = self.consignee.address
                }.onChange({ (row) in
                    self.consignee.address = row.value ?? ""
                })
            <<< FormalTextFloatingFieldRow() {
                $0.placeholder = must + Localizable.User.addressZip
                $0.textType = .zipCode
                $0.value = self.consignee.zip
                }.onChange({ (row) in
                    self.consignee.zip = row.value
                })
            <<< FormalImageCheckRow() {
//            <<< FormalCheckRow() {
                $0.title = Localizable.User.addressIsDefault
                $0.value = self.consignee.isDefault
                }.onChange({ (row) in
                    self.consignee.isDefault = row.value ?? false
                })
        
        navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(createConsignee))
        
        navigationItem.leftBarButtonItem = UIBarButtonItem(barButtonSystemItem: .cancel, target: self, action: #selector(cancelCreation))
    }
    
    func cancelCreation() {
        JoyupNotice.showConfirm(Localizable.Generic.close, message: nil, leftButton: Localizable.Generic.yes, leftAction: {
            self.navigationController?.popViewController(animated: true)
        }, rightButton: Localizable.Generic.no, rightAction: nil)
    }
    
    func createConsignee() {
        guard let _ = consignee.areaID else {
            JoyupNotice.showWarning(Localizable.Generic.needToFillUp)
            return
        }
        guard let _ = consignee.zip else {
            JoyupNotice.showWarning(Localizable.Generic.needToFillUp)
            return
        }
        guard consignee.name.isNotEmpty && consignee.phone.isNotEmpty && consignee.address.isNotEmpty else {
            JoyupNotice.showWarning(Localizable.Generic.needToFillUp)
            return
        }
        let loading = JoyupNotice.showLoading()
        Request.addingConsignee(consignee).request(params: nil, completion: { (json, _, _) in
            loading.hide()
            if let error = Request.checkResult(json) {
                JoyupNotice.showError(error)
            } else {
                self.callback()
                self.navigationController?.popViewController(animated: true)
            }
        }) { (resp, error) in
            loading.hide()
            JoyupNotice.showFireError(error, response: resp)
        }
    }
}
