//
//  ProductDetailViewController.swift
//  Joyup
//
//  Created by Meniny on 2017-07-13.
//  Copyright © 2017年 Meniny. All rights reserved.
//

import UIKit
import Imager
import EasyGCD
import Jsonify
import SVPullToRefresh
import NAIManager

class ProductDetailViewController: BaseViewController {
    
    var product: JoyupProduct?
    var quantity: Int = 1
    let productID: String
    var productStandardIndex: Int = 0
    
    var cartButton: UIButton = UIButton(type: .custom)
    var addingButton: UIButton = UIButton(type: .custom)
  
    let fromOrder: Bool
    
    let scrollView: UIScrollView = UIScrollView()
    let tableView: BaseTableView! = BaseTableView()
    let webView: UIWebView = UIWebView()
    
    func getHTMLContainerCode(content: String) -> String? {
        guard let path = Bundle.main.path(forResource: "ProductDetailDescriptionTemplate", ofType: "html") else {
            return nil
        }
        guard let template = try? String(contentsOfFile: path) else {
            return nil
        }
        
        return template.replace("[#CONTENT#]", with: content).replace("[#TITLE#]", with: self.title ?? Joyup.projectName)
    }
  
    init(id: String, fromOrder fo: Bool = false) {
        productID = id
        fromOrder = fo
        super.init()
    }
    
    public override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        fatalError("\(#function) has not been implemented")
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("\(#function) has not been implemented")
    }
    
    let segment = UISegmentedControl(items: ["\t商品\t", "\t详情\t"])
    
    func segmentDidTapOption(_ aSegment: UISegmentedControl) {
        let contentOffset = CGPoint(x: CGFloat(aSegment.selectedSegmentIndex) * scrollView.bounds.size.width, y: 0)
        scrollView.setContentOffset(contentOffset, animated: true)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        title = product?.title
        navigationItem.backBarButtonItem?.title = Localizable.Generic.back
        
        view.addSubview(scrollView)
        scrollView.contentSize = CGSize(width: UIScreen.main.bounds.size.width * 2, height: UIScreen.main.bounds.size.height - Joyup.bottomBarHeight - 64 - 49)
        scrollView.isPagingEnabled = true
        scrollView.showsVerticalScrollIndicator = false
        scrollView.showsHorizontalScrollIndicator = false
        scrollView.bounces = false
        scrollView.alwaysBounceVertical = false
        
        scrollView.snp.makeConstraints { (make) in
            make.top.equalTo(0)
            make.bottom.equalTo(-Joyup.bottomBarHeight)
            make.left.equalTo(0)
            make.right.equalTo(0)
        }
        
        scrollView.addSubview(tableView)
        tableView.tableFooterView = UIView()
        tableView.snp.makeConstraints { (make) in
            make.top.equalTo(0)
            make.left.equalTo(0)
            make.height.equalTo(self.scrollView.snp.height)
            make.width.equalTo(self.scrollView.snp.width)
        }
        
        tableView.register(cell: BannerTableViewCell.self, useNib: false, forCellReuseIdentifier: BannerTableViewCell.nameOfClass)
        tableView.register(cell: ProductDetailInfoTableViewCell.self)
        tableView.register(cell: ProductDescriptionTableViewCell.self)
        tableView.register(cell: CheckboxTableViewCell.self)
        
        tableView.delegate = self
        tableView.dataSource = self
        
        scrollView.addSubview(webView)
        webView.delegate = self
        webView.scrollView.bounces = false
        webView.snp.makeConstraints { (make) in
            make.top.equalTo(0)
            make.left.equalTo(self.tableView.snp.right)
            make.height.equalTo(self.scrollView.snp.height)
            make.width.equalTo(self.scrollView.snp.width)
        }
        
        navigationItem.titleView = segment
        segment.selectedSegmentIndex = 0
        segment.addTarget(self, action: #selector(segmentDidTapOption(_:)), for: .valueChanged)
        
        let bottomBar = UIView()
        bottomBar.backgroundColor = UIColor.white
        bottomBar.enableShadow(y: -1)
        view.addSubview(bottomBar)
        
        bottomBar.snp.makeConstraints { (make) in
            make.bottom.equalTo(0)
            make.left.equalTo(0)
            make.right.equalTo(0)
            make.height.equalTo(Joyup.bottomBarHeight)
        }
        
        addingButton.setTitle(Localizable.Cart.addToCart, for: .normal)
        addingButton.setTitleColor(Configuration.submitTextColor, for: .normal)
        addingButton.backgroundColor = Configuration.submitButtonColor
        bottomBar.addSubview(addingButton)
        
        addingButton.snp.makeConstraints { (make) in
            make.top.equalTo(0)
            make.bottom.equalTo(0)
            make.right.equalTo(0)
            make.width.equalTo(150)
        }
        
        cartButton.setImage(#imageLiteral(resourceName: "shopping_cars").withRenderingMode(.alwaysTemplate), for: .normal)
        cartButton.tintColor = UIColor.black
        bottomBar.addSubview(cartButton)
        cartButton.isHidden = true
        
        cartButton.snp.makeConstraints { (make) in
            make.top.equalTo(8)
            make.bottom.equalTo(-8)
            make.left.equalTo(8)
            make.width.equalTo(self.cartButton.snp.height)
        }
        
        addingButton.addTarget(self, action: #selector(addToCart), for: .touchUpInside)
        cartButton.addTarget(self, action: #selector(showCart), for: .touchUpInside)
        
        tableView.addPullToRefresh {
            self.request()
        }
      
        request()
        
        navigationItem.backBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "backward-chevron"), style: .plain, target: self, action: #selector(goBack))
//        navigationController?.interactivePopGestureRecognizer?.delegate = self
        
        scrollView.delegate = self
    }
    
    func goBack() {
        navigationController?.popViewController(animated: true)
    }
    
    func request() {
        tableView.pullToRefreshView.startAnimating()
//        let loading = JoyupNotice.showLoading(nil)
        Request.productDetail(productID, fromOrder).request(params: nil, completion: { (json, _, _) in
            if let message = Request.checkResult(json) {
                self.tableView.pullToRefreshView.stopAnimating()
//                loading.hide()
                JoyupNotice.showError(message)
            } else {
                if let goodsContent = json["data"]["goodsContent"].arrayValue.first,
                    let first = goodsContent["goodsList"].arrayValue.first {
                    self.product = JoyupProduct(good: first)

                    self.title = self.product?.title
                    self.tableView.pullToRefreshView.stopAnimating()
                    self.tableView.reloadData()
                    
                    if let htmlContent = self.product?.desc?.content,
                        let html = self.getHTMLContainerCode(content: htmlContent) {
//                        try? html.write(toFile: "/Users/Meniny/Desktop/html.html", atomically: true, encoding: .utf8)
                        self.webView.loadHTMLString(html, baseURL: nil)
                    }
                  //                        loading.hide()
                } else {
                    self.tableView.pullToRefreshView.stopAnimating()
//                    loading.hide()
                    JoyupNotice.showError(Localizable.Generic.unknownError)
                }
            }
        }) { (resp, error) in
            self.tableView.pullToRefreshView.stopAnimating()
//            loading.hide()
            JoyupNotice.showFireError(error, response: resp)
        }
    }
    
    func showCart() {
        let next = CartViewController()
        next.hasDismissButton = true
        let navigation = NavigationController(rootViewController: next)
        present(navigation, animated: true, completion: nil)
    }
    
    var selectedStandardIndex: IndexPath?
    
    func addToCart() {
        guard JoyupUser.signedIn else {
            JoyupNotice.signIn()
            return
        }
        let selIdx = selectedStandardIndex ?? IndexPath(row: 0, section: 0)
        if let standard = product?.productsStandards.object(at: selIdx.section)/*,
            let _ = standard.specifications.object(at: selIdx.row)*/ {
            addProduct(standard)
        } else {
            JoyupNotice.showError(Localizable.Cart.addToCartFailed)
        }
    }
    
    func addProduct(_ theProduct: JoyupProduct) {
        let field = JoyupNotice.FieldConfiguration("1", placeholder: Localizable.Generic.quantity, keyboard: .numberPad)
        
        JoyupNotice.showFields(Localizable.Generic.quantity, title: Localizable.Cart.addToCart, fields: [field], buttons: [Localizable.Cart.addToCart], action: { (index, oops) in
            
            self.quantity = oops.textFields.first?.text?.integerValue ?? 1
            oops.hideView()
            
            let loading = JoyupNotice.showLoading(nil)
            let animation = { (success: Bool, error: String?) in
                loading.hide()
                if success {
                    NotificationCenter.default.post(name: Joyup.updateCartItemCountNotification, object: nil)
//                    self.addingAnimation()
                } else {
                    JoyupNotice.showError(error ?? Localizable.Cart.addToCartFailed)
                }
            }
            CartManager.shared.addRequest(product: theProduct, quantity: self.quantity, completion: animation)
        })
    }
    
    var animationView: UIImageView?
    
    func addingAnimation() {
        animationView = UIImageView(image: #imageLiteral(resourceName: "shopping_cars").withRenderingMode(.alwaysTemplate))
        animationView?.contentMode = .scaleAspectFit
        animationView?.frame = CGRect(origin: addingButton.superview!.convert(addingButton.frame.origin, to: view), size: addingButton.bounds.size)
        view.addSubview(animationView!)
        
        let endPoint = cartButton.superview!.convert(cartButton.center, to: view)
        let passPoint = CGPoint(x: (animationView!.center.x - endPoint.x) / 2, y: endPoint.y - 100)
        
        animationView?.tintColor = Configuration.mainColor
        
        animationView?.layer.cartAnimation(from: animationView!.center, pass: passPoint, to: endPoint, delegate: self)
    }
}

extension ProductDetailViewController: UIScrollViewDelegate {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if scrollView.contentOffset.x > scrollView.bounds.width * 0.5 {
            segment.selectedSegmentIndex = 1
        } else {
            segment.selectedSegmentIndex = 0
        }
    }
}

extension ProductDetailViewController: UIWebViewDelegate {
    
    func webView(_ webView: UIWebView, shouldStartLoadWith request: URLRequest, navigationType: UIWebViewNavigationType) -> Bool {
        if let url = request.url?.absoluteString {
            if url.hasPrefix("joyup://") {
                let img = url.replace("joyup://", with: "https://").URLDecodedString
                Imager.show(URLStrings: [img], controller: self)
                return false
            }
        }
        return true
    }
    
    func webViewDidStartLoad(_ webView: UIWebView) {
        NAIManager.operationStarted()
    }
    
    func webViewDidFinishLoad(_ webView: UIWebView) {
//        webView.stringByEvaluatingJavaScript(from: "setImageClickAction();")
        NAIManager.operationFinished()
    }
    
    func webView(_ webView: UIWebView, didFailLoadWithError error: Error) {
        JoyupNotice.showError(error.localizedDescription, title: webView.request?.url?.absoluteString)
        NAIManager.operationFinished()
    }
    
}

extension ProductDetailViewController: CAAnimationDelegate {
    func animationDidStop(_ anim: CAAnimation, finished flag: Bool) {
        animationView?.removeFromSuperview()
    }
}

extension ProductDetailViewController: UITableViewDelegate, UITableViewDataSource {
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let id = self.cellIdentifier(at: indexPath)
        let cell = tableView.dequeueReusableCell(withIdentifier: id)
        self.config(cell: cell!, at: indexPath)
        return cell!
    }
    
    func cellIdentifier(at indexPath: IndexPath) -> String {
        if indexPath.section == 0 {
            return BannerTableViewCell.nameOfClass
        } else if indexPath.section == tableView.numberOfSections - 1 {
            return ProductDescriptionTableViewCell.nameOfClass
        }
        if indexPath.row == 0 {
            return ProductDetailInfoTableViewCell.nameOfClass
        }
        return CheckboxTableViewCell.nameOfClass
    }
    
    func config(cell aCell: UITableViewCell, at indexPath: IndexPath) {
        aCell.clipsToBounds = true
        aCell.selectionStyle = .none
        aCell.accessoryType = .none
        
        if aCell is BannerTableViewCell {
            let cell = aCell as! BannerTableViewCell
            let banner = product?.bannerData ?? [BannerData.empty()]
            cell.config(with: banner, action: { [weak self] (b) in
                if let p = self?.product {
                    Imager.show(URLStrings: p.images, controller: self!)
                }
            })
        } else if aCell is ProductDetailInfoTableViewCell {
            let cell = aCell as! ProductDetailInfoTableViewCell
            if let p = product, let standard = product?.productsStandards.object(at: indexPath.section - 1) {
                cell.config(p.title, SN: standard.serialNumber, price: standard.price, PV: standard.PVPoint, domain: p.domain)
            } else {
                cell.config("N/A", SN: "N/A", price: 0, PV: 0, domain: nil)
            }
        } else if aCell is CheckboxTableViewCell {
            let cell = aCell as! CheckboxTableViewCell
            if let selidx = selectedStandardIndex {
                cell.checkbox.isSelected = (selidx == IndexPath(row: indexPath.row - 1, section: indexPath.section - 1))
            } else {
                cell.checkbox.isSelected = (indexPath.section == 1 && indexPath.row == 1)
            }
            cell.briefLabel.text = product?.productsStandards.object(at: indexPath.section - 1)?.specifications.object(at: indexPath.row - 1)?.value
        } else {
            let cell = aCell as! ProductDescriptionTableViewCell
            cell.config(description: product?.desc)
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section != 0 && indexPath.section != tableView.numberOfSections - 1 {
            selectedStandardIndex = IndexPath(row: indexPath.row - 1, section: indexPath.section - 1)
            tableView.reloadData()
        }
    }
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return 1
        }
        if section == tableView.numberOfSections - 1 {
            return 0
        }
        if let standard = product?.productsStandards.object(at: section - 1) {
            return standard.specifications.count + 1
        }
        return 1
    }
    
    public func numberOfSections(in tableView: UITableView) -> Int {
        return 2 + (product?.productsStandards.count ?? 1)
    }
    
    public func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0 { // Banner
            return 200
        }
        let id = self.cellIdentifier(at: indexPath)
        return tableView.fd_heightForCell(withIdentifier: id, configuration: { (cell) in
            self.config(cell: cell as! UITableViewCell, at: indexPath)
        })
    }
}
