//
//  TransferViewController.swift
//  Joyup
//
//  Created by Meniny on 2017-09-05.
//  Copyright © 2017年 Choice. All rights reserved.
//

import Foundation
import UIKit
import SnapKit

public enum KeyboardKeyType {
    case number
    case dot
    case hide
    case done
    case delete
    
    public var textColor: UIColor {
        if self == .done {
            return UIColor.white
        }
        return UIColor.darkText
    }
    
    public var backgroundColor: UIColor {
        if self == .done {
            return #colorLiteral(red: 0.18, green:0.59, blue:0.96, alpha:1.00)
        }
        return UIColor.white
    }
    
    public var borderColor: CGColor {
        if self == .done {
            return UIColor.clear.cgColor
        }
        return UIColor.lightGray.cgColor
    }
}

open class KeyboardButton: UIButton {
    open internal(set) var keyType: KeyboardKeyType
    
    public init(type: KeyboardKeyType, title: String?, image: UIImage?) {
        keyType = type
        super.init(frame: .zero)
        titleLabel?.numberOfLines = 0
        setImage(image, for: .normal)
        setTitle(title, for: .normal)
        setTitleColor(type.textColor, for: .normal)
        setTitleColor(UIColor.lightGray, for: .highlighted)
        backgroundColor = type.backgroundColor
        layer.borderColor = type.borderColor
        layer.borderWidth = 0.5
    }
    
    public required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

open class TransferViewController: BaseViewController {
    
    open internal(set) var receiver: String
    open internal(set) var nickname: String
    
    public init(receiver: String, name: String) {
        self.receiver = receiver
        self.nickname = name
        super.init(nibName: nil, bundle: nil)
    }
    
    public required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    var textField: UITextField = UITextField()
    var keyboard: UIView = UIView()
    
    open override func viewDidLoad() {
        super.viewDidLoad()
        
        title = Localizable.MyTransferRecords.transfer
        
        view.backgroundColor = UIColor.white
        
        let topLabel = UILabel()
        view.addSubview(topLabel)
        topLabel.text = String(format: "Transfer to".localized(), nickname + " (\(receiver))")
        topLabel.font = UIFont.systemFont(ofSize: 14)
        topLabel.textColor = UIColor.lightGray
        topLabel.textAlignment = .center
        topLabel.snp.makeConstraints { (make) in
            make.top.equalTo(20)
            make.left.equalTo(20)
            make.right.equalTo(-20)
            make.height.equalTo(21)
        }
        
        let leftLabel = UILabel()
        leftLabel.text = "US$"
        leftLabel.font = UIFont.boldSystemFont(ofSize: 21)
        leftLabel.textColor = UIColor.black
        leftLabel.textAlignment = .center
        leftLabel.bounds = CGRect(x: 0, y: 0, width: 60, height: 49)
        
        textField.leftView = leftLabel
        textField.leftViewMode = .always
        textField.placeholder = "TransferAmount".localized()
        view.addSubview(textField)
        textField.font = UIFont.boldSystemFont(ofSize: 20)
        textField.textAlignment = .right
        textField.borderStyle = .roundedRect
        textField.layer.borderColor = #colorLiteral(red: 0.15, green:0.82, blue:0.55, alpha:1.00).cgColor
        textField.layer.cornerRadius = 5
        textField.snp.makeConstraints { (make) in
            make.top.equalTo(topLabel.snp.bottom).offset(8)
            make.left.equalTo(20)
            make.right.equalTo(-20)
            make.height.equalTo(49)
        }
        
        view.addSubview(keyboard)
        layoutKeyboard(isHidden: false)
        var leftButtons = [KeyboardButton]()
        for i in 1...9 {
            leftButtons.append(KeyboardButton(type: .number, title: "\(i)", image: nil))
        }
        leftButtons.append(KeyboardButton(type: .dot, title: ".", image: nil))
        leftButtons.append(KeyboardButton(type: .number, title: "0", image: nil))
        leftButtons.append(KeyboardButton(type: .hide, title: "↓", image: nil))
        
        for btn in leftButtons {
            let index = leftButtons.index(of: btn)!
//            let row = index / 3
            let column = index % 3
            let last = keyboard.subviews.last
            keyboard.addSubview(btn)
            btn.snp.makeConstraints({ (make) in
                make.width.equalToSuperview().multipliedBy(0.25)
                make.height.equalToSuperview().multipliedBy(0.25)
                if let last = last {
                    if column == 0 {
                        make.left.equalTo(0)
                        make.top.equalTo(last.snp.bottom)
                    } else {
                        make.left.equalTo(last.snp.right)
                        make.top.equalTo(last)
                    }
                } else {
                    make.left.equalTo(0)
                    make.top.equalTo(0)
                }
            })
            switch btn.keyType {
            case .hide:
                btn.addTarget(self, action: #selector(hideAction), for: .touchUpInside)
                break
            default:
                btn.addTarget(self, action: #selector(numberAction(_:)), for: .touchUpInside)
                break
            }
        }
        
        let deleteButton = KeyboardButton(type: .delete, title: "←", image: nil)
        keyboard.addSubview(deleteButton)
        deleteButton.snp.makeConstraints { (make) in
            make.width.equalToSuperview().multipliedBy(0.25)
            make.height.equalToSuperview().multipliedBy(0.5)
            make.right.equalToSuperview()
            make.top.equalToSuperview()
        }
        deleteButton.addTarget(self, action: #selector(deleteAction), for: .touchUpInside)
        
        let doneButton = KeyboardButton(type: .done, title: Localizable.MyTransferRecords.transferButton, image: nil)
        keyboard.addSubview(doneButton)
        doneButton.snp.makeConstraints { (make) in
            make.width.equalToSuperview().multipliedBy(0.25)
            make.height.equalToSuperview().multipliedBy(0.5)
            make.right.equalToSuperview()
            make.bottom.equalToSuperview()
        }
        doneButton.addTarget(self, action: #selector(doneAction), for: .touchUpInside)
        
        textField.delegate = self
    }
    
    func layoutKeyboard(isHidden hidden: Bool) {
        keyboard.snp.remakeConstraints { (make) in
            let height: CGFloat = 250
            make.bottom.equalTo(hidden ? height : 0)
            make.left.equalTo(0)
            make.right.equalTo(0)
            make.height.equalTo(height)
        }
        UIView.animate(withDuration: 0.25) { [weak self] in
            self?.view.layoutIfNeeded()
        }
    }
    
    func numberAction(_ sender: KeyboardButton) {
        var current = textField.text ?? ""
        if current == "0" {
            current = ""
        }
        if sender.keyType == .dot {
            if current.isEmpty {
                textField.text = "0."
            } else if !current.contains(".") {
                textField.text = current + "."
            }
        } else {
            guard let number = sender.title(for: .normal) else {
                return
            }
            textField.text = current + number
        }
    }
    
    func deleteAction() {
        guard let current = textField.text, !current.isEmpty else {
            textField.text = "0"
            return
        }
        let newText = current.substring(to: current.index(current.endIndex, offsetBy: -1))
        textField.text = newText
    }
    
    func hideAction() {
        layoutKeyboard(isHidden: true)
    }
    
    func doneAction() {
        guard let amount = (textField.text as NSString?)?.floatValue, amount > 0 else {
            JoyupNotice.showError()
            return
        }
        let loading = JoyupNotice.showLoading()
        let memo = (JoyupUser.main?.userName ?? "--") + String(format: "Transfer to".localized(), nickname + " (\(receiver))")
        Request.transferToMerchant(receiver, amount, memo: memo).request(params: nil, completion: { [weak self] (json, _, _) in
            loading.hide()
            if let error = Request.checkResult(json) {
                JoyupNotice.showError(error)
            } else {
                JoyupNotice.showSuccess(action: { (n, _) in
                    n.hide()
                    self?.navigationController?.dismiss(animated: true, completion: nil)
                })
            }
        }) { (r, e) in
            loading.hide()
            JoyupNotice.showFireError(r, error: e.localizedDescription)
        }
    }
}

extension TransferViewController: UITextFieldDelegate {
    public func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        layoutKeyboard(isHidden: false)
        return false
    }
}
