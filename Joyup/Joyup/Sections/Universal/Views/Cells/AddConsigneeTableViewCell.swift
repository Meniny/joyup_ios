//
//  AddConsigneeTableViewCell.swift
//  Joyup
//
//  Created by Meniny on 2017-08-02.
//  Copyright © 2017年 Meniny. All rights reserved.
//

import UIKit

class AddConsigneeTableViewCell: UITableViewCell {
    
    @IBOutlet weak var consigneeLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        consigneeLabel.text = Localizable.Cart.dontHaveAnyAddress
    }
}
