//
//  ProductDescriptionTableViewCell.swift
//  Joyup
//
//  Created by Meniny on 2017-07-21.
//  Copyright © 2017年 Meniny. All rights reserved.
//

import UIKit
import CocoaHelper
import ColaExpression
import EasyGCD

class ProductDescriptionTableViewCell: UITableViewCell {

    @IBOutlet weak var descriptionLabel: UILabel!
    
    public func config(description desc: JoyupProduct.Description?) {
//        descriptionLabel.attributedText = desc?.attributedStringWithoutImages
        descriptionLabel.text = (desc?.content as NSString?)?.convertingHTMLToPlainText() ?? ""
//        if let c = (desc?.content as NSString?) {
//        } else {
//            descriptionLabel.text = ""
//        }
//        descriptionLabel.text = desc?.attributedString.string.trim(.whitespacesAndNewlines)
//        descriptionLabel.attributedText = desc?.attributedString
    }
    
    
}
