//
//  ProductDetailInfoTableViewCell.swift
//  Joyup
//
//  Created by Meniny on 2017-07-21.
//  Copyright © 2017年 Meniny. All rights reserved.
//

import UIKit

class ProductDetailInfoTableViewCell: UITableViewCell {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var snLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var pvLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    
    func config(_ title: String, SN: String, price: Float, PV: Int, domain: Domain?) {
        if let domain = domain {
            let area = NSMutableAttributedString(string: " \(domain.area.localizedName) ", attributes: [
                NSFontAttributeName: UIFont.systemFont(ofSize: 10),
                NSForegroundColorAttributeName: UIColor.white,
                NSBackgroundColorAttributeName: UIColor.valencia
                ])
            let normal = NSAttributedString(string: " \(title)", attributes: [
                NSFontAttributeName: UIFont.systemFont(ofSize: 14),
                NSForegroundColorAttributeName: UIColor.black
                ])
            area.append(normal)
            titleLabel.text = nil
            titleLabel.attributedText = area
        } else {
            titleLabel.attributedText = nil
            titleLabel.text = title
        }
        
        snLabel.text = "SN: \(SN)"
        
        priceLabel.text = price.amountString
        pvLabel.text = "\(PV)"
    }
    
}
