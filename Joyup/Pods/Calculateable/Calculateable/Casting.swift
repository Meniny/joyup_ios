//
//  Casting.swift
//  Pods
//
//  Created by Meniny on 2017-08-19.
//
//

import Foundation
import CoreGraphics

extension Double {
    public var stringValue: String {
        return "\(self)"
    }

    public var vectorValue: CGVector {
        return CGVector(value: self)
    }
    
    public var pointValue: CGPoint {
        return CGPoint(dot: self)
    }
    
    public var sizeValue: CGSize {
        return CGSize(length: self)
    }
    
    public var rectangleValue: CGRect {
        return CGRect(value: self)
    }

    public var doubleValue: Double {
        return self
    }
    
    public var floatValue: Float {
        return Float(self)
    }
    
    public var cgFloatValue: CGFloat {
        return CGFloat(self)
    }
    
    public var integerValue: Int {
        return Int(self)
    }
}

extension Float {
    public var stringValue: String {
        return "\(self)"
    }
    
    public var vectorValue: CGVector {
        return CGVector(value: self)
    }
    
    public var pointValue: CGPoint {
        return CGPoint(dot: self)
    }
    
    public var sizeValue: CGSize {
        return CGSize(length: self)
    }
    
    public var rectangleValue: CGRect {
        return CGRect(value: self)
    }
    
    public var doubleValue: Double {
        return Double(self)
    }
    
    public var floatValue: Float {
        return self
    }
    
    public var cgFloatValue: CGFloat {
        return CGFloat(self)
    }
    
    public var integerValue: Int {
        return Int(self)
    }
}

extension CGFloat {
    public var stringValue: String {
        return "\(self)"
    }
    
    public var vectorValue: CGVector {
        return CGVector(value: self)
    }
    
    public var pointValue: CGPoint {
        return CGPoint(dot: self)
    }
    
    public var sizeValue: CGSize {
        return CGSize(length: self)
    }
    
    public var rectangleValue: CGRect {
        return CGRect(value: self)
    }
    
    public var doubleValue: Double {
        return Double(self)
    }
    
    public var floatValue: Float {
        return Float(self)
    }
    
    public var cgFloatValue: CGFloat {
        return self
    }
    
    public var integerValue: Int {
        return Int(self)
    }
}

extension Int {
    public var stringValue: String {
        return "\(self)"
    }
    
    public var vectorValue: CGVector {
        return CGVector(value: self)
    }
    
    public var pointValue: CGPoint {
        return CGPoint(dot: self)
    }
    
    public var sizeValue: CGSize {
        return CGSize(length: self)
    }
    
    public var rectangleValue: CGRect {
        return CGRect(value: self)
    }
    
    public var doubleValue: Double {
        return Double(self)
    }
    
    public var floatValue: Float {
        return Float(self)
    }
    
    public var cgFloatValue: CGFloat {
        return CGFloat(self)
    }
    
    public var integerValue: Int {
        return self
    }
}

extension CGVector {
    public var stringValue: String {
        return "\(self)"
    }
    
    public var vectorValue: CGVector {
        return self
    }
    
    public var pointValue: CGPoint {
        return CGPoint(x: dx, y: dy)
    }
    
    public var sizeValue: CGSize {
        return CGSize(width: dx, height: dy)
    }
    
    public var rectangleValue: CGRect {
        return CGRect(origin: pointValue, size: sizeValue)
    }
}

