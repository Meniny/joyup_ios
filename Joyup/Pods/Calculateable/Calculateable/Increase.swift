//
//  CGRectUtils.swift
//  Crucio
//
//  Created by Meniny on 2017-08-19.
//  Copyright © 2017年 Sky Platanus. All rights reserved.
//

import Foundation
import CoreGraphics

// MARK: - Values calculating

public extension CGFloat {
    public var half: CGFloat {
        return self * 0.5
    }
    
    public var third: CGFloat {
        return self / 3
    }
    
    public var quarter: CGFloat {
        return self * 0.25
    }
    
    public var double: CGFloat {
        return self * 2
    }
    
    public var treble: CGFloat {
        return self * 3
    }
    
}

public extension Double {
    public var half: Double {
        return self * 0.5
    }
    
    public var third: Double {
        return self / 3
    }
    
    public var quarter: Double {
        return self * 0.25
    }
    
    public var double: Double {
        return self * 2
    }
    
    public var treble: Double {
        return self * 3
    }
    
}

public extension Float {
    public var half: Float {
        return self * 0.5
    }
    
    public var third: Float {
        return self / 3
    }
    
    public var quarter: Float {
        return self * 0.25
    }
    
    public var double: Float {
        return self * 2
    }
    
    public var treble: Float {
        return self * 3
    }
    
}

public extension Int {
    public var half: Int {
        return self / 2
    }
    
    public var third: Int {
        return self / 3
    }
    
    public var quarter: Int {
        return self / 4
    }
    
    public var double: Int {
        return self * 2
    }
    
    public var treble: Int {
        return self * 3
    }
    
    public var next: Int {
        return self + 1
    }
    
    public var last: Int {
        return self - 1
    }
    
    
}

// MARK: - Geometry calculating

public extension CGVector {
    public var half: CGVector {
        return CGVector(dx: dx.half, dy: dy.half)
    }
    
    public var double: CGVector {
        return CGVector(dx: dx.double, dy: dy.double)
    }
    
    public var treble: CGVector {
        return CGVector(dx: dx.double, dy: dy.double)
    }
    
}

public extension CGPoint {
    public var half: CGPoint {
        return CGPoint(x: x.half, y: y.half)
    }
    
    public var double: CGPoint {
        return CGPoint(x: x.double, y: y.double)
    }
    
    public var treble: CGPoint {
        return CGPoint(x: x.treble, y: y.treble)
    }
    
}

public extension CGSize {
    public var half: CGSize {
        return CGSize(width: width.half, height: height.half)
    }
    
    public var double: CGSize {
        return CGSize(width: width.double, height: height.double)
    }
    
    public var treble: CGSize {
        return CGSize(width: width.treble, height: height.treble)
    }
    
}

public extension CGRect {
    public var half: CGRect {
        return CGRect(origin: origin.half, size: size.half)
    }
    
    public var double: CGRect {
        return CGRect(origin: origin.double, size: size.double)
    }
    
    public var treble: CGRect {
        return CGRect(origin: origin.treble, size: size.treble)
    }
    
}

public extension CGRect {
    public var halfWidth: CGFloat {
        return width * 0.5
    }
    
    public var halfHeight: CGFloat {
        return height * 0.5
    }
}
