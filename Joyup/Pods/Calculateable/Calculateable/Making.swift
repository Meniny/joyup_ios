//
//  CGRectUtils.swift
//  Crucio
//
//  Created by Meniny on 2017-08-19.
//  Copyright © 2017年 Sky Platanus. All rights reserved.
//

import Foundation
import CoreGraphics

// MARK: - Making

public extension CGVector {
    public init(value: CGFloat) {
        self.init(dx: value, dy: value)
    }
    
    public init(value: Double) {
        self.init(dx: value, dy: value)
    }
    
    public init(value: Float) {
        self.init(dx: CGFloat(value), dy: CGFloat(value))
    }
    
    public init(value: Int) {
        self.init(dx: value, dy: value)
    }
}

public extension CGPoint {
    public init(dot: CGFloat) {
        self.init(x: dot, y: dot)
    }
    
    public init(dot: Double) {
        self.init(x: dot, y: dot)
    }
    
    public init(dot: Float) {
        self.init(x: CGFloat(dot), y: CGFloat(dot))
    }
    
    public init(dot: Int) {
        self.init(x: dot, y: dot)
    }
}

public extension CGSize {
    public init(length: CGFloat) {
        self.init(width: length, height: length)
    }
    
    public init(length: Float) {
        self.init(width: CGFloat(length), height: CGFloat(length))
    }
    
    public init(length: Double) {
        self.init(width: length, height: length)
    }
    
    public init(length: Int) {
        self.init(width: length, height: length)
    }
}

public extension CGRect {
    public init(value: CGFloat) {
        self.init(x: value, y: value, width: value, height: value)
    }
    
    public init(origin: CGFloat, size: CGFloat) {
        self.init(origin: CGPoint(dot: origin), size: CGSize(length: size))
    }
    
    
    
    public init(value: Float) {
        self.init(x: CGFloat(value), y: CGFloat(value), width: CGFloat(value), height: CGFloat(value))
    }
    
    public init(origin: Float, size: Float) {
        self.init(origin: CGPoint(dot: origin), size: CGSize(length: size))
    }
    
    
    
    public init(value: Double) {
        self.init(x: value, y: value, width: value, height: value)
    }
    
    public init(origin: Double, size: Double) {
        self.init(origin: CGPoint(dot: origin), size: CGSize(length: size))
    }
    
    
    
    public init(value: Int) {
        self.init(x: CGFloat(value), y: CGFloat(value), width: CGFloat(value), height: CGFloat(value))
    }
    
    public init(origin: Int, size: Int) {
        self.init(origin: CGPoint(dot: origin), size: CGSize(length: size))
    }
}

