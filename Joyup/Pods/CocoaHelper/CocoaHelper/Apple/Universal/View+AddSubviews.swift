
#if !os(watchOS)
    #if os(macOS)
        import Cocoa
    #else
        import Foundation
        import UIKit
    #endif

    public extension AppleView {
        
        public func addSubviews(_ views: AppleView...) {
            addSubviews(views)
        }
        
        public func addSubviews(_ views: [AppleView]) {
            views.forEach {
                addSubview($0)
            }
        }
    }
#endif
