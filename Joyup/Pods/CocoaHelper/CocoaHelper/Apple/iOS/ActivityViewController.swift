
import Foundation
import UIKit

public extension UIViewController {
    
    public func share(items: [Any],
                      barButtonItem: UIBarButtonItem?,
                      completion: UIActivityViewControllerCompletionWithItemsHandler? = nil) {
        let activityController = UIActivityViewController(activityItems: items, applicationActivities: nil)
        activityController.popoverPresentationController?.barButtonItem = barButtonItem
        activityController.completionWithItemsHandler = completion
        self.present(activityController, animated: true, completion: nil)
    }
    
    public func share(items: [Any],
                      sourceView: UIView?,
                      sourceRect: CGRect?,
                      completion: UIActivityViewControllerCompletionWithItemsHandler? = nil) {
        let activityController = UIActivityViewController(activityItems: items, applicationActivities: nil)
        activityController.popoverPresentationController?.sourceView = sourceView
        if let rect = sourceRect {
            activityController.popoverPresentationController?.sourceRect = rect
        }
        activityController.completionWithItemsHandler = completion
        self.present(activityController, animated: true, completion: nil)
    }
}

public extension UIActivityViewController {
    
    public static func share(items: [Any],
                             from controller: UIViewController,
                             barButtonItem: UIBarButtonItem?,
                             completion: UIActivityViewControllerCompletionWithItemsHandler? = nil) {
        controller.share(items: items, barButtonItem: barButtonItem, completion: completion)
    }
    
    public static func share(items: [Any],
                             from controller: UIViewController,
                             sourceView: UIView?,
                             sourceRect: CGRect?,
                             completion: UIActivityViewControllerCompletionWithItemsHandler? = nil) {
        controller.share(items: items, sourceView: sourceView, sourceRect: sourceRect, completion: completion)
    }
}
