
import Foundation

public extension Array {
    public var prettyString: String {
        return "\(self as NSArray)"
    }
    
    public mutating func appendAfterRemoveAll(with: Array) {
        self.removeAll()
        self.append(contentsOf: with)
    }
}

public protocol Queueable {
    func process() -> Bool
}

public extension Array where Element: Queueable {
    
    public mutating func processQueue(from: Int = 0, to: Int? = nil, process: ((_ element: Element) -> Void)? = nil) {
        let to = to != nil ? to! : count
        let currentQueue = self[from..<to]
        for (index, element) in currentQueue.enumerated() where element.process() {
            process?(element)
            remove(at: index - (currentQueue.count - self.count))
        }
    }
}

