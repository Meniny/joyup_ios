
import Foundation

public extension Dictionary {
    public var prettyString: String {
        return "\(self as NSDictionary)"
    }
}

public extension Dictionary {

    /// Get the value of a given key
    ///
    /// - Parameter key: the given keys
    /// - Returns: Value, nil if the given key is not exists
    public func value(forKey key: Key) -> Value? {
        if let _ = self.index(forKey: key) {
            return self[key]
        }
        return nil
    }
    
    /// Get all values of given keys
    ///
    /// - Parameter givenKeys: Array of the given keys
    /// - Returns: Values array, contains nil if the given key is not exists
    public func values(forKeys givenKeys: [Key]) -> [Value?] {
        return givenKeys.map { (k) -> Value? in
            return self.value(forKey: k)
        }
    }
    
    /// Get all values of given keys
    ///
    /// - Parameter givenKeys: Array of the given keys
    /// - Returns: Nonnil values array, even if the given key is not exists
    public func nonnilValues(forKeys givenKeys: [Key]) -> [Value] {
        return values(forKeys: givenKeys).filter { (v) -> Bool in
            return v != nil
        }.map { (v) -> Value in
            return v!
        }
    }
    
    /// Array of all keys
    public var allKeys: [Key] {
        return keys.map({ (k) -> Key in
            return k
        })
    }
    
    /// Array of all values
    public var allValues: [Value] {
        return values.map({ (v) -> Value in
            return v
        })
    }
}
