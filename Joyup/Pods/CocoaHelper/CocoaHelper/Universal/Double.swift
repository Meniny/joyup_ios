
import Foundation

public extension Double {
    public func stringValue() -> String {
        return "\(self)"
    }
}
