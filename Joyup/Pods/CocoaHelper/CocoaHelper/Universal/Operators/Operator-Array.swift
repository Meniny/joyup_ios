//
//  Operator-Array.swift
//  Pods
//
//  Created by Meniny on 2017-05-16.
//
//

import Foundation

/// append to array
public func + <T> (lhs: [T], rhs: T) -> [T] {
    return lhs + [rhs]
}

/// append to array
public func + <T> (lhs: [T], rhs: [T]) -> [T] {
    var cp = lhs
    cp.append(contentsOf: rhs)
    return cp
}

/// append to array
public func += <T> (lhs: inout [T], rhs: T) {
    lhs.append(rhs)
}

/// append to array
public func += <T> (lhs: inout [T], rhs: [T]) {
    lhs.append(contentsOf: rhs)
}

/// remove from array
public func - <T: Comparable> (lhs: [T], rhs: T) -> [T] {
    if let idx = lhs.index(of: rhs) {
        var cp = lhs
        cp.remove(at: idx)
        return cp
    }
    return lhs
}

/// remove from array
public func - <T: Comparable> (lhs: [T], rhs: [T]) -> [T] {
    if lhs.isEmpty || rhs.isEmpty {
        return lhs
    }
    var cp = lhs
    for i in rhs {
        if let idx = cp.index(of: i) {
            cp.remove(at: idx)
        }
    }
    return cp
}

/// remove from array
public func -= <T: Comparable> (lhs: inout [T], rhs: T) {
    if let idx = lhs.index(of: rhs) {
        lhs.remove(at: idx)
    }
}

/// remove from array
public func -= <T: Comparable> (lhs: inout [T], rhs: [T]) {
    for i in rhs {
        if let idx = lhs.index(of: i) {
            lhs.remove(at: idx)
        }
    }
}

/// repeat array
public func * <T> (lhs: [T], rhs: Int) -> [T] {
    if rhs > 1 {
        var res = [T]()
        for _ in 1...rhs {
            res.append(contentsOf: lhs)
        }
        return res
    }
    return lhs
}

/// repeat array
public func *= <T> (lhs: inout [T], rhs: Int) {
    if rhs > 1 {
        let cp = lhs
        for _ in 2...rhs {
            lhs.append(contentsOf: cp)
        }
    }
}

/// reverse array
public prefix func ~ <T> (rhs: [T]) -> [T] {
    return rhs.reversed()
}
