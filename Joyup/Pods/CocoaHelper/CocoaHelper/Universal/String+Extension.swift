
import Foundation

public extension String {
    
    public func repeating(_ time: Int, separator: String? = nil) -> String {
        if time > 1 {
            var res = [String]()
            for _ in 1...time {
                res.append(self)
            }
            if let sep = separator {
                return res.joined(separator: sep)
            }
            return res.joined()
        }
        return self
    }
    
    public func replace(_ string: String, with withString: String) -> String {
        return replacingOccurrences(of: string, with: withString)
    }
    
    public func truncate(_ length: Int, suffix: String = "...") -> String {
        return self.length > length
            ? substring(to: characters.index(startIndex, offsetBy: length)) + suffix
            : self
    }
    
    public func split(_ delimiter: String) -> [String] {
        let components = self.components(separatedBy: delimiter)
        return components != [""] ? components : []
    }
    
    public func trim(_ charactersSets: CharacterSet = CharacterSet.whitespaces) -> String {
        return trimmingCharacters(in: charactersSets)
    }
    
    public var uppercaseFirstLetter: String {
        guard isPresent else { return self }
        
        var string = self
        string.replaceSubrange(string.startIndex...string.startIndex,
                               with: String(string[string.startIndex]).capitalized)
        
        return string
    }
    
    public func substring(pattern: String, options: NSRegularExpression.Options = NSRegularExpression.Options.caseInsensitive) -> String? {
        if let regex = try? NSRegularExpression(pattern: pattern, options: options) {
            if let match = regex.firstMatch(in: self, options: .reportProgress, range: NSMakeRange(0, self.characters.count)) {
                return (self as NSString).substring(with: match.range)
            }
        }
        return nil
    }
    
    public func substring(from: Int) -> String {
        return (self as NSString).substring(from: from)
    }
    
    public func substring(to: Int) -> String {
        return (self as NSString).substring(to: to)
    }
}

public extension String {
    
    public var length: Int {
        return self.characters.count
    }
    
    public var localized: String {
        return Bundle.main.localizedString(forKey: self, value: nil, table: nil)
    }
    
    public var integerValue: Int? { return Int(self) }
    public var intValue: Int? { return self.integerValue }
    
    public var unsignedIntegerValue: UInt? { return UInt(self) }
    public var uintValue: UInt? { return self.unsignedIntegerValue }
    
    public var doubleValue: Double? { return Double(self) }
    
    public var floatValue: Float? { return Float(self) }
}

public extension String {
    
    public func uppercasedAtSentenceBoundary() -> String {
        var string = self.lowercased()
        
        let capacity = string.characters.count
        let mutable = NSMutableString(capacity: capacity)
        mutable.append(string)
        
        let pattern = "(?:^|\\b\\.[ ]*)(\\p{Ll})"
        
        if let regex = try? NSRegularExpression(pattern: pattern, options: .anchorsMatchLines) {
            let results = regex.matches(in: string, options: [], range: NSMakeRange(0, capacity))
            for result in results {
                let numRanges = result.numberOfRanges
                if numRanges >= 1 {
                    for i in 1..<numRanges {
                        let range = result.rangeAt(i)
                        let substring = mutable.substring(with: range)
                        mutable.replaceCharacters(in: range, with: substring.uppercased())
                    }
                }
            }
        }
        
        return mutable as String
    }
}

public extension String {
    /// return base64 string of self String
    public var base64String: String? {
        if let utf8EncodeData = self.data(using: String.Encoding.utf8, allowLossyConversion: true) {
            let base64EncodingData = utf8EncodeData.base64EncodedString(options: [])
            return base64EncodingData
        }
        return nil
    }
    
    /// decode unicode to plain string
    public var unicodeDecodedString: String? {
        let tempString = self.replacingOccurrences(of: "\\u", with: "\\U")
          .replacingOccurrences(of:"\"", with: "\\\"")
      
        if let tempData = "\"\(tempString)\"".data(using: .utf8) {
            if let res = try? PropertyListSerialization.propertyList(from: tempData, options: [], format: nil) {
                return res as? String
            }
        }
        return nil
    }
    
    public var URLEncodedString: String {
        if #available(iOS 10.0, macOS 10.12, *) {
            if let esc = self.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed) {
                return esc
            }
            return self
        } else {
            let chars = ":&=;+!@#$()',*"
            let legalURLCharactersToBeEscaped: CFString = chars as CFString
            return CFURLCreateStringByAddingPercentEscapes(nil, self as CFString!, nil, legalURLCharactersToBeEscaped, CFStringBuiltInEncodings.UTF8.rawValue) as String
        }
    }
    
    public var URLDecodedString: String {
        if #available(iOS 10.0, macOS 10.12, *) {
            if let esc = self.removingPercentEncoding {
                return esc
            }
            return self
        } else {
            return CFURLCreateStringByReplacingPercentEscapesUsingEncoding(nil, self as CFString!, "" as CFString!, CFStringBuiltInEncodings.UTF8.rawValue) as String
        }
    }
}
