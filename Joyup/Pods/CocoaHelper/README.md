## Introduction

`CocoaHelper` is a bundle Swift categories for Cocoa and Cocoa Touch.

## Requirements

* iOS 8.0+
* watchOS 2.0+ (v2.0.0+)
* tvOS 9.0+ (v2.0.0+)
* macOS 10.10+ (v1.1.2+)
* Linux (v2.0.0+)
* Xcode 8 with Swift 3

## Installation

#### CocoaPods

```ruby
pod 'CocoaHelper'
```
