//
//  Logify.swift
//  Logify
//
//  Created by Meniny on 2017-04-24.
//  Copyright © 2017年 Meniny. All rights reserved.
//

import Foundation

public typealias Logify = Log
public typealias LogifyLite = Log

open class Log {
    
    #if DEBUG
    /// Disable Log.d outputs
    public static var DEBUG: Bool = true
    #else
    /// Disable Log.d outputs
    public static var DEBUG: Bool = false
    #endif
    
    /// Disable all Logify outputs. Default is false.
    public static var DISABLED: Bool = false
    
    private enum LogType: String {
        case verbose = "VERBOSE"
        case debug = "DEBUG"
        case information = "INFORMATION"
        case warning = "WARNING"
        case error = "ERROR"
        
        var emoji: String {
            switch self {
            case .verbose:
                return "🌀"
            case .debug:
                return "🏁"
            case .information:
                return "ℹ️"
            case .warning:
                return "⚠️"
            default:
                return "❌"
            }
        }
    }
    
    private static let sepTop = "<!-------------------------------"
    private static let sepBot = "-------------------------------->"
    
    private class func log<T>(_ message: T, type: Log.LogType, file: String, method: String, line: Int, dump d: Bool = false) {
        
        if Log.DISABLED { return }
        
        if !Log.DEBUG && type == .debug { return }
        
        let date = Date()
        
        if d {
            let lines = ["\(Log.sepTop)",
                "[TYPE] \(type.emoji) \(type.rawValue)",
                "[PATH] \(file)",
                "[FILE] \((file as NSString).lastPathComponent)",
                "[LINE] \(line)",
                "[FUNC] \(method)",
                "[DATE] \(date) | \(Int(date.timeIntervalSince1970))",
                "[INFO]", message,
                "\(Log.sepBot)"
            ] as [Any]
            indentPrint(lines)
        } else {
            let lines = ["\n\(Log.sepTop)\n",
                         "[TYPE] \(type.emoji) \(type.rawValue)\n",
                         "[PATH] \(file)\n",
                         "[FILE] \((file as NSString).lastPathComponent)\n",
                         "[LINE] \(line)\n",
                         "[FUNC] \(method)\n",
                         "[DATE] \(date) | \(Int(date.timeIntervalSince1970))\n",
                         "[INFO] \(message)\n\(Log.sepBot)\n"]
            print(lines.joined())
        }
    }
    
    /// Log verbose
    ///
    /// - Parameters:
    ///   - message: the log message stuffs
    public static func v<T>(_ message: T, file: String = #file, method: String = #function, line: Int = #line) {
        Log.log(message, type: .verbose, file: file, method: method, line: line)
    }
    /// Log debug, won't show in release env
    ///
    /// - Parameters:
    ///   - message: the log message stuffs
    public static func d<T>(_ message: T, file: String = #file, method: String = #function, line: Int = #line) {
        Log.log(message, type: .debug, file: file, method: method, line: line)
    }
    /// Log information
    ///
    /// - Parameters:
    ///   - message: the log message stuffs
    public static func i<T>(_ message: T, file: String = #file, method: String = #function, line: Int = #line) {
        Log.log(message, type: .information, file: file, method: method, line: line)
    }
    /// Log warning
    ///
    /// - Parameters:
    ///   - message: the log message stuffs
    public static func w<T>(_ message: T, file: String = #file, method: String = #function, line: Int = #line) {
        Log.log(message, type: .warning, file: file, method: method, line: line)
    }
    /// Log error
    ///
    /// - Parameters:
    ///   - message: the log message stuffs
    public class func e<T>(_ message: T, file: String = #file, method: String = #function, line: Int = #line) {
        Log.log(message, type: .error, file: file, method: method, line: line)
    }
    /// Log originally
    ///
    /// - Parameters:
    ///   - message: the log message stuffs
    public class func o<T>(_ message: T) {
        print(message)
    }
    
    /// Log with indents
    ///
    /// - Parameters:
    ///   - message: the log message stuffs
    public class func indent<T>(_ message: T, file: String = #file, method: String = #function, line: Int = #line) {
        Log.log(message, type: .debug, file: file, method: method, line: line, dump: true)
    }
}

public func indentPrint<T>(_ value: T) {
    dump(value)
}
