
<p align="center">
  <h1>Logify</h1><br/>
  <a href="https://cocoapods.org/pods/Logify">
    <img alt="Version" src="https://img.shields.io/badge/version-3.0.0-brightgreen.svg">
    <img alt="Author" src="https://img.shields.io/badge/author-Meniny-blue.svg">
    <img alt="Build Passing" src="https://img.shields.io/badge/build-passing-brightgreen.svg">
    <img alt="Swift" src="https://img.shields.io/badge/swift-3.0%2B-orange.svg">
    <br/>
    <img alt="Platforms" src="https://img.shields.io/badge/platform-macOS%20%7C%20iOS-lightgrey.svg">
    <img alt="MIT" src="https://img.shields.io/badge/license-MIT-blue.svg">
    <br/>
    <img alt="Cocoapods" src="https://img.shields.io/badge/cocoapods-compatible-brightgreen.svg">
    <img alt="Carthage" src="https://img.shields.io/badge/carthage-working%20on-red.svg">
    <img alt="SPM" src="https://img.shields.io/badge/swift%20package%20manager-working%20on-red.svg">
  </a>
</p>

# Introduction

## What's this?

Logify is an Android like logger framework written in Swift.

* iOS 8.0+
* macOS 10.10+
* Xcode 8 with Swift 3
<!-- * watchOS 2.0+ -->
<!-- * tvOS 9.0+ -->

## Installation

#### CocoaPods

```ruby
pod 'Logify'
```

## Contribution

You are welcome to fork and submit pull requests.

## License

Logify is open-sourced software, licensed under the `MIT` license.

## Usage

```swift
Log.i("Hello world")
let dic = ["key": "value"]
Log.d(dic)
Log.DISABLED = true
Log.e("ERROR!!!")
```

![Sample](https://ooo.0o0.ooo/2017/05/06/590d6fe21b601.jpg)
